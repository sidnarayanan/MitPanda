#!/usr/bin/env python

import ROOT as root
from sys import argv
from array import array

puFile = root.TFile('${ROOT}/data2016/puWeight.root')
hpu = puFile.Get('hPU')

fMC = root.TFile(argv[1],'UPDATE')
t = fMC.Get('events')
weight = array('f',[0])
bpu = t.Branch('newPUWeight',weight,'newPUWeight/F')
nEntries = t.GetEntries()
for iE in xrange(nEntries):
  if (iE%100000)==0:
    print '%10i/%i'%(iE,nEntries)
  t.GetEntry(iE)
  npv = t.npv
  weight[0] = hpu.GetBinContent(hpu.FindBin(npv))
  bpu.Fill()

fMC.Write()
fMC.Close()

