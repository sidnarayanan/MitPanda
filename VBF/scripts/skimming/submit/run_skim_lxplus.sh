#!/bin/bash
label=$1
cfgName=$2
nPerJob=$3
WD=${PWD}
pwd

executable=skimMultiCoreLXBATCH.py
#executable=skimMultiCoreLXBATCHMonitor.py
#executable=skimSingleCoreEOS.py

scramdir=/afs/cern.ch/user/s/snarayan/work/private/CMSSW_8_0_11/
cd ${scramdir}/src/
eval `scramv1 runtime -sh`
source MitPanda/VBF/setup.sh
cd $WD

time cp -r ${scramdir}/src/MitPanda/Monotop/data/ .
#time cp -r ${scramdir}/src/MitPanda/Monotop/data.tgz .
#time tar -xvf data.tgz
#cp -r /afs/cern.ch/work/s/snarayan/metfilters/ data/metfilters
cp ${scramdir}/src/MitPanda/VBF/config/${cfgName}.cfg local.cfg
cp ${scramdir}/src/MitPanda/VBF/scripts/skimming/$executable .

python $executable $label $nPerJob ${PANDA_FLATDIR}/batch/

rm nero.root
rm -rf *root *.py local.cfg data

exit 0
