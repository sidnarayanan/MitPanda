#!/usr/bin/env python

from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv,exit
from os import system,getenv
from json import load as loadJson
import cPickle as pickle

if __name__ == "__main__":

  panda=getenv('CMSSW_BASE')+'/src/MitPanda/'
  with open(panda+'/Monotop/data/Cert_246908-260627_13TeV_PromptReco_Collisions15_25ns_JSON_v2.txt') as jsonFile:
    json=loadJson(jsonFile)

  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/NeroSkimmer.h")
  gSystem.Load('libMitPandaSkimmers.so')
  gSystem.Load('libNeroProducerCore.so')
  
  def fn(fullPath):

    skimmer = root.NeroSkimmer()
   
    skimmer.usePuppiMET=True;
    skimmer.set_ak4Jetslabel("puppi")
    skimmer.set_ca15Jetslabel("CA15Puppi")
#    skimmer.set_ak8Jetslabel("AK8Puppi")

    isData=not('SIM' in fullPath)

    skimmer.isData=isData
    skimmer.applyJson=True
    skimmer.fromBambu=not('eoscms' in fullPath or 'EOS' in fullPath)
    if not isData:
      processType=root.NeroSkimmer.kNone
      if 'ZJets' in fullPath or 'DY' in fullPath:
        processType=root.NeroSkimmer.kZ
      elif 'WJets' in fullPath:
        processType=root.NeroSkimmer.kW
      elif 'GJets' in fullPath:
        processType=root.NeroSkimmer.kA
      elif 'TTJets' in fullPath:
        processType=root.NeroSkimmer.kTT
      skimmer.processType=processType
    
    if isData:
      for run,lumi in json.iteritems():
        for l in lumi:
          for lll in xrange(l[0],l[1]+1):
            skimmer.AddLumi(int(run),int(lll))
      for i in xrange(10):
        badEventsFile = open(panda+'/Monotop/data/metfilters/badEvents_%i.pkl'%i,'rb')
        badEvents = pickle.load(badEventsFile)
        for e in badEvents:
          skimmer.AddBadEvent(e[0],e[1],e[2])
        del badEvents
        badEventsFile.close()

    skimmer.SetDataDir(panda+"/Monotop/data/")
#    skimmer.SetPreselectionBit(root.NeroSkimmer.kMonotopCA15)
#    skimmer.SetPreselectionBit(root.NeroSkimmer.kMonotopRes)
    fin = root.TFile.Open(fullPath)
    print fullPath, fin
    tree = fin.FindObjectAny("events")
    alltree = fin.Get('nero/all')
    print tree
    skimmer.SetOutputFile('/tmp/%s/testskim.root'%getenv('USER'))
    skimmer.Init(tree,alltree)
    skimmer.Run(1)
    skimmer.Terminate()

fn(argv[1]) 
