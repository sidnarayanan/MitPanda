#!/usr/bin/env python

from ROOT import gSystem, gROOT
import ROOT as root
from array import array
from glob import glob
from re import sub
from sys import argv
from os import environ,system
from MitPanda.Tools.process import *

gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/Normalizer.h")
gSystem.Load('libMitPandaTools.so')

def normalizeFast(fpath,opt):
  xsec=-1
  if type(opt)==type(1.) or type(opt)==type(1):
    xsec = opt
  else:
    try:
      xsec = processes[opt][2]
    except KeyError:
      for k,v in processes.iteritems():
        if opt in k:
          xsec = v[2]
  if xsec<0:
    print 'could not find xsec, skipping %s!'%opt
    return
  print 'fast normalizing %s (%s) ...'%(fpath,opt)
  n = root.Normalizer();
  n.histName = 'htotal'
  n.NormalizeTree(fpath,xsec)

fpath = argv[1]
key = sub('monojet_','',sub('.root','',fpath.split('/')[-1]))

normalizeFast(fpath,key)
