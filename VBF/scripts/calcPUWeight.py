#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from MitPanda.Tools.Misc import *
from sys import exit
from os import getenv

lumi = 12918.

gROOT.LoadMacro('${CMSSW_BASE}/src/MitPanda/Tools/interface/HistogramDrawer.h')
gSystem.Load('libMitPandaTools.so')

plot = root.HistogramDrawer()
plot.SetLumi(lumi/1000)
plot.SetTDRStyle()
plot.InitLegend()
plot.Stack(True)
plot.DrawMCErrors(True)
plot.AddCMSLabel()
plot.AddLumiLabel(True)

baseDir = getenv('PANDA_FLATDIR')+'/'
fData = root.TFile(baseDir+'SingleElectron.root')
tData = fData.Get('events')
fMC   = root.TFile(baseDir+'ZJets_nlo.root')
tMC   = fMC.Get('events')

cut = 'nLooseElectron==2 && nLooseMuon==0 && looseLep1IsTight==1 && 60<diLepMass && diLepMass<120'
trigger = '(trigger&2)!=0'
mcWeight = 'normalizedWeight*%f*sf_lep*sf_eleTrig'%lumi

NPV=40

hData = root.TH1F('hdata','data',NPV+1,-0.5,NPV+0.5)
hMC   = root.TH1F('hmc','mc',NPV+1,-0.5,NPV+0.5)
hZData = root.TH1F('hzdata','zdata',30,60,120)
hZMC   = root.TH1F('hzmc','mc',30,60,120)


tData.Draw('TMath::Min(npv,%i)>>hdata'%NPV,tAND(trigger,cut))
tMC.Draw('TMath::Min(npv,%i)>>hmc'%NPV,tTIMES(mcWeight,cut))
hMC.GetXaxis().SetTitle('N_{PV}')
hMC.GetYaxis().SetTitle('Events')

plotdir = '~/www/figs/puweight_13invfb/'

plot.Reset()
plot.AddHistogram(hMC,'Z#rightarrowee',root.kZjets)
plot.AddHistogram(hData,'Data',root.kData)
plot.Draw(plotdir,'npv')

plot.Reset()
plot.Stack(False)
hData.Scale(1./hData.Integral())
hMC.Scale(1./hMC.Integral())
hData.Divide(hMC)
hData.GetXaxis().SetTitle('N_{PV}')
hData.GetYaxis().SetTitle('PU weight')
plot.AddHistogram(hData,'Data/MC',root.kExtra2)
plot.Draw(plotdir,'puweight')

tData.Draw('diLepMass>>hzdata',tAND(trigger,cut))
tMC.Draw('diLepMass>>hzmc',tTIMES(mcWeight,cut))
hZMC.GetXaxis().SetTitle('m_{ll} [GeV]')
hZMC.GetYaxis().SetTitle('Events/2 GeV')

plot.Reset()
plot.Stack(True)
plot.Logy(True)
plot.AddHistogram(hZMC,'Z#rightarrowee',root.kZjets)
plot.AddHistogram(hZData,'Data',root.kData)
plot.Draw(plotdir,'diLepMass')

fOut = root.TFile(baseDir+'puWeight.root','RECREATE')
fOut.WriteTObject(hData,"hPU")
fOut.Close()
