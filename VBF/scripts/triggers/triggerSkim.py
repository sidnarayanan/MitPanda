#!/usr/bin/env python

from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv,exit
from os import system,getenv
from json import load as loadJson
import cPickle as pickle

if __name__ == "__main__":

  panda=getenv('PANDA')
  with open(panda+'/Monotop/data/Cert_271036-273450_13TeV_PromptReco_Collisions16_JSON_NoL1T.txt') as jsonFile:
    json=loadJson(jsonFile)


  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/NeroSkimmer.h")
  gSystem.Load('libMitPandaSkimmers.so')
  gSystem.Load('libNeroProducerCore.so')
  
  def fn():
    fullPath = '/home/snarayan/cms/root/triggerStudies/nero_SingleMuon.root'

    skimmer = root.NeroSkimmer()
   
    skimmer.usePuppiMET=False;
    skimmer.set_ak4Jetslabel("")
    skimmer.set_ca15Jetslabel("CA15CHS")

    isData=True
    if isData:
      for run,lumi in json.iteritems():
        for l in lumi:
          for lll in xrange(l[0],l[1]+1):
            skimmer.AddLumi(int(run),int(lll))

    skimmer.isData=isData
    skimmer.processType=root.NeroSkimmer.kNone
    skimmer.applyJson=True
    skimmer.fromBambu=False

    skimmer.SetDataDir(panda+"/Monotop/data/")
    fin = root.TFile(fullPath)
    tree = fin.FindObjectAny("events")
    skimmer.SetOutputFile('/home/snarayan/cms/root/triggerStudies/SingleMuon.root')
    skimmer.Init(tree)
    skimmer.Run(1)
    skimmer.Terminate()

fn() 
