#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from sys import argv,exit
from array import array
from MitPanda.Tools.Misc import *
from math import sqrt
from ROOT import gROOT

gROOT.LoadMacro('triggerFunc.C')
fitFunc = root.TF1('fitFunc',root.ErfCB,60,900,5)
initParams = [80,1,0.064,1.72,1]
for iP in xrange(5):
  #fitFunc.SetParameter(iP,initParams[iP])
  fitFunc.SetParameter(iP,2)

gROOT.LoadMacro('${CMSSW_BASE}/src/MitPanda/Tools/interface/HistogramDrawer.h')
gSystem.Load('libMitPandaTools.so')

plot = root.HistogramDrawer()
plot.SetLumi(588.8)
plot.SetTDRStyle()
plot.AddCMSLabel()
plot.AddLumiLabel(False)

counter=0

fIn = root.TFile('/home/snarayan/cms/root/data_590pb/SingleMuon.root')
events = fIn.Get('events')
hOne = root.TH1F('hone','one',1,0.,2.)

baseCuts = {
            'monojet':'((trigger&8)!=0) && nLooseMuon>0 && nJet>0 && jet1Pt>100',
            'vbf':'((trigger&8)!=0) && nLooseMuon>0 && nJet>1 && jet1Pt>40 && jet2Pt>40 && jet12DEta>3.5',
            }

metCuts  = {'all':'(trigger&1)!=0',
            'met170' :'(trigger&16)!=0',
            'metmht' :'(trigger&32)!=0'
           }

def getHist(tree,cut,bins):
  global counter
  N = len(bins)-1
  h = root.TH1F('h%i'%counter,'h%i'%counter,N,bins)
  tree.Draw('metnomu>>h%i'%counter,cut)
  counter += 1
  return h

def getMETHist(tree,cut,bins):
  global counter
  N = len(bins)-1
  h = root.TH1F('h%i'%counter,'h%i'%counter,N,bins)
  tree.Draw('met>>h%i'%counter,cut)
  counter += 1
  return h

def runEff(label,trigger,wrt,extra=None,fn=getHist,doFit=False):
  baseCut = baseCuts[wrt]
  if extra:
    baseCut = tAND(baseCut,metCuts[extra])
  if 'monojet' in wrt:
    bins = array('f',[60,80,100,120,140,160,180,200,220,240,300,600,900])
  else:
    bins = array('f',[60,80,100,150,200,300,900])
  hBase = fn(events,baseCut,bins)
  hMET = fn(events,tAND(baseCut,metCuts[trigger]),bins)
  x = []; central = []; up = []; down = []; zero = []
  for iB in xrange(1,len(bins)):
    passed=hMET.GetBinContent(iB)
    total=hBase.GetBinContent(iB)
    eff = passed/total
    try:
#      err = eff*sqrt(1/total+1/passed)
#      err = sqrt(pow(passed,2)/pow(total,3)+passed/pow(total,2))
#      err = sqrt(passed*(1-passed/total))/total 
       errUp = root.TEfficiency.ClopperPearson(int(total),int(passed),0.68,True)
       errDown = root.TEfficiency.ClopperPearson(int(total),int(passed),0.68,False)
    except ZeroDivisionError:
      err = eff
    hMET.SetBinContent(iB,eff)
    hMET.SetBinError(iB,(errUp-errDown)/2)
    x.append(hMET.GetBinCenter(iB))
    central.append(eff)
    up.append(errUp-eff)
    down.append(eff-errDown)
#    up.append(min(1-eff,err))
#    down.append(min(eff,err))
    zero.append(0)
  N = len(x)
  x = array('f',x); central = array('f',central); zero=array('f',zero)
  up=array('f',up); down=array('f',down)
  errs = root.TGraphAsymmErrors(N,x,central,zero,zero,down,up)
  errs.SetLineWidth(2)
  if fn==getHist:
    hMET.GetXaxis().SetTitle('U')
  else:
    hMET.GetXaxis().SetTitle('MET')
  hMET.GetYaxis().SetTitle('Efficiency')
  hMET.SetMaximum(1)
  if doFit:
    hMET.Fit(fitFunc)
    plot.AddPlotLabel('#mu=%.3f, #sigma=%.3f'%(fitFunc.GetParameter(0),fitFunc.GetParameter(1)),0.5,0.5,True,42,0.05,11)
  for iB in xrange(1,len(bins)):
    hMET.SetBinError(iB,0.00001)
  fitFunc.SetLineColor(root.kBlue)
  plot.AddHistogram(hMET,'Data',root.kData)
  plot.AddAdditional(errs,'p')
  if doFit:
    plot.AddAdditional(fitFunc,'l')
  plot.Draw('~/public_html/figs/triggerStudies2/',label)
  plot.Reset()


runEff('monojet_allMET_wrtSingleMu','all','monojet',None,getHist,False) 
runEff('vbf_allMET_wrtSingleMu','all','vbf') #; exit(0)

runEff('monojet_MET170_wrtSingleMu_met','met170','monojet',None,getMETHist)
runEff('vbf_MET170_wrtSingleMu_met','met170','vbf',None,getMETHist)

runEff('monojet_MET170_wrtSingleMu','met170','monojet')
runEff('vbf_MET170_wrtSingleMu','met170','vbf')

runEff('monojet_METMHT_wrtSingleMu','metmht','monojet')
runEff('vbf_METMHT_wrtSingleMu','metmht','vbf')

runEff('monojet_METMHT_wrtSingleMuAndMET170','all','monojet')
runEff('vbf_METMHT_wrtSingleMuAndMET170','all','vbf')
