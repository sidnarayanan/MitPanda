#!/usr/bin/env python
from MitPanda.Tools.MultiThreading import GenericRunner
from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv
from os import path,getenv,system
from json import load as loadJson


if __name__ == "__main__":
  
  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/NeroSkimmer.h")
  gSystem.Load('libMitPandaSkimmers.so')
  gSystem.Load('libNeroProducerCore.so')
  
  panda=getenv('PANDA')
  with open(panda+'/Monotop/data/Cert_271036-273730_13TeV_PromptReco_Collisions16_JSON.txt') as jsonFile:
    json=loadJson(jsonFile)

  def fn(counter):
    system('touch test_%i'%counter)
    fullPath = '/home/snarayan/cms/root/triggerStudies/crab/NeroNtuples_%i.root'%counter

    skimmer = root.NeroSkimmer()
    
    skimmer.usePuppiMET=False;
    skimmer.set_ak4Jetslabel("")
    skimmer.set_ca15Jetslabel("CA15CHS")

    isData=True
    skimmer.isData=isData
    isData=True
    if isData:
      for run,lumi in json.iteritems():
        for l in lumi:
          for lll in xrange(l[0],l[1]+1):
            skimmer.AddLumi(int(run),int(lll))
    skimmer.isData=isData
    skimmer.processType=root.NeroSkimmer.kNone
    skimmer.applyJson=True
    skimmer.fromBambu=False

    skimmer.SetDataDir(panda+"/Monotop/data/")
    fin = root.TFile(fullPath)
    tree = fin.FindObjectAny("events")
    skimmer.SetOutputFile('/home/snarayan/cms/root/triggerStudies/split/SingleMuon_%i.root'%counter)
    skimmer.Init(tree)
    skimmer.Run(1)
    skimmer.Terminate()
    system('rm test_%i'%counter)

  gr = GenericRunner(fn)
  #argList = [[x] for x in range(1,13)]
  argList = [[x] for x in [4,5,6,7,8]]
  gr.setArgList(argList)
  gr.run(12)

