#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from sys import argv,exit
from array import array
from MitPanda.Tools.Misc import *
from math import sqrt
from ROOT import gROOT
from MitPanda.VBF.Selection import *

baseDir = '/local/snarayan/monojet/'
lumi = 2300.

### DEFINE REGIONS ###
try:
  vbfcut = argv[1]
except:
  vbfcut = 'noid'

vbfsel = cuts[vbfcut]

gROOT.LoadMacro('${CMSSW_BASE}/src/MitPanda/Tools/interface/HistogramDrawer.h')
gSystem.Load('libMitPandaTools.so')

# recoilBins = [200., 260.0, 320.0,  390.0,  470.0, 550.0,  640.0, 740.0, 840.0, 1250.0]
recoilBins = [200,250,300,350,400,500,600,1000]
nRecoilBins = len(recoilBins)-1
recoilBins = array('f',recoilBins)

logy=True
plot = root.HistogramDrawer()
plot.SetLumi(2.3)
plot.SetTDRStyle()
plot.AddCMSLabel()
plot.AddLumiLabel()
plot.SetNormFactor(True)
plot.Logy(logy)
plot.SetAbsMin(0.0001)
plot.SetTDRStyle()
plot.InitLegend()

plotr = root.HistogramDrawer()
plotr.SetLumi(2.3)
plotr.SetRatioStyle()
plotr.AddCMSLabel()
plotr.AddLumiLabel()
plotr.InitLegend(.15,.6,.5,.8)

counter=0

fvbf = root.TFile(baseDir+'monojet_VBF_HToInvisible_M125_13TeV_powheg_pythia8.root'); tvbf = fvbf.Get('events')
fqcdz = root.TFile(baseDir+'ZQCD.root'); tqcdz = fqcdz.Get('events')
fewkz = root.TFile(baseDir+'ZEWK.root'); tewkz = fewkz.Get('events')
fqcdw = root.TFile(baseDir+'WQCD.root'); tqcdw = fqcdw.Get('events')
fewkw = root.TFile(baseDir+'WEWK.root'); tewkw = fewkw.Get('events')

ctmp = root.TCanvas()

def getMETHist(tree,cut,binwidth=False):
  global counter
  ctmp.cd()
  h = root.TH1F('h%i'%counter,'h%i'%counter,nRecoilBins,recoilBins)
  h.GetXaxis().SetTitle('U')
  h.GetYaxis().SetTitle('arbitrary units')
  if binwidth:
    h.Scale(1,'width')
  tree.Draw('met>>h%i'%counter,cut)
  counter += 1
  h.SetFillStyle(0)
  return h

def getMjjHist(tree,cut):
  global counter
  ctmp.cd()
  h = root.TH1F('h%i'%counter,'h%i'%counter,10,0,2000)
  h.GetXaxis().SetTitle('m_{jj} [GeV]')
  h.GetYaxis().SetTitle('arbitrary units')
  tree.Draw('fixed_mjj>>h%i'%counter,cut)
  counter += 1
  h.SetFillStyle(0)
  return h

def getDEtaHist(tree,cut):
  global counter
  ctmp.cd()
  h = root.TH1F('h%i'%counter,'h%i'%counter,20,0,10)
  h.GetXaxis().SetTitle('Leading jets delta #eta')
  h.GetYaxis().SetTitle('arbitrary units')
  tree.Draw('fabs(jjDEta)>>h%i'%counter,cut)
  counter += 1
  h.SetFillStyle(0)
  return h


def plotMETShapes(cut,weight,label):
  totalcut = tTIMES(weight,cut)
  hvbf = getMETHist(tvbf,totalcut,True)
  hqcdz = getMETHist(tqcdz,totalcut,True)
  hewkz = getMETHist(tewkz,totalcut,True)
  hqcdw = getMETHist(tqcdw,totalcut,True)
  hewkw = getMETHist(tewkw,totalcut,True)
  hewkw.SetLineStyle(2); hewkz.SetLineStyle(2)
  plot.AddHistogram(hqcdw,'QCD W+jets',root.kExtra1,root.kMagenta+1)
  plot.AddHistogram(hqcdz,'QCD Z+jets',root.kExtra4,root.kCyan+1)
  plot.AddHistogram(hewkw,'EWK W+jets',root.kExtra3,root.kMagenta+1)
  plot.AddHistogram(hewkz,'EWK Z+jets',root.kExtra2,root.kCyan+1)
  plot.AddHistogram(hvbf,'VBF H#rightarrowInv',root.kSignal)
  plot.Draw('~/public_html/figs/vbf/shapes/',label)
  plot.Reset()

def plotMjjShapes(cut,weight,label):
  totalcut = tTIMES(weight,cut)
  hvbf = getMjjHist(tvbf,totalcut)
  hqcdz = getMjjHist(tqcdz,totalcut)
  hewkz = getMjjHist(tewkz,totalcut)
  hqcdw = getMjjHist(tqcdw,totalcut)
  hewkw = getMjjHist(tewkw,totalcut)
  hewkw.SetLineStyle(2); hewkz.SetLineStyle(2)
  plot.AddHistogram(hqcdw,'QCD W+jets',root.kExtra1,root.kMagenta+1)
  plot.AddHistogram(hqcdz,'QCD Z+jets',root.kExtra4,root.kCyan+1)
  plot.AddHistogram(hewkw,'EWK W+jets',root.kExtra3,root.kMagenta+1)
  plot.AddHistogram(hewkz,'EWK Z+jets',root.kExtra2,root.kCyan+1)
  plot.AddHistogram(hvbf,'VBF H#rightarrowInv',root.kSignal)
  plot.Draw('~/public_html/figs/vbf/shapes/',label)
  plot.Reset()

def plotDEtaShapes(cut,weight,label):
  totalcut = tTIMES(weight,cut)
  hvbf = getDEtaHist(tvbf,totalcut)
  hqcdz = getDEtaHist(tqcdz,totalcut)
  hewkz = getDEtaHist(tewkz,totalcut)
  hqcdw = getDEtaHist(tqcdw,totalcut)
  hewkw = getDEtaHist(tewkw,totalcut)
  hewkw.SetLineStyle(2); hewkz.SetLineStyle(2)
  plot.AddHistogram(hqcdw,'QCD W+jets',root.kExtra1,root.kMagenta+1)
  plot.AddHistogram(hqcdz,'QCD Z+jets',root.kExtra4,root.kCyan+1)
  plot.AddHistogram(hewkw,'EWK W+jets',root.kExtra3,root.kMagenta+1)
  plot.AddHistogram(hewkz,'EWK Z+jets',root.kExtra2,root.kCyan+1)
  plot.AddHistogram(hvbf,'VBF H#rightarrowInv',root.kSignal)
  plot.Draw('~/public_html/figs/vbf/shapes/',label)
  plot.Reset()

def plotRatio(V,label):
  if V=='Z':
    tqcd = tqcdz
    tewk = tewkz
  else:
    tqcd = tqcdw
    tewk = tewkw
  hsigqcd = getMETHist(tqcd,tTIMES(tAND(vbfsel,cuts['signal']),weights['met']))
  hsigewk = getMETHist(tewk,tTIMES(tAND(vbfsel,cuts['signal']),weights['met']))
  hsigewk.Divide(hsigqcd)
  if V=='Z':
    hmuqcd = getMETHist(tqcd,tTIMES(tAND(vbfsel,cuts['dimuon']),weights['met']))
    hmuewk = getMETHist(tewk,tTIMES(tAND(vbfsel,cuts['dimuon']),weights['met']))
    heleqcd = getMETHist(tqcd,tTIMES(tAND(vbfsel,cuts['dielectron']),weights['nomet']))
    heleewk = getMETHist(tewk,tTIMES(tAND(vbfsel,cuts['dielectron']),weights['nomet']))
  else:
    hmuqcd = getMETHist(tqcd,tTIMES(tAND(vbfsel,cuts['singlemuon']),weights['met']))
    hmuewk = getMETHist(tewk,tTIMES(tAND(vbfsel,cuts['singlemuon']),weights['met']))
    heleqcd = getMETHist(tqcd,tTIMES(tAND(vbfsel,cuts['singleelectron']),weights['nomet']))
    heleewk = getMETHist(tewk,tTIMES(tAND(vbfsel,cuts['singleelectron']),weights['nomet']))
  hmuewk.Add(heleewk)
  hmuqcd.Add(heleqcd)
  hmuewk.Divide(hmuqcd)
  for h in [hsigewk,heleewk,hmuewk]:
    h.GetYaxis().SetTitle('EWK %s/QCD %s'%(V,V))
  hsigerr = hsigewk.Clone()
  hsigerr.SetFillStyle(3004)
  hsigerr.SetFillColorAlpha(root.kBlack,0.5)
  hsigerr.SetLineWidth(0)
  hmuerr = hmuewk.Clone()
  hmuerr.SetFillStyle(3005)
  hmuerr.SetFillColorAlpha(root.kMagenta+1,0.5)
  hmuerr.SetLineWidth(0)
  plotr.AddHistogram(hsigewk,'Signal region',root.kExtra2,root.kBlack)
  if V=='W':
    plotr.AddHistogram(hmuewk,'Single lep CRs',root.kExtra1)
  #  plotr.AddHistogram(heleewk,'Single e CR',root.kExtra4)
  else:
    plotr.AddHistogram(hmuewk,'Dilep CRs',root.kExtra1)
  #  plotr.AddHistogram(heleewk,'Dielectron CR',root.kExtra4)
  plotr.AddAdditional(hsigerr,'e2')
  plotr.AddAdditional(hmuerr,'e2')
  plotr.Draw('~/public_html/figs/vbf/shapes/',label)
  plotr.Reset()

def plotWZ(label):
  totalcut = tTIMES(tAND(vbfsel,cuts['signal']),weights['met'])
  hqcdz = getMETHist(tqcdz,totalcut)
  hqcdw = getMETHist(tqcdw,totalcut)
  hewkz = getMETHist(tewkz,totalcut)
  hewkw = getMETHist(tewkw,totalcut)
  hewkw.Divide(hqcdw)
  hewkz.Divide(hqcdz)
  for h in [hewkw,hewkz]:
    h.GetYaxis().SetTitle('EWK V/QCD V')
  hwerr = hewkw.Clone()
  hwerr.SetFillStyle(3004)
  hwerr.SetFillColorAlpha(root.kGreen+1,0.5)
  hwerr.SetLineWidth(0)
  hzerr = hewkz.Clone()
  hzerr.SetFillStyle(3005)
  hzerr.SetFillColorAlpha(root.kCyan+1,0.5)
  hzerr.SetLineWidth(0)
  plotr.AddHistogram(hewkz,'EWK Z/QCD Z',root.kExtra2)
  plotr.AddHistogram(hewkw,'EWK W/QCD W',root.kExtra3)
  plotr.AddAdditional(hzerr,'e2')
  plotr.AddAdditional(hwerr,'e2')
  plotr.Draw('~/public_html/figs/vbf/shapes/',label)
  plotr.Reset()




if not logy:
  plotWZ('%s_WZ_ratio'%vbfcut)
  plotRatio('Z','%s_Z_ratio'%vbfcut)
  plotRatio('W','%s_W_ratio'%vbfcut)
  plotMETShapes(tAND(vbfsel,cuts['signal']),weights['met'],'%s_signal_shapes_lin'%vbfcut)
  plotMjjShapes(tAND(vbfsel,cuts['signal']),weights['met'],'%s_signal_shapes_mjj_lin'%vbfcut)
  plotDEtaShapes(tAND(vbfsel,cuts['signal']),weights['met'],'%s_signal_shapes_deta_lin'%vbfcut)
else:
  plotWZ('%s_WZ_ratio'%vbfcut)
  plotRatio('Z','%s_Z_ratio'%vbfcut)
  plotRatio('W','%s_W_ratio'%vbfcut)
  plotMETShapes(tAND(vbfsel,cuts['signal']),weights['met'],'%s_signal_shapes'%vbfcut)
  plotMjjShapes(tAND(vbfsel,cuts['signal']),weights['met'],'%s_signal_shapes_mjj'%vbfcut)
  plotDEtaShapes(tAND(vbfsel,cuts['signal']),weights['met'],'%s_signal_shapes_deta'%vbfcut)
