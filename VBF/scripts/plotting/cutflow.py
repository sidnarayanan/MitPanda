#!/usr/bin/env python

import ROOT as root
from MitPanda.Tools.Misc import *
import MitPanda.VBF.Selection as sel


fIn = root.TFile('/local/snarayan/monojet/monojet_VBF_HToInvisible_M125_13TeV_powheg_pythia8.root')
tSig = fIn.Get('events')
hone = root.TH1F('hone','hone',1,0,2)

for c in ['noid','oldid','baseline']:
  # print tSig.GetEntries(),hone.GetBinCenter(1)
  tSig.Draw('1.>>hone',tAND(sel.cuts[c],sel.cuts['signal']))
  print c,hone.Integral()
