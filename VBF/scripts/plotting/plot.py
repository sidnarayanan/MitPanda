#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv
from MitPanda.Tools.Misc import *
import MitPanda.VBF.Selection as sel
# sel = __import__('../../cuts.py')

### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

### HELPER FUNCTIONS ###

### SET GLOBAL VARIABLES ###
region = argv[1]
try:
  vbfcut = argv[2]
except IndexError:
  vbfcut = 'baseline'
baseDir = '/local/snarayan/monojet/'
lumi = 2300.
blind=True
linear=False

### DEFINE REGIONS ###

cut = tAND(sel.cuts[region],sel.cuts[vbfcut])

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
if not linear:
  plot.Logy(True)
if 'signal' in region and not blind:
  plot.SetLumi(lumi/5000.)
else:
  plot.SetLumi(lumi/1000)
if not ('signal' in region and blind):
  plot.Ratio(True)
  plot.FixRatio()
plot.SetTDRStyle()
plot.InitLegend()
plot.DrawMCErrors(True)
plot.AddCMSLabel()
plot.AddLumiLabel(True)
plot.SetEvtNum("eventNum")
if 'signal' in region and not blind:
  plot.SetEvtMod(5)
plot.SetCut(root.TCut(cut))

print cut
if 'signal' in region or 'muon' in region:
  plot.SetMCWeight(root.TCut('%f*%s'%(lumi,sel.weights['met'])))
else:
  plot.SetMCWeight(root.TCut('%f*%s'%(lumi,sel.weights['nomet'])))


### DEFINE PROCESSES ###
zjets     = root.Process('QCD Z+jets',root.kZjets)
zewk      = root.Process('EWK Z+jets',root.kExtra2)
wjets     = root.Process('QCD W+jets',root.kWjets)
wewk      = root.Process('EWK W+jets',root.kExtra3)
diboson   = root.Process('Diboson',root.kDiboson)
ttbar     = root.Process('t#bar{t}',root.kTTbar)
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
gjets     = root.Process('#gamma+jets',root.kGjets)
vbf       = root.Process("VBF H#rightarrowInv",root.kSignal)
#ggf       = root.Process("ggF H#rightarrowInv",root.kSignal1)
data      = root.Process("Data",root.kData)
if region=='singlemuon':
  processes = [diboson,singletop,ttbar,wewk,zewk,wjets,zjets]
else:
  processes = [qcd,diboson,singletop,ttbar,wewk,zewk,wjets,zjets]

### ASSIGN FILES TO PROCESSES ###
zjets.AddFile(baseDir+'ZQCD.root')
zewk.AddFile(baseDir+'ZEWK.root')
wjets.AddFile(baseDir+'WQCD.root')
wewk.AddFile(baseDir+'WEWK.root')
diboson.AddFile(baseDir+'Diboson.root')
ttbar.AddFile(baseDir+'monojet_TTJets.root')
singletop.AddFile(baseDir+'SingleTop.root')
qcd.AddFile(baseDir+'QCD.root')
if contains(region,"photon"):
  processes = [qcd,gjets]
  gjets.AddFile(baseDir+'GJets.root')

if 'signal' in region:
  vbf.AddFile(baseDir+'monojet_VBF_HToInvisible_M125_13TeV_powheg_pythia8.root')
#  ggf.AddFile(baseDir+'monojet_GluGlu_HToInvisible_M125_13TeV_powheg_pythia8.root')
  processes += [vbf]
if 'signal' in region or 'muon' in region:
  data.AddFile(baseDir+'monojet_MET.root')
elif 'electron' in region:
  data.AddFile(baseDir+'monojet_SingleElectron.root')
elif 'photon' in region:
  data.AddFile(baseDir+'monojet_SinglePhoton.root')
elif 'lepton' in region:
  data.AddFile(baseDir+'monojet_Data.root')
if not blind:
  processes.append(data)

for p in processes:
  plot.AddProcess(p)

#recoilBins = [200., 230., 260.0, 290.0, 320.0, 350.0, 390.0, 430.0, 470.0, 510.0, 550.0, 590.0, 640.0, 690.0, 740.0, 790.0, 840.0, 900.0, 960.0, 1020.0, 1090.0, 1160.0, 1250.0]
# recoilBins = [200., 260.0, 320.0,  390.0,  470.0, 550.0,  640.0, 740.0, 840.0, 1250.0]
#recoilBins = [250,300,350,400,500,600,750,1000]
recoilBins = [200,250,300,350,400,500,600,750,1000]
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
# define what "recoil" means
if 'muon' in region:
  lep='#mu'
elif 'electron' in region:
  lep='e'
else:
  lep='l'

# plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
if not blind:
  maxval = 10**4
  minval = 1
  if 'signal' in region and vbfcut=='dEtaAndMjjCut':
    maxval = 10**3
  elif 'di' in region and 'lepton' not in region:
    maxval = 10**2
    minval = 0.01
  if linear:
    maxval = -1000
    minval = 1000 # override
  recalcmjj = root.Distribution(sel.fixedmjj,0,4000,10,"Dijet mass [GeV]","Events/400 GeV",minval,maxval)
  recalcmjj.filename = 'mjj_recalc'
  plot.AddDistribution(recalcmjj)
  plot.AddDistribution(root.Distribution("jjDEta",0,10,10,"Delta #eta leading jets","Events",minval,maxval))
  plot.AddDistribution(root.Distribution("jot1Pt",100,850,12,"Jet 1 p_{T}","Events/60 GeV",minval,maxval))
  plot.AddDistribution(root.Distribution("jot2Pt",40,790,12,"Jet 2 p_{T}","Events/60 GeV",minval,maxval))
  plot.AddDistribution(root.Distribution("fabs(jot1Eta)",0,5,10,"Jet 1 #eta","Events",minval,maxval))
  plot.AddDistribution(root.Distribution("fabs(jot2Eta)",0,5,10,"Jet 2 #eta","Events",minval,maxval))
  plot.AddDistribution(root.Distribution("jot1Phi",-3.142,3.142,10,"Jet 1 #phi","Events",minval,maxval))
  plot.AddDistribution(root.Distribution("jot2Phi",-3.142,3.142,10,"Jet 2 #phi","Events",minval,maxval))

recoil=None
if 'signal' in region:
  recoil=root.Distribution("met",nRecoilBins,"MET","Events/GeV")
elif 'single' in region:
  recoil=root.Distribution('met',nRecoilBins,'U(%s)'%lep,"Events/GeV")
elif 'di' in region:
  recoil=root.Distribution('met',nRecoilBins,'U(%s%s)'%(lep,lep),"Events/GeV")
elif 'photon' in region:
  recoil=root.Distribution('met',nRecoilBins,'U(#gamma)',"Events/GeV")
print recoil
if recoil:
  setBins(recoil,recoilBins)
  plot.AddDistribution(recoil)

### DRAW AND CATALOGUE ###
try:
  mkdir(baseDir+'/figs/'+region)
except OSError:
  pass
extrastring=''
if blind:
  extrastring+='blind_'
plot.DrawAll(baseDir+'/figs/'+region+'/'+extrastring+vbfcut+'_')
