#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv
from MitPanda.Tools.Misc import *
from MitPanda.VBF.Selection import *

### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

### HELPER FUNCTIONS ###

### SET GLOBAL VARIABLES ###
region = argv[1]
try:
  vbfcut = argv[2]
except IndexError:
  vbfcut = 'noid'
makePlots = True
baseDir = '/local/snarayan/monojet/'
lumi = 2300.

cut = tAND(cuts[region],cuts[vbfcut])


### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
plot.Logy(True)
plot.Ratio(True)
plot.SetLumi(lumi/1000)
plot.SetTDRStyle()
plot.InitLegend()
plot.DrawMCErrors(True)
plot.AddCMSLabel()
plot.AddLumiLabel(True)
# plot.SetAbsMin(0.01)
plot.SetCut(root.TCut(cut))

print cut
if 'signal' in region or 'muon' in region:
  plot.SetMCWeight(root.TCut('%f*normalizedWeight*lepton_SF*zkfactor*wkfactor*akfactor*ewk_a*ewk_z*ewk_w*METTrigger*puWeight'%lumi))
else:
  plot.SetMCWeight(root.TCut('%f*normalizedWeight*lepton_SF*zkfactor*wkfactor*akfactor*ewk_a*ewk_z*ewk_w*puWeight'%(lumi)))

### DEFINE PROCESSES ###
zjets     = root.Process('QCD Z+jets',root.kZjets)
zewk      = root.Process('EWK Z+jets',root.kExtra2)
wjets     = root.Process('QCD W+jets',root.kWjets)
wewk      = root.Process('EWK W+jets',root.kExtra3)
diboson   = root.Process('Diboson',root.kDiboson)
ttbar     = root.Process('t#bar{t}',root.kTTbar)
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
gjets     = root.Process('#gamma+jets',root.kGjets)
signal    = root.Process("Signal",root.kSignal)
data      = root.Process("Data",root.kData)
processes = [qcd,diboson,singletop,ttbar,wewk,zewk,wjets,zjets]

### ASSIGN FILES TO PROCESSES ###
zjets.AddFile(baseDir+'ZQCD.root')
zewk.AddFile(baseDir+'ZEWK.root')
wjets.AddFile(baseDir+'WQCD.root')
wewk.AddFile(baseDir+'WEWK.root')
diboson.AddFile(baseDir+'Diboson.root')
ttbar.AddFile(baseDir+'monojet_TTJets.root')
singletop.AddFile(baseDir+'SingleTop.root')
qcd.AddFile(baseDir+'QCD.root')
if contains(region,"photon"):
  processes = [qcd,gjets]
  gjets.AddFile(baseDir+'GJets.root')

if 'signal' in region:
  signal.AddFile(baseDir+'monojet_VBF_HToInvisible_M125_13TeV_powheg_pythia8.root')
  # signal.AddFile(baseDir+'monojet_GluGlu_HToInvisible_M125_13TeV_powheg_pythia8.root')
  processes.append(signal)
if 'signal' in region or 'muon' in region:
  data.AddFile(baseDir+'monojet_MET.root')
elif 'electron' in region:
  data.AddFile(baseDir+'monojet_SingleElectron.root')
elif 'photon' in region:
  data.AddFile(baseDir+'monojet_SinglePhoton.root')
processes.append(data)

for p in processes:
  plot.AddProcess(p)

### CHOOSE DISTRIBUTIONS, LABELS ###

plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))

### DRAW AND CATALOGUE ###
try:
  mkdir(baseDir+'/figs/'+region)
except OSError:
  pass
plot.DrawAll(baseDir+'/figs/'+region+'/'+vbfcut+'_')
