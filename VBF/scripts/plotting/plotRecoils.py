#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from MitPanda.Tools.Misc import *
from sys import exit

lumi = 588.76
#lumi = 215.4

gROOT.LoadMacro('${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h')
gSystem.Load('libMitPandaTools.so')

plot = root.PlotUtility()
plot.SetLumi(lumi)
#plot.Ratio(True)
plot.SetTDRStyle()
plot.InitLegend()
plot.Stack(True)
plot.DrawMCErrors(True)
plot.AddCMSLabel()
plot.AddLumiLabel(False)
cut = 'nLooseMuon==2 && nLooseElectron==0 && looseLep1IsTight==1 && 60<diLepMass && diLepMass<120'
trigger = '(trigger&8)!=0'
mcWeight = 'newPUWeight*mcWeight*%f'%lumi
#mcWeight = 'mcWeight*%f'%lumi

plot.SetCut(cut)
plot.SetMCWeight(mcWeight)

baseDir = '~/cms/root/data2016/'
data = root.Process('Data',root.kData); data.additionalCut = root.TCut(trigger); data.AddFile(baseDir+'../data_590pb/SingleMuon.root')
#data = root.Process('Data',root.kData); data.additionalCut = root.TCut(trigger); data.AddFile(baseDir+'SingleMuon.root')
zll  = root.Process('Z#rightarrow#mu#mu',root.kZjets); zll.AddFile(baseDir+'../data_590pb/ZJets.root')
#zll  = root.Process('Z#rightarrow#mu#mu',root.kZjets); zll.AddFile(baseDir+'ZJets.root')
tt   = root.Process('t#bar{t}',root.kTTbar); tt.AddFile(baseDir+'TTbar.root')
for p in [tt,zll,data]:
#for p in [zll,data]:
  plot.AddProcess(p)

#plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
#plot.Logy(True)
#plot.AddDistribution(root.Distribution("sumET/1000",0,3,75,"Sum E_{T} [TeV]","Events/40 GeV"))
#plot.DrawAll('~/public_html/figs/triggerStudies/recoilStudies/')

plot.Logy(False)
plot.AddDistribution(root.Distribution("npv",-0.5,35.5,36,"N_{PV}","Events"))
'''
plot.AddDistribution(root.Distribution("sumET/1000",0,3,75,"Sum E_{T} [TeV]","Events/40 GeV"))
plot.AddDistribution(root.Distribution("recoBosonPt",0,250,50,"p_{T}^{Z} [GeV]","Events/5 GeV"))
plot.AddDistribution(root.Distribution("Upara+recoBosonPt",-100,100,50,"U_{#parallel}+p_{T}^{Z} [GeV]","Events/4 GeV"))
plot.AddDistribution(root.Distribution("Uperp",-100,100,50,"U_{#perp} [GeV]","Events/4 GeV"))
plot.AddDistribution(root.Distribution("diLepMass",60,120,60,"m_{#mu#mu} [GeV]","Events/GeV"))
'''
plot.DrawAll('~/public_html/figs/triggerStudies2/recoilStudies/')
