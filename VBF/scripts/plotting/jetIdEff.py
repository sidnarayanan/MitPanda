#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from sys import argv,exit
from array import array
from MitPanda.Tools.Misc import *
from math import sqrt
from ROOT import gROOT
from glob import glob

gROOT.LoadMacro('${CMSSW_BASE}/src/MitPanda/Tools/interface/HistogramDrawer.h')
gSystem.Load('libMitPandaTools.so')

plot = root.HistogramDrawer()
plot.SetLumi(2.3)
plot.SetTDRStyle()
plot.AddCMSLabel()

counter=0

indir = '/afs/cern.ch/user/s/snarayan/eos/cms/store/user/ceballos/nero/setup76x/v1.3/VBF_HToInvisible_M125_13TeV_powheg_pythia8/VBF_HToInvisible_M125_new/160602_201009/0000/*'
infiles = glob(indir)

c = root.TChain('nero/events')
#for f in infiles:
for f in infiles[:1]:
  c.AddFile(f)


basecut = 'metP4.Pt()>200 && jetP4[0].Pt()>100'
loose = '(jetSelBits&8)!=0'
mjidloose = '(jetSelBits&512)!=0'
mjid2015 = '(jetSelBits&1024)!=0'

def getHist(tree,cut):
  global counter
  h = root.TH1F('h%i'%counter,'h%i'%counter,20,0,5)
  tree.Draw('TMath::Abs(jetP4[0].Eta())>>h%i'%counter,cut)
  counter += 1
  return h

def calcEff(label,idcut):
  hbase = getHist(c,basecut)
  hnum = getHist(c,tAND(idcut,basecut))
  hone = hnum.Clone('hone')
  hone.SetLineColor(root.kRed); hone.SetLineStyle(2)
  x = []; central = []; up = []; down = []; zero = []
  for iB in xrange(1,hbase.GetNbinsX()+1):
    passed=hnum.GetBinContent(iB)
    total=hbase.GetBinContent(iB)
    if total>0:
      eff = passed/total
      errUp = root.TEfficiency.ClopperPearson(int(total),int(passed),0.68,True)
      errDown = root.TEfficiency.ClopperPearson(int(total),int(passed),0.68,False)
    else:
      eff = 0
      errUp = 1 
      errDown = 0
    hone.SetBinContent(iB,1)
    hnum.SetBinContent(iB,eff)
    hnum.SetBinError(iB,0.0001)
    x.append(hnum.GetBinCenter(iB))
    central.append(eff)
    up.append(errUp-eff)
    down.append(eff-errDown)
    zero.append(0)
  N = len(x)
  x = array('f',x); central = array('f',central); zero=array('f',zero)
  up=array('f',up); down=array('f',down)
  errs = root.TGraphAsymmErrors(N,x,central,zero,zero,down,up)
  errs.SetLineWidth(2)
  hnum.GetXaxis().SetTitle('Leading jet #eta')
  hnum.GetYaxis().SetTitle('Efficiency')
  hnum.SetMaximum(1)
  plot.AddHistogram(hnum,'MC eff',root.kData)
  plot.AddAdditional(hone,'hist')
  plot.AddAdditional(errs,'p')
  plot.Draw('~/www/figs/vbf/jetid/',label)
  plot.Reset()

calcEff('mjid2015',mjid2015)
calcEff('mjidloose',mjidloose)
calcEff('loose',loose)
