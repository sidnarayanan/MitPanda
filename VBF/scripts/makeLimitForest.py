#!/usr/bin/env python
from MitPanda.Tools.MultiThreading import GenericRunner
from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv,exit
from os import path,getenv,system
from MitPanda.Tools.Misc import *
import MitPanda.VBF.Selection as sel

if __name__ == "__main__":
  
  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/LimitTreeBuilder.h")
  gSystem.Load('libMitPandaSkimmers.so')

  baseDir = '/local/snarayan/monojet/'
  lumi = 2320

  factory = root.LimitTreeBuilder()
  factory.SetOutFile(baseDir+'limitForest.root')


  def getChain(fpaths):
    c = root.TChain('events')
    for f in fpaths:
      c.AddFile('~/local/monojet/monojet_%s.root'%f)
    return c

  # input
  tZll = getChain(['DYJetsToLL_M-50_HT-100to200',
                  'DYJetsToLL_M-50_HT-200to400',
                  'DYJetsToLL_M-50_HT-400to600',
                  'DYJetsToLL_M-50_HT-600toInf',
                  ])
  tPho = getChain(['GJets_HT-100To200',
                  'GJets_HT-200To400',
                  'GJets_HT-400To600',
                  'GJets_HT-600ToInf',
                  ])
  tZvv = getChain(['ZJetsToNuNu_HT-100To200_13TeV',
                  'ZJetsToNuNu_HT-200To400_13TeV',
                  'ZJetsToNuNu_HT-400To600_13TeV',
                  'ZJetsToNuNu_HT-600To800_13TeV',
                  'ZJetsToNuNu_HT-800To1200_13TeV',
                  'ZJetsToNuNu_HT-1200To2500_13TeV',
                  'ZJetsToNuNu_HT-2500ToInf_13TeV',
                  ])
  tWlv = getChain(['WJetsToLNu_HT-100To200',
                  'WJetsToLNu_HT-200To400',
                  'WJetsToLNu_HT-400To600',
                  'WJetsToLNu_HT-600To800',
                  'WJetsToLNu_HT-800To1200',
                  'WJetsToLNu_HT-1200To2500',
                  'WJetsToLNu_HT-2500ToInf',
                  ])
  tTT  = getChain(['TTJets'])
  tVV  = getChain(['WW','ZZ','WZ'])
  tQCD = getChain(['QCD_HT200to300',
                   'QCD_HT300to500',
                   'QCD_HT500to700',
                   'QCD_HT700to1000',
                   'QCD_HT1000to1500',
                   'QCD_HT1500to2000',
                   'QCD_HT2000toInf',
                  ])
  tST  = getChain(['ST_t-channel_antitop_4f_leptonDecays_13TeV-powheg-pythia8',
                   'ST_t-channel_top_4f_leptonDecays_13TeV-powheg-pythia8',
                   'ST_tW_antitop_5f_inclusiveDecays_13TeV-powheg-pythia8',
                   'ST_tW_top_5f_inclusiveDecays_13TeV-powheg-pythia8',
                  ])
  tSig = getChain(['VBF_HToInvisible_M125_13TeV_powheg_pythia8'])
  tEWKZll = getChain(['EWKZ2Jets_ZToLL_13TeV']) 
  tEWKZvv = getChain(['EWKZ2Jets_ZToNuNu_13TeV']) 
  tEWKWlv = getChain(['EWKWPlus2Jets_WToLNu_13TeV','EWKWMinus2Jets_WToLNu_13TeV']) 
  tData = getChain(['Data'])

  processes = {}

  vm = root.VariableMap('met','metPhi','','genBos_pt','genBos_phi')

  # signal region
  region = root.Region('signal')
  cut = tAND(sel.cuts['noid'],sel.cuts['signal'])
  weight = '%f*%s'%(lumi,sel.weights['met'])
  processes['signal'] = [
    root.Process('Data',tData,vm,tAND(sel.triggers['met'],cut),'1'),
    root.Process('Zvv',tZvv,vm,cut,weight),
    root.Process('Wlv',tWlv,vm,cut,weight),
    root.Process('TT',tTT,vm,cut,weight),
    root.Process('VV',tVV,vm,cut,weight),
    root.Process('QCD',tQCD,vm,cut,weight),
    root.Process('ST',tST,vm,cut,weight),
    root.Process('VBF_mH125',tSig,vm,cut,weight),
  ]
  for p in processes['signal']:
    region.AddProcess(p)
  factory.AddRegion(region)

  # single muon
  region = root.Region('singlemuon')
  cut = tAND(sel.cuts['noid'],sel.cuts['singlemuon'])
  weight = '%f*%s'%(lumi,sel.weights['met'])
  processes['singlemuon'] = [
    root.Process('Data',tData,vm,tAND(sel.triggers['met'],cut),'1'),
    root.Process('Zll',tZll,vm,cut,weight),
    root.Process('Wlv',tWlv,vm,cut,weight),
    root.Process('TT',tTT,vm,cut,weight),
    root.Process('VV',tVV,vm,cut,weight),
    root.Process('QCD',tQCD,vm,cut,weight),
    root.Process('ST',tST,vm,cut,weight),
  ]
  for p in processes['singlemuon']:
    region.AddProcess(p)
  factory.AddRegion(region)

  # di muon
  region = root.Region('dimuon')
  cut = tAND(sel.cuts['noid'],sel.cuts['dimuon'])
  weight = '%f*%s'%(lumi,sel.weights['met'])
  processes['dimuon'] = [
    root.Process('Data',tData,vm,tAND(sel.triggers['met'],cut),'1'),
    root.Process('Zll',tZll,vm,cut,weight),
    root.Process('Wlv',tWlv,vm,cut,weight),
    root.Process('TT',tTT,vm,cut,weight),
    root.Process('VV',tVV,vm,cut,weight),
    root.Process('QCD',tQCD,vm,cut,weight),
    root.Process('ST',tST,vm,cut,weight),
  ]
  for p in processes['dimuon']:
    region.AddProcess(p)
  factory.AddRegion(region)

  # single electron
  region = root.Region('singleelectron')
  cut = tAND(sel.cuts['noid'],sel.cuts['singleelectron'])
  weight = '%f*%s'%(lumi,sel.weights['nomet'])
  processes['singleelectron'] = [
    root.Process('Data',tData,vm,tAND(sel.triggers['ele'],cut),'1'),
    root.Process('Zll',tZll,vm,cut,weight),
    root.Process('Wlv',tWlv,vm,cut,weight),
    root.Process('TT',tTT,vm,cut,weight),
    root.Process('VV',tVV,vm,cut,weight),
    root.Process('QCD',tQCD,vm,cut,weight),
    root.Process('ST',tST,vm,cut,weight),
  ]
  for p in processes['singleelectron']:
    region.AddProcess(p)
  factory.AddRegion(region)

  # di electron
  region = root.Region('dielectron')
  cut = tAND(sel.cuts['noid'],sel.cuts['dielectron'])
  weight = '%f*%s'%(lumi,sel.weights['nomet'])
  processes['dielectron'] = [
    root.Process('Data',tData,vm,tAND(sel.triggers['ele'],cut),'1'),
    root.Process('Zll',tZll,vm,cut,weight),
    root.Process('Wlv',tWlv,vm,cut,weight),
    root.Process('TT',tTT,vm,cut,weight),
    root.Process('VV',tVV,vm,cut,weight),
    root.Process('QCD',tQCD,vm,cut,weight),
    root.Process('ST',tST,vm,cut,weight),
  ]
  for p in processes['dielectron']:
    region.AddProcess(p)
  factory.AddRegion(region)

  # photon
  region = root.Region('photon')
  cut = tAND(sel.cuts['noid'],sel.cuts['photon'])
  weight = '%f*%s'%(lumi,sel.weights['nomet'])
  processes['photon'] = [
    root.Process('Data',tData,vm,tAND(sel.triggers['pho'],cut),'1'),
    root.Process('Pho',tPho,vm,cut,weight),
    root.Process('QCD',tQCD,vm,cut,weight),
  ]
  for p in processes['photon']:
    region.AddProcess(p)
  factory.AddRegion(region)

  # exit(1)

  def fn(p):
    p.Run()

  print 'starting...'
  for r,ps in processes.iteritems():
    for p in ps:
      print r,p.name
      p.Run()

  print 'finishing...'

  factory.Output()

  exit(1)

  # gr = GenericRunner(fn)
  # argList = []
  # for name,r in regions.iteritems():
  #   for p in r.GetProcesses():
  #     argList.append([p])
  # gr.setArgList(argList)
  # gr.run(6)

  # factory.Output()
