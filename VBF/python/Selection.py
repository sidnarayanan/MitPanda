from MitPanda.Tools.Misc import *

fixedmjj = "fixed_mjj"

cuts = {}
weights = {}
triggers = {}

baseline = 'jot1Eta*jot2Eta < 0 && jot1Pt>100. && jot2Pt>40. && ((fabs(jot1Eta)<2.5&&jet1isMonoJetIdNew==1)||(fabs(jot2Eta)<2.5&&jet2isMonoJetIdNew==1))'
noid = 'jot1Eta*jot2Eta < 0 && jot1Pt>100. && jot2Pt>40.'
oldid = 'jot1Eta*jot2Eta < 0 && jot1Pt>100. && jot2Pt>40. && jet1isMonoJetIdNew==1 && jet2isMonoJetIdNew==1'

#vbf cuts
cuts['baseline'] = baseline
cuts['noid' ] = noid
cuts['oldid' ] = oldid
cuts['dEtaCut'] = tAND(noid,'jjDEta>3.')
cuts['mjjCut'] = tAND(noid, '%s>500'%fixedmjj)
cuts['dEtaAndMjjCut'] =tAND(noid, 'jjDEta>3 && %s>500'%fixedmjj)

#regions
cuts['signal'] = 'n_tau==0 && n_bjetsMedium==0 && n_loosepho==0 && abs(minJetMetDPhi_withendcap)>0.5 && n_looselep==0 && met>200.0'
cuts['singlemuon'] = 'n_tau==0 && n_bjetsMedium==0 && n_loosepho==0 && abs(minJetMetDPhi_withendcap)>0.5 && n_looselep == 1 && abs(lep1PdgId)==13 && n_tightlep > 0 && met>200.0'
cuts['dimuon'] = 'n_tau==0 && n_bjetsMedium==0 && n_loosepho==0 && abs(minJetMetDPhi_withendcap)>0.5 && n_looselep == 2 && lep2PdgId*lep1PdgId==-169 && n_tightlep > 0 && met>200.0'
cuts['singleelectron'] = 'n_tau==0 && n_bjetsMedium==0 && n_loosepho==0 && abs(minJetMetDPhi_withendcap)>0.5 && n_looselep == 1 && abs(lep1PdgId)==11 && n_tightlep > 0 && met>200.0 && trueMet>40'
cuts['dielectron'] = 'n_tau==0 && n_bjetsMedium==0 && n_loosepho==0 && abs(minJetMetDPhi_withendcap)>0.5 && n_looselep == 2 && lep2PdgId*lep1PdgId==-121 && n_tightlep > 0 && met>200.0'
cuts['photon'] = 'n_tau==0 && n_bjetsMedium==0 && n_mediumpho==1 && abs(minJetMetDPhi_withendcap)>0.5 && n_looselep == 0 && met>200.0'
cuts['dilepton'] = tOR(cuts['dimuon'],cuts['dielectron'])
cuts['singlelepton'] = tOR(cuts['singlemuon'],cuts['singleelectron'])

weights['met'] = 'normalizedWeight*lepton_SF*zkfactor*wkfactor*akfactor*ewk_a*ewk_z*ewk_w*METTrigger*puWeight'
weights['nomet'] = 'normalizedWeight*lepton_SF*zkfactor*wkfactor*akfactor*ewk_a*ewk_z*ewk_w*puWeight'

triggers['met'] = 'triggerFired[4]==1||triggerFired[5]==1||triggerFired[6]==1'
triggers['ele'] = 'triggerFired[0]==1'
triggers['pho'] = 'triggerFired[14]==1||triggerFired[15]==1'