#!/usr/bin/env python

from sys import argv
from glob import glob
from os import stat,environ

from process import *

#listOfFiles = glob('%s/t2mit/filefi/042/*/nero*'%(argv[1]))
listOfFiles = glob('%s/%s/*/nero*'%(environ['PANDA_HISTDIR'],environ['PANDA_MITBOOK']))

cfgFile = open(argv[1],'w')

for f in sorted(listOfFiles):
  if stat(f).st_size==0:
    continue
  ff = f.split('/')
  dataset = ff[-2]
  fileName = ff[-2]+'/'+ff[-1]
  try:
    properties = processes[dataset]
  except KeyError:
    try:
      pd = dataset.split('+')[0]
      properties = processes[pd]
    except KeyError:
      print 'COULD NOT FIND',pd
      properties = ('UNKNOWN','UNKNOWN',-1)
      continue
  cfgFile.write('{0:<25} ${{PANDA_HISTDIR}}/${{PANDA_MITBOOK}}/{1:<180} {2:<10}{3}\n'.format(properties[0],fileName,properties[1],properties[2]))

