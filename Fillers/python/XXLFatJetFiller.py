from MitAna.TreeMod.bambu import mithep
import os

xxlFatJetFiller = mithep.XXLFatJetFiller(
    InputName = "FatJets",
    OutputName = 'XlFatJets',
    ProcessNJets = 4,
    ConeSize = 0.8
)
