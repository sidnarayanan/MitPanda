#ifndef MVAFLATTENER
#define MVAFLATTENER 1

#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TString.h"
#include <vector>

/*
   Allows one to smooth the efficiency or yield of an MVA
   in a particular sample (or set of samples).
   Author(s): S.Narayanan
*/

class MVATransformer
{
public:

  MVATransformer(bool doEff=true);
  ~MVATransformer();

  void AddSample(TTree *t)  { fTrees.push_back(t); useCachedBigHist = false; }
  void ResetSamples()       { fTrees.clear(); useCachedBigHist = false; }
  void SetCuts(TH1F *h)     { cachedHist = h; externalCuts = true; }
  void SetVariable(TString v, unsigned int n, double l, double h);
  void SetMVAVariable(TString v, unsigned int n);

  TH1F *GetCuts()           { return cachedHist;}
  void ComputeAll(double t, const char *cut="");
  double Apply(double x);

protected:
  
  std::vector<TTree*> fTrees;
  bool doEfficiency; // if false, smooth the yield
  double targetValue;
  bool externalCuts; // tells us whether to delete on deletion or not

  TH2D *cachedBigHist{0};
  bool useCachedBigHist;
  TH1F *cachedHist{0};
  TString variable; // variable to smooth in
  TString mvaVariable; // the name of the response in the tree
  unsigned int nMVABins;
  unsigned int nVarBins;
  double lowVar;
  double highVar; 
};

#endif
