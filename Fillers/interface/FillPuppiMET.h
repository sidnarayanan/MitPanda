
// $Id: FillPuppiMET.h,v 1.9 2011/03/01 17:27:22 mzanetti Exp $
//
// FatJetExtenderCustom
//
#include "MitAna/DataTree/interface/Types.h"
// This module processes a collection of input PFCandidates, 
// returns the uncorrected MET
//
// Authors: S.Narayanan
//--------------------------------------------------------------------------------------------------

#ifndef MITPHYSICS_MODS_FILLPUPPIMET_H
#define MITPHYSICS_MODS_FILLPUPPIMET_H

#include <TVector2.h>

#include "MitAna/DataTree/interface/PFMetFwd.h"
#include "MitAna/DataTree/interface/PFMet.h"
#include "MitAna/DataTree/interface/PFMetCol.h"
#include "MitAna/DataTree/interface/PFCandidateCol.h"

#include "MitAna/TreeMod/interface/BaseMod.h"

namespace mithep
{
  class FillPuppiMET : public BaseMod
  {
    public:
      FillPuppiMET(const char *name = "FillPuppiMET",
                   const char *title = "Puppi MET filler module");
      ~FillPuppiMET();

      void IsData(Bool_t b)                { fIsData = b;           }
      void PublishOutput(Bool_t b)         { fPublishOutput = b;    }

      void SetInputName(const char *n)      { fPFCandsName = n;         }
      void SetInputFromBranch(Bool_t b)     { fPFCandsFromBranch = b;   }

      void SetOutputName(const char *n)   { fMETName = n;    }
      const char * GetOutputName()        { return fMETName;  }

      void SetEtaCut(double d)            { fEtaCut = d; }
      void SetApplyEtaCut(Bool_t b)       { fApplyEtaCut = b; }

    protected:
      void Process();
      void SlaveBegin();
      void SlaveTerminate();

    private:
      Bool_t fIsData;                      //is this data or MC?
      Bool_t fPublishOutput;               //=true if output collection are published

      TString fPFCandsName;                   //(i) name of inputs
      Bool_t fPFCandsFromBranch;              //are inputs from Branch?
      const PFCandidateCol *fPFCands;                 //inputs

      TString fMETName;              //name of output  collection
      PFMetArr *fMETCol;             //array of METs

      Bool_t fApplyEtaCut;
      Double_t fEtaCut;

      ClassDef(FillPuppiMET, 0)       
  };
}
#endif
