#ifndef MITPANDA_FILLERS_LINKDEF_H
#define MITPANDA_FILLERS_LINKDEF_H

#include "MitPanda/Fillers/interface/XXLFatJetFiller.h"
#include "MitPanda/Fillers/interface/FillPuppiMET.h"
#endif

#ifdef __CLING__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;
#pragma link C++ namespace mithep;

#pragma link C++ class mithep::XXLFatJetFiller+;
#pragma link C++ class mithep::FillPuppiMET+;
#endif
