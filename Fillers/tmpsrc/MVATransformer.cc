#include "MitPanda/Fillers/interface/MVATransformer.h"
#include "TFile.h"
#include "TROOT.h"
#include "TCanvas.h"

MVATransformer::MVATransformer(bool doEff):
  doEfficiency(doEff),
  nMVABins(-1),
  nVarBins(-1),
  lowVar(-1),
  highVar(-1)
{
  externalCuts = false;
  useCachedBigHist = false;
}


MVATransformer::~MVATransformer() {
  if (!externalCuts && cachedHist)
    delete cachedHist;
}

void MVATransformer::SetVariable(TString v, unsigned int n, double l, double h) {
  variable = v;
  nVarBins = n;
  lowVar = l;
  highVar = h;
}

void MVATransformer::SetMVAVariable(TString v, unsigned int n) {
  mvaVariable = v;
  nMVABins = n;
}

void MVATransformer::ComputeAll(double t,const char *cut/*=""*/) {
  targetValue = t;
  // this is the real workhorse
  if (!externalCuts && cachedHist!=NULL) {
    fprintf(stderr,"deleting %p\n",cachedHist);
    delete cachedHist;
  }
  externalCuts = false;
  
  cachedHist = new TH1F("cuts","cuts",nVarBins,lowVar,highVar);
  
  if (cachedBigHist==NULL || !useCachedBigHist) {
    if (cachedBigHist!=NULL) {
      fprintf(stderr,"deleting big %p\n",cachedBigHist);
      delete cachedBigHist;
    }
	  cachedBigHist = new TH2D("big","big",nVarBins,lowVar,highVar,nMVABins,0.,1.);
	  TH2D *hMassNN;
	  for (TTree *t : fTrees) {
	    t->Draw(TString::Format("%s:%s>>hMassNN(%i,%f,%f,%i,0,1)",mvaVariable.Data(),variable.Data(),(int)nVarBins,lowVar,highVar,(int)nMVABins),cut,"colz");
	    hMassNN = (TH2D*)gPad->GetPrimitive("hMassNN");
	    cachedBigHist->Add(hMassNN);
	    delete hMassNN;
    }
    useCachedBigHist = true; // no need to reolad for every targetValue
  }

  TFile *fBuffer = new TFile("/tmp/buffer.root","RECREATE"); // once this file is made, nothing can be cached
  double varStep = (highVar-lowVar)/nVarBins;

  TH1D *hProjection;
  for (unsigned int iB=0; iB!=nVarBins; ++iB) {
    hProjection = cachedBigHist->ProjectionY("",iB+1,iB+2);
    float integral = hProjection->Integral(1,nMVABins);
    float failed=0;
    std::vector<double>errors;
    std::vector<double>rejections; // used for debugging
    for (unsigned int jB=0; jB!=nMVABins; ++jB) {
    //for (unsigned int jB=nMVABins; jB!=0; --jB) {
//      accepted += hProjection->Integral(jB,nMVABins);
      failed = hProjection->Integral(1,jB+1);        
      if (doEfficiency) {
        errors.push_back(TMath::Abs(1-targetValue - failed/integral));
        rejections.push_back(failed/integral);
      }
      else
        errors.push_back(TMath::Abs(integral-failed - targetValue));
    }
    double minError = 1.;
    float cutVal=-1;
    int minIdx = -1;
    for (unsigned int jB=0; jB!=nMVABins; ++jB) {
      if (errors[jB]<minError) {
        minError = errors[jB];
        cutVal = (1.+jB)/nMVABins;
        minIdx = jB;
      }
    }
    fprintf(stderr,"%f %f(%f) ==> %f\n",1.*iB/nVarBins*highVar,minError,rejections[minIdx],cutVal);
    cachedHist->Fill(iB*varStep,cutVal);
    delete hProjection;
  }
  fBuffer->Close();
//fprintf(stderr,"hello\n");

}

double MVATransformer::Apply(double x) {
  if (cachedHist)
    return cachedHist->GetBinContent(cachedHist->FindBin(x));
  else
    return -1;
}

