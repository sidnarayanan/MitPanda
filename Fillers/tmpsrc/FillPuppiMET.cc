#include "MitAna/DataTree/interface/Names.h"
#include "MitPanda/Fillers/interface/FillPuppiMET.h"
#include "MitAna/DataTree/interface/Types.h"
#include "MitAna/DataTree/interface/EventHeader.h"

#include "MitCommon/MathTools/interface/MathUtils.h"


using namespace mithep;

ClassImp(mithep::FillPuppiMET)

//--------------------------------------------------------------------------------------------------
FillPuppiMET::FillPuppiMET(const char *name, const char *title) :
  BaseMod (name,title),
  fIsData (kFALSE),
  fPublishOutput(kTRUE),
  fPFCandsName ("PuppiParticles"),
  fPFCandsFromBranch (kFALSE),
  fPFCands (0),
  fMETName ("PuppiMET"),
  fApplyEtaCut(kFALSE),
  fEtaCut(99.)
{
  // Constructor.

}

FillPuppiMET::~FillPuppiMET()
{
}

//--------------------------------------------------------------------------------------------------
void FillPuppiMET::Process()
{
  // make sure the out collections are empty before starting
  fMETCol->Delete();
  
  fPFCands = GetObject<PFCandidateCol>(fPFCandsName);

  PFMet *met = fMETCol->AddNew();
  FourVectorM sumVec(0,0,0,0);
  float sumET=0;

  const PFCandidate *pfCand=0;
  // Loop over particles
  float chargedEMEnergy=0, neutralEMEnergy=0, neutralHadEnergy=0, chargedHadEnergy=0, muonEnergy=0;
  for (UInt_t iPF=0; iPF<fPFCands->GetEntries(); ++iPF) {
    pfCand = fPFCands->At(iPF);
    if (fApplyEtaCut && pfCand->AbsEta()>fEtaCut)
      continue;
    sumET += pfCand->Et();
    sumVec += pfCand->Mom();
    switch (pfCand->PFType()) {
      case PFCandidate::eHadron:
        chargedHadEnergy+=pfCand->Et();
        break;
      case PFCandidate::eElectron:
        chargedEMEnergy+=pfCand->Et();
        break;
      case PFCandidate::eMuon:
        muonEnergy+=pfCand->Et();
        break;
      case PFCandidate::eGamma:
      case PFCandidate::eEGammaHF:
        neutralEMEnergy+=pfCand->Et();
        break;
      case PFCandidate::eNeutralHadron:
      case PFCandidate::eHadronHF:
        neutralHadEnergy+=pfCand->Et();
        break;
      default:
        break;
    }
  }

  // is this different from sumET? what if a PFCandidate doesn't have a defined type?
  float totalET = chargedEMEnergy + chargedHadEnergy + neutralEMEnergy + neutralHadEnergy + muonEnergy;

  met->SetMex(-1*sumVec.Px());
  met->SetMey(-1*sumVec.Py());
  met->SetElongitudinal(-1*sumVec.Pz());
  met->SetSumEt(sumET);
  met->SetChargedEMFraction(chargedEMEnergy/totalET);
  met->SetChargedHadFraction(chargedHadEnergy/totalET);
  met->SetNeutralEMFraction(neutralEMEnergy/totalET);
  met->SetNeutralHadFraction(neutralHadEnergy/totalET);
  met->SetMuonFraction(muonEnergy/totalET);

  Info("Process","%i %f %f\n",GetEventHeader()->EvtNum(),met->Pt(),met->Phi());
  //fprintf(stderr,"UNCORRECTED MET: %f\n",met->Pt());
  return;
}

//--------------------------------------------------------------------------------------------------
void FillPuppiMET::SlaveBegin()
{
  // Create the new output collection
  fMETCol = new PFMetArr(1,fMETName); // but what if there are TWO METs???
  // Publish collection for further usage in the analysis
  if (fPublishOutput) 
    PublishObj(fMETCol);

  return;
}

//--------------------------------------------------------------------------------------------------
void FillPuppiMET::SlaveTerminate()
{
  RetractObj(fMETCol->GetName());
  
  if (fMETCol)
    delete fMETCol;

}

