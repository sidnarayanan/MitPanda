#!/bin/bash
label=$1
cfgName=$2
pwd

scramdir=/home/snarayan/cms/cmssw/041/CMSSW_7_4_4/
# workdir=/condor/execute/dir_$PPID/ # this should work - the directory created is specified by the condor job PID
cd ${scramdir}/src/
eval `scramv1 runtime -sh`
cd -
# cd /tmp

cp ${scramdir}/src/MitPanda/PerformanceStudies/qcdStudy/skim.C .
cp ${scramdir}/src/MitPanda/PerformanceStudies/qcdStudy/rootlogon.C .

root -b -l -q rootlogon.C skim.C+\("\"${scramdir}/src/MitPanda/PerformanceStudies/config/${2}.cfg\",${label},0,\"/home/snarayan/cms/root/dijet25ns_v1/\""\)
exit $status
