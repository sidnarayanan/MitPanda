#!/usr/bin/env python

baseDir = '${ROOT}/dijet25ns_v1/'
lumi = 996.

import ROOT as root
from os import system

#root.gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
root.gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
root.gSystem.Load('libMitPandaTools.so')

plot = root.PlotUtility()
plot.Stack(True)
plot.SetNormFactor(True)
plot.Logy(False)
plot.SetLumi(lumi)
plot.Ratio(True)
plot.SetCut("nSelectedJet>=2 && nSelectedJet<=4 && jet1Pt>250 && jet2Pt>250 && puppimet<50 && AK8fj1_pt>470")
#plot.SetCut("nSelectedJet==2 && jet1Pt>250 && jet2Pt>250 && jet1Btag<0.89 && jet2Btag<0.89 && puppimet<50 && AK8fj1_pt<470")
plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.0f pb^{-1} (13 TeV)}{AK 0.8, p_{T}>470}}"%(lumi))
plot.SetMCWeight("%f*mcWeight"%(lumi))

processes = []
data = root.Process("Data",root.kData)
data.AddFile(baseDir+'Data.root')
processes.append(data)
ewk = root.Process('EWK',root.kDiboson)
ewk.AddFile(baseDir+'Diboson.root')
ewk.AddFile(baseDir+'ZtoQQ.root')
ewk.AddFile(baseDir+'WtoQQ.root')
processes.append(ewk)
top = root.Process('Top',root.kTTbar)
top.AddFile(baseDir+'TTbar.root')
top.AddFile(baseDir+'SingleTop.root')
processes.append(top)
qcd = root.Process("QCD",root.kQCD)
qcd.AddFile(baseDir+'QCD.root')
processes.append(qcd)

for p in processes:
  plot.AddProcess(p)

#plot.AddDistribution(root.Distribution("nSelectedAK8fj",0,5,5,"n_{AK8 jets}","arb. units"))
#plot.AddDistribution(root.Distribution("nSelectedAK8fj",0,5,5,"n_{AK8 jets}","arb. units"))
plot.AddDistribution(root.Distribution("AK8fj1_pt",470,1000,50,"leading AK8 p_{T}","arb. units"))
'''
plot.AddDistribution(root.Distribution("AK8fj1_tau3/AK8fj1_tau2",0.5,1,50,"leading AK8 #tau_{3}/#tau_{2}","arb. units"))
plot.AddDistribution(root.Distribution("AK8fj1_mSD",0,250,50,"Leading AK8 m_{SD}","arb. units"))
plot.AddDistribution(root.Distribution("AK8fj1_QGTag",0,1,50,"Leading AK8 QGTag","arb. units"))
plot.AddDistribution(root.Distribution("AK8fj1_QjetVol",0,2,50,"Leading AK8 QjetVol","arb. units"))
plot.AddDistribution(root.Distribution("AK8fj1_logchi",-25,0,15,"Leading AK8 log #chi","arb. units"))
plot.AddDistribution(root.Distribution("AK8fj1_sjqgtag0",0,1,50,"Leading AK8 Subjet 0 QGTag","arb. units"))
plot.AddDistribution(root.Distribution("AK8fj1_sjqgtag1",0,1,50,"Leading AK8 Subjet 1 QGTag","arb. units"))
plot.AddDistribution(root.Distribution("AK8fj1_groomedIso",0,1,50,"Leading AK8 Subjet 1 QGTag","arb. units"))
dummy = root.Distribution("1",0,2,1,"dummy","dummy")
plot.AddDistribution(dummy)
'''
plot.DrawAll(baseDir+'/figs/')
system('genWebViewer %s/figs/'%(baseDir))
