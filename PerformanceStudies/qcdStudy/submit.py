#!/usr/bin/env python

from os import system
from time import sleep
from sys import exit

notFirst = False

cfgName='dijet25'

def submit(l):
  print "submitting",l
  global notFirst
  if notFirst:
    sleep(2) # make sure nothing else is running
  notFirst=True
  condorJDLString = '''Executable  = run_skim.sh
Universe  = vanilla
requirements            = UidDomain == "mit.edu" && Arch == "X86_64" && OpSysAndVer == "SL6"
Error = /scratch5/snarayan/condor/logs/%i.err
Output  = /scratch5/snarayan/condor/logs/%i.out
Log = /scratch5/snarayan/condor/logs/%i.log
Arguments = "%i %s"
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
GetEnv = True
accounting_group = group_t3mit
Queue 1'''%(l,l,l,l,cfgName)
  with open('condor.jdl','w') as jdlFile:
    jdlFile.write(condorJDLString)
  system('condor_submit condor.jdl')

counter=0
with open("../../config/%s.cfg"%(cfgName)) as cfgFile:
  for line in cfgFile:
    if len(line)>0:
      print line
#      if not(counter==72):
#        counter+=1
#        continue
      submit(counter)
      counter+=1
