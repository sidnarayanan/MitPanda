#!/usr/bin/env python

from sys import argv
from ROOT import TCanvas,TFile,gStyle,TColor

import numpy as np
import MitPanda.Tools.SquarePlots as SP
from array import array

ncontours=999
stops = [0.,.5,1.]
red = [0.,1.,1.]
green = [0.,1.,0.]
blue = [1.,1.,0.]

s = array('d', stops)
r = array('d', red)
g = array('d', green)
b = array('d', blue)

npoints = len(s)
TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
gStyle.SetNumberContours(ncontours)

workDir = argv[1]

algo = argv[1]
ptlow = int(argv[2])
pthigh = int(argv[3])
etahigh = float(argv[4])
c1 = TCanvas()

plotter = SP.Plotter(workDir+'/Data.root',None,"events")
cut = "AK8fj1_pt<%i&&AK8fj1_pt>%i&&TMath::Abs(AK8fj1_eta)<%f"%(pthigh,ptlow,etahigh)
#varNames = ['mSD','mSDbm1','mTrimmed','mPruned','logchi','tau3/AK8fj1_tau2','groomedIso','QjetVol','QGTag','sjqgtag0','sjqgtag1','maxsubjetbtag','NNResponse']
varNames = [('mSD',(100,0,500)),
				('mSDbm1',(100,0,500)),
				('mTrimmed',(100,0,500)),
				('mPruned',(100,0,500)),
				('logchi',(100,-40,5)),
				('tau3/AK8fj1_tau2',(100,0,1)),
				('groomedIso',(100,0,1)),
				('QjetVol',(100,0,1)),
				('QGTag',(100,0,1)),
				('sjqgtag0',(100,0,1)),
				('sjqgtag1',(100,0,1)),
				('maxsubjetbtag',(100,0,1)),
				('NNResponse',(100,0,1))]
treeVarNames = [('AK8fj1_'+v[0],v[1]) for v in varNames]
plotter.customBins=True
hplot,C = plotter.makeCorrelationPlot(treeVarNames,treeVarNames,cut)
#hplot,M = plotter.makeCovariancePlot(['logchi','tau3/tau2','groomedIso','QjetVol','QGTag','sjqgtag0','sjqgtag1','sjqgtag2'],cut)
gStyle.SetPaintTextFormat("1.3f")
suffix = '_ptGT%.1fANDptLT%.1fANDabsetaLT%.1f'%(ptlow,pthigh,etahigh)
suffix = suffix.replace('.','p')
#fin = TFile(workDir+'/figs/correlation'+suffix+'.root')
#hplot = fin.Get('c')
c1.cd()
c1.SetLeftMargin(0.2)
hplot.SetStats(0)
hplot.SetTitle('%i < p_{T} < %i'%(ptlow,pthigh))
hplot.SetMaximum(1)
hplot.SetMinimum(-1)
hplot.Draw("colz text")
xaxis = hplot.GetXaxis()
yaxis = hplot.GetYaxis()
for iV in xrange(C.shape[0]):
  if varNames[iV][0].find("fj1")>=0:
    label = 'tau3/tau2'
  else:
    label = varNames[iV][0]
  xaxis.SetBinLabel(iV+1,label)
  yaxis.SetBinLabel(iV+1,label)

c1.SaveAs(workDir+'/figs/correlation'+suffix+'.png')
c1.SaveAs(workDir+'/figs/correlation'+suffix+'.pdf')

fout = TFile(workDir+'/figs/correlation'+suffix+'.root',"RECREATE")
fout.WriteTObject(hplot,'c')
fout.Close()
