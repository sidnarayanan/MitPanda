#include "TH1F.h"
#include "TH2F.h"
#include "TClonesArray.h"
#include "TLorentzVector.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "vector"
#include "TMath.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TCanvas.h"
#include <iostream>
#include <fstream>
#include <string>

#define NMAX 50


using namespace std;
using namespace TMath;

class sjpair {
  public:
    sjpair (float d, float m):
      mjj(m) { 
        dR = (d>0) ? d : 999;
      }
    ~sjpair() { }
    float dR;
    float mjj;
};

bool compsjpairs(sjpair p1, sjpair p2) {
  return p1.dR<p2.dR;
}

void processFile(TList*,TH2F*,TString,int,const char*);
double computePull(TLorentzVector *v1, TLorentzVector *v2);

double clean(double x, double def=-1) {
  if (!(x==x)) return def;
  else return x;  
}

void fillTaggingVars_noMVA(TString fileListPath="../config/signal.cfg", TString prefix="AK8fj",int isSignal=1, int offset=0, TString outPath = "") {
  if (outPath=="") { 
    outPath = "/tmp/test/";
    if (isSignal!=0)  outPath += "signal_";
    else  outPath += "qcd_";
    outPath += prefix;
    outPath += ".root";
  }
  
  string line;
  ifstream fileList(fileListPath.Data());
  fprintf(stderr,"%s\n",fileListPath.Data());

  if (fileList.is_open()) {
    TFile *fOut = new TFile(outPath,"RECREATE");
    TList *lOut = new TList();
    float minPt = (prefix!="AK8fj") ? 250 : 450;
    TH2F *hPartonPtEta = new TH2F("hPartonPtEta","hPartonPtEta",100,minPt,2000,100,-2.5,2.5);
    int counter=0;
    while (getline(fileList,line)) {
      if (counter<offset) {
        ++counter;
        continue;
      }
      else if (counter>=offset+20)
        break;
      processFile(lOut,hPartonPtEta,prefix,isSignal,line.c_str()); 
      ++counter;
    }
    TTree * mergedTree = TTree::MergeTrees(lOut);
    
    mergedTree->SetName("jets");
    fOut->WriteTObject(mergedTree,"jets");
    fOut->WriteTObject(hPartonPtEta,"partonPtEta");
    // fOut->WriteTObject(hPtEta,"ptEta");
    fOut->Close();
    delete fOut;
    TIter iNext(lOut);
    TTree *tNext;
    while ((tNext=(TTree*)iNext()))
      delete tNext;
    delete mergedTree;
    // hPartonPtEta->Delete();
  }
  return;
}


void processFile(TList *lFlat, TH2F *hPartons, TString prefix, int isSignal, const char* flatPath) {

  float totalWeight=-1,xsec=0;
  TObjArray *o = TString(flatPath).Tokenize("/");
  TString datasetName = ((TObjString*)o->At(8))->String();
  printf("dataset: %s\n",datasetName.Data());
  if (isSignal==1) {
    xsec=1;
    totalWeight = 1;
  } else {
    if (datasetName.Contains("170to300"))
      xsec = 117276;
    else if (datasetName.Contains("300to470"))
      xsec = 7823;
    else if (datasetName.Contains("470to600"))
      xsec = 648.2;
    else if (datasetName.Contains("600to800"))
      xsec = 186.9;
    else if (datasetName.Contains("800to1000"))
      xsec = 32.293;
    else if (datasetName.Contains("1000to1400"))
      xsec = 9.4183;
    else if (datasetName.Contains("1400to1800"))
      xsec = 0.84265;
    else if (datasetName.Contains("1800to2400"))
      xsec = 0.114943;
    else {
      fprintf(stderr,"COULD NOT FIND DATASET XSEC");
      exit(1);
    }
    TString jobName = ((TObjString*)o->At(4))->String();
    TFile *fHists = new TFile("/home/snarayan/cms/hist/"+jobName+"/t2mit/filefi/042/"+datasetName+"/hist.root");
    TH1D *hMCWeight = (TH1D*)fHists->Get("hDTotalMCWeight");
    totalWeight = hMCWeight->Integral();
    fHists->Close();
  }

  TFile *fFlat = new TFile(flatPath);
  TTree *tFlat = (TTree*)fFlat->FindObjectAny("events");
	gROOT->cd();
  TTree *tOut = new TTree("skimmed","skimmed");
  TString mcQuarkPrefix("mcQuark");

  float tau1[NMAX];
  float tau2[NMAX];
  float tau3[NMAX];
  float chi[NMAX];
  float tau1IVF[NMAX];
  float tau2IVF[NMAX];
  float tau3IVF[NMAX];
  float massSoftDrop[NMAX];
  float massPruned[NMAX];
  float massTrimmed[NMAX];
  float MassSDb0[NMAX];
  float MassSDb1[NMAX];
  float MassSDb2[NMAX];
  float MassSDbm1[NMAX];
  float tauDot[NMAX];
  float zRatio[NMAX];
  float maxSubjetBtag[NMAX];
  float submaxSubjetBtag[NMAX];
  float sjqgtag0[NMAX];
  float sjqgtag1[NMAX];
  float sjqgtag2[NMAX];
  float sjqgtag3[NMAX];
  int nSubjets[NMAX];
  float qjetVol[NMAX];
  float QGTag[NMAX];
  float FMin[NMAX];
  float MassCMSTT[NMAX];
  float MassHTT[NMAX];
  float ROpt[NMAX];
  float ROptCalc[NMAX];
  float GroomedTau1[NMAX];
  float GroomedTau2[NMAX];
  float GroomedTau3[NMAX];
  float JEC[NMAX];
  int mcQuarkIsHadronicTop[2*NMAX];
  float mcQuarkTopSize[2*NMAX];
  TClonesArray *fjMomentum = new TClonesArray("TLorentzVector",20);
  TClonesArray *sj0Momentum = new TClonesArray("TLorentzVector",20);
  TClonesArray *sj1Momentum = new TClonesArray("TLorentzVector",20);
  TClonesArray *sj2Momentum = new TClonesArray("TLorentzVector",20);
  TClonesArray *sj3Momentum = new TClonesArray("TLorentzVector",20);
  TClonesArray *hjMomentum = new TClonesArray("TLorentzVector",20); // huge jet used for computing jet isolation
  TClonesArray *fjSDMomentum = new TClonesArray("TLorentzVector",20);
  TClonesArray *mcQuarkMomentum = new TClonesArray("TLorentzVector",20);
  TClonesArray *genJetMomentum = new TClonesArray("TLorentzVector",20);

  tFlat->SetBranchAddress(prefix+"_Tau1",&tau1);
  tFlat->SetBranchAddress(prefix+"_Tau2",&tau2);
  tFlat->SetBranchAddress(prefix+"_Tau3",&tau3);
  tFlat->SetBranchAddress(prefix+"_GroomedTau1",&GroomedTau1);
  tFlat->SetBranchAddress(prefix+"_GroomedTau2",&GroomedTau2);
  tFlat->SetBranchAddress(prefix+"_GroomedTau3",&GroomedTau3);
  tFlat->SetBranchAddress(prefix+"_chi",&chi);
  tFlat->SetBranchAddress(prefix+"_Tau1IVF",&tau1IVF);
  tFlat->SetBranchAddress(prefix+"_Tau2IVF",&tau2IVF);
  tFlat->SetBranchAddress(prefix+"_Tau3IVF",&tau3IVF);
  tFlat->SetBranchAddress(prefix+"_tauDot",&tauDot);
  tFlat->SetBranchAddress(prefix+"_QGTag",&QGTag);
  tFlat->SetBranchAddress(prefix+"_QJetVol",&qjetVol);
  tFlat->SetBranchAddress(prefix+"_MassSoftDrop",&massSoftDrop);
  tFlat->SetBranchAddress(prefix+"_MassPruned",&massPruned);
  tFlat->SetBranchAddress(prefix+"_MassTrimmed",&massTrimmed);
  tFlat->SetBranchAddress(prefix+"_MassSDb0",&MassSDb0);
  tFlat->SetBranchAddress(prefix+"_MassSDb1",&MassSDb1);
  tFlat->SetBranchAddress(prefix+"_MassSDb2",&MassSDb2);
  tFlat->SetBranchAddress(prefix+"_MassSDbm1",&MassSDbm1);
  tFlat->SetBranchAddress(prefix+"_nSubJets",&nSubjets);
  tFlat->SetBranchAddress(prefix+"_subJetBTag0",&maxSubjetBtag);
  tFlat->SetBranchAddress(prefix+"_subJetBTag1",&submaxSubjetBtag);
  tFlat->SetBranchAddress(prefix+"_Momentum",&fjMomentum);
  tFlat->SetBranchAddress(prefix+"_SubJet0Momentum",&sj0Momentum);
  tFlat->SetBranchAddress(prefix+"_SubJet1Momentum",&sj1Momentum);
  tFlat->SetBranchAddress(prefix+"_SubJet2Momentum",&sj2Momentum);
  tFlat->SetBranchAddress(prefix+"_SubJet3Momentum",&sj3Momentum);
  tFlat->SetBranchAddress(prefix+"_sjQGTag0",&sjqgtag0);
  tFlat->SetBranchAddress(prefix+"_sjQGTag1",&sjqgtag1);
  tFlat->SetBranchAddress(prefix+"_sjQGTag2",&sjqgtag2);
  tFlat->SetBranchAddress(prefix+"_sjQGTag3",&sjqgtag3);
  tFlat->SetBranchAddress(prefix+"_FMin",&FMin);
  tFlat->SetBranchAddress(prefix+"_ROpt",&ROpt);
  tFlat->SetBranchAddress(prefix+"_ROptCalc",&ROptCalc);
  tFlat->SetBranchAddress(prefix+"_MassHTT",&MassHTT);
  tFlat->SetBranchAddress(prefix+"_MassCMSTT",&MassCMSTT);
  tFlat->SetBranchAddress(prefix+"_JEC",&JEC);
  if (prefix=="AK8fj")
    tFlat->SetBranchAddress("CA15fj_Momentum",&hjMomentum);
  tFlat->SetBranchAddress(prefix+"_SoftDropMomentum",&fjSDMomentum);
  if (isSignal==0) {
  	tFlat->SetBranchAddress("genJet_Momentum",&genJetMomentum);
  }
  else {
      tFlat->SetBranchAddress("mcQuark_IsHadronicTop",&mcQuarkIsHadronicTop);
      tFlat->SetBranchAddress("mcQuark_TopSize",&mcQuarkTopSize);
      tFlat->SetBranchAddress(mcQuarkPrefix+"_Momentum",&mcQuarkMomentum);
  }

  float nn_tau32;

  float out_Pt;
  float out_Eta;
  float out_groomedIso;
  float out_telescopingIso;
  float out_tau1;
  float out_tau2;
  float out_tau3;
  float out_logchi;
  float out_tau1IVF;
  float out_tau2IVF;
  float out_tau3IVF;
  float out_tauDot;
  float out_zRatio;
  float out_massSoftDrop;
  float out_massPruned;
  float out_massTrimmed;
  float out_MassSDb0;
  float out_MassSDb1;
  float out_MassSDb2;
  float out_MassSDbm1;
  int out_nSubjets;
  float out_maxSubjetBtag;
  float out_submaxSubjetBtag;
  float out_QjetVol;
  float out_QGTag;
  float out_NNResponse=-1;
  float out_sjqgtag0;
  float out_sjqgtag1;
  float out_sjqgtag2;
  float out_sjqgtag3;
  float out_weight;

  tOut->Branch("eta",&out_Eta,"eta/F");
  tOut->Branch("pt",&out_Pt,"pt/F");
  tOut->Branch("groomedIso",&out_groomedIso,"groomedIso/F");
  if (prefix=="AK8fj")
    tOut->Branch("telescopingIso",&out_telescopingIso,"telescopingIso/F");
  tOut->Branch("logchi",&out_logchi,"logchi/F");
  tOut->Branch("tau1",&out_tau1,"tau1/F");
  tOut->Branch("tau2",&out_tau2,"tau2/F");
  tOut->Branch("tau3",&out_tau3,"tau3/F");
  tOut->Branch("tau1IVF",&out_tau1IVF,"tau1IVF/F");
  tOut->Branch("tau2IVF",&out_tau2IVF,"tau2IVF/F");
  tOut->Branch("tau3IVF",&out_tau3IVF,"tau3IVF/F");
  tOut->Branch("tauDot",&out_tauDot,"tauDot/F");
  tOut->Branch("zRatio",&out_zRatio,"zRatio/F");
  tOut->Branch("massSoftDrop",&out_massSoftDrop,"massSoftDrop/F");
  tOut->Branch("massPruned",&out_massPruned,"massPruned/F");
  tOut->Branch("massTrimmed",&out_massTrimmed,"massTrimmed/F");
  tOut->Branch("MassSDb0",&out_MassSDb0,"MassSDb0/F");
  tOut->Branch("MassSDb1",&out_MassSDb1,"MassSDb1/F");
  tOut->Branch("MassSDb2",&out_MassSDb2,"MassSDb2/F");
  tOut->Branch("MassSDbm1",&out_MassSDbm1,"MassSDbm1/F");
  tOut->Branch("nSubjets",&out_nSubjets,"nSubjets/I");
  tOut->Branch("maxSubjetBtag",&out_maxSubjetBtag,"maxSubjetBtag/F");
  tOut->Branch("submaxSubjetBtag",&out_submaxSubjetBtag,"submaxSubjetBtag/F");
  tOut->Branch("QjetVol",&out_QjetVol,"QjetVol/F");
  tOut->Branch("QGTag",&out_QGTag,"QGTag/F");
  tOut->Branch("sjqgtag0",&out_sjqgtag0,"sjqgtag0/F"); 
  tOut->Branch("sjqgtag1",&out_sjqgtag1,"sjqgtag1/F"); 
  tOut->Branch("sjqgtag2",&out_sjqgtag2,"sjqgtag2/F"); 
  tOut->Branch("sjqgtag3",&out_sjqgtag3,"sjqgtag3/F"); 
  tOut->Branch("NNResponse",&out_NNResponse,"NNResponse/F");
  tOut->Branch("mcWeight",&out_weight,"mcWeight/F");
  
  float groomedTau1=0; tOut->Branch("groomedTau1",&groomedTau1,"groomedTau1/F");
  float groomedTau2=0; tOut->Branch("groomedTau2",&groomedTau2,"groomedTau2/F");
  float groomedTau3=0; tOut->Branch("groomedTau3",&groomedTau3,"groomedTau3/F");
  float fMin=0; tOut->Branch("fMin",&fMin,"fMin/F");
  float deltaROpt=0; tOut->Branch("deltaROpt",&deltaROpt,"deltaROpt/F");
  float massHTT=0; tOut->Branch("massHTT",&massHTT,"massHTT/F");
  float massCMSTT=0; tOut->Branch("massCMSTT",&massCMSTT,"massCMSTT/F");
  float jec=0; tOut->Branch("jec",&jec,"jec/F");
  float sj0dR=0; tOut->Branch("sj0dR",&sj0dR,"sj0dR/F"); 
  float sj1dR=0; tOut->Branch("sj1dR",&sj1dR,"sj1dR/F"); 
  float sj2dR=0; tOut->Branch("sj2dR",&sj2dR,"sj2dR/F"); 
  float sj3dR=0; tOut->Branch("sj3dR",&sj3dR,"sj3dR/F"); 
  float sj4dR=0; tOut->Branch("sj4dR",&sj4dR,"sj4dR/F"); 
  float sj5dR=0; tOut->Branch("sj5dR",&sj5dR,"sj5dR/F"); 
  float sj0mjj=0; tOut->Branch("sj0mjj",&sj0mjj,"sj0mjj/F"); 
  float sj1mjj=0; tOut->Branch("sj1mjj",&sj1mjj,"sj1mjj/F"); 
  float sj2mjj=0; tOut->Branch("sj2mjj",&sj2mjj,"sj2mjj/F"); 
  float sj3mjj=0; tOut->Branch("sj3mjj",&sj3mjj,"sj3mjj/F"); 
  float sj4mjj=0; tOut->Branch("sj4mjj",&sj4mjj,"sj4mjj/F"); 
  float sj5mjj=0; tOut->Branch("sj5mjj",&sj5mjj,"sj5mjj/F"); 
  
  unsigned int nFlat = tFlat->GetEntries();
  int nJets;
  int nHugeJets;
  int nQuarks;
  int nBosons;
  int nGenJets;
  tFlat->SetBranchAddress(TString("_")+prefix+"Counter",&nJets);
  if (isSignal!=0) {
      tFlat->SetBranchAddress(TString("_")+mcQuarkPrefix+"Counter",&nQuarks);
  } else {
  		tFlat->SetBranchAddress("_genJetCounter",&nGenJets);
  }
  if (prefix=="AK8fj")
    tFlat->SetBranchAddress("_CA15fjCounter",&nHugeJets);

  TLorentzVector *fjMom, *fjSDMom, *mcMom, *hjMom, *sj0Mom, *sj1Mom, *sj2Mom, *sj3Mom;

  float R0=0.6;  // for matching
  float topSize = 0.6;
  float minPt = 450;
  float maxPt = 3000;
  if (prefix=="CA15fj") {
  	R0 = 1.2;
  	topSize = 0.8;
  	minPt = 250;
    maxPt = 470;
  }
  topSize *= topSize; // I compute dR2 for speed, hard-code dR 


  int nTotal=0;
  int nAccepted=0;
  out_NNResponse = -1;

  
  std::vector<int>goodPartonIndices;
  for (unsigned int i=0; i<nFlat; ++i) {
    tFlat->GetEntry(i);

    out_weight = xsec/totalWeight; // only using LO samples for training
    goodPartonIndices.clear();
    if (isSignal!=0) {
    	for (int iQuark=0; iQuark!=nQuarks; ++iQuark) {
        mcMom = (TLorentzVector*)(*mcQuarkMomentum)[iQuark];
        if (mcQuarkIsHadronicTop[iQuark]==1  && mcQuarkTopSize[iQuark]<topSize) {
    			goodPartonIndices.push_back(iQuark);
    			hPartons->Fill(mcMom->Pt(),mcMom->Eta());
      	} 
      }
    } else {
    	for (int iGenJet=0; iGenJet!=nGenJets; ++iGenJet) {
        mcMom = (TLorentzVector*)(*genJetMomentum)[iGenJet];
  			goodPartonIndices.push_back(iGenJet);
  			hPartons->Fill(mcMom->Pt(),mcMom->Eta());
    	} 
    }
    
//    nTotal += nJets;
    for (int iJet=0; iJet<nJets; ++iJet) {
      if ( (!isSignal) && iJet>0)
        break; // let's only take the leading jet
      ++nTotal;
      fjMom = (TLorentzVector*)(*fjMomentum)[iJet];
      if (fjMom->Pt()<minPt || fjMom->Pt()>maxPt) continue;
      if (TMath::Abs(fjMom->Eta())>2.5) continue;
      fjSDMom = (TLorentzVector*)(*fjSDMomentum)[iJet];
      sj0Mom = (TLorentzVector*)(*sj0Momentum)[iJet];
      sj1Mom = (TLorentzVector*)(*sj1Momentum)[iJet];
      sj2Mom = (TLorentzVector*)(*sj2Momentum)[iJet];
      sj3Mom = (TLorentzVector*)(*sj3Momentum)[iJet];

      if (isSignal!=0) {
        Bool_t foundMCTop = kFALSE;
        for (int iQuark : goodPartonIndices) {
          mcMom = (TLorentzVector*)(*mcQuarkMomentum)[iQuark];
          if (mcMom->DeltaR(*fjMom) < R0) {
            foundMCTop = kTRUE;
          }
          if(foundMCTop) break;
        }
        if (!foundMCTop) continue;
      } else {
        Bool_t foundMCParton = kFALSE;
        for (int iGenJet : goodPartonIndices) {
          if (!foundMCParton) {
            mcMom = (TLorentzVector*)(*genJetMomentum)[iGenJet];
            if (mcMom->DeltaR(*fjMom) < R0) {
              if (TMath::Abs(mcMom->Pt()-fjMom->Pt())/fjMom->Pt() < 0.1) //require no more than a 10% pT diff to kill soft genjets
                foundMCParton = kTRUE;
            }
          }
          if(foundMCParton) break;
        }
        if (!foundMCParton) continue; 
      }
      ++nAccepted;

      // kinematics
      out_Pt = fjMom->Pt();
      out_Eta = fjMom->Eta();

      // SUBSTRUCTURE
      // qjet vol
      out_QjetVol = clean(qjetVol[iJet]);
      if (out_QjetVol>10) {
        out_QjetVol = 10;
      } else if (out_QjetVol<0) {
        out_QjetVol = -1; 
      }
      
      // isolation
      out_groomedIso = computePull(fjMom,fjSDMom);

      // shower deco
      if (chi[iJet]>0) {
        out_logchi = TMath::Log(chi[iJet]);
        if (out_logchi>15)
          out_logchi = 15;
      } else {
        out_logchi = -30;
      }

      // njettiness
      out_tau1 = clean(tau1[iJet]);
      out_tau2 = clean(tau2[iJet]);
      out_tau3 = clean(tau3[iJet]);
      out_tau1IVF = clean(tau1IVF[iJet]);
      out_tau2IVF = clean(tau2IVF[iJet]);
      out_tau3IVF = clean(tau3IVF[iJet]);
      out_tauDot = clean(tauDot[iJet]);
      out_zRatio = clean(zRatio[iJet]);

      // groomed masses
      out_massSoftDrop = clean(massSoftDrop[iJet]);
      out_massTrimmed = clean(massTrimmed[iJet]);
      out_massPruned = clean(massPruned[iJet]);
      out_MassSDb0 = clean(MassSDb0[iJet]);
      out_MassSDb1 = clean(MassSDb1[iJet]);
      out_MassSDb2 = clean(MassSDb2[iJet]);
      out_MassSDbm1 = clean(MassSDbm1[iJet]);

      // SUBJETS
      // n and btagging
      out_nSubjets = clean(nSubjets[iJet]);
      out_maxSubjetBtag = clean(maxSubjetBtag[iJet]);
      out_submaxSubjetBtag = clean(submaxSubjetBtag[iJet]);
      
      //load qg tags and sort
      std::vector<float> qgtags;
      qgtags.push_back(clean(sjqgtag0[iJet])); qgtags.push_back(clean(sjqgtag1[iJet]));
      qgtags.push_back(clean(sjqgtag2[iJet])); qgtags.push_back(clean(sjqgtag3[iJet]));
      std::sort(qgtags.begin(),qgtags.end());
      out_sjqgtag3 = qgtags[0];
      out_sjqgtag2 = qgtags[1];
      out_sjqgtag1 = qgtags[2];
      out_sjqgtag0 = qgtags[3];

      // compute subjet pair variables: dR and mjj, and sort
      sj0dR = -1; sj0mjj = -1;
      sj1dR = -1; sj1mjj = -1;
      sj2dR = -1; sj2mjj = -1;
      sj3dR = -1; sj3mjj = -1;
      sj4dR = -1; sj4mjj = -1;
      sj5dR = -1; sj5mjj = -1;
      if (out_nSubjets>1) {
        sj0dR = sj0Mom->DeltaR(sj1Mom);  sj0mjj = (*sj0Mom+*sj1Mom).M();
        if (out_nSubjets>2) {
          sj1dR = sj0Mom->DeltaR(sj2Mom);  sj1mjj = (*sj0Mom+*sj2Mom).M();
          sj2dR = sj1Mom->DeltaR(sj2Mom);  sj2mjj = (*sj1Mom+*sj2Mom).M();
          if (out_nSubjets>3) {
            sj3dR = sj0Mom->DeltaR(sj3Mom);  sj3mjj = (*sj0Mom+*sj3Mom).M();
            sj4dR = sj1Mom->DeltaR(sj3Mom);  sj4mjj = (*sj1Mom+*sj3Mom).M();
            sj5dR = sj2Mom->DeltaR(sj3Mom);  sj5mjj = (*sj2Mom+*sj3Mom).M();
          }
        }
      }
      std::vector<sjpair> sjpairs;
      sjpairs.push_back(sjpair(clean(sj0dR),clean(sj0mjj)));
      sjpairs.push_back(sjpair(clean(sj1dR),clean(sj1mjj)));
      sjpairs.push_back(sjpair(clean(sj2dR),clean(sj2mjj)));
      sjpairs.push_back(sjpair(clean(sj3dR),clean(sj3mjj)));
      sjpairs.push_back(sjpair(clean(sj4dR),clean(sj4mjj)));
      sjpairs.push_back(sjpair(clean(sj5dR),clean(sj5mjj)));
      std::sort(sjpairs.begin(),sjpairs.end(),compsjpairs);
      sj0dR = sjpairs[0].dR; sj0mjj = sjpairs[0].mjj;
      sj1dR = sjpairs[1].dR; sj1mjj = sjpairs[1].mjj;
      sj2dR = sjpairs[2].dR; sj2mjj = sjpairs[2].mjj;
      sj3dR = sjpairs[3].dR; sj3mjj = sjpairs[3].mjj;
      sj4dR = sjpairs[4].dR; sj4mjj = sjpairs[4].mjj;
      sj5dR = sjpairs[5].dR; sj5mjj = sjpairs[5].mjj;

      // CMS and HTT
      massCMSTT = clean(MassCMSTT[iJet]);
      massHTT = clean(MassHTT[iJet]);
      fMin = clean(FMin[iJet]);
      deltaROpt = clean(ROpt[iJet]-ROptCalc[iJet]);

      // groomed njettiness
      groomedTau1 = clean(GroomedTau1[iJet]);
      groomedTau2 = clean(GroomedTau2[iJet]);
      groomedTau3 = clean(GroomedTau3[iJet]);

      // jec
      jec = clean(JEC[iJet]);
      
      out_QGTag = clean(QGTag[iJet]);

      //compute telescoping pull mag
      /*
      if (prefix=="AK8fj") {
        float bestDR = 999;
        TLorentzVector *closestHugeJet = 0;
        for (int iHuge=0; iHuge<nHugeJets; ++iHuge) {
          hjMom = (TLorentzVector*)(*hjMomentum)[iJet];
          if (hjMom->Pt()<250) continue;
          float dR = hjMom->DeltaR(*fjMom);
          if(dR<bestDR) {
            closestHugeJet = hjMom;
            bestDR = dR;
          }
        }
        if (bestDR < 1.5) {
          out_telescopingIso = computePull(fjMom,closestHugeJet);
        } else {
          out_telescopingIso = 10;
        }
      }
      */

      out_NNResponse = -1;
      
      tOut->Fill();
    }
  }
  fprintf(stderr,"accepted %i/%i\n",nAccepted,nTotal);
  lFlat->Add(tOut);
  delete fjMomentum;
  delete fjSDMomentum;
  delete genJetMomentum;
  delete mcQuarkMomentum;
  delete hjMomentum;
  fFlat->Close();
  return;
}

inline double computePull(TLorentzVector *v1, TLorentzVector *v2) {

  float pull = Sqrt( Power(v1->Px()-v2->Px(),2)+
			               Power(v1->Py()-v2->Py(),2)+
			               Power(v1->Pz()-v2->Pz(),2)  )/v1->Pt();
  if (pull>=0)
    return pull;
  else {
    return -1;
  }
}
