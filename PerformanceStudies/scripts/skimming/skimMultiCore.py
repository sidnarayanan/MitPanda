#!/usr/bin/env python
from MitPanda.Tools.MultiThreading import GenericRunner
from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv
from os import path,getenv,system
from json import load as loadJson


if __name__ == "__main__":
  
  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/TopTagSkimmer.h")
  gSystem.Load('libMitPandaSkimmers.so')
 
  def fn(shortName,longName,counter,xsec,isSignal):
    label = 'signal' if isSignal else 'qcd'
    fullPath = '/home/snarayan/cms/hist/ca15_'+label+'/t2mit/filefi/042/'+longName
    outputfile = '/home/snarayan/local/ca15/split/%s_%i.root'%(shortName,counter)
    #outputfile = '/home/snarayan/cms/root/ca15_'+label+'/split/%s_%i.root'%(shortName,counter)
    #if path.isfile(outputfile):
    #  return
    histPath = sub(r'[0-9]*_flat.root','hist.root',fullPath)
    fhist = root.TFile(histPath)
    totalWeight = fhist.Get('hDTotalMCWeight').Integral()

    skimmer = root.TopTagSkimmer()
    skimmer.xsec = xsec/totalWeight
    skimmer.isSignal = isSignal;
    skimmer.Init(fullPath,outputfile)
    skimmer.Run()
    skimmer.Terminate()

  gr = GenericRunner(fn)
  argList = []
  counter=0
  cfg = open('../../config/%s.cfg'%(argv[1]))
  for line in cfg:
    ll = line.split()
    isSignal = ('Zprime' in ll[1])
    argList.append([ll[0],ll[1],counter,float(ll[3]),isSignal])
    counter+=1
  gr.setArgList(argList)
  gr.run(8)
