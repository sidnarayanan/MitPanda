#!/usr/bin/env python

from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv
from os import system
from json import load as loadJson

nPerJob = int(argv[2])

if __name__ == "__main__":
  
  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/TopTagSkimmer.h")
  gSystem.Load('libMitPandaSkimmers.so')

  def fn(shortName,longName,counter,xsec,isSignal):
    label = 'signal' if isSignal else 'qcd'
    fullPath = '/home/snarayan/cms/hist/ca15_'+label+'/t2mit/filefi/042/'+longName
    histPath = sub(r'[0-9]*_flat.root','hist.root',fullPath)
    fhist = root.TFile(histPath)
    totalWeight = fhist.Get('hDTotalMCWeight').Integral()
    
    skimmer = root.TopTagSkimmer()
    skimmer.xsec = xsec/totalWeight
    skimmer.isSignal = isSignal;

    system('cp %s flat.root'%(fullPath))
    skimmer.Init('flat.root','%s_%i.root'%(shortName,counter));
    skimmer.Run()
    skimmer.Terminate()
  
  which = int(argv[1])
  counter=0
  cfg = open('local.cfg')
  for line in cfg:
    if counter>=which*nPerJob and counter<(which+1)*nPerJob:
      ll = line.split()
      isSignal = ('Zprime' in ll[1])
      print ll[1]
      fn(ll[0],ll[1],counter,float(ll[3]),isSignal)
    counter+=1


