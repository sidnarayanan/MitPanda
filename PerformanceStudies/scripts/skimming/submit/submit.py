#!/usr/bin/env python

from os import system
from time import sleep
from sys import exit,stdout

notFirst = False

cfgName='nero'

def submit(l):
  print "submitting",l
  global notFirst
  if notFirst:
    sleep(0.1) # make sure nothing else is running
  notFirst=True
  condorJDLString = '''Executable  = run_skim.sh
Universe  = vanilla
requirements            = UidDomain == "mit.edu" && Arch == "X86_64" && OpSysAndVer == "SL6"
Error = /scratch5/snarayan/condor/logs/%i_b.err
Output  = /scratch5/snarayan/condor/logs/%i_b.out
Log = /scratch5/snarayan/condor/logs/%i_b.log
Arguments = "%i"
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
GetEnv = True
accounting_group = group_t3mit.urgent
Queue 1'''%(l,l,l,l)
  with open('condor.jdl','w') as jdlFile:
    jdlFile.write(condorJDLString)
  system('condor_submit condor.jdl')

counter=0
with open("../../../config/%s.cfg"%(cfgName)) as cfgFile:
  for line in cfgFile:
    if len(line)>0:
#      if not(line.find('TTJets')>=0):
      if False:
        counter+=1
        continue
      stdout.write(line)
      submit(counter)
      counter+=1
