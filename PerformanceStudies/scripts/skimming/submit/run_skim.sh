#!/bin/bash
label=$1
cfgName=$2
nPerJob=$3
pwd

#executable=skimSingleCoreRemote.py
executable=skimSingleCore.py

scramdir=/home/snarayan/cms/cmssw/042/CMSSW_7_4_15/
cd ${scramdir}/src/
eval `scramv1 runtime -sh`
cd -

cp ${scramdir}/src/MitPanda/PerformanceStudies/config/${cfgName}.cfg local.cfg
cp ${scramdir}/src/MitPanda/PerformanceStudies/scripts/skimming/$executable .

python $executable $label $nPerJob

rm flat.root
cp *.root /home/snarayan/cms/root/ca15_signal/condor/
rm *root *.py

exit $status
