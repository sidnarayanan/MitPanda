#!/usr/bin/env python

from os import system
from sys import exit,stdout

#cfgName='mine_topup'
cfgName='qcd_new'
nPerJob = 10

def submit(l):
  print "submitting",l
  condorJDLString = '''Executable  = run_skim.sh
Universe  = vanilla
requirements            = UidDomain == "mit.edu" && Arch == "X86_64" && OpSysAndVer == "SL6"
Error = /scratch/snarayan/condor/logs/%s_$(Process).err
Output  = /scratch/snarayan/condor/logs/%s_$(Process).out
Log = /scratch/snarayan/condor/logs/%s_$(Process).log
Arguments = "$(Process) %s %i"
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
GetEnv = True
accounting_group = group_cmsuser.snarayan
Queue %i'''%(cfgName,cfgName,cfgName,cfgName,nPerJob,l)
  with open('condor.jdl','w') as jdlFile:
    jdlFile.write(condorJDLString)
  system('condor_submit condor.jdl')

with open("../../../config/%s.cfg"%(cfgName)) as cfgFile:
  nJobs = len(list(cfgFile))/nPerJob+1
  submit(nJobs)
