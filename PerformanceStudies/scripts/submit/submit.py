#!/usr/bin/env python

from os import system
from time import sleep
from sys import argv

algo = argv[2]

def submit(l):
  global notFirst
  with open("../../config/"+l+".cfg") as cfgFile:
    nFiles = -1
    for nFiles,line in enumerate(cfgFile):
      pass
    nFiles += 1
  nPerJob = 5
  nJobs = int(1.*nFiles/nPerJob)+1
  print "submitting",l,nJobs
  condorJDLString = '''Executable  = run_skim.sh
Universe  = vanilla
requirements            = UidDomain == "mit.edu" && Arch == "X86_64" && OpSysAndVer == "SL6"
Error = /scratch/snarayan/condor/logs/{0}_{1}_$(Process).err
Output  = /scratch/snarayan/condor/logs/{0}_{1}_$(Process).out
Log = /scratch/snarayan/condor/logs/{0}_{1}_$(Process).log
Input = /dev/null
GetEnv = True
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
Arguments = "{0} {1} $(Process)"
accounting_group = group_t3mit
Queue {2}'''.format(l,algo,nJobs)
  with open('condor.jdl','w') as jdlFile:
    jdlFile.write(condorJDLString)
  system('condor_submit condor.jdl')

submit(argv[1])
