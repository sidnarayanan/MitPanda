#!/bin/bash
label=$1
algo=$2
num=$3
echo -n "pwd: "
pwd

isSig=1
if [ $label == "qcd" ]; then
  isSig=0
fi

scramdir=/home/snarayan/cms/cmssw/042/CMSSW_7_4_15/
cd ${scramdir}/src/
eval `scramv1 runtime -sh`
cd -

cp ${scramdir}/src/MitPanda/PerformanceStudies/scripts/fillTaggingVars.C .
cp ${scramdir}/src/MitPanda/PerformanceStudies/scripts/rootLogon.C .

root -b -l -q rootLogon.C fillTaggingVars.C+\("\"${scramdir}/src/MitPanda/PerformanceStudies/config/${label}.cfg\",\"${algo}fj\",${isSig},${num},\"${label}_${num}.root\""\)

ls
cp ${label}_${num}.root /home/snarayan/cms/root/topTagging_${algo}/split/${label}_${num}.root
rm * 

pwd
