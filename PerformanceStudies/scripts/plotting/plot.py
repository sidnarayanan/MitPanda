#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv
tcut = root.TCut
### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')
### HELPER FUNCTIONS ###
def setBins(dist,bins):
  for b in bins:
    dist.AddBinEdge(b)

def contains(s1,s2):
  if type(s2)==type(""):
    return s1.find(s2)>=0
  else:
    for s in s2:
      if s1.find(s)>=0:
        return True
    return False

def tAND(s1,s2):
  return "(("+s1+")&&("+s2+"))"

def tOR(s1,s2):
  return "(("+s1+")||("+s2+"))"

def tTIMES(w,s):
  return "("+w+")*("+s+")"

### SET GLOBAL VARIABLES ###
#region = argv[1]

makePlots = True
#baseDir = '/home/snarayan/local/BaconSkim/MonoXSelection/baconbits/ntuples/'
baseDir = '/home/snarayan/local/ca15/'
lumi = 2.26

### DEFINiE REGIONS ###
#cut = 'mSD>110 && mSD<210 && tau3/tau2<0.7'
cut = 'mSD>110 && mSD<210'
#cut = ''

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(False)
#plot.Logy(True)
#plot.DrawMCErrors(True)
#plot.Ratio(True)
plot.SetNormFactor(True)
plot.SetLumi(lumi)
plot.SetCut(tcut(cut))
plot.SetMCWeight(tcut("mcweight"))
#plot.CloneTrees(True) # doesn't work if friends are added
### DEFINE PROCESSES ###
signal = root.Process('Signal',root.kExtra1)
qcd = root.Process('QCD',root.kQCD)
processes = [qcd,signal]

for p in processes:
  p.Init("events")

for p in processes:
  p.AddFriend('windowedNN')
  p.AddFriend('nowindowedNN')

### ASSIGN FILES TO PROCESSES ###
signal.AddFile(baseDir+'zprime.root')
qcd.AddFile(baseDir+'qcd.root')

for p in processes:
  plot.AddProcess(p)


### CHOOSE DISTRIBUTIONS, LABELS ###
plot.SetTDRStyle()
plot.AddCMSLabel()
plot.AddPlotLabel('110 < m_{SD} < 210 GeV',.16,.8,False,42,.04)

#plot.AddDistribution(root.Distribution("(iotaM0+iotaM1+iotaM2+iotaM3+iotaM4+iotaM5+iotaM6+iotaM7+iotaM8+iotaM9-10*mSD)/10",-50,300,40,"average #Delta_{T} M","Events","averageDeltaM"))
#plot.AddDistribution(root.Distribution("iotaM0-mSD",-200,200,40,"#Delta_{T} M_{0}","Events","DeltaM0"))
#plot.AddDistribution(root.Distribution("iotaM9-mSD",-50,500,40,"#Delta_{T} M_{9}","Events","DeltaM9"))
#plot.AddDistribution(root.Distribution("iotaM9-iotaM0",-50,300,40,"M9-M0","Events","M9-M0"))
#plot.AddDistribution(root.Distribution("iotaDR0",0,.8,40,"#Delta R_0","Events","DeltaR0"))
#plot.AddDistribution(root.Distribution("iotaDR9",0,.8,40,"#Delta R_0","Events","DeltaR9"))
#plot.AddDistribution(root.Distribution("iotaRMS",0,200,40,"p_{T}^{RMS,T}","Events","iotaRMS"))
#plot.AddDistribution(root.Distribution("iotaDRRMS",0,200,40,"#DeltaR^{RMS,T}","Events","iotaDRRMS"))
plot.AddDistribution(root.Distribution("iotaMRMS",0,200,40,"M^{RMS,T}","Events","iotaMRMS"))
plot.AddDistribution(root.Distribution("sumQG_sumQG0",-1,1,40,"min Sum QG","Events"))
#plot.AddDistribution(root.Distribution("sumQGMin",0,2.2,40,"min sum QG","Events"))
plot.AddDistribution(root.Distribution("dR_dR0",0,1.5,40,"min #DeltaR","Events"))
#plot.AddDistribution(root.Distribution("nSubjets",0,5,5,"nSubjets","Events"))
plot.AddDistribution(root.Distribution("mW_dR0",0,200,40,"m_{W} (min #DeltaR)","Events"))
plot.AddDistribution(root.Distribution("mW_sumQG0",0,200,40,"m_{W} (min sum QG)","Events"))
#plot.AddDistribution(root.Distribution("d12",0,80,40,"d_{12}","Events"))
#plot.AddDistribution(root.Distribution("d23",0,80,40,"d_{23}","Events"))
#plot.AddDistribution(root.Distribution("dij_d0",0,80,40,"min d_{ij}","Events"))
plot.AddDistribution(root.Distribution("mSD",0,400,50,"m_{SD}","Events"))
#plot.AddDistribution(root.Distribution("pt",250,1250,100,"p_{T}","Events"))
#plot.AddDistribution(root.Distribution("2*TMath::Log(mSD/pt)",-10,2,100,"#rho","Events"))
plot.AddDistribution(root.Distribution("windowedNN",0,1,100,"NN response","Events"))
plot.AddDistribution(root.Distribution("nowindowedNN",0,1,100,"NN response","Events"))
plot.AddDistribution(root.Distribution("tau3/tau2",0,1.2,40,"#tau_{32}","Events"))
#plot.AddDistribution(root.Distribution("QjetVol",0,1.2,40,"Q-jet volatility","Events"))
plot.AddDistribution(root.Distribution("logchi",-30,10,40,"ln #chi","Events"))


#plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
### DRAW AND CATALOGUE ###
#plot.DrawAll(baseDir+'/figs/nomasscut/')
plot.DrawAll(baseDir+'/figs/')

