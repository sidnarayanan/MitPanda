#!/usr/bin/env python

from sys import argv
from os import system
import makeROC as ROC

if len(argv)==5:
  shortLabel = argv[1]
  ptLow = float(argv[2])
  ptHigh = float(argv[3])
  eta = float(argv[4])
else:
  shortLabel = ""
  ptLow = float(argv[1])
  ptHigh = float(argv[2])
  eta = float(argv[3])

basicCut = '_ptGT%.1fANDptLT%.1fANDabsetaLT%.1f'%(ptLow,ptHigh,eta)
basicCut = basicCut.replace('.','p')

#varNames = ['MassNNResponse','NNResponse','massSoftDrop','logchi','tau3Overtau2','QjetVol','QGTag','groomedIso','sjqgtag0','sjqgtag1','sjqgtag2']
varNames = ['WindowNNResponse','tau3Overtau2','logchi','QjetVol','groomedIso','QGTag','sjqgtag0']
#varNames = ['MassNNResponse','NNResponse','WindowNNResponse','PCANNResponse','tau3/tau2','logchi']
#cuts = [basicCut]
cuts = []
#cuts += [basicCut+'ANDmassSoftDropGT152ANDmassSoftDropLT192']
#cuts += [basicCut+'ANDmassSoftDropGT142ANDmassSoftDropLT202']
cuts += [basicCut+'ANDmassSoftDropGT150ANDmassSoftDropLT240']

workDir="/home/snarayan/cms/root/topTagging%s/"%(shortLabel)
system('rm -f %s/figs/rocs.root'%(workDir))

for cut in cuts:
	cutString = cut if cut else ''
	for var in varNames:
		ROC.make("%s%s"%(var,cutString),workDir,ptLow,ptHigh,eta)
	ROC.stack('%s/figs/'%(workDir),cut,"%i < p_{T} < %i"%(ptLow,ptHigh))
