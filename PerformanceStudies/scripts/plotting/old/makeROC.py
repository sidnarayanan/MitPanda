#!/usr/bin/env python

from ROOT import TH1F,TGraph,TFile,TCanvas,TLegend,TMultiGraph,gDirectory,TLatex
from sys import argv,exit
from numpy import array,zeros,linspace,minimum

nBins = 1000
nPoints = 50
colors = [1,2,4,6,8]
nColors = len(colors)
styles = [1,2,3,4]

doLogy = True

def stack(plotsDir,cutName=None,title=None):
    fIn = TFile(plotsDir+'rocs.root',"UPDATE")
    global colors
    global styles
    c = TCanvas("c","c",1000,800)
    if doLogy:
      c.SetLogy(1)
    c.SetRightMargin(0.01)
    c.SetTopMargin(0.1)
    l = TLegend(0.7,.15,.99,.5)
    mg = TMultiGraph()
    fIn.cd()
    i=0
    for key in gDirectory.GetListOfKeys():
        if i==10 or i==5:
            i+=1
        keyType = key.GetTitle()
        if not(keyType=="Graph"):
            continue
        varName = key.GetName()
        if cutName and not(varName.find(cutName)>=0):
            continue
        if not(cutName) and (varName.find('_')>=0):
            continue
        g = fIn.Get(varName)
        g.SetLineColor(colors[i%nColors])
        if varName.find('NN')>=0:
            g.SetLineStyle(styles[0])
        else:
          g.SetLineStyle(styles[int(i/nColors)+1])
        g.SetLineWidth(2);
        l.AddEntry(g,varName.split('_')[0],"l")
        mg.Add(g)
        i+=1
    if doLogy:
      mg.SetMaximum(0.7)
      mg.SetMinimum(.0001)
    else:
      mg.SetMaximum(1.0)
      mg.SetMinimum(0.0)
    mg.Draw("AL")
    mg.GetXaxis().SetTitle("signal efficiency")
    mg.GetYaxis().SetTitle("background acceptance")
    mg.GetYaxis().SetTitleOffset(1.5)
    l.SetFillStyle(0)
    l.SetBorderSize(0);
    l.Draw()
    label = TLatex()
    if title is not None:
      label.DrawLatex(.15,.9,title)

    if cutName:
        fIn.WriteTObject(c,"stacked_"+cutName)
        c.SaveAs(plotsDir+'rocs_'+cutName+'.png')
        c.SaveAs(plotsDir+'rocs_'+cutName+'.pdf')
    else:
        fIn.WriteTObject(c,"stacked")
        c.SaveAs(plotsDir+'rocs.png')
        c.SaveAs(plotsDir+'rocs.pdf')


def smooth(effs,rejs):
    # smooth ROC curves that are choppy due to binning
    i=0
    while i<nPoints-1:
        if rejs[i]<rejs[i+1]:
            tmp = (rejs[i]+rejs[i+1])/2
            rejs[i] = tmp
            rejs[i+1] = tmp
            tmp = (effs[i]+effs[i+1])/2
            effs[i] = tmp
            effs[i+1] = tmp
            i += 2
        else:
            i+=1
    return effs,rejs

def checkMonotonicity(rejs):
    for a,b in zip(rejs[:-1],rejs[1:]):
        if a<=b:
            return False
    return True

def make(varName,workDir,ptCutLow=-1.,ptCutHigh=3000.,etaCut=2.4):
    outFile = TFile(workDir+"/figs/rocs.root","UPDATE")
#    sigFile = TFile(workDir+"/signal.root")
#    bgFile = TFile(workDir+"/qcd.root")
    
    ### construct histograms
    hBg = TH1F("hBg",varName,nBins,0,nBins)
    hSig = TH1F("hSig",varName,nBins,0,nBins)
    with open("%s/figs/%s.C"%(workDir,varName)) as srcFile:
        for line in srcFile:
            if line.find("SetBinContent")>=0:
                binIdx,val = line.strip().split('(')[-1].strip(');').split(',')
                binIdx = int(binIdx)
                val = float(val)
                # print line, binIdx, val
                if line.strip().split('-')[0][0:3]=="hBg":
                    hBg.SetBinContent(binIdx,val)
                elif line.strip().split('-')[0][0:4]=="hSig":
                    hSig.SetBinContent(binIdx,val)

    nQCDPartons = hBg.Integral()
    nTopPartons = hSig.Integral()

    ### draw ROC
    cut1s = zeros(nPoints)
    cut2s = zeros(nPoints)
    effs = linspace(0,1,nPoints)
    rejs = zeros(nPoints)
    for cut1 in xrange(1,nBins+1):
        if varName.find('NN')>=0 and False:
          # only scan 1 cut
          eff = hSig.Integral(cut1,nBins+1)/nTopPartons
          rej = hBg.Integral(cut1,nBins+1)/nTopPartons
          if rej>rejs[int(nPoints*eff)-1]:
               rejs[int(nPoints*eff)-1] = rej
               cut1s[int(nPoints*eff)-1] = cut1
        else:
          for cut2 in xrange(cut1,nBins+1):
              eff = hSig.Integral(cut1,cut2)/nTopPartons
              rej = 1-hBg.Integral(cut1,cut2)/nQCDPartons
              try:
                  if rej>rejs[int(nPoints*eff)-1]:
                      rejs[int(nPoints*eff)-1] = rej
                      cut1s[int(nPoints*eff)-1] = cut1
                      cut2s[int(nPoints*eff)-1] = cut2
              except IndexError:
                  print varName,cut1,cut2
                  print effs
                  print rejs
                  exit(-1)
    print 'hello'
    rejs[nPoints-1]=0 # something weird
    depth=0
    while not(checkMonotonicity(rejs)):
        depth += 1
        if depth>400:
          break
        effs,rejs = smooth(effs,rejs)
    g = TGraph(effs.size,effs,1-rejs)
    outFile.WriteTObject(g,varName)
    outFile.Close()
