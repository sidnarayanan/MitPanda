#!/bin/bash

#mkdir -p /home/snarayan/public_html/figs/topTagging${1}/
#mkdir -p /home/snarayan/root/topTagging${1}/
#ln -sf  /home/snarayan/public_html/figs/topTagging${1}/ /home/snarayan/cms/root/topTagging${1}/figs

sed -i "s/shortLabel=.*/shortLabel=\"${1}\";/" runPlots.C
sed -i "s/ptLow=[.0-9]*/ptLow=${2}/" runPlots.C
sed -i "s/ptHigh=[.0-9]*/ptHigh=${3}/" runPlots.C
sed -i "s/eta=[.0-9]*/eta=${4}/" runPlots.C
root -b -l -q runPlots.C

#./runROCs.py $@

