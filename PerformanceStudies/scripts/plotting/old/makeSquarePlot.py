#!/usr/bin/env python

from sys import argv
from ROOT import TCanvas,TFile,gStyle,TColor

import numpy as np
import MitPanda.Tools.SquarePlots as SP
from operator import itemgetter
from array import array

ncontours=999
stops = [0.,.5,1.]
red = [0.,1.,1.]
green = [0.,1.,0.]
blue = [1.,1.,0.]

s = array('d', stops)
r = array('d', red)
g = array('d', green)
b = array('d', blue)

npoints = len(s)
TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
gStyle.SetNumberContours(ncontours)

workDir = "/home/snarayan/cms/root/topTagging_"+argv[1]

ptlow = int(argv[2])
pthigh = int(argv[3])
etahigh = float(argv[4])
c1 = TCanvas()

plotter = SP.Plotter(workDir+'/qcd.root')
cut = "massSoftDrop>150&&massSoftDrop<240&&pt<%i&&pt>%i&&TMath::Abs(eta)<%f"%(pthigh,ptlow,etahigh)
#varNames = ['massSoftDrop','MassSDbm1','massTrimmed','massPruned','logchi','tau3/tau2','groomedIso','QjetVol','QGTag','sjqgtag0','sjqgtag1','maxSubjetBtag','NNResponse']
varNames = [
            ('sumQG_sj0dR',(100,0,2)),
            ('sumQG_sj0mW',(100,0,200)),
            ('sumQG_sj0dPhiTRF',(100,0,3.2)),
            ('sumQG_sj1dR',(100,0,2)),
            ('sumQG_sj1mW',(100,0,200)),
            ('sumQG_sj1dPhiTRF',(100,0,3.2)),
            ('sumQG_sj2dR',(100,0,2)),
            ('sumQG_sj2mW',(100,0,200)),
            ('sumQG_sj2dPhiTRF',(100,0,3.2)),
            ('sumQG_sj3dR',(100,0,2)),
            ('sumQG_sj3mW',(100,0,200)),
            ('sumQG_sj3dPhiTRF',(100,0,3.2)),
           ]
'''
varNames = [('massSoftDrop',(100,0,500)),
				('MassSDbm1',(100,0,500)),
				('massTrimmed',(100,0,500)),
				('massPruned',(100,0,500)),
				('logchi',(100,-40,5)),
				('tau3/tau2',(100,0,1)),
				('groomedIso',(100,0,1)),
				('QjetVol',(100,0,1)),
				('QGTag',(100,0,1)),
				('sjqgtag0',(100,0,1)),
				('sjqgtag1',(100,0,1)),
				('maxSubjetBtag',(100,0,1)),
				('NNResponse',(100,0,1))]
'''
#varNames = [('logchi',(100,-40,5)),('tau3/tau2',(100,0,1)),('groomedIso',(100,0,1))]
#hplot,M = plotter.makeCorrelationPlot(varNames,varNames,cut)
#hplot,M = plotter.makeCovariancePlot(varNames,cut)
plotter.customBins=True
hplot,M = plotter.makeCorrelationPlot(varNames,varNames,cut)
'''
D = np.linalg.inv(np.power(np.diag(M)*np.eye(M.shape[0]),.5))
C = np.dot(D,np.dot(M,D))
nVars = D.shape[0]
for iV in xrange(nVars):
  for jV in xrange(nVars):
     hplot.SetBinContent(iV+1,jV+1,abs(C[iV,jV]))
gStyle.SetPaintTextFormat("1.3f")

alphas,vs = np.linalg.eigh(M)
eigs=[(alphas[i],vs[:,i]) for i in xrange(alphas.shape[0])]
eigs.sort(key=itemgetter(0),reverse=True)
nVars = len(eigs)
xaxis = hplot.GetXaxis()
yaxis = hplot.GetYaxis()
for iV in xrange(nVars):
  yaxis.SetBinLabel(iV+1,"V%i"%(iV))
  xaxis.SetBinLabel(iV+1,varNames[iV][0])
  for jV in xrange(nVars):
    hplot.SetBinContent(jV+1,iV+1,eigs[iV][1][jV])
'''

suffix = '_ptGT%.1fANDptLT%.1fANDabsetaLT%.1f'%(ptlow,pthigh,etahigh)
suffix = suffix.replace('.','p')
#fin = TFile(workDir+'/figs/correlation'+suffix+'.root')
#hplot = fin.Get('c')
c1.cd()
c1.SetLeftMargin(0.15)
c1.SetBottomMargin(0.15)
hplot.SetMinimum(-1.)
hplot.SetMaximum(1.)
hplot.SetStats(0)
hplot.SetTitle('%i < p_{T} < %i'%(ptlow,pthigh))
hplot.GetXaxis().SetLabelSize(0.05)
hplot.GetYaxis().SetLabelSize(0.05)
hplot.Draw("colz text")
c1.SaveAs(workDir+'/figs/correlation'+suffix+'.png')
c1.SaveAs(workDir+'/figs/correlation'+suffix+'.pdf')

fout = TFile(workDir+'/figs/correlation'+suffix+'.root',"RECREATE")
fout.WriteTObject(hplot,'c')
fout.Close()
