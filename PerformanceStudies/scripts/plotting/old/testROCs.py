#!/usr/bin/env python

from sys import argv
from os import system
import MitPanda.Tools.ROCTools as ROC
from ROOT import TFile,TTree

fSignal = TFile('$ROOT/topTagging_AK8/signal.root')
fBackground = TFile('$ROOT/topTagging_AK8/qcd.root')
tSig = fSignal.Get('jets')
tBg = fBackground.Get('jets')
varNames = ['tau3/tau2','QjetVol']
prettyVarName = 'tau32+QjetVol'
#cut = 'pt>450'
cut=None
factory = ROC.Factory(tSig,tBg,'$ROOT/topTagging_AK8/buffer.root',cut)
factory.addVariableList(varNames,prettyVarName)
factory.close()
