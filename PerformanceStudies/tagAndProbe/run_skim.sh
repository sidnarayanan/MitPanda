#!/bin/bash
label=$1
cfgName=$2
pwd

scramdir=/home/snarayan/cms/cmssw/041/CMSSW_7_4_4/
cd ${scramdir}/src/
eval `scramv1 runtime -sh`
cd -

cp ${scramdir}/src/MitPanda/PerformanceStudies/qcdStudy/skim.C .
cp ${scramdir}/src/MitPanda/PerformanceStudies/qcdStudy/rootlogon.C .

root -b -l -q skim.C+\("\"${scramdir}/src/MitPanda/PerformanceStudies/config/${2}.cfg\",${label},0,\"/home/snarayan/cms/root/tagAndProbe_v0/\""\)
exit $status
