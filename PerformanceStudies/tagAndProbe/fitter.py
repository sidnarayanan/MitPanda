import ROOT as root

workDir = '/home/snarayan/public_html/figs/tagAndProbe/tagAndProbe'

def loadFromFile(workDir,x):
  fIn = root.TFile(workDir+'/hists.root')

  hSig = fIn.Get('h_CA15fj1_mSD_TTbar')
  rhSig = root.RooDataHist('ttbar','ttbar',root.RooArgList(x),hSig)

  hData = fIn.Get('h_CA15fj1_mSD_Data')
  rhData = root.RooDataHist('data','data',root.RooArgList(x),hData)

  backgrounds = {}  
  bgNames = ['DY','QCD','ZtoNuNu','ZJets','WJets','SingleTop','FakeTTbar']
  for name in bgNames:
    hTmp = fIn.Get('h_CA15fj1_mSD_'+name)
    rhTmp = root.RooDataHist(name,name,root.RooArgList(x),hTmp)
    backgrounds[name] = rhTmp

  return {'data':rhData, 'sig':rhSig, 'bg':backgrounds}

def runFit(x,d,workDir,label='_test',title=''):
  rhData = d['data']
  ndata = rhData.sumEntries()
  rhSig = d['sig']
  
  # signal
  sigpdf = root.RooHistPdf('sigpdf','sigpdf',root.RooArgSet(x),rhSig)
  nsig = root.RooRealVar('nsig','nsig',ndata*.1,0,ndata*2.)

  # smearing of signal
  mu = root.RooRealVar('mu','mu',0,-.01,.01)
  sigma = root.RooRealVar('sigma','sigma',999.,0,99999.)
  gauss = root.RooGaussian('gauss','gauss',x,mu,sigma)
  sigsmeared = root.RooFFTConfPdf('sigsmeared','sigsmeared',x,sigpdf,gauss)

  # background
  bgpdfs = []  
  nbgs = []
  nbgNames = []
  for n,h in d['bg'].iteritems():
    bgpdfs.append(root.RooHistPdf(n+'pdf',n+'pdf',root.RooArgSet(x),h))
    nbgs.append(root.RooRealVar('n'+n,'n'+n,ndata*.1,0,ndata*2.))
    nbgNames.append('n'+n)

  # collect all pdfs
  allpdfs = root.RooArgList(sigsmeared)
  allnorms = root.RooArgList(nsig)
  for pdf,norm in zip(bgpdfs,nbgs):
    allpdfs.add(pdf)
    allnorms.add(norm)

  model = root.RooAddPdf('model','model',allpdfs,allnorms)
  model.fitTo(rhData)

  params = model.getVariables()
  nsigFit = params.find('nsig').getValue()
  nsigFitErrUp =  params.find('nsig').getAsymErrorHi()
  nsigFitErrDown =  params.find('nsig').getAsymErrorLo()

  nbgFit = 0
  for n in nbgNames:
    nbgFit += params.find(n).getValue()

  c1 = root.TCanvas()
  xFrame = x.frame()
  xFrame.SetTitle('Fit result')
  rhData.plotOn(xFrame,root.RooFit.Name('Data'))
  model.plotOn(xFrame,root.RooFit.Name('Fit'))
  model.plotOn(xFrame,root.RooFit.Components('sigsmeared'),root.RooFit.Name('TTBar'),root.RooFit.LineColor(root.kRed))
  xFrame.Draw()
  c1.SaveAs(workDir+'/fits'+label+'.png')
  c1.SaveAs(workDir+'/fits'+label+'.pdf')

  return nsigFit,nbgFit

mass = root.RooRealVar('mSD','mSD',0.,500.)
d = loadFromFile(workDir,mass)
print runFit(mass,d,workDir)
