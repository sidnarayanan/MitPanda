from os import system,mkdir
from numpy import arange

effs = arange(0.01,0.11,0.01)
for e in effs:
  name = ("h%.2f"%(e)).replace('.','')
  dirName = '/home/snarayan/cms/root/tagAndProbe_v1/figs/tagAndProbe'+name+'_passed/'
  try:
    print 'mkdir',dirName
    mkdir(dirName)
  except OSError:
    pass
  system('root -b -l -q makePlots_passed.C+\(\\"tagAndProbe\\",\\"%s\\"\)'%(name))
  system('genWebViewer '+dirName)
