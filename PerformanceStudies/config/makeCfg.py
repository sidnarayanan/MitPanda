#!/usr/bin/env python

from sys import argv
from glob import glob
from os import stat

processes =  {
'DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v3+AODSIM':('ZJets','MC',6025.2),
'DYJetsToNuNu_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('ZtoNuNu','MC',2405),
#'QCD_HT200to300_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('QCD_ht200to300','MC',1735000),
#'QCD_HT300to500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('QCD_ht300to500','MC',366800),
'QCD_HT500to700_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('QCD_ht500to700','MC',29370),
'QCD_HT700to1000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('QCD_ht700to100','MC',6524),
'QCD_HT1000to1500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('QCD_ht1000to1500','MC',1064),
'QCD_HT1500to2000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('QCD_ht1500to2000','MC',121.5),
'QCD_HT2000toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('QCD_ht2000toinf','MC',25.42),
'ST_s-channel_4f_leptonDecays_13TeV-amcatnlo-pythia8_TuneCUETP8M1+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('SingleTop_schannel','MC',10.11),
'ST_t-channel_5f_leptonDecays_13TeV-amcatnlo-pythia8_TuneCUETP8M1+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('SingleTop_tchannel','MC',216.99),
'ST_tW_antitop_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('SingleTop_tWantitop','MC',42.2),
'ST_tW_top_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('SingleTop_tWtop','MC',42.2),
'TTJets_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('TTbar','MC',831.76),
'TTJets_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('TTbar','MC',831.76),
'WJetsToLNu_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('WJets','MC',61527),
'WW_TuneCUETP8M1_13TeV-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('Diboson_ww','MC',118.7),
'WZ_TuneCUETP8M1_13TeV-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('Diboson_wz','MC',47.2),
'ZZ_TuneCUETP8M1_13TeV-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v3+AODSIM':('Diboson_zz','MC',16.6),
'Monotop_S1_Mres-2100_Mchi-100_TuneCUETP8M1_13TeV-madgraph-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('Signal2p1','MC',0.14),
'Monotop_S1_Mres-1100_Mchi-100_TuneCUETP8M1_13TeV-madgraph-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('Signal1p1','MC',3.98),
'DYJetsToLL_M-50_HT-100to200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('ZJets_ht100to200','MC',148.),
'DYJetsToLL_M-50_HT-200to400_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('ZJets_ht200to400','MC',40.94),
'DYJetsToLL_M-50_HT-400to600_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9_ext1-v1+AODSIM':('ZJets_ht400to600','MC',5.497),
'DYJetsToLL_M-50_HT-600toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('ZJets_ht600toinf','MC',2.193),
'WJetsToLNu_HT-100To200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('WJets_ht100to200','MC',1343),
'WJetsToLNu_HT-200To400_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('WJets_ht200to400','MC',359.6),
'WJetsToLNu_HT-400To600_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v3+AODSIM':('WJets_ht400to600','MC',48.85),
'WJetsToLNu_HT-600ToInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('WJets_ht600toinf','MC',18.91),
'ZJetsToNuNu_HT-100To200_13TeV-madgraph+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('ZtoNuNu_ht100to200','MC',280.5),
'ZJetsToNuNu_HT-200To400_13TeV-madgraph+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('ZtoNuNu_ht200to400','MC',77.7),
'ZJetsToNuNu_HT-400To600_13TeV-madgraph+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('ZtoNuNu_ht400to600','MC',10.71),
'ZJetsToNuNu_HT-600ToInf_13TeV-madgraph+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('ZtoNuNu_ht600toinf','MC',4.098),
'TTbarDMJets_pseudoscalar_Mchi-1_Mphi-100_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('TTbarDM','MC',1),
'MET+Run2015D-PromptReco-v3+AOD':('MET2015Dv3','Data',1),
'MET+Run2015D-PromptReco-v4+AOD':('MET2015Dv4','Data',1),
'SingleElectron+Run2015D-PromptReco-v3+AOD':('SingleElectron2015Dv3','Data',1),
'SingleElectron+Run2015D-PromptReco-v4+AOD':('SingleElectron2015Dv4','Data',1),
'SinglePhoton+Run2015D-PromptReco-v3+AOD':('SinglePhoton2015Dv3','Data',1),
'SinglePhoton+Run2015D-PromptReco-v4+AOD':('SinglePhoton2015Dv4','Data',1),
'GJets_HT-100To200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('GJets_ht100to200','MC',9235),
'GJets_HT-200To400_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('GJets_ht200to400','MC',2298),
'GJets_HT-400To600_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('GJets_ht400to600','MC',277.6),
'GJets_HT-600ToInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('GJets_ht600toinf','MC',93.47),
'ZprimeToTT_M-750_W-7p5_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('Zprime_M750','MC',1),
'ZprimeToTT_M-500_W-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('Zprime_M500','MC',1),
'ZprimeToTT_M-2500_W-25_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v1+AODSIM':('Zprime_M2500','MC',1),
'ZprimeToTT_M-2000_W-20_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('Zprime_M2000','MC',1),
'ZprimeToTT_M-1500_W-15_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('Zprime_M1500','MC',1),
'ZprimeToTT_M-1000_W-10_TuneCUETP8M1_13TeV-madgraphMLM-pythia8+RunIISpring15DR74-Asympt25ns_MCRUN2_74_V9-v2+AODSIM':('Zprime_M1000','MC',1),
            }

#listOfFiles = glob('%s/t2mit/filefi/042/*/nero*'%(argv[1]))
listOfFiles = glob('/home/snarayan/cms/hist/%s/t2mit/filefi/042/*/*flat.root'%(argv[1]))

cfgFile = open(argv[2],'w')

for f in sorted(listOfFiles):
  if stat(f).st_size==0:
    print f
    continue
  ff = f.split('/')
  dataset = ff[-2]
  fileName = ff[-2]+'/'+ff[-1]
  try:
    properties = processes[dataset]
  except KeyError:
#    print 'COULD NOT FIND',dataset
    properties = ('UNKOWN','UNKNOWN',-1)
    continue
  cfgFile.write('{0:<25}{1:<150}{2:<10}{3}\n'.format(properties[0],fileName,properties[1],properties[2]))

