# MonoHiggs analysis code


### Running skims

First, set up your environment by editing the variables in `MitPanda/MonoHiggs/setup.sh`. Then call `source setup.sh`. `PANDA_HISTDIR` should point to the location of the Nero ntuples made in the above step.

The core skimming object is defined in `MitPanda/Skimmers/src/NeroSkimmer.cc`. There are three main scripts to utilize it when running on Bambu. All are located in `MitPanda/MonoHiggs/scripts/skimming/`. The first is used for testing things locally:
```
./skimLocal.py /path/to/input/file 
```
It runs on a single file and produces output in `/tmp/$USER/testskim.root`. 

There are two options to run on a large number of files. First, make the configuration file using `Monotop/config/makeCfgEOS.py`. Then, call either `scripts/skimming/skimMultiCoreEOS.py <cfgname>` or edit and run the submission script at `scripts/skimming/submit/submitLxplus.py`

### Merging skims

The output files then need to be merged and the MC weights need to be normalized. Warning: the current setup assumes MC weights are +/- X, for some fixed value X. This is no longer true for Powheg samples and needs to be fixed. 

The workhorse is `MitPanda/MonoHiggs/scripts/merge.py`. Make sure to edit line 29 to point to the correct subdirectory of `${PANDA_FLATDIR}`. Then, you can call multiple instances of `merge.py` by running `merge.sh` (simply passes a bunch of different physics process types through `xargs`).


