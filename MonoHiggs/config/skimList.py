#!/usr/bin/env python

from os import path,getenv
from sys import argv
from re import sub

cfgpath = argv[1]
flatdir = getenv('PANDA_FLATDIR')+'/batch/'
cfgin = open(cfgpath,'r')
cfgout = open(sub('.cfg','_skimmed.cfg',cfgpath),'w')
counter=0
bad=0
for line in cfgin:
  ll = line.split()
  filepath = flatdir + '%s_%i.root'%(ll[0],counter)
  if not path.isfile(filepath):
    cfgout.write(line.strip() + '  %i\n'%(counter))
    bad+=1
  counter += 1
cfgout.close()
print "Found %i missing output files"%bad

