#!/usr/bin/env python

from sys import argv
fpath = argv[1]

import MitPanda.Tools.Load as Load
import ROOT as root
from MitPanda.Monotop.Selection import *
from MitPanda.Tools.Misc import *

Load.Load('Tools','BranchAdder')

ba = root.BranchAdder()
ba.formula = tTIMES(cuts['signal'],weights['signal']%1)
ba.newBranchName = 'signalWeight' 
ba.AddBranch(fpath)

