#!/usr/bin/env python

from ROOT import gSystem, gROOT
import ROOT as root
from array import array
from glob import glob
from re import sub
from sys import argv
from os import environ,system,path
from MitPanda.Tools.process import *

gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/Normalizer.h")
gSystem.Load('libMitPandaTools.so')

pds = {}
for k,v in processes.iteritems():
  if v[1]=='MC':
    pds[v[0]] = (k,v[2])  
  else:
    pds[v[0]] = (k,-1)

VERBOSE=False

user = environ['USER']
system('mkdir -p /tmp/%s/split'%user) # tmp dir
system('mkdir -p /tmp/%s/merged'%user) # tmp dir

base = environ['PANDA_FLATDIR']
inbase = base+'/batch/'


def hadd(inpath,outpath):
  if type(inpath)==type('str'):
    infiles = glob(inpath)
    print 'hadding %s into %s'%(inpath,outpath)
    cmd = 'hadd -ff -n 100 -f %s %s > /dev/null'%(outpath,inpath)
    system(cmd)
    return
  else:
    infiles = inpath
  if len(infiles)==0:
    print 'WARNING: nothing hadded into',outpath
    return
  elif len(infiles)==1:
    cmd = 'cp %s %s'%(infiles[0],outpath)
  else:
    cmd = 'hadd -ff -n 100 -f %s '%outpath
    for f in infiles:
      if path.isfile(f):
        cmd += '%s '%f
  if VERBOSE: print cmd
  system(cmd+' >/dev/null 2>/dev/null')

def normalize(fpath,opt):
  xsec=-1
  if type(opt)==type(1.) or type(opt)==type(1):
    xsec = opt
  else:
    try:
      xsec = processes[proc][2]
    except KeyError:
      for k,v in processes.iteritems():
        if proc in k:
          xsec = v[2]
  if xsec<0:
    print 'could not find xsec, skipping %s!'%opt
    return
  print 'normalizing %s (%s) ...'%(fpath,opt)
  nw = array('f',[0])
  w = array('f',[0])
  fin = root.TFile(fpath,'UPDATE')
  tree = fin.Get('events')
  total = fin.Get('hDTotalMCWeight').Integral()
  b = tree.Branch('normalizedWeight',nw,'normalizedWeight/F')
  tree.SetBranchAddress('mcWeight',w)
  nEntries = tree.GetEntries()
  for iE in xrange(nEntries):
    tree.GetEntry(iE)
    nw[0] = xsec*w[0]/total
    b.Fill()
  fin.WriteTObject(tree)
  fin.Close()

def normalizeFast(fpath,opt):
  xsec=-1
  if type(opt)==type(1.) or type(opt)==type(1):
    xsec = opt
  else:
    try:
      xsec = processes[proc][2]
    except KeyError:
      for k,v in processes.iteritems():
        if proc in k:
          xsec = v[2]
  if xsec<0:
    print 'could not find xsec, skipping %s!'%opt
    return
  print 'fast normalizing %s (%s) ...'%(fpath,opt)
  n = root.Normalizer();
  n.NormalizeTree(fpath,xsec)

def merge(shortnames,mergedname):
  for shortname in shortnames:
    try:
      pd = pds[shortname][0]
      xsec = pds[shortname][1]
    except KeyError:
      for shortname_ in [shortname.split('_')[0],shortname.split('_')[-1]]:
        if shortname_ in pds:
          pd = pds[shortname_][0]
          xsec = pds[shortname_][1]
          break
    '''
    inpath = ''
    for i in xrange(4):
      q = '[0-9]'*(i+1)
      inpath += inbase+shortname+'_'+q+'.root '
    '''
    inpath = inbase+shortname+'_*.root'
    hadd(inpath,'/tmp/%s/split/%s.root'%(user,shortname))
    if xsec>0:
      normalizeFast('/tmp/%s/split/%s.root'%(user,shortname),xsec)
  hadd(['/tmp/%s/split/%s.root'%(user,x) for x in shortnames],'/tmp/%s/merged/%s.root'%(user,mergedname))

d = {
  'test'                : ['Diboson_ww'],
  'Diboson'             : ['Diboson_ww','Diboson_wz','Diboson_zz'],
  'ZJets'               : ['ZJets_ht100to200','ZJets_ht200to400','ZJets_ht400to600','ZJets_ht600toinf'],
  #'ZtoNuNu'             : ['ZtoNuNu_ht200to400','ZtoNuNu_ht600to800','ZtoNuNu_ht800to1200','ZtoNuNu_ht1200to2500','ZtoNuNu_ht2500toinf'],
  'ZtoNuNu'             : ['ZtoNuNu_ht100to200','ZtoNuNu_ht200to400','ZtoNuNu_ht400to600','ZtoNuNu_ht600to800','ZtoNuNu_ht800to1200','ZtoNuNu_ht1200to2500','ZtoNuNu_ht2500toinf'],
  'GJets'               : ['GJets_ht100to200','GJets_ht200to400','GJets_ht400to600','GJets_ht600toinf'],
  'WJets'               : ['WJets_ht100to200','WJets_ht200to400','WJets_ht400to600','WJets_ht600to800','WJets_ht800to1200','WJets_ht1200to2500','WJets_ht2500toinf'],
  'TTbar_MLM'           : ['TTbar_1LT','TTbar_1LTbar','TTbar_2L'],
  'TTbar_FXFX'          : ['TTbar_FXFX'],
  'TTbar_Powheg'        : ['TTbar_Powheg'],
  'TTbar_1LT'           : ['TTbar_1LT'],
  'TTbar_1LTbar'        : ['TTbar_1LTbar'],
  'TTbar_2L'            : ['TTbar_2L'],
  'TTbarDM'             : ['TTbarDM'],
  'TTbarDM_fast'        : ['TTbarDM_fast'],
  #'SingleTop'           : ['SingleTop_tTbar','SingleTop_tbarW','SingleTop_tW','tZq_ll','tZq_nunu'],
  'SingleTop'           : ['SingleTop_tTbar','SingleTop_tbarW','SingleTop_tW'],
  'QCD'                 : ['QCD_ht200to300','QCD_ht300to500','QCD_ht500to700','QCD_ht700to100','QCD_ht1000to1500','QCD_ht1500to2000','QCD_ht2000toinf'], # FIXME PLEASE
  'MET'                 : ['MET'],
  'SingleElectron'      : ['SingleElectron'],
  'DoubleEG'            : ['DoubleEG'],
  'SinglePhoton'        : ['SinglePhoton'],
  'MET_nt'              : ['NoTrigger_MET'],
  'SingleElectron_nt'   : ['NoTrigger_SingleElectron'],
  'DoubleEG_nt'         : ['NoTrigger_DoubleEG'],
  'SinglePhoton_nt'     : ['NoTrigger_SinglePhoton'],
  'monotop_fcnc_mMed300' : ['monotop_fcnc_mMed300'],
  'monotop_fcnc_mMed500' : ['monotop_fcnc_mMed500'],
  'monotop_fcnc_mMed700' : ['monotop_fcnc_mMed700'],
  'monotop_fcnc_mMed900' : ['monotop_fcnc_mMed900'],
  'monotop_fcnc_mMed1100' : ['monotop_fcnc_mMed1100'],
  'monotop_fcnc_mMed1300' : ['monotop_fcnc_mMed1300'],
  'monotop_fcnc_mMed1500' : ['monotop_fcnc_mMed1500'],
  'monotop_res_mMed900' : ['monotop_res_mMed900'],
  'monotop_res_mMed1100' : ['monotop_res_mMed1100'],
  'monotop_res_mMed1300' : ['monotop_res_mMed1300'],
  'monotop_res_mMed1500' : ['monotop_res_mMed1500'],
  'monotop_res_mMed1700' : ['monotop_res_mMed1700'],
  'monotop_res_mMed1900' : ['monotop_res_mMed1900'],
  'monotop_res_mMed2100' : ['monotop_res_mMed2100'],
  'ZJets_nlo'           : ['ZJets_nlo'],
  'WJets_nlo'           : ['WJets_nlo'],
}

args = {}

for pd in argv[1:]:
  args[pd] = d[pd]

for pd in args:
  merge(args[pd],pd)
  system('cp -r /tmp/%s/merged/%s.root %s'%(user,pd,base))
  print 'finished with',pd

