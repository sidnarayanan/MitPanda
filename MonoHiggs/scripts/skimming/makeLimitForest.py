#!/usr/bin/env python
from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv,exit
from os import path,getenv
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.Selection import *

if __name__ == "__main__":

  do80xSignals=True
  do76xSignals=False

  toProcess=None
  if len(argv)>1:
    toProcess = argv[1]
  
  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/LimitTreeBuilder.h")
  gSystem.Load('libMitPandaSkimmers.so')

  baseDir = '/local/snarayan/monotop_80_13fb/'
  signalDir = sub('prod','prod',getenv('PANDA_PRODFLATDIR'))
  #signalDir = sub('prod','prod_backup',getenv('PANDA_PRODFLATDIR'))
  lumi = 12918

  factory = root.LimitTreeBuilder()
  if toProcess:
    factory.SetOutFile(baseDir+'/limits/limitForest_%s.root'%toProcess)
  else:
    factory.SetOutFile(baseDir+'/limits/limitForest.root')

  def dataCut(basecut,trigger):
    #return tAND('metFilter==1',tAND(trigger,basecut))
    return tAND(trigger,basecut)

  def getTree(fpath):
    fIn = root.TFile(baseDir+fpath+'.root')
    tIn = fIn.Get('events')
    return tIn,fIn

  def getSigTree(fpath):
    fpath_ = signalDir+fpath+'.root'
    if path.isfile(fpath_):
      fIn = root.TFile(fpath_)
      tIn = fIn.Get('events')
      return tIn,fIn
    else:
      PWarning("makeLimitForest.getSigTree","Skipping %s"%fpath)
      return None,None

  def shiftBtags(label,tree,varmap,cut,basecut,trigger):
    # basecut is really the baseweight
    ps = []
    for shift in ['BUp','BDown','MUp','MDown']:
      for cent in ['sf_btag','sf_sjbtag']:
        shiftedlabel = '_'
        if 'sj' in cent:
          shiftedlabel += 'sj'
        if 'B' in shift:
          shiftedlabel += 'btag'
        else:
          shiftedlabel += 'mistag'
        if 'Up' in shift:
          shiftedlabel += 'Up'
        else:
          shiftedlabel += 'Down'
        weight = tTIMES(trigger,weights[basecut+'_'+cent+shift]%lumi)
        #print shiftedlabel
        #print '\t',weight
        shiftedProcess = root.Process(label,tree,varmap,cut,weight)
        shiftedProcess.syst = shiftedlabel
        ps.append(shiftedProcess)
    return ps

  def enable(regionName):
    if toProcess:
      return (toProcess==regionName)
    else:
      return True

  # input
  tZll,fZll = getTree('ZJets')
  tZvv,fZvv = getTree('ZtoNuNu')
  tWlv,fWlv = getTree('WJets')
  tPho,fPho = getTree('GJets')
  tTTbar,fTT = getTree('TTbar_Powheg')
  tVV,fVV = getTree('Diboson')
  tQCD,fQCD = getTree('QCD')
  tST,fST = getTree('SingleTop')
  tMET,fMET = getTree('MET')
  tSingleEle,fSEle = getTree('SingleElectron')
  tSinglePho,fSPho = getTree('SinglePhoton')
  tfcnc_76x = {}; tres_76x = {}
  tfcnc_v0 = {}; tres_v0 = {}; tfcnc_v3 = {}
  if do76xSignals:
    for m in range(300,1700,200):
      filename = 'monotop_fcnc_mMed%i'%m
      t,f = getTree(filename)
      tfcnc_76x[filename] = (t,f)
    for m in range(900,2300,200):
      filename = 'monotop_res_mMed%i'%m
      t,f = getTree(filename)
      tres_76x[filename] = (t,f)
  if do80xSignals:
    # for m in [50]+range(100,2300,200):
    for m in range(300,2300,200):
      for mchi in [10,50,100,200,500]:
        filename = 'monotop-nr-v3-%i-%i_med-%i_dm-%i'%(m,mchi,m,mchi)
        t,f = getSigTree(filename)
        if t:
          tfcnc_v3[filename] = (t,f)
    for m in range(900,3500,200):
      treename = 'monotop_med-%i_dm-100'%(m)
      filename = 'monotop_med-%i'%(m)
      t,f = getSigTree(filename)
      if t:
        tres_v0[treename] = (t,f)

  factory.cd()
  regions = {}
  processes = {}

  vm = root.VariableMap('puppimet','dPhiPuppiMET','','genBosonPt','genBosonPhi')
  vmZ = root.VariableMap('UZmag','dPhiPuppiMET','','genBosonPt','genBosonPhi')
  vmW = root.VariableMap('UWmag','dPhiPuppiMET','','genBosonPt','genBosonPhi')
  vmA = root.VariableMap('UAmag','dPhiPuppiMET','','genBosonPt','genBosonPhi')


  # signal region
  if enable('signal'):
    regions['signal'] = root.Region('signal')
    cut = cuts['signal']
    weight = tTIMES('sf_metTrig',weights['signal']%lumi)
    processes['signal'] = [
      root.Process('Data',tMET,vm,dataCut(cut,metTrigger),'1'),
      root.Process('Zvv',tZvv,vm,cut,weight),
      root.Process('Wlv',tWlv,vm,cut,weight),
      root.Process('ttbar',tTTbar,vm,cut,weight),
      root.Process('ST',tST,vm,cut,weight),
      root.Process('Zll',tZll,vm,cut,weight),
      root.Process('Diboson',tVV,vm,cut,weight),
      root.Process('QCD',tQCD,vm,cut,weight),
    ]
    processes['signal'] += shiftBtags('ttbar',tTTbar,vm,cut,'signal','sf_metTrig')
    processes['signal'] += shiftBtags('Zvv',tZvv,vm,cut,'signal','sf_metTrig')
    processes['signal'] += shiftBtags('Wlv',tWlv,vm,cut,'signal','sf_metTrig')
    processes['signal'] += shiftBtags('ST',tST,vm,cut,'signal','sf_metTrig')
    processes['signal'] += shiftBtags('Zll',tZll,vm,cut,'signal','sf_metTrig')
    processes['signal'] += shiftBtags('Diboson',tVV,vm,cut,'signal','sf_metTrig')
    processes['signal'] += shiftBtags('QCD',tQCD,vm,cut,'signal','sf_metTrig')
    if do76xSignals:
      for m in tfcnc_76x:
        sigtree = tfcnc_76x[m][0]
        processes['signal'].append(root.Process(m,sigtree,vm,cuts['signal_nomf'],weight))
        processes['signal'] += shiftBtags(m,sigtree,vm,cuts['signal_nomf'],'signal','sf_metTrig')
      for m in tres_76x:
        sigtree = tres_76x[m][0]
        processes['signal'].append(root.Process(m,sigtree,vm,cuts['signal_nomf'],weight))
        processes['signal'] += shiftBtags(m,sigtree,vm,cuts['signal_nomf'],'signal','sf_metTrig')
    if do80xSignals:
      for m in tfcnc_v3:
        sigtree = tfcnc_v3[m][0]
        processes['signal'].append(root.Process(m,sigtree,vm,cut,weight))
        processes['signal'] += shiftBtags(m,sigtree,vm,cut,'signal','sf_metTrig')
      for m in tfcnc_v0:
        sigtree = tfcnc_v0[m][0]
        processes['signal'].append(root.Process(m,sigtree,vm,cut,weight))
        processes['signal'] += shiftBtags(m,sigtree,vm,cut,'signal','sf_metTrig')
      for m in tres_v0:
        sigtree = tres_v0[m][0]
        processes['signal'].append(root.Process(m,sigtree,vm,cut,weight))
        processes['signal'] += shiftBtags(m,sigtree,vm,cut,'signal','sf_metTrig')

    for p in processes['signal']:
      regions['signal'].AddProcess(p)
    factory.AddRegion(regions['signal'])

  # singlemuontop
  if enable('singlemuontop'):
    regions['singlemuontop'] = root.Region('singlemuontop')
    cut = cuts['singlemuontop']
    weight = tTIMES('sf_metTrig',weights['top']%lumi)
    processes['singlemuontop'] = [
      root.Process('Data',tMET,vmW,dataCut(cut,metTrigger),'1'),
      root.Process('Wlv',tWlv,vmW,cut,weight),
      root.Process('ttbar',tTTbar,vmW,cut,weight),
      root.Process('ST',tST,vmW,cut,weight),
      root.Process('Zll',tZll,vmW,cut,weight),
      root.Process('Diboson',tVV,vmW,cut,weight),
      root.Process('QCD',tQCD,vmW,cut,weight),
    ]
    processes['singlemuontop'] += shiftBtags('ST',tST,vmW,cut,'top','sf_metTrig')
    processes['singlemuontop'] += shiftBtags('ttbar',tTTbar,vmW,cut,'top','sf_metTrig')
    processes['singlemuontop'] += shiftBtags('Wlv',tWlv,vmW,cut,'top','sf_metTrig')
    processes['singlemuontop'] += shiftBtags('Zll',tZll,vmW,cut,'top','sf_metTrig')
    processes['singlemuontop'] += shiftBtags('Diboson',tVV,vmW,cut,'top','sf_metTrig')
    processes['singlemuontop'] += shiftBtags('QCD',tQCD,vmW,cut,'top','sf_metTrig')

    for p in processes['singlemuontop']:
      regions['singlemuontop'].AddProcess(p)
    factory.AddRegion(regions['singlemuontop'])

  # singleelectrontop
  if enable('singleelectrontop'):
    regions['singleelectrontop'] = root.Region('singleelectrontop')
    cut = cuts['singleelectrontop']
    weight = tTIMES('sf_eleTrig',weights['top']%lumi)
    processes['singleelectrontop'] = [
      root.Process('Data',tSingleEle,vmW,dataCut(cut,eleTrigger),'1'),
      root.Process('Wlv',tWlv,vmW,cut,weight),
      root.Process('ttbar',tTTbar,vmW,cut,weight),
      root.Process('ST',tST,vmW,cut,weight),
      root.Process('Zll',tZll,vmW,cut,weight),
      root.Process('Diboson',tVV,vmW,cut,weight),
      root.Process('QCD',tQCD,vmW,cut,weight),
    ]
    processes['singleelectrontop'] += shiftBtags('ST',tST,vmW,cut,'top','sf_eleTrig')
    processes['singleelectrontop'] += shiftBtags('ttbar',tTTbar,vmW,cut,'top','sf_eleTrig')
    processes['singleelectrontop'] += shiftBtags('Wlv',tWlv,vmW,cut,'top','sf_eleTrig')
    processes['singleelectrontop'] += shiftBtags('Zll',tZll,vmW,cut,'top','sf_eleTrig')
    processes['singleelectrontop'] += shiftBtags('Diboson',tVV,vmW,cut,'top','sf_eleTrig')
    processes['singleelectrontop'] += shiftBtags('QCD',tQCD,vmW,cut,'top','sf_eleTrig')
    for p in processes['singleelectrontop']:
      regions['singleelectrontop'].AddProcess(p)
    factory.AddRegion(regions['singleelectrontop'])

  # singlemuonw
  if enable('singlemuonw'):
    regions['singlemuonw'] = root.Region('singlemuonw')
    cut = cuts['singlemuonw']
    weight = tTIMES('sf_metTrig',weights['w']%lumi)
    processes['singlemuonw'] = [
      root.Process('Data',tMET,vmW,dataCut(cut,metTrigger),'1'),
      root.Process('Wlv',tWlv,vmW,cut,weight),
      root.Process('ttbar',tTTbar,vmW,cut,weight),
      root.Process('ST',tST,vmW,cut,weight),
      root.Process('Zll',tZll,vmW,cut,weight),
      root.Process('Diboson',tVV,vmW,cut,weight),
      root.Process('QCD',tQCD,vmW,cut,weight),
    ]
    processes['singlemuonw'] += shiftBtags('ST',tST,vmW,cut,'w','sf_metTrig')
    processes['singlemuonw'] += shiftBtags('Wlv',tWlv,vmW,cut,'w','sf_metTrig')
    processes['singlemuonw'] += shiftBtags('ttbar',tTTbar,vmW,cut,'w','sf_metTrig')
    processes['singlemuonw'] += shiftBtags('Zll',tZll,vmW,cut,'w','sf_metTrig')
    processes['singlemuonw'] += shiftBtags('Diboson',tVV,vmW,cut,'w','sf_metTrig')
    processes['singlemuonw'] += shiftBtags('QCD',tQCD,vmW,cut,'w','sf_metTrig')
    for p in processes['singlemuonw']:
      regions['singlemuonw'].AddProcess(p)
    factory.AddRegion(regions['singlemuonw'])

  # singleelectronw
  if enable('singleelectronw'):
    regions['singleelectronw'] = root.Region('singleelectronw')
    cut = cuts['singleelectronw']
    weight = tTIMES('sf_eleTrig',weights['w']%lumi)
    processes['singleelectronw'] = [
      root.Process('Data',tSingleEle,vmW,dataCut(cut,eleTrigger),'1'),
      root.Process('Wlv',tWlv,vmW,cut,weight),
      root.Process('ttbar',tTTbar,vmW,cut,weight),
      root.Process('ST',tST,vmW,cut,weight),
      root.Process('Zll',tZll,vmW,cut,weight),
      root.Process('Diboson',tVV,vmW,cut,weight),
      root.Process('QCD',tQCD,vmW,cut,weight),
    ]
    processes['singleelectronw'] += shiftBtags('ST',tST,vmW,cut,'w','sf_eleTrig')
    processes['singleelectronw'] += shiftBtags('Wlv',tWlv,vmW,cut,'w','sf_eleTrig')
    processes['singleelectronw'] += shiftBtags('ttbar',tTTbar,vmW,cut,'w','sf_eleTrig')
    processes['singleelectronw'] += shiftBtags('Zll',tZll,vmW,cut,'w','sf_eleTrig')
    processes['singleelectronw'] += shiftBtags('Diboson',tVV,vmW,cut,'w','sf_eleTrig')
    processes['singleelectronw'] += shiftBtags('QCD',tQCD,vmW,cut,'w','sf_eleTrig')
    for p in processes['singleelectronw']:
      regions['singleelectronw'].AddProcess(p)
    factory.AddRegion(regions['singleelectronw'])

  # dimuon
  if enable('dimuon'):
    regions['dimuon'] = root.Region('dimuon')
    cut = cuts['dimuon']
    weight = tTIMES('sf_metTrig',weights['notag']%lumi)
    processes['dimuon'] = [
      root.Process('Data',tMET,vmZ,dataCut(cut,metTrigger),'1'),
      root.Process('ttbar',tTTbar,vmZ,cut,weight),
      root.Process('ST',tST,vmZ,cut,weight),
      root.Process('Zll',tZll,vmZ,cut,weight),
      root.Process('Diboson',tVV,vmZ,cut,weight),
      root.Process('QCD',tQCD,vmZ,cut,weight),
    ]
    for p in processes['dimuon']:
      regions['dimuon'].AddProcess(p)
    factory.AddRegion(regions['dimuon'])

  # dielectron
  if enable('dielectron'):
    regions['dielectron'] = root.Region('dielectron')
    cut = cuts['dielectron']
    weight = tTIMES('sf_eleTrig',weights['notag']%lumi)
    processes['dielectron'] = [
      root.Process('Data',tSingleEle,vmZ,dataCut(cut,tOR(eleTrigger,phoTrigger)),'1'),
      root.Process('ttbar',tTTbar,vmZ,cut,weight),
      root.Process('ST',tST,vmZ,cut,weight),
      root.Process('Zll',tZll,vmZ,cut,weight),
      root.Process('Diboson',tVV,vmZ,cut,weight),
      root.Process('QCD',tQCD,vmZ,cut,weight),
    ]
    for p in processes['dielectron']:
      regions['dielectron'].AddProcess(p)
    factory.AddRegion(regions['dielectron'])

  # photon
  if enable('photon'):
    regions['photon'] = root.Region('photon')
    cut = cuts['photon']
    weight = tTIMES('sf_phoTrig',weights['notag']%lumi)
    print photonSF
    processes['photon'] = [
      root.Process('Data',tSinglePho,vmA,dataCut(cut,phoTrigger),'1'),
      root.Process('Pho',tPho,vmA,cut,tTIMES(weight,photonSF)),
      root.Process('QCD',tSinglePho,vmA,dataCut(cut,phoTrigger),'photonPurityWeight'),
      #root.Process('QCD',tQCD,vmA,cut,weight),
    ]
    for p in processes['photon']:
      regions['photon'].AddProcess(p)
    factory.AddRegion(regions['photon'])

  PInfo('makeLimitForest.__main__','Starting '+str(toProcess))
  factory.Run()
  PInfo('makeLimitForest.__main__','Finishing '+str(toProcess))

  factory.Output()

