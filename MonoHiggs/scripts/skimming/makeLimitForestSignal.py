#!/usr/bin/env python
from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv,exit
from os import path,getenv
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.Selection import *

if __name__ == "__main__":

  do80xSignals=True
  do76xSignals=False

  fpathin = argv[1]
  outname = argv[2]

  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/LimitTreeBuilder.h")
  gSystem.Load('libMitPandaSkimmers.so')

  lumi = 12918

  factory = root.LimitTreeBuilder()
  factory.SetOutFile(outname+'.root')

  def dataCut(basecut,trigger):
    #return tAND('metFilter==1',tAND(trigger,basecut))
    return tAND(trigger,basecut)

  def shiftBtags(label,tree,varmap,cut,basecut,trigger):
    # basecut is really the baseweight
    ps = []
    for shift in ['BUp','BDown','MUp','MDown']:
      for cent in ['sf_btag','sf_sjbtag']:
        shiftedlabel = '_'
        if 'sj' in cent:
          shiftedlabel += 'sj'
        if 'B' in shift:
          shiftedlabel += 'btag'
        else:
          shiftedlabel += 'mistag'
        if 'Up' in shift:
          shiftedlabel += 'Up'
        else:
          shiftedlabel += 'Down'
        weight = tTIMES(trigger,weights[basecut+'_'+cent+shift]%lumi)
        weight = sub('normalizedWeight','weight',weight)
        #print shiftedlabel
        #print '\t',weight
        shiftedProcess = root.Process(label,tree,varmap,cut,weight)
        shiftedProcess.syst = shiftedlabel
        ps.append(shiftedProcess)
    return ps

  # input
  fin = root.TFile.Open(fpathin)
  f_ = fpathin.split('/')[-1]
  mV = int(f_.split('_')[1])
  mChi = int(f_.split('_')[2])
  gq = float(f_.split('_')[3])
  tin = fin.Get('A_%i_%i_0.25_1.0_signal'%(mV,mChi))
  #tin = fin.Get('events')

  factory.cd()
  regions = {}
  processes = {}

  vm = root.VariableMap('puppimet','dPhiPuppiMET','','genBosonPt','genBosonPhi')

  region = root.Region('signal')
  cut = cuts['signal']
  weight = tTIMES('sf_metTrig',weights['signal']%lumi)
  weight = sub('normalizedWeight','weight',weight)
  processes = []
  processes.append(root.Process(outname,tin,vm,cut,weight))
  processes += shiftBtags(outname,tin,vm,cut,'signal','sf_metTrig')
  for p in processes:
    region.AddProcess(p)
  factory.AddRegion(region)

  PInfo('makeLimitForest.__main__','Starting '+outname)
  factory.Run()
  PInfo('makeLimitForest.__main__','Finishing '+outname)

  factory.Output()

