#!/usr/bin/env python

from os import system,environ
from sys import exit,stdout
from re import sub
from glob import glob

filestoprocess = glob('/afs/cern.ch/user/s/snarayan/eos/cms/store/cmst3/group/monojet/mc/model4/MonoTop*.root')
#filestoprocess = glob('/afs/cern.ch/user/s/snarayan/eos/cms/store/cmst3/group/monojet/mc/model4/MonoTop*0.25_1_801.root')
#filestoprocess = glob('/afs/cern.ch/user/s/snarayan/eos/cms/store/user/snarayan/monotop80/private/*root') # to be updated
eosPath = 'root://eoscms//eos'

scramdir=environ['CMSSW_BASE']
nPerJob=40
queue = '2nw4cores' 
#queue = '1nh'

nF=len(filestoprocess)
nJ = nF/nPerJob+1
for iJ in xrange(nJ):
  cfgpath = '%s/cfg/%i.cfg'%(environ['PANDA_LIMITDIR'],iJ)
  with open(cfgpath,'w') as cfgfile:
    for iF in xrange(iJ*nPerJob,min((iJ+1)*nPerJob,nF)):
      realpath = sub('/afs/cern.ch/user/s/snarayan/eos',eosPath,filestoprocess[iF])
      outname = sub('.root','',realpath.split('/')[-1])
      cfgfile.write('%s %s\n'%(realpath,outname))
  logpath = '/afs/cern.ch/work/s/%s/logs/limits_%i'%(environ['USER'],iJ)
  cmd = 'bsub -o %s.out -e %s.err -q %s run_limit_lxplus.sh %s'%(logpath,logpath,queue,cfgpath)
  print cmd
  system(cmd)
