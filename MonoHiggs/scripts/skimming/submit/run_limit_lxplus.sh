#!/bin/bash
cfgName=$1
WD=${PWD}
pwd

executable=makeLimitForestSignal.py

scramdir=/afs/cern.ch/user/s/snarayan/work/private/CMSSW_8_0_11/
cd ${scramdir}/src/
eval `scramv1 runtime -sh`
source MitPanda/Monotop/setup.sh
cd $WD

cp $cfgName local.cfg
cp ${scramdir}/src/MitPanda/Monotop/scripts/skimming/$executable .

echo ##############################
ls
echo ##############################

cat $cfgName | xargs -n 2 -P 4 python ${executable}

echo ##############################

cp *root ${PANDA_LIMITDIR}

rm local.cfg makeLimitForestSignal.py

exit 0
