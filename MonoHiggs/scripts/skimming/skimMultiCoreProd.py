#!/usr/bin/env python
from MitPanda.Tools.MultiThreading import GenericRunner
from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv
from os import path,getenv,system,environ
from json import load as loadJson


if __name__ == "__main__":
  
  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/NeroSkimmer.h")
  gSystem.Load('libMitPandaSkimmers.so')
  gSystem.Load('libNeroProducerCore.so')
  

  panda = getenv('CMSSW_BASE')+'/src/MitPanda/'

  def fn(shortName,longName,counter,isData):
    fullPath = longName
    skimmer = root.NeroSkimmer()
    
    isData=False
    skimmer.fromBambu=False
    skimmer.usePuppiMET=True;
    skimmer.set_ak4Jetslabel("puppi")
    #skimmer.set_ak4Jetslabel("puppi")
    skimmer.set_ca15Jetslabel("CA15Puppi")
#    skimmer.set_ak8Jetslabel("ak8")
#    skimmer.SetPreselectionBit(root.NeroSkimmer.kMonotopCA15)

    print fullPath
    skimmer.isData=isData
    skimmer.applyJson=True
    skimmer.processType=root.NeroSkimmer.kMonotopSignal
    
    skimmer.SetDataDir(panda+"/Monotop/data/")
    fin = root.TFile(fullPath)
    tree = fin.FindObjectAny("events")
    alltree = fin.Get('nero/all')
    skimmer.SetOutputFile('%s/split/%s_%i.root'%(environ['PANDA_PRODFLATDIR'],shortName,counter))
    skimmer.Init(tree,alltree)
    skimmer.Run(1)
    skimmer.Terminate()

  gr = GenericRunner(fn)
  argList = []
  counter=0
  cfg = open(panda+'/Monotop/config/%s.cfg'%(argv[1]))
  for line in cfg:
    ll = line.split()
    isData = not(ll[1]=='MC')
    argList.append([ll[0],ll[3],counter,isData])
    counter+=1
  gr.setArgList(argList)
  gr.run(14)

