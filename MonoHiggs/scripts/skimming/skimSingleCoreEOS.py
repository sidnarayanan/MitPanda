#!/usr/bin/env python

from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv
from os import system,getenv,path
from json import load as loadJson
import cPickle as pickle
from time import clock

nPerJob = int(argv[2])

if __name__ == "__main__":
  
  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/NeroSkimmer.h")
  gSystem.Load('libMitPandaSkimmers.so')
  gSystem.Load('libNeroProducerCore.so')

  with open('data/Cert_271036-274443_13TeV_PromptReco_Collisions16_JSON.txt') as jsonFile:
    json=loadJson(jsonFile)

  skimmer = root.NeroSkimmer()
  
  skimmer.usePuppiMET=True;
  skimmer.fromBambu=False;
  skimmer.set_ak4Jetslabel("puppi")
  skimmer.set_ca15Jetslabel("CA15Puppi")

  for run,lumi in json.iteritems():
    for l in lumi:
      for lll in xrange(l[0],l[1]+1):
        skimmer.AddLumi(int(run),int(lll))
  del json
  skimmer.applyJson=True

  skimmer.SetPreselectionBit(root.NeroSkimmer.kMonotopCA15)

  def fn(shortName,longName,counter,xsec,isData,outPath=None):
    if outPath and path.isfile('%s/%s_%i.root'%(outPath,shortName,counter)):
      print 'found %s/%s_%i.root, skipping!'%(outPath,shortName,counter)
      return
    start = clock()
    eosPath = 'root://eoscms//eos/cms/store/user/%s'%(getenv('USER'))
    cernboxPath = 'root://eosuser//eos/user/%s/%s'%(getenv('USER')[0],getenv('USER'))
    fullPath = sub(r'\${CERNBOX}',cernboxPath,sub(r'\${EOS}',eosPath,longName))
    print fullPath
    fin = root.TFile.Open(fullPath)
    tree = fin.FindObjectAny("events")
    alltree = fin.Get('nero/all')
    print 'opened input',clock()-start; start=clock()

    skimmer.isData=isData
    if not isData:
      processType=root.NeroSkimmer.kNone
      if 'ZJets' in fullPath or 'DY' in fullPath:
        processType=root.NeroSkimmer.kZ
      elif 'WJets' in fullPath:
        processType=root.NeroSkimmer.kW
      elif 'GJets' in fullPath:
        processType=root.NeroSkimmer.kA
      elif 'TTJets' in fullPath or 'TT_' in fullPath:
        processType=root.NeroSkimmer.kTT
      skimmer.processType=processType
    else:
      skimmer.processType=root.NeroSkimmer.kNone

    skimmer.SetDataDir("data/")
    print 'finished setup',clock()-start; start=clock()
    skimmer.SetOutputFile('%s_%i.root'%(shortName,counter))
    skimmer.Init(tree,alltree)
    skimmer.Run(1)
    skimmer.Terminate()
    print 'finished output',clock()-start;start=clock();
    if outPath:
      system('mv %s_%i.root %s'%(shortName,counter,outPath))
      print 'moved output',clock()-start; start=clock()
  
  which = int(argv[1])
  counter=0
  cfg = open('local.cfg')
  for line in cfg:
    if counter>=which*nPerJob and counter<(which+1)*nPerJob:
      ll = line.split()
      isData = not(ll[1]=='MC')
      if len(ll)==5:
        fn(ll[0],ll[3],int(ll[4]),float(ll[2]),isData,argv[3])
      else:
        fn(ll[0],ll[3],counter,float(ll[2]),isData,argv[3])
    counter+=1


