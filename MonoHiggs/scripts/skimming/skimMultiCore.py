#!/usr/bin/env python
from MitPanda.Tools.MultiThreading import GenericRunner
from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv
from os import path,getenv,system,environ
from json import load as loadJson


if __name__ == "__main__":
  
  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/NeroSkimmer.h")
  gSystem.Load('libMitPandaSkimmers.so')
  gSystem.Load('libNeroProducerCore.so')
  
  panda = getenv('PANDA')
  with open(panda+'/Monotop/data/Cert_246908-260627_13TeV_PromptReco_Collisions15_25ns_JSON_v2.txt') as jsonFile:
    json=loadJson(jsonFile)

  def fn(shortName,longName,counter,isData):
    fullPath = '%s/%s/%s'%(environ['PANDA_HISTDIR'],environ['PANDA_MITBOOK'],longName)
    skimmer = root.NeroSkimmer()
    
    skimmer.usePuppiMET=True;
    skimmer.set_ak4Jetslabel("puppi")
    skimmer.set_ca15Jetslabel("CA15Puppi")
#    skimmer.set_ak8Jetslabel("ak8")
#    skimmer.SetPreselectionBit(root.NeroSkimmer.kMonotopCA15)

    print fullPath
    skimmer.isData=isData
    skimmer.applyJson=True
    if not isData:
      processType=root.NeroSkimmer.kNone
      if 'ZJets' in fullPath or 'DY' in fullPath:
        processType=root.NeroSkimmer.kZ
      elif 'WJets' in fullPath:
        processType=root.NeroSkimmer.kW
      elif 'GJets' in fullPath:
        processType=root.NeroSkimmer.kA
      elif 'TTJets' in fullPath:
        processType=root.NeroSkimmer.kTT
      skimmer.processType=processType
    
    if isData:
      for run,lumi in json.iteritems():
        for l in lumi:
          for lll in xrange(l[0],l[1]+1):
            skimmer.AddLumi(int(run),int(lll))
      badEventsFile = open(panda+'/Monotop/data/metfilters/badEvents.pkl','rb')
      badEvents = pickle.load(badEventsFile)
      for e in badEvents:
        skimmer.AddBadEvent(e[0],e[1],e[2])


    skimmer.SetDataDir(panda+"/Monotop/data/")
    fin = root.TFile(fullPath)
    tree = fin.FindObjectAny("events")
    alltree = fin.Get('nero/all')
    skimmer.SetOutputFile('%s/split/%s_%i.root'%(environ['PANDA_FLATDIR'],shortName,counter))
    #skimmer.SetOutputFile('/local/snarayan/monotop/split/%s_%i.root'%(shortName,counter))
    skimmer.Init(tree,alltree)
    skimmer.Run(1)
    skimmer.Terminate()

  gr = GenericRunner(fn)
  argList = []
  counter=0
  cfg = open(panda+'/Monotop/config/%s.cfg'%(argv[1]))
  for line in cfg:
    print line
    ll = line.split()
    isData = not(ll[2]=='MC')
    argList.append([ll[0],ll[1],counter,isData])
    counter+=1
  gr.setArgList(argList)
  gr.run(6)

