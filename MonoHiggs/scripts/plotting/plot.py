#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv,exit
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.Selection import *
from MitPanda.Tools.Load import *

blind=False
totalUnblind=False

### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
#gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
#gSystem.Load('libMitPandaTools.so')
Load('Tools','PlotUtility')

### SET GLOBAL VARIABLES ###
region = argv[1]
nminusone = None
if len(argv)>2:
  nminusone = argv[2]
makePlots = True
#baseDir = '/local/snarayan/nero_pfchs_v3/'
#baseDir = '/local/snarayan/monotop_80/'
#baseDir = '/afs/cern.ch/work/s/snarayan/skims/monotop_80_v6/'
#dataBaseDir = '/afs/cern.ch/work/s/snarayan/skims/monotop_80_7fb/'
baseDir = '/afs/cern.ch/work/s/snarayan/skims/monotop_80_v9/'
dataBaseDir = '/afs/cern.ch/work/s/snarayan/skims/monotop_80_v9/'
lumi = 12918.
#lumi = 7706.
#lumi = 4336.

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
if nminusone==None:
  plot.Logy(True)
#plot.Logy(False)
if nminusone=='tau32':
  plot.SetMaxScale(2)
if not(blind and 'signal' in region):
  plot.Ratio(1) 
plot.SetLumi(lumi/1000)
#if 'signal' in region:
#  plot.SetEvtMod(3)
#  plot.SetLumi(lumi/3000)
plot.DrawMCErrors(True)
plot.SetTDRStyle()
plot.SetNormFactor(False)
plot.FixRatio(1.)
#if 'photon' not in region:
#  plot.FixRatio(1)
#plot.FixRatio(0.5 if 'photon' in region  else 1)
#plot.FixRatio(0)
#plot.FixRatio(True, 0.5 if region=='photon' else 1)
plot.AddCMSLabel()
plot.AddLumiLabel()

#label = region+'_MLM'
label = region
cut = cuts[region]
if nminusone!=None:
  if nminusone=='tau32':
    cut = removeCut(cut,'CA15fj1_tau3/CA15fj1_tau2')
  elif nminusone=='sjbtag':
    cut = removeCut(cut,'CA15fj1_maxsjbtag')
  elif nminusone=='mSD':
    cut = removeCut(cut,'CA15fj1_mSD')
  elif nminusone=='btag':
    cut = removeCut(cut,'jetNIsoBtags')
  elif nminusone=='allbtag':
    cut = removeCut(removeCut(cut,'jetNIsoBtags'),'CA15fj1_maxsjbtag')
  label = 'NMinus1_%s_%s'%(nminusone,label)

plot.SetCut(root.TCut(cut))

#if 'signal' in region or 'muon' in region:
#  triggerEff = "(((met<240)*(0.975+(met-200)*0.000625))+(met>240))"
#else:
#  triggerEff = '1'

weight = '%f*normalizedWeight'%lumi
if 'signal' in region:
  weight = weights['signal']%lumi
elif 'single' in region and 'w' in region:
  weight = weights['w']%lumi
elif ('single' in region and 'top' in region) or region=='nobtag':
  weight = weights['top']%lumi
elif 'di' in region or 'photon' in region:
  weight = weights['notag']%lumi
  if 'photon' in region:
    weight = tTIMES(weight,photonSF)
if 'electron' in region:
  weight = tTIMES(weight,'sf_eleTrig')
elif 'photon' in region:
  weight = tTIMES(weight,'sf_phoTrig')
else:
  weight = tTIMES(weight,'sf_metTrig')
plot.SetMCWeight(weight)
#if not contains(region,['di','photon']):
#  plot.AddSyst(weight.replace('btag0','btag0BUp').replace('btag1','btag1BUp'),weight.replace('btag0','btag0BDown').replace('btag1','btag1BDown'),'b-tag',root.kBlue)
#  plot.AddSyst(weight.replace('btag0','btag0MUp').replace('btag1','btag1MUp'),weight.replace('btag0','btag0MDown').replace('btag1','btag1MDown'),'b-mistag',root.kRed)

### DEFINE PROCESSES ###
zjets     = root.Process('Z+jets',root.kZjets)
wjets     = root.Process('W+jets',root.kWjets)
diboson   = root.Process('Diboson',root.kDiboson)
if nminusone==None:
  ttbar     = root.Process('t#bar{t}',root.kTTbar)
else:
  ttbar     = root.Process('t#bar{t} [matched]',root.kTTbar); ttbar.additionalCut = root.TCut('CA15fj1_isMatched==1')
  ttbarunmatched     = root.Process('t#bar{t} [unmatched]',root.kExtra1); ttbarunmatched.additionalCut = root.TCut('CA15fj1_isMatched==0')
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
gjets     = root.Process('#gamma+jets',root.kGjets)
signal    = root.Process("Signal",root.kSignal)
data      = root.Process("Data",root.kData)
if nminusone==None:
  processes = [qcd,diboson,singletop,wjets,ttbar,zjets]
else:
  processes = [qcd,diboson,singletop,wjets,ttbarunmatched,ttbar,zjets]

### ASSIGN FILES TO PROCESSES ###
if 'signal' in region:
  zjets.AddFile(baseDir+'ZtoNuNu.root')
else:
#  zjets.AddFile(baseDir+'ZJets_nlo.root')
  zjets.AddFile(baseDir+'ZJets.root')
#wjets.AddFile(baseDir+'WJets_nlo.root')
wjets.AddFile(baseDir+'WJets.root')
diboson.AddFile(baseDir+'Diboson.root')
ttbarfile = 'TTbar_Powheg.root'
ttbar.AddFile(baseDir+ttbarfile)
if nminusone!=None:
  ttbarunmatched.AddFile(baseDir+ttbarfile)
singletop.AddFile(baseDir+'SingleTop.root')
if contains(region,"photon"):
  qcd.AddFile(dataBaseDir+'SinglePhoton.root')
  qcd.useCommonWeight=False
  qcd.additionalWeight = root.TCut('photonPurityWeight')
  qcd.additionalCut = root.TCut(tAND(metFilter,phoTrigger))
  gjets.AddFile(baseDir+'GJets.root')
  processes = [qcd,gjets]
else:
  qcd.AddFile(baseDir+'QCD.root')

if contains(region,"photon"):
  #data.AddFile(dataBaseDir+"SinglePhoton_ext.root") ; data.additionalCut = root.TCut(tAND(metFilter,phoTrigger))
  data.AddFile(dataBaseDir+"SinglePhoton.root") ; data.additionalCut = root.TCut(tAND(metFilter,phoTrigger))
elif contains(region,"electron"):
  #data.AddFile(dataBaseDir+"SingleElectron_ext.root"); 
  data.AddFile(dataBaseDir+"SingleElectron.root"); 
  if 'di' in region:
    data.additionalCut = root.TCut(tAND(metFilter,tOR(eleTrigger,phoTrigger)))
  else:
    data.additionalCut = root.TCut(tAND(metFilter,eleTrigger))
else:
  #data.AddFile(dataBaseDir+'MET.root') ;  data.additionalCut = root.TCut(tAND(metFilter,metTrigger))
  data.AddFile(dataBaseDir+'MET.root') ;  data.additionalCut = root.TCut(metTrigger)
processes.append(data)

for p in processes:
  plot.AddProcess(p)

#recoilBins = [250,275,300,325,350,375,400,425,450,475,500,600,700,800,1000]
#recoilBins = [250,300,350,400,500,1000]
recoilBins = [250,280,310,350,400,450,600,1000]
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
# define what "recoil" means
if contains(region,"electron"):
  lep='e'
else:
  lep='#mu'

recoil=None
recoilvar=None
if nminusone==None:
#  plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
  if 'signal' in region or region=='qcdtest':
    recoil=root.Distribution("puppimet",nRecoilBins,"Puppi MET [GeV]","Events/GeV")
    pfrecoil=root.Distribution("met",nRecoilBins,"MET [GeV]","Events/GeV")
    recoilvar = 'puppimet'
  elif 'photon' in region:
    recoil=root.Distribution("UAmag",nRecoilBins,"Recoil (#gamma) [GeV]","Events/GeV")
    pfrecoil=root.Distribution("pfUAmag",nRecoilBins,"PF Recoil (#gamma) [GeV]","Events/GeV")
    recoilvar = 'UAmag'
  elif 'di' in region:
    recoil=root.Distribution("UZmag",nRecoilBins,"Recoil (%s%s) [GeV]"%(lep,lep),"Events/GeV")
    pfrecoil=root.Distribution("pfUZmag",nRecoilBins,"PF Recoil (%s%s) [GeV]"%(lep,lep),"Events/GeV")
    recoilvar = 'UZmag'
  elif ('single' in region and 'w' in region) or region=='test':
    recoil=root.Distribution("UWmag",nRecoilBins,"Recoil (%s) [GeV]"%(lep),"Events/GeV")
    pfrecoil=root.Distribution("pfUWmag",nRecoilBins,"PF Recoil (%s) [GeV]"%(lep),"Events/GeV")
    recoilvar = 'UWmag'
  elif 'single' in region and 'top' in region:
    recoil=root.Distribution("UWmag",nRecoilBins,"Recoil (%s) [GeV]"%(lep),"Events/GeV")
    pfrecoil=root.Distribution("pfUWmag",nRecoilBins,"PF Recoil (%s) [GeV]"%(lep),"Events/GeV")
    recoilvar = 'UWmag'
if recoil and not(blind and 'signal' in region) and not totalUnblind:
  #print region
  #plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
  plot.AddDistribution(root.Distribution("npv",0,40,40,"N_{PV}","Events",0.1))
  for r in [recoil,pfrecoil]:
    setBins(r,recoilBins)
    plot.AddDistribution(r)
  #plot.AddDistribution(root.Distribution("nJet",-0.5,10.5,11,"N(AK4 jets)","Events",0.1)) 
  if ('electron' in region or 'muon' in region):
    plot.AddDistribution(root.Distribution("looseLep1Pt",30,930,30,"leading lepton p_{T} [GeV]","Events/30 GeV",0.1)) 
    plot.AddDistribution(root.Distribution("looseLep1Eta",-5,5,20,"leading lepton #eta","Events",0.1)) 
  if 'photon' in region:
    plot.AddDistribution(root.Distribution("loosePho1Pt",100,1500,20,"leading photon p_{T}","Events",0.1)) 
  plot.AddDistribution(root.Distribution("CA15fj1_pt",250,1000,30,"fat jet p_{T} [GeV]","Events/25 GeV",0.1)) 
  plot.AddDistribution(root.Distribution("jet1Pt",0,1000,40,"leading AK4 jet p_{T} [GeV]","Events/25 GeV",0.1)) 
  '''
  if recoilvar:
    if 'signal' in region:
      plot.AddDistribution(root.Distribution("TMath::Abs(met-calomet)/%s"%(recoilvar),0,1,20,"|PFMET-caloMET|/MET","Events",999,-999,"pfcalobalance"))
    else:
      plot.AddDistribution(root.Distribution("TMath::Abs(met-calomet)/%s"%(recoilvar),0,1,20,"|PFMET-caloMET|/Recoil","Events",999,-999,"pfcalobalance"))
  '''

plotvars = ((nminusone==None) and not('test' in region) and not(blind and 'signal' in region) and not totalUnblind) or ('base' in region)
plotvars=False

if nminusone=="tau32" or plotvars:
  plot.AddDistribution(root.Distribution("CA15fj1_tau3/CA15fj1_tau2",0,1.2,12,"#tau_{3}/#tau_{2}","Events",0.5))
if nminusone=="sjbtag" or nminusone=='allbtag' or plotvars:
  plot.AddDistribution(root.Distribution("CA15fj1_maxsjbtag",0,1,20,"max subjet b-tag","Events/0.05",0.1))
if nminusone=="mSD" or plotvars:
  plot.AddDistribution(root.Distribution("CA15fj1_mSD",40,440,20,"soft drop mass [GeV]","Events/20 GeV"))
if nminusone=="btag" or nminusone=='allbtag' or plotvars:
  plot.AddDistribution(root.Distribution("jetNIsoBtags",-0.5,3.5,4,"AK4 btags","Events"))

### DRAW AND CATALOGUE ###
plot.DrawAll('~/www/figs/monotop_2016_13fb/rebinning160726/'+label+'_')

