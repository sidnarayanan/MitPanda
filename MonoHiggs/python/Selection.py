from MitPanda.Tools.Misc import *
from re import sub


metTrigger='(trigger&1)!=0'
eleTrigger='(trigger&2)!=0'
phoTrigger='(trigger&4)!=0'
metFilter='metFilter==1'
newMetFilter='newMetFilter==1'
topTagSF = '%f*(CA15fj1_isMatched==1)+%f*(CA15fj1_isMatched==0)'%(1.007,1.02)
ak4bTagSF = 'sf_btag0*(jetNIsoBtags==0)+sf_btag1*(jetNIsoBtags==1)+1*(jetNIsoBtags>1)'
photonSF = '0.93'


presel = 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_tau3/CA15fj1_tau2<0.61 && 110<CA15fj1_mSD && CA15fj1_mSD<210'
cuts = {
    # analysis regions
    'signal'            : tAND(metFilter,tAND(presel,'met>175 && puppimet>250 && dPhiPuppiMET>1.1 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==0 && CA15fj1_isTight==1 && TMath::Abs(met-calomet)/puppimet<0.5')),
    'signal_nomf'       : tAND(presel,'met>175 && puppimet>250 && dPhiPuppiMET>1.1 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==0 && CA15fj1_isTight==1 && TMath::Abs(met-calomet)/puppimet<0.5'),
    'singlemuontop'     : tAND(metFilter,tAND(presel,'UWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==1')),
    'singleelectrontop' : tAND(metFilter,tAND(presel,'UWmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==1 && puppimet>40')),
    'singlemuonw'       : tAND(metFilter,tAND(presel,'UWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag<0.46 && jetNIsoBtags==0')),
    'singleelectronw'   : tAND(metFilter,tAND(presel,'UWmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag<0.46 && jetNIsoBtags==0 && puppimet>40')),
    'dimuon'            : tAND(metFilter,tAND(presel,'UZmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==2 && looseLep1IsTight==1')),
    'dielectron'        : tAND(metFilter,tAND(presel,'UZmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==2 && looseLep1IsTight==1')),
    'photon'            : tAND(metFilter,tAND(presel,'UAmag>250 && (nLooseMuon+nLooseElectron+nTau)==0 && nLoosePhoton==1 && loosePho1IsTight==1')),
    # test selections
    'signal_nocalomet'            : tAND(presel,'met>175 && puppimet>250 && dPhiPuppiMET>1.1 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==0 && CA15fj1_isTight==1'),
    'qcdtest'           : 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && met>175 && puppimet>250 && dPhiPuppiMET<0.5 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0',
    'signal_nodphi'     : tAND(presel,'met>175 && puppimet>250 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==0'),
    'singleelectron'    : tAND(presel,'UWmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==1 && looseLep1IsTight==1 && puppimet>40 && TMath::Abs(met-calomet)/UWmag<0.5'),
    'singlemuon'        : tAND(presel,'UWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && TMath::Abs(met-calomet)/UWmag<0.5'),
    'test'              : 'nCA15fj==1 && CA15fj1_pt>250 && nLooseMuon==1 && looseLep1IsTight==1 && (nLooseElectron+nLoosePhoton+nTau)==0 && UWmag>250',
    'nobtag'            : 'nCA15fj==1 && CA15fj1_pt>250 && nLooseMuon==1 && looseLep1IsTight==1 && (nLooseElectron+nLoosePhoton+nTau)==0 && UWmag>250',
    'singlemuontop_base'     : removeCut(removeCut(tAND(presel,'UWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==1'),'CA15fj1_tau3/CA15fj1_tau2'),'CA15fj1_mSD'),
    'singleelectrontop_base' : removeCut(removeCut(tAND(presel,'UWmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==1 && puppimet>40'),'CA15fj1_tau3/CA15fj1_tau2'),'CA15fj1_mSD'),
    'singlemuon_base'   : tAND('CA15fj1_mSD>50',removeCut(removeCut(tAND(presel,'UWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && puppimet>40'),'CA15fj1_tau3/CA15fj1_tau2'),'CA15fj1_mSD')),
    'photon_base'       : removeCut(removeCut(tAND(presel,'UAmag>250 && (nLooseMuon+nLooseElectron+nTau)==0 && nLoosePhoton==1 && loosePho1IsTight==1'),'CA15fj1_tau3/CA15fj1_tau2'),'CA15fj1_mSD'),
}

tag_presel = removeCut(removeCut(tOR(cuts['singlemuontop'],cuts['singleelectrontop']),'CA15fj1_tau3/CA15fj1_tau2'),'CA15fj1_mSD')
mistag_presel = tAND(removeCut(removeCut(cuts['photon'],'CA15fj1_tau3/CA15fj1_tau2'),'CA15fj1_mSD'),'CA15fj1_mSD>40')
tag = 'CA15fj1_tau3/CA15fj1_tau2<0.61 && 110<CA15fj1_mSD && CA15fj1_mSD<210'
tt_cuts = {
  'tag' : tag_presel,
  'tag_pass' : tAND(tag,tag_presel),
  'tag_fail' : tAND(tNOT(tag),tag_presel),
  'mistag' : mistag_presel,
  'mistag_pass' : tAND(tag,mistag_presel),
  'mistag_fail' : tAND(tNOT(tag),mistag_presel),
}

'''
  'signal'    : tTIMES(tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_sjbtag1*sf_lepTrack',topTagSF),ak4bTagSF),
  'top'       : tTIMES(tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_sjbtag1*sf_lepTrack',topTagSF),ak4bTagSF),
  'w'         : tTIMES(tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_sjbtag0*sf_lepTrack',topTagSF),ak4bTagSF),
  'notag'     : tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_lepTrack',topTagSF),
  'signal_sf' : tTIMES(tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_sjbtag1',topTagSF),ak4bTagSF),
  'top_sf'    : tTIMES(tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_sjbtag1',topTagSF),ak4bTagSF),
  'w_sf'      : tTIMES(tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_sjbtag0',topTagSF),ak4bTagSF),
'''

weights = {
    # analysis weights
  'signal'    : tTIMES(tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_sjbtag1',topTagSF),ak4bTagSF),
  'top'       : tTIMES(tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_lepTrack*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_sjbtag1',topTagSF),ak4bTagSF),
  'w'         : tTIMES(tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_lepTrack*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_sjbtag0',topTagSF),ak4bTagSF),
  'notag'     : tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_lepTrack*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt',topTagSF),
    # testing weights
  'test_sf'   : tTIMES('%f*normalizedWeight*sf_lep*puWeight*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*(sf_btag1*(jetNIsoBtags==1)+sf_btag0*(jetNIsoBtags==0)+1*(jetNIsoBtags>=2))',topTagSF),
  'top_nosf'  : tTIMES('%f*normalizedWeight*sf_lep*puWeight*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt',topTagSF),
  'w_nosf'    : tTIMES('%f*normalizedWeight*sf_lep*puWeight*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt',topTagSF),
  'signal_csv': tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_newBtag*sf_newSjBtag',topTagSF),
  'top_csv'   : tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_newBtag*sf_newSjBtag',topTagSF),
  'test'      : tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_newBtag*sf_newSjBtag',topTagSF),
  'w_csv'     : tTIMES('%f*normalizedWeight*puWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_newBtag*sf_newSjBtag',topTagSF),
}


for r in ['signal','top','w']:
  for shift in ['BUp','BDown','MUp','MDown']:
    for cent in ['sf_btag','sf_sjbtag']:
      weights[r+'_'+cent+shift] = sub(cent+'0',cent+'0'+shift,sub(cent+'1',cent+'1'+shift,weights[r]))
