#!/usr/bin/env python

from ROOT import gSystem, gROOT
import ROOT as root
from array import array
from glob import glob
from re import sub
from sys import argv
from os import environ,system
from MitPanda.Tools.process import *

gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/Normalizer.h")
gSystem.Load('libMitPandaTools.so')

pds = {}
for k,v in processes.iteritems():
  if v[1]=='MC':
    pds[v[0]] = (k,v[2])  
  else:
    pds[v[0]] = (k,-1)

VERBOSE=False

user = environ['USER']
system('mkdir -p /tmp/%s/split'%user) # tmp dir
system('mkdir -p /tmp/%s/merged'%user) # tmp dir

base = environ['PANDA_PRODFLATDIR']
inbase = base+'/split/'

def hadd(inpath,outpath):
  if type(inpath)==type('str'):
    infiles = glob(inpath)
    print 'hadding %s into %s'%(inpath,outpath)
    cmd = 'hadd -ff -n 100 -f %s %s > /dev/null'%(outpath,inpath)
    #cmd = 'hadd -n 100 -f %s %s > /dev/null'%(outpath,inpath)
    system(cmd)
    return
  else:
    infiles = inpath
  if len(infiles)==0:
    print 'WARNING: nothing hadded into',outpath
    return
  elif len(infiles)==1:
    cmd = 'cp %s %s'%(infiles[0],outpath)
  else:
    cmd = 'hadd -n 100 -f %s '%outpath
    for f in infiles:
      if path.isfile(f):
        cmd += '%s '%f
  if VERBOSE: print cmd
  system(cmd+' >/dev/null 2>/dev/null')

def normalize(fpath,opt):
  xsec=-1
  if type(opt)==type(1.) or type(opt)==type(1):
    xsec = opt
  else:
    try:
      xsec = processes[proc][2]
    except KeyError:
      for k,v in processes.iteritems():
        if proc in k:
          xsec = v[2]
  if xsec<0:
    print 'could not find xsec, skipping %s!'%opt
    return
  print 'normalizing %s (%s) ...'%(fpath,opt)
  nw = array('f',[0])
  w = array('f',[0])
  fin = root.TFile(fpath,'UPDATE')
  tree = fin.Get('events')
  total = fin.Get('hDTotalMCWeight').Integral()
  b = tree.Branch('normalizedWeight',nw,'normalizedWeight/F')
  tree.SetBranchAddress('mcWeight',w)
  nEntries = tree.GetEntries()
  for iE in xrange(nEntries):
    tree.GetEntry(iE)
    nw[0] = xsec*w[0]/total
    b.Fill()
  fin.WriteTObject(tree)
  fin.Close()

def normalizeFast(fpath,opt):
  xsec=-1
  if type(opt)==type(1.) or type(opt)==type(1):
    xsec = opt
  else:
    try:
      xsec = processes[proc][2]
    except KeyError:
      for k,v in processes.iteritems():
        if proc in k:
          xsec = v[2]
  if xsec<0:
    print 'could not find xsec, skipping %s!'%opt
    return
  print 'fast normalizing %s (%s) ...'%(fpath,opt)
  n = root.Normalizer();
  n.NormalizeTree(fpath,xsec)

def merge(shortnames,mergedname):
  for shortname in shortnames:
    try:
      pd = pds[shortname][0]
      xsec = pds[shortname][1]
    except KeyError:
      try:
        shortname_ = shortname.split('_')[0]
        pd = pds[shortname_][0]
        xsec = pds[shortname_][1]
      except KeyError:
        pd = shortname
    xsec = 1
    '''
    inpath = ''
    for i in xrange(4):
      q = '[0-9]'*(i+1)
      inpath += inbase+shortname+'_'+q+'.root '
    '''
    inpath = inbase+shortname+'_*.root'
    hadd(inpath,'/tmp/%s/split/%s.root'%(user,shortname))
    if xsec>0:
      normalizeFast('/tmp/%s/split/%s.root'%(user,shortname),xsec)
  hadd(['/tmp/%s/split/%s.root'%(user,x) for x in shortnames],'/tmp/%s/merged/%s.root'%(user,mergedname))


pd = argv[1]
merge([pd],pd)
system('cp -r /tmp/%s/merged/%s.root %s'%(user,pd,base))
print 'finished with',pd

