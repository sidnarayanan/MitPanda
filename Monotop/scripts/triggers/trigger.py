#! /usr/bin/env python                                                                                                                                                                                                                   
import sys, os, string, re, time, datetime
from multiprocessing import Process
from array import *

from ROOT import *
from math import *
from tdrStyle import *
#from pretty import plot_ratio, plot_cms

import numpy as n

photon =False
met = False

setTDRStyle()

gROOT.LoadMacro("functions.C+");

#0 HLT_Ele25_eta2p1_WPTight_Gsf_v,
#1 HLT_Ele27_eta2p1_WPLoose_Gsf_v,
#2 HLT_Ele27_WPTight_Gsf_v,
#3 HLT_Ele35_WPLoose_Gsf_v,
#4 HLT_Ele105_CaloIdVT_GsfTrkIdT_v,
#5 HLT_ECALHT800_v,
#6 HLT_IsoTkMu22_v,
#7 HLT_IsoMu22_v,
#8 HLT_Mu50_v,
#9 HLT_PFMET170_NoiseCleaned_v,
#10 HLT_PFMET170_NotCleaned_v,
#11 HLT_PFMETNoMu90_PFMHTNoMu90_IDTight_v,
#12 HLT_PFMETNoMu100_PFMHTNoMu100_IDTight_v,
#13 HLT_PFMETNoMu110_PFMHTNoMu110_IDTight_v,
#14 HLT_PFMETNoMu120_PFMHTNoMu120_IDTight_v,
#15 HLT_Photon175_v,
#16 HLT_Photon165_HE10_v,
#17 HLT_Photon120_R9Id90_HE10_Iso40_EBOnly_PFMET40_v,
#18 HLT_Photon135_PFMET100_v,
#19 HLT_Photon300_NoHE_v,
#20 HLT_PFJet320_v,
#21 HLT_PFJet400_v,
#22 HLT_PFHT400_SixJet30_DoubleBTagCSV_p056_v,
#23 HLT_PFHT450_SixJet40_BTagCSV_p056_v2,
#24 HLT_PFHT750_4JetPt50_v,
#25 HLT_PFHT650_v,
#26 HLT_PFHT800_v,
#27 HLT_AK8DiPFJet280_200_TrimMass30_BTagCSV_p20_v,
#28 HLT_AK8PFHT600_TrimR0p1PT0p03Mass50_BTagCSV_p20_v,

if photon:
    #fout = ROOT.TFile("pho_trig_recovery.root","recreate")
    fout = ROOT.TFile("pho_trig_jetonly.root","recreate")
elif met:
    fout = ROOT.TFile("met.root","recreate")
else:
    fout = ROOT.TFile("ele_trig_barrel.root","recreate")
    #fout = ROOT.TFile("ele_trig_barrel_recovery.root","recreate")

#bin = 15
bins = array('f',[0,100,200,300,400,500,700,1000])
nbins = len(bins)-1
bin = 25
#bin = 50
low = 0
high = 1000

#fdir = "/afs/cern.ch/work/z/zdemirag/public/ichep/setup80x/Skim_v7/"
#fdir = "/tmp/zdemirag/Skim_v6/"

fdir = "~/eos/cms/store/user/zdemirag/setup80x/Skim_v9/"

if photon:
    f = ROOT.TFile(fdir+"monojet_JetHT.root","read")
    tree = f.Get("events")
elif met:
    f = ROOT.TFile(fdir+"monojet_SingleElectron.root","read")
    tree = f.Get("events")
else:
    f = ROOT.TFile(fdir+"monojet_JetHT.root","read")
    tree = f.Get("events")    

#h_denom = TH1F('h_denom', 'h_denom', bin, low, high)
#h_num   = TH1F('h_num'  , 'h_num'  , bin, low, high)
#h_eff2  = TH1F('h_eff', 'h_eff', bin, low, high)

h_denom = TH1F('h_denom', 'h_denom', nbins, bins)
h_num   = TH1F('h_num'  , 'h_num'  , nbins, bins)
h_eff2  = TH1F('h_eff', 'h_eff', nbins, bins)

h_denom.Sumw2()
h_num.Sumw2()
h_eff2.Sumw2()

'''
h_denom_s = TH1F('h_denom_s', 'h_denom_s', bin, low, high)
h_num_s   = TH1F('h_num_s'  , 'h_num_s'  , bin, low, high)
'''

h_denom_s = TH1F('h_denom_s', 'h_denom_s', nbins, bins )
h_num_s   = TH1F('h_num_s'  , 'h_num_s'  , nbins, bins )

h_denom_s.Sumw2()
h_num_s.Sumw2()

h_eff   = ROOT.TGraphAsymmErrors()
h_eff_s = ROOT.TGraphAsymmErrors()

if photon:

    common_cuts = "photonPt > 15 && abs(photonEta) < 1.4442 && n_mediumpho == 1 && jet1Pt>300"

    #big_OR = "(triggerFired[16] || triggerFired[18] || triggerFired[19] || triggerFired[17] || triggerFired[15] || triggerFired[5] || triggerFired[26])"
    big_OR = "(triggerFired[16] || triggerFired[18] || triggerFired[19] || triggerFired[17] || triggerFired[15])"
    jetOR = "(triggerFired[20])"# || triggerFired[25] || triggerFired[26])"

    denocuts = common_cuts + "&&" + jetOR
    numcuts  = common_cuts + "&&" + jetOR + "&&" + big_OR

    ###
    denocuts_s = "photonPt > 15 && abs(photonEta) < 1.4442 && n_mediumpho == 1 && "+jetOR+" && jet1Pt>300"
    numcuts_s  = "photonPt > 15 && abs(photonEta) < 1.4442 && n_mediumpho == 1 && "+jetOR+"&& jet1Pt>300 &&"+ big_OR 


    tree.Draw("photonPt>>h_denom",denocuts,"goff")
    tree.Draw("photonPt>>h_num"  ,numcuts ,"goff")

    tree.Draw("photonPt>>h_denom_s",denocuts_s,"goff")
    tree.Draw("photonPt>>h_num_s"  ,numcuts_s ,"goff")

elif met:

    big_OR = "(triggerFired[11] || triggerFired[12] || triggerFired[13] || triggerFired[14] || triggerFired[9] || triggerFired[10])"
    jetOR  = "(triggerFired[1] || triggerFired[2] || triggerFired[3])"
  
    common_cuts = "lep1Pt>40. && n_tightlep>0 && jet1Pt>100. && metfilter==1 && leadingJet_outaccp==0 && jet1isMonoJetIdNew==1 && abs(jet1Eta)<2.5 && abs(minJetMetDPhi_withendcap) > 0.5 && filterbadChCandidate && filterbadPFMuon && (abs(caloMet-trueMet)/met) < 0.5"
    
    denocuts = common_cuts + "&&" + jetOR
    numcuts  = common_cuts + "&&" + jetOR + "&&" + big_OR

    tree.Draw("trueMet>>h_denom",denocuts,"goff")
    tree.Draw("trueMet>>h_num"  ,numcuts ,"goff")

else:
    common1 = "met > 50 &&lep1Pt > 40 && abs(lep1Eta) >= 1.4442 && n_tightlep == 1 && abs(lep1PdgId)==11"
    common2 = "met > 50 &&lep1Pt > 40 && abs(lep1Eta) <  1.4442 && n_tightlep == 1 && abs(lep1PdgId)==11"

    #big_OR = "(triggerFired[0] || triggerFired[1] || triggerFired[2] || triggerFired[3] || triggerFired[4] || triggerFired[5] || triggerFired[26])"
    big_OR = "(triggerFired[0] || triggerFired[1] || triggerFired[2] || triggerFired[3] || triggerFired[4])"
    jetOR = "(triggerFired[20] || triggerFired[25] || triggerFired[26])"
    #jetOR = "(triggerFired[20] || triggerFired[25])"

    denocuts_s = common1 + "&& "+jetOR
    numcuts_s  = common1 + "&& "+jetOR + "&& "+big_OR

    denocuts = common2 + "&& "+jetOR
    numcuts  = common2 + "&& "+jetOR + "&& "+big_OR

    tree.Draw("lep1Pt>>h_denom",denocuts,"goff")
    tree.Draw("lep1Pt>>h_num"  ,numcuts ,"goff")

    tree.Draw("photonPt>>h_denom_s",denocuts_s,"goff")
    tree.Draw("photonPt>>h_num_s"  ,numcuts_s ,"goff")


print h_denom.Integral() , h_num.Integral()

c1 = TCanvas("c1","c1",800,800);
c1.cd();

#dummy = TH1F("dummy","",bin, low, high)
dummy = TH1F("dummy","",nbins,bins)
dummy.SetBinContent(1,0.0);
dummy.SetLineColor(0);
dummy.SetMarkerColor(0);
dummy.SetLineWidth(0);
dummy.SetMarkerSize(0);
dummy.GetYaxis().SetTitle("Efficiency");
if photon:
    dummy.GetXaxis().SetTitle("Photon p_{T} [GeV]");
elif met:
    dummy.GetXaxis().SetTitle("E_{T}^{miss} [GeV]");
else:
    dummy.GetXaxis().SetTitle("Electron p_{T} [GeV]");
if photon:
    dummy.SetMinimum(0.01);
    dummy.SetMaximum(1.25);
else:
    dummy.SetMinimum(0.01);
    dummy.SetMaximum(1.25);


dummy.Draw("hist");

h_eff.BayesDivide(h_num,h_denom,"");
h_eff.SetLineWidth(2);
h_eff.SetLineColor(2);
h_eff.SetMarkerColor(2);
h_eff.SetMarkerStyle(20);
h_eff.SetMarkerSize(1.2);
h_eff.Draw("psame");

fout.cd()
h_eff2 = h_num
h_eff2.Divide(h_denom)
h_eff2up = h_num.Clone()
h_eff2down = h_num.Clone()
for iB in xrange(1,h_eff2.GetNbinsX()+1):
  up = h_eff.GetErrorYhigh(iB-1)
  down = h_eff.GetErrorYlow(iB-1)
  cent = h_eff2.GetBinContent(iB)
  h_eff2up.SetBinContent(iB,cent+up)
  h_eff2down.SetBinContent(iB,cent-down)

fout.WriteTObject(h_eff2,'h_num')
fout.WriteTObject(h_eff2up,'h_num_up')
fout.WriteTObject(h_eff2down,'h_num_down')
fout.WriteTObject(h_eff,'g_num')

if not met and not photon:
    print 'endcap'
    h_eff_s.BayesDivide(h_num_s,h_denom_s,"");
    h_eff_s.SetLineWidth(2);
    h_eff_s.SetLineColor(4);
    h_eff_s.SetMarkerColor(4);
    h_eff_s.SetMarkerStyle(20);
    h_eff_s.SetMarkerSize(1.2);
    h_eff_s.Draw("psame");

    h_eff2_s = h_num_s
    h_eff2_s.Divide(h_denom_s)

    h_eff2up_s = h_num_s.Clone()
    h_eff2down_s = h_num_s.Clone()
    for iB in xrange(1,h_eff2_s.GetNbinsX()+1):
      up = h_eff_s.GetErrorYhigh(iB-1)
      down = h_eff_s.GetErrorYlow(iB-1)
      cent = h_eff2_s.GetBinContent(iB)
      h_eff2up_s.SetBinContent(iB,cent+up)
      h_eff2down_s.SetBinContent(iB,cent-down)


    fout.WriteTObject(h_eff2_s,'h_num_endcap')
    fout.WriteTObject(h_eff2up_s,'h_num_up_endcap')
    fout.WriteTObject(h_eff2down_s,'h_num_down_endcap')
    fout.WriteTObject(h_eff_s,'g_num_endcap')

    leg = TLegend(.6,.2,.9,.45)
    leg.AddEntry(h_eff,'Barrel','ple')
    leg.AddEntry(h_eff_s,'Endcap','ple')
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.Draw()

fout.Close()
    
f1 = TF1("f1","1",0,1000);
f1.SetLineColor(kGreen+2);
f1.SetLineStyle(2);
f1.SetLineWidth(2);
f1.Draw("same");

#plot_cms(True,'7.63',c1)

folder = '/afs/cern.ch/user/s/snarayan/www/figs/monotop_2016_13fb/triggers/'

if photon:
    #c1.SaveAs(folder+"/photon_trig_recovery.pdf")
    #c1.SaveAs(folder+"/photon_trig_recovery.png")
    #c1.SaveAs(folder+"/photon_trig_recovery.C")
    c1.SaveAs(folder+"/photon_trig.pdf")
    c1.SaveAs(folder+"/photon_trig.png")
    c1.SaveAs(folder+"/photon_trig.C")

elif met:
    c1.SaveAs(folder+"/met_trig.pdf")
    c1.SaveAs(folder+"/met_trig.png")
    c1.SaveAs(folder+"/met_trig.C")

else:
    #c1.SaveAs(folder+"/electron_trig.pdf")
    #c1.SaveAs(folder+"/electron_trig.png")
    #c1.SaveAs(folder+"/electron_trig.C")

    c1.SaveAs(folder+"/electron_trig.pdf")
    c1.SaveAs(folder+"/electron_trig.png")
    c1.SaveAs(folder+"/electron_trig.C")

