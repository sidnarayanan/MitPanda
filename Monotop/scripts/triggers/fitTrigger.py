#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from sys import argv,exit
from array import array
from MitPanda.Tools.Misc import *
from math import sqrt
from ROOT import gROOT

gROOT.LoadMacro('triggerFunc.C')
fitFunc = root.TF1('fitFunc',root.linear,100,1000,2)
initParams = [1,0]
for iP in xrange(2):
  fitFunc.SetParameter(iP,initParams[iP])

gROOT.LoadMacro('${CMSSW_BASE}/src/MitPanda/Tools/interface/CanvasDrawer.h')
gSystem.Load('libMitPandaTools.so')

plot = root.CanvasDrawer()
plot.SetTDRStyle()
plot.AddCMSLabel()

counter=0

fIn = root.TFile('ele_trig_barrel.root')
geff = fIn.Get('g_num')
hone = root.TH1F('hone','hone',1,100,1000)
for iB in xrange(1,hone.GetNbinsX()+1):
  hone.SetBinContent(iB,1.)
hone.GetXaxis().SetTitle('electron p_{T} [GeV]')
hone.GetYaxis().SetTitle('Efficiency')
hone.SetMaximum(1.2); hone.SetMinimum(0)
hone.SetLineStyle(2)


result = geff.Fit(fitFunc,'S','',100,1000)
print result

fitFunc.SetLineColor(root.kBlack)

plot.ClearCanvas()
plot.cd()

hone.Draw('hist')
geff.Draw('psame')
fitFunc.Draw('same')

for iP in xrange(2):
  print 'par[%i] = %.3g +/- %.3g'%(iP,fitFunc.GetParameter(iP),fitFunc.GetParError(iP))
print 'P(fit) = %.3g'%(result.Prob())
print 'chi2= %.3g'%(result.Chi2())

plot.Draw('~/www/figs/monotop_2016_13fb/triggers/fits/','barrel')
