#!/usr/bin/env python

#baseDir = '/home/snarayan/cms/root/monotop25ns_v2/'
baseDir = '/local/snarayan/monotop25ns_v2/'
lumi = 1200.

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv

#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

region = argv[1]

try:
  additionalcut = argv[2]
except:
  additionalcut = None

cuts = {}
cuts['res'] = 'nAK8fj==0 && nCA15fj==0 && puppimet>120 && nJet>=3 && nJet<6 && (nTightElectron+nTightMuon+nPhoton)==0 && TMath::Abs(dPhiPuppiMET)>1.1'
cuts['res_cuttag'] = cuts['res'] + ' && jet123Mass>130 && jet123Mass<220 && jetNBtags==1'

#cuts['ca15'] = 'nAK8fj==0 && nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && puppimet>250 && (nTightElectron+nTightMuon+nPhoton)==0 && CA15fj1_mSD>150 && CA15fj1_mSD<240 && TMath::Abs(dPhiPuppiMET)>1.1'
cuts['ca15'] = 'nAK8fj==0 && nCA15fj==1 && CA15fj1_pt>250 && CA15fj1_pt<470 && TMath::Abs(CA15fj1_eta)<2.4 && (nTightElectron+nTightMuon+nPhoton)==0 && CA15fj1_mSD>150 && CA15fj1_mSD<240 '
cuts['ca15_lowpt'] = cuts['ca15'] + ' && CA15fj1_pt<350'
cuts['ca15_hipt'] = cuts['ca15'] + ' && CA15fj1_pt>350'
cuts['ca15_cuttag'] = cuts['ca15'] + ' && CA15fj1_tau3/CA15fj1_tau2<0.6  && CA15fj1_maxsubjetbtag>0.89'
cuts['ca15_sdtag'] = cuts['ca15'] + ' && CA15fj1_logchi>-10 && CA15fj1_maxsubjetbtag>0.89'
cuts['ca15_qsdtag'] = cuts['ca15'] + ' && CA15fj1_tau3/CA15fj1_tau2<0.6 && CA15fj1_logchi>-10 && CA15fj1_maxsubjetbtag>0.89'
cuts['ca15_NNtag'] = cuts['ca15'] + ' && CA15fj1_weighted5NNResponse>0.4  && CA15fj1_maxsubjetbtag>0.89'
cuts['ca15_BDTtag'] = cuts['ca15'] + ' && CA15fj1_BDTResponse>0.1  && CA15fj1_maxsubjetbtag>0.89'

cuts['ak8'] = 'nAK8fj==1 && AK8fj1_pt>470 && TMath::Abs(AK8fj1_eta)<2.1 && puppimet>470 && (nTightElectron+nTightMuon+nPhoton)==0 && AK8fj1_mSD>110 && AK8fj1_mSD<210 && TMath::Abs(dPhiPuppiMET)>1.1'
cuts['ak8_lowpt'] = cuts['ak8'] + ' && AK8fj1_pt<700'
cuts['ak8_hipt'] = cuts['ak8'] + ' && AK8fj1_pt>700'
cuts['ak8_cuttag'] = cuts['ak8'] + ' && AK8fj1_tau3/AK8fj1_tau2<0.6 && AK8fj1_maxsubjetbtag>0.89'
cuts['ak8_sdtag'] = cuts['ak8'] + ' && AK8fj1_tau3/AK8fj1_tau2<0.6 && AK8fj1_logchi>-12 && AK8fj1_maxsubjetbtag>0.89'
cuts['ak8_qsdtag'] = cuts['ak8'] + ' && AK8fj1_groomedIso<0.2 && AK8fj1_tau3/AK8fj1_tau2<0.6 && AK8fj1_logchi>-12 && AK8fj1_maxsubjetbtag>0.89'
cuts['ak8_NNtag'] = cuts['ak8'] + ' && AK8fj1_analysisNNResponse>0.45 && AK8fj1_maxsubjetbtag>0.89'
cuts['ak8_BDTtag'] = cuts['ak8'] + ' && AK8fj1_BDTResponse>0.15 && AK8fj1_maxsubjetbtag>0.89'

plot = root.PlotUtility()
plot.Stack(False)
plot.Logy(False)
plot.SetNormFactor(True)
plot.SetLumi(lumi)
fullCut = region + ('_'+additionalcut if additionalcut!=None else '')
print fullCut
plot.SetCut(cuts[fullCut])
plot.SetMCWeight("%f*mcWeight"%(lumi))

qcdlike = root.Process('QCD-like',root.kZjets) # just choosing colors that are easy to distinguish
toplike = root.Process('top-like',root.kWjets)
diboson = root.Process('Diboson',root.kDiboson)
signal = root.Process("Signal",root.kSignal)
processes = [signal,qcdlike,toplike]
for p in processes:
  p.AddFriend('BDTResponse')
  p.AddFriend('weightedNNResponse')
  p.AddFriend('weighted5NNResponse')

qcdlike.AddFile(baseDir+'ZJets.root')
qcdlike.AddFile(baseDir+'ZtoNuNu.root')
qcdlike.AddFile(baseDir+'WJets.root')
diboson.AddFile(baseDir+'Diboson.root')
toplike.AddFile(baseDir+'TTbar.root')
toplike.AddFile(baseDir+'SingleTop.root')
qcdlike.AddFile(baseDir+'QCD.root')
if region.find('ak8')>=0:
  signal.AddFile(baseDir+'Signal2p1.root')
else:
  signal.AddFile(baseDir+'Signal1p1.root')

for p in processes:
  plot.AddProcess(p)

if region.find('ak8')>=0:
  # plot.AddDistribution(root.Distribution("puppimet",470,1000,100,"Puppi MET [GeV]","Events/5.3 GeV"))
  if region.find('lowpt')>=0:
    ptLabel = "470<p_{T}<700 GeV"
  elif region.find('hipt')>=0:
    ptLabel = "p_{T}>700 GeV"
  else:
    ptLabel = "p_{T}>470 GeV"
  if additionalcut=="cuttag":
    cutLabel = "; #tau_{32}, m_{SD}"
  elif additionalcut=="NNtag":
    cutLabel = "; NN tag"
  else:
    cutLabel = ""
  plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.0f pb^{-1} (13 TeV)}{AK8, %s%s}}"%(lumi,ptLabel,cutLabel))
  if additionalcut is None:
#    plot.AddDistribution(root.Distribution("TMath::Abs(dPhiPuppiMET)",0,3.14,20,"#Delta#phi(MET,j)","Events"))
    plot.AddDistribution(root.Distribution("AK8fj1_weightedNNResponse",0,1,20,"NN Response","Events/0.05"))
    plot.AddDistribution(root.Distribution("AK8fj1_weighted5NNResponse",0,1,20,"NN Response","Events/0.05"))
#    plot.AddDistribution(root.Distribution("AK8fj1_BDTResponse",0,1,20,"BDT Response","Events/0.01"))
#    plot.AddDistribution(root.Distribution("AK8fj1_QjetVol",0,1,20,"Qjet volatility","Events/0.01"))
#    plot.AddDistribution(root.Distribution("AK8fj1_NNResponse",0,1,20,"NN Response","Events/0.01"))
#    plot.AddDistribution(root.Distribution("AK8fj1_tau3/AK8fj1_tau2",0,1,20,"tau32","Events/0.01"))
elif region.find('ca15')>=0:
  # plot.AddDistribution(root.Distribution("puppimet",250,500,250,"Puppi MET [GeV]","Events/10 GeV"))
  plot.AddDistribution(root.Distribution("CA15fj1_pt",250,500,25,"Leading fat jet pT [GeV]","Events/10 GeV"))
  if region.find('lowpt')>=0:
    ptLabel = "250<p_{T}<350 GeV"
  elif region.find('hipt')>=0:
    ptLabel = "p_{T}>350 GeV"
  else:
    ptLabel = "p_{T}>250 GeV"
  if additionalcut=="cuttag":
    cutLabel = "; #tau_{32}, m_{SD}"
  elif additionalcut=="NNtag":
    cutLabel = "; NN tag"
  else:
    cutLabel = ""
  plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.0f pb^{-1} (13 TeV)}{CA15, %s%s}}"%(lumi,ptLabel,cutLabel))
  if additionalcut is None:
#    plot.AddDistribution(root.Distribution("TMath::Abs(dPhiPuppiMET)",0,3.14,20,"#Delta#phi(MET,j)","Events"))
    plot.AddDistribution(root.Distribution("CA15fj1_BDTResponse",0,0.5,20,"BDT Response","Events/0.05"))
#    plot.AddDistribution(root.Distribution("CA15fj1_NNResponse",0,1,20,"NN Response","Events/0.05"))
    plot.AddDistribution(root.Distribution("CA15fj1_weightedNNResponse",0,1,20,"NN Response","Events/0.05"))
    plot.AddDistribution(root.Distribution("CA15fj1_weighted5NNResponse",0,1,20,"NN Response","Events/0.05"))
#    plot.AddDistribution(root.Distribution("CA15fj1_BDTResponse",0,1,20,"BDT Response","Events/0.01"))
#    plot.AddDistribution(root.Distribution("CA15fj1_NNResponse",0,1,20,"NN Response","Events/0.01"))
    plot.AddDistribution(root.Distribution("CA15fj1_QjetVol",0,0.2,20,"Qjet volatility","Events/0.05"))
    plot.AddDistribution(root.Distribution("CA15fj1_QGTag",0,0.2,20,"QGTag","Events/0.05"))
    plot.AddDistribution(root.Distribution("CA15fj1_logchi",-35,5,100,"logchi","Events/0.4"))
    plot.AddDistribution(root.Distribution("CA15fj1_sjqgtag0",0,1,20,"sjqgtag0","Events/0.05"))
    plot.AddDistribution(root.Distribution("CA15fj1_sjqgtag1",0,1,20,"sjqgtag1","Events/0.05"))
    plot.AddDistribution(root.Distribution("CA15fj1_sjqgtag2",0,1,20,"sjqgtag2","Events/0.05"))
    plot.AddDistribution(root.Distribution("CA15fj1_groomedIso",0,1,20,"groomedIso","Events/0.05"))
    plot.AddDistribution(root.Distribution("CA15fj1_tau3/CA15fj1_tau2",0,1,20,"tau32","Events/0.01"))
else:
  # plot.AddDistribution(root.Distribution("puppimet",120,500,250,"Puppi MET [GeV]","Events/10 GeV"))
#  plot.AddDistribution(root.Distribution("TMath::Abs(dPhiPuppiMET)",0,3.14,20,"#Delta#phi(MET,j)","Events"))
  plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.0f pb^{-1} (13 TeV)}{CA15, p_{T}>250 GeV}}"%(lumi))

plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))

try:
  mkdir(baseDir+'/figs/'+fullCut)
except OSError:
  pass
plot.DrawAll(baseDir+'/figs/'+fullCut)
system('genWebViewer %s/figs/%s'%(baseDir,fullCut))
