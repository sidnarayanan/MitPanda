#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv

### LOAD LIBRARIES ###
gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

### HELPER FUNCTIONS ###
def setBins(dist,bins):
  for b in bins:
    dist.AddBinEdge(b)

def contains(s1,s2):
  if type(s2)==type(""):
    return s1.find(s2)>=0
  else:
    for s in s2:
      if s1.find(s)>=0:
        return True
    return False

### SET GLOBAL VARIABLES ###
region = argv[1]
try:
  additionalcut = argv[2]
except:
  additionalcut = None
makePlots = True
baseDir = '/home/snarayan/cms/root/nero_pfchs_v2/'
lumi = 2245.

### DEFINE REGIONS ###
cuts = {}

metTrigger=' && (trigger&1!=0||trigger&2!=0) '
eleTrigger=' && (trigger&4!=0||trigger&8!=0) '
phoTrigger=' && (trigPho120==1||trigPho165==1||trigPho175) '

cuts['res']         = 'met>250 && nJet==1 && (nLooseElectron+nLooseMuon+nLoosePhoton)==0 && TMath::Abs(dPhiPuppiMET)>1.1 && (trigger&1!=0||trigger&2!=0)'
cuts['res_cuttag']  = cuts['res'] + ' && jet123Mass>130 && jet123Mass<220 && jetNBtags==1'

# basic CA15 selection
cuts['ca15']             = 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_tau3/CA15fj1_tau2<0.78' 
cuts['ca15_nottau32SR']  = 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_mSD>150 && CA15fj1_mSD<240 && CA15fj1_maxsubjetbtag>0.89 && met>250 && (nLooseElectron+nLooseMuon+nLoosePhoton)==0 && TMath::Abs(dPhiMET)>1.1' 
cuts['ca15_notmsdSR']    = 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_tau3/CA15fj1_tau2<0.78 && CA15fj1_maxsubjetbtag>0.89 && met>250 && (nLooseElectron+nLooseMuon+nLoosePhoton)==0 && TMath::Abs(dPhiMET)>1.1'
cuts['ca15_notbSR']      = 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_mSD>150 && CA15fj1_mSD<240 && CA15fj1_tau3/CA15fj1_tau2<0.78 && met>250 && (nLooseElectron+nLooseMuon+nLoosePhoton)==0 && TMath::Abs(dPhiMET)>1.1' 
cuts['ca15_nottau32CR']  = 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_mSD>150 && CA15fj1_mSD<240 && CA15fj1_maxsubjetbtag>0.89 && UWmag>250 && nTightMuon==1 && (nLooseElectron+nLoosePhoton)==0 && TMath::Abs(dPhiUW)>1.1' 
cuts['ca15_notmsdCR']    = 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_tau3/CA15fj1_tau2<0.78 && CA15fj1_maxsubjetbtag>0.89 && UWmag>250 && nLooseMuon==1 && looseLep1IsTight==1 && (nLooseElectron+nLoosePhoton)==0  && TMath::Abs(dPhiUW)>1.1' 
cuts['ca15_notbCR']      = 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_mSD>150 && CA15fj1_mSD<240 && CA15fj1_tau3/CA15fj1_tau2<0.78 && UWmag>250 && nTightMuon==1 && (nLooseElectron+nLoosePhoton)==0 && TMath::Abs(dPhiUW)>1.1' 
cuts['ca15_notbCR'] = cuts['ca15'] + ' && UWmag>250 && nTightMuon==1 && (nLooseElectron+nLoosePhoton)==0 && TMath::Abs(dPhiUW)>1.1 && (trigger&1!=0||trigger&2!=0) && CA15fj1_mSD>150 && CA15fj1_mSD<240'
# regions 
cuts['ca15_notagSR']= 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4' + ' && met>250 && (nLooseElectron+nLooseMuon+nLoosePhoton)==0 && TMath::Abs(dPhiMET)>1.1'
cuts['ca15_notagCR']= 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4' + ' && UWmag>250 && nTightMuon==1 && (nLooseElectron+nLoosePhoton)==0 && TMath::Abs(dPhiUW)>1.1 && (trigger&1!=0||trigger&2!=0)'
cuts['ca15_SR']     = cuts['ca15'] + ' && met>250 && (nLooseElectron+nLooseMuon+nLoosePhoton)==0 && TMath::Abs(dPhiMET)>1.1 && CA15fj1_maxsubjetbtag>0.89 && (trigger&1!=0||trigger&2!=0) && CA15fj1_mSD>150 && CA15fj1_mSD<240'
cuts['ca15_muttCR'] = cuts['ca15'] + ' && UWmag>250 && nTightMuon==1 && (nLooseElectron+nLoosePhoton)==0 && TMath::Abs(dPhiUW)>1.1 && CA15fj1_maxsubjetbtag>0.89 && (trigger&1!=0||trigger&2!=0) && CA15fj1_mSD>150 && CA15fj1_mSD<240'
#cuts['ca15_muZCR']  = cuts['ca15'] + ' && UZmag>250 && nLooseMuon==2 && (looseLep1IsTight==1||looseLep2IsTight==1) && (nLooseElectron+nLoosePhoton)==0 && TMath::Abs(dPhiMET)>1.1 && 60<diLepMass && diLepMass<120 && (trigger&1!=0||trigger&2!=0)'
cuts['ca15_muZCR']  = cuts['ca15'] + ' && UZmag>250 && nLooseMuon==2 && (looseLep1IsTight==1||looseLep2IsTight==1) && (nLooseElectron+nLoosePhoton)==0 && 60<diLepMass && diLepMass<120'
cuts['ca15_eZCR']   = cuts['ca15'] + ' && UZmag>250 && nLooseElectron==2 && (looseLep1IsTight==1||looseLep2IsTight==1) && (nLooseMuon+nLoosePhoton)==0 && 60<diLepMass && diLepMass<120 && (trigger&4!=0||trigger&8!=0)'
cuts['ca15_muWCR']  = 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_tau3/CA15fj1_tau2>0.78 && UWmag>250 && nLooseMuon==1 && looseLep1IsTight==1 && (nLooseElectron+nLoosePhoton)==0 && TMath::Abs(dPhiUW)>1.1 && (trigger&1!=0||trigger&2!=0)'
cuts['ca15_ettCR']  = cuts['ca15'] + ' && UWmag>250 && nLooseElectron==1 && looseLep1IsTight==1 && (nLooseMuon+nLoosePhoton)==0 && TMath::Abs(dPhiUW)>1.1 && CA15fj1_maxsubjetbtag>0.89 && met>50 && CA15fj1_mSD>150 && CA15fj1_mSD<240'
cuts['ca15_eWCR']   = '(nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_tau3/CA15fj1_tau2>0.78 && UWmag>250 && met>50 && nLooseElectron==1 && looseLep1IsTight==1 && (nLooseMuon+nLoosePhoton)==0 && TMath::Abs(dPhiUW)>1.1 && (trigger&4!=0||trigger&8!=0))'
#cuts['ca15_ACR']   = 'nJet>=1 && UAmag>250 && nLoosePhoton==1 && loosePho1IsTight==1 && (nLooseMuon+nLooseElectron)==0 && TMath::Abs(dPhiUA)>0.5 && jetNBtags==0'
cuts['ca15_ACR']   = 'nJet>=1 && UAmag>250 && nLoosePhoton==1 && loosePho1IsTight==1 && (nLooseMuon+nLooseElectron)==0 && TMath::Abs(dPhiUA)>0.5 && jetNBtags==0'
cuts['ca15_ACR']  = cuts['ca15'] + ' && UAmag>250 && nLoosePhoton==1 && loosePho1IsTight && (nLooseMuon+nLooseElectron+nTau)==0'


### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
plot.Logy(False)
plot.Ratio(True)
plot.SetNormFactor(False)
plot.SetLumi(lumi)
fullCut = region + ('_'+additionalcut if additionalcut!=None else '')
plot.SetCut(root.TCut(cuts[fullCut]))
basicWeight="%f*mcWeight*lepSF*puWeight*ewkWFactor*ewkZFactor*ewkAFactor"%(lumi)
if contains(additionalcut,["SR","ttCR"]):
  plot.SetMCWeight(root.TCut("btagSF*"+basicWeight))
  #plot.SetMCWeight(root.TCut(basicWeight))
else:
  plot.SetMCWeight(root.TCut(basicWeight))
#  plot.SetMCWeight("%f*mcWeight"%(lumi))
#plot.SetSignalScale(100)

### DEFINE PROCESSES ###
zjets     = root.Process('Z+jets',root.kZjets)
wjets     = root.Process('W+jets',root.kWjets)
diboson   = root.Process('Diboson',root.kDiboson)
ttbar     = root.Process('t#bar{t}',root.kTTbar)
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
gjets     = root.Process('#gamma+jets',root.kGjets)
signal    = root.Process("Signal",root.kSignal)
data      = root.Process("Data",root.kData)
processes = [zjets,wjets,diboson,singletop,ttbar,qcd]

### ASSIGN FILES TO PROCESSES ###
zjets.AddFile(baseDir+'ZJets.root')
zjets.AddFile(baseDir+'ZtoNuNu.root')
wjets.AddFile(baseDir+'WJets.root')
diboson.AddFile(baseDir+'Diboson.root')
ttbar.AddFile(baseDir+'TTbar.root')
singletop.AddFile(baseDir+'SingleTop.root')
if contains(additionalcut,"ACR"):
  processes.append(gjets)
  gjets.AddFile(baseDir+'GJets.root')
qcd.AddFile(baseDir+'QCD.root')

if not contains(additionalcut,"SR"):
  if contains(additionalcut,"ACR"):
    data.AddFile(baseDir+"SinglePhoton.root")
  elif contains(additionalcut,"e"):
    data.AddFile(baseDir+"SingleElectron.root")
  else:
    data.AddFile(baseDir+'MET.root')
  processes.append(data)
else:
  plot.Ratio(False)
  if not contains(additionalcut,["notag"]):
    plot.SetSignalScale(0.1)
  signal.AddFile(baseDir+'Signal1p1.root')
  processes.append(signal)

for p in processes:
  plot.AddProcess(p)

recoilBins = [250,300,350,400,450,500,600,700]
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
if region.find('ak8')>=0:
  ptLabel = "p_{T}>470 GeV"
  cutLabel = "; #tau_{32}, sj b-tag" if additionalcut=="cuttag" else ""
  plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.0f pb^{-1} (13 TeV)}{AK8, %s%s}}"%(lumi,ptLabel,cutLabel))
  if makePlots:
      plot.AddDistribution(root.Distribution("met",250,650,10,"MET","Events/40 GeV"))
elif region.find('ca15')>=0:
  ptLabel = "p_{T}>250 GeV"
  cutLabel = "cut-based top tag" if additionalcut=="SR" else ""
  plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.1f fb^{-1} (13 TeV)}{%s}}"%(lumi/1000,cutLabel))
  if makePlots:
    # define what "recoil" means
    if contains(additionalcut,"e"):
      lep='e'
    else:
      lep='#mu'
    recoil=None
    if additionalcut=='SR' or additionalcut=='testSR':
      recoil=root.Distribution("met",nRecoilBins,"MET","Events/GeV")
    elif contains(additionalcut,'ZCR'):
      recoil=root.Distribution("UZmag",nRecoilBins,"Recoil (%s%s)"%(lep,lep),"Events/GeV")
    elif contains(additionalcut,'WCR'):
      recoil=root.Distribution("UWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
    elif contains(additionalcut,'ttCR'):
      recoil=root.Distribution("UWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
    elif contains(additionalcut,'ACR'):
      recoil=root.Distribution("UAmag",nRecoilBins,"Recoil (#gamma)","Events/GeV")
    if recoil:
      setBins(recoil,recoilBins)
      plot.AddDistribution(recoil)
      plot.AddDistribution(root.Distribution("CA15fj1_mSD",0,400,20,"soft drop mass","Events/20 GeV"))
    if contains(additionalcut,["notag","not"]):
      plot.AddDistribution(root.Distribution("CA15fj1_tau3/CA15fj1_tau2",0,1.2,12,"#tau_{3}/#tau_{3}","Events"))
      plot.AddDistribution(root.Distribution("CA15fj1_maxsubjetbtag",0,1,20,"max subjet b-tag","Events/0.05"))
      plot.AddDistribution(root.Distribution("CA15fj1_mSD",40,440,20,"soft drop mass","Events/20 GeV"))
else:
  if makePlots:
      plot.AddDistribution(root.Distribution("met",250,650,10,"MET","Events/40 GeV"))
      plot.AddDistribution(root.Distribution("jet123Mass",0,400,10,"trijet mass","Events/40 GeV"))

plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))

### DRAW AND CATALOGUE ###
try:
  mkdir(baseDir+'/figs/'+fullCut)
except OSError:
  pass
plot.DrawAll(baseDir+'/figs/'+fullCut)
