#!/usr/bin/env python

import ROOT as root
from array import array
from re import sub

metbins = array('d',[250,300,350,400,450,700])
nbins = len(metbins)-1

#nbins=2

baseDir = '/home/snarayan/cms/root/nero_pfchs_v3/'

fHists = root.TFile(baseDir+'histogramsHighMet.root')

processNumbers = { 'ZJets':1, 'SingleLep':2, 'Diboson':3, 'QCD':4, 'Signal':0 }

regions = { 'signal' : ['Signal','SingleLep','ZJets'],
            #'singlemu' :  ['QCD','SingleLep','ZJets','Data'],
            'singlemu' :  ['SingleLep','ZJets','Data'],
            'doublemu' :  ['SingleLep','ZJets','Data'] }

histograms = { }

#jmax=5*nbins-1
#kmax=8
jmax="*"
kmax="*"
for k,v in regions.iteritems():
  histograms[k] = {}
  for vv in v:
    histograms[k][vv] = fHists.Get('h_'+k+'_'+vv)

imax = nbins*3

datacard = {}
for region,hists in histograms.iteritems():
  for iB in xrange(nbins):
    datacard[region+str(iB)] = {'obs':0, 'exp':[]}
    for histName,hist in hists.iteritems():
      if histName=='Data':
        datacard[region+str(iB)]['obs'] = hist.GetBinContent(iB+1)
      else:
        datacard[region+str(iB)]['exp'].append((histName+str(iB),hist.GetBinContent(iB+1)))
    


binobsline  ='bin           '
obsrateline ='observation   '

binline      ='bin          '
process1line ='process      '
process2line ='process      '
rateline     ='rate         '
lumiline     ='lumi  lnN    '
xszjets      ='xszjets lnU  '
xssig        ='xssig lnU    '
xsslep       ='xsslep lnU   '
xsVV         ='xsVV lnN     '
xsqcd        ='xsqcd lnN    '
lepsf        ='lepsf lnN    '
met          ='met lnN      '

two = "%12f "%(2.0)
dash = "%12s "%('-')
lepsf1 = "%12f "%(1.01)
lepsf2 = "%12f "%(1.02)

regionCounter=1
regionOrder=['signal','singlemu','doublemu']
allRegionOrder=[]
for i in xrange(nbins):
  for r in regionOrder:
    allRegionOrder.append(r+str(i))
#for region,card in datacard.iteritems():
for region in allRegionOrder:
  card = datacard[region]
  binobsline += "%12i "%(regionCounter)
  obsrateline += "%12f "%(card['obs'])
  for pname,rate in card['exp']:
    binline += "%12i "%(regionCounter)
    process1line += "%12s "%(pname)
    shortpname = sub('[0-9]*','',pname)
    process2line += "%12i "%(processNumbers[shortpname])
    rateline += "%12f "%(rate)
    lumiline += "%12f "%(1.046)
    xszjets  += two if (shortpname=='ZJets' and region.find('doublemu')<0) else dash
    xsslep  += two if (shortpname=='SingleLep' and region.find('singlemu')<0) else dash
    xssig  += two if shortpname=='Signal' else dash
    xsqcd  += '%12f '%(1.5) if shortpname=='QCD' else dash
    met +=  dash if (shortpname=='ZJets' or shortpname=='SingleLep') else ('%12f ')%(1.05)
    if region.find('signal')>=0:
      lepsf += dash
    elif region.find('singlemu')>=0:
      lepsf += lepsf1
    elif region.find('doublemu')>=0:
      lepsf += lepsf2
  regionCounter += 1

print 'imax',imax,'number of channels'
print 'jmax',jmax,'number of processes minus one'
print 'kmax',kmax,'number of nuisance parameters'
print '------------'
print binobsline
print obsrateline
print '------------'
print binline
print process1line
print process2line
print rateline
print '------------'
print lumiline
print xszjets
print xsslep
print xssig
print xsqcd
print lepsf
print met
