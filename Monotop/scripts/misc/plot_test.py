#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv
from MitPanda.Tools.Misc import *

blind=False

### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

### HELPER FUNCTIONS ###

### SET GLOBAL VARIABLES ###
region = argv[1]
makePlots = True
#baseDir = '/local/snarayan/nero_pfchs_v3/'
baseDir = '/local/snarayan/monotop_miniaod/'
lumi = 2300.

### DEFINE REGIONS ###
cuts = {}

metTrigger='(trigger&1)!=0'
eleTrigger='(trigger&2)!=0'
phoTrigger='(trigger&4)!=0'
presel    ='nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_tau3/CA15fj1_tau2<0.61 && 110<CA15fj1_mSD && CA15fj1_mSD<210'
#presel    ='nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4'
#cuts['signal'] = tAND(metTrigger,tAND(presel,'met>250 && dPhiMET>1.1 && CA15fj1_maxsjbtag>0.76 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0 && jetNIsoBtags==0'))
cuts['signal'] = tAND(presel,'puppimet>250 && dPhiPuppiMET>1.1 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0 && CA15fj1_maxsjbtag>0.76 && jetNIsoBtags==0')
cuts['singlemuontop'] = tAND(presel,'UWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag>0.76 && jetNIsoBtags==1')
#cuts['singlemuontop'] = tAND(presel,'UWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1')
cuts['singleelectrontop'] = tAND(presel,'UWmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag>0.76 && jetNIsoBtags==1')
cuts['singlemuonw'] = tAND(presel,'UWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag<0.76 && jetNIsoBtags==0')
cuts['singleelectronw'] = tAND(presel,'UWmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag<0.76 && jetNIsoBtags==0')
cuts['dimuon'] = tAND(presel,'UZmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==2 && looseLep1IsTight==1')
cuts['dielectron'] = tAND(presel,'UZmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==2 && looseLep1IsTight==1')
cuts['photon'] = tAND(presel,'UAmag>250 && (nLooseMuon+nLooseElectron+nTau)==0 && nLoosePhoton==1 && loosePho1IsTight==1')


### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
plot.Logy(True)
plot.Ratio(False)
plot.DrawMCErrors(True)
plot.SetTDRStyle()
plot.SetNormFactor(False)
plot.FixRatio(True)
plot.SetLumi(lumi/1000)
plot.AddCMSLabel()
plot.AddLumiLabel()
fullCut = region
plot.SetCut(root.TCut(cuts[fullCut]))
print cuts[fullCut]
if 'signal' in region or 'muon' in region:
  triggerEff = "(((met<240)*(0.975+(met-200)*0.000625))+(met>240))"
else:
  triggerEff = '1'
#basicWeight="%f*mcWeight*sf_lep*puWeight*sf_ewkW*sf_ewkZ*sf_ewkA*sf_qcdW*sf_qcdZ*sf_qcdA*sf_tt*%s"%(lumi,triggerEff)
#if 'signal' in region:
#  plot.SetMCWeight(root.TCut(tTIMES(basicWeight,"sf_btag0")))
#else:
#  plot.SetMCWeight(root.TCut(basicWeight))
'''
if 'signal' in region  or ('single' in region and 'w' in region):
  plot.SetMCWeight(root.TCut('%f*mcWeight*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_tt*sf_btag0*%s'%(lumi,triggerEff)))
elif 'single' in region and 'top' in region:
  plot.SetMCWeight(root.TCut('%f*mcWeight*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_tt*sf_btag1*%s'%(lumi,triggerEff)))
else:
  plot.SetMCWeight(root.TCut('%f*mcWeight*sf_ewkZ*sf_qcdZ*sf_ewkA*sf_qcdA*sf_tt'%(lumi)))
'''
if 'signal' in region  or ('single' in region and 'w' in region):
  plot.SetMCWeight(root.TCut('%f*mcWeight*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_btag0*%s'%(lumi,triggerEff)))
elif 'single' in region and 'top' in region:
  plot.SetMCWeight(root.TCut('%f*mcWeight*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_btag1*%s'%(lumi,triggerEff)))
else:
  plot.SetMCWeight(root.TCut('%f*mcWeight*sf_ewkZ*sf_qcdZ*sf_ewkA*sf_qcdA'%(lumi)))

### DEFINE PROCESSES ###
zjets     = root.Process('Z+jets',root.kZjets)
wjets     = root.Process('W+jets',root.kWjets)
diboson   = root.Process('Diboson',root.kDiboson)
ttbar     = root.Process('t#bar{t}',root.kTTbar)
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
gjets     = root.Process('#gamma+jets',root.kGjets)
signal    = root.Process("Signal",root.kSignal)
data      = root.Process("Data",root.kData)
processes = [qcd,diboson,singletop,wjets,ttbar,zjets]

### ASSIGN FILES TO PROCESSES ###
zjets.AddFile(baseDir+'ZJets.root')
zjets.AddFile(baseDir+'ZtoNuNu.root')
wjets.AddFile(baseDir+'WJets.root')
diboson.AddFile(baseDir+'Diboson.root')
ttbar.AddFile(baseDir+'TTbar.root')
singletop.AddFile(baseDir+'SingleTop.root')
if contains(region,"photon"):
  processes.append(gjets)
  gjets.AddFile(baseDir+'GJets.root')
qcd.AddFile(baseDir+'QCD.root')

'''
if contains(region,"photon"):
  print 'Using SinglePhoton'
  data.AddFile(baseDir+"SinglePhoton.root"); data.additionalCut = root.TCut(phoTrigger)
elif contains(region,"electron"):
  print 'Using SingleElectron'
  data.AddFile(baseDir+"SingleElectron.root"); data.additionalCut = root.TCut(eleTrigger)
else:
  print 'Using MET'
  data.AddFile(baseDir+'MET.root');  data.additionalCut = root.TCut(metTrigger)
if not(blind and 'signal' in region):
  processes.append(data)
'''

for p in processes:
  plot.AddProcess(p)

recoilBins = [250,300,350,400,500,1000]
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
ptLabel = "p_{T}>250 GeV"
cutLabel = "cut-based top tag" if region=="SR" else ""
plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.1f fb^{-1} (13 TeV)}{%s}}"%(lumi/1000,cutLabel))
# define what "recoil" means
if contains(region,"electron"):
  lep='e'
else:
  lep='#mu'
plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))

recoil=None
if region=='signal':
  recoil=root.Distribution("met",nRecoilBins,"MET","Events/GeV")
elif 'photon' in region:
  recoil=root.Distribution("UAmag",nRecoilBins,"Recoil (#gamma)","Events/GeV")
elif 'di' in region:
  recoil=root.Distribution("UZmag",nRecoilBins,"Recoil (%s%s)"%(lep,lep),"Events/GeV")
elif 'single' in region and 'w' in region:
  recoil=root.Distribution("UWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
elif 'single' in region and 'top' in region:
  recoil=root.Distribution("UWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
if recoil:
  setBins(recoil,recoilBins)
  plot.AddDistribution(recoil)
plot.AddDistribution(root.Distribution("CA15fj1_mSD",0,400,20,"soft drop mass","Events/20 GeV",0.1))
if contains(region,["notag","not"]):
  plot.AddDistribution(root.Distribution("CA15fj1_tau3/CA15fj1_tau2",0,1.2,12,"#tau_{3}/#tau_{3}","Events"))
  plot.AddDistribution(root.Distribution("CA15fj1_maxsubjetbtag",0,1,20,"max subjet b-tag","Events/0.05"))
  plot.AddDistribution(root.Distribution("CA15fj1_mSD",40,440,20,"soft drop mass","Events/20 GeV"))

### DRAW AND CATALOGUE ###
plot.DrawAll(baseDir+'/figs/'+fullCut+'_')

