#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv
tcut = root.TCut
### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')
### HELPER FUNCTIONS ###
def setBins(dist,bins):
  for b in bins:
    dist.AddBinEdge(b)

def contains(s1,s2):
  if type(s2)==type(""):
    return s1.find(s2)>=0
  else:
    for s in s2:
      if s1.find(s)>=0:
        return True
    return False

def tAND(s1,s2):
  return "(("+s1+")&&("+s2+"))"

def tOR(s1,s2):
  return "(("+s1+")||("+s2+"))"

def tTIMES(w,s):
  return "("+w+")*("+s+")"

### SET GLOBAL VARIABLES ###
boson = argv[1]

makePlots = True
#baseDir = '/home/snarayan/local/BaconSkim/MonoXSelection/baconbits/ntuples/'
baseDir = '/home/snarayan/local/BaconSkim/ntuples_v3/'
lumi = 2.26

### DEFINE REGIONS ###
cuts = {}
# resolved

# regions 
topTagCut                = 'bst15_PUPPIjet1msd>110 && bst15_PUPPIjet1msd<210 && bst15_PUPPIjet1tau32<0.61' 
cut = tAND(topTagCut, 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && (puppet.Pt()>250 || fakepuppet.Pt()>250)')

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
#plot.Stack(True)
plot.Logy(True)
#plot.DrawMCErrors(True)
#plot.Ratio(True)
#plot.SetNormFactor(False)
plot.SetLumi(lumi)
plot.SetCut(tcut(cut))
plot.SetMCWeight(tcut("%f*scale1fb*evtWeight*lepWeight"%(lumi)))

kfactor = tcut('nloKfactor_CENT')
ewkcorr = tcut('ewkCorr_CENT*nloKfactor_CENT')

if boson=='G':
  V = '#gamma'
elif boson=='Z':
  V = 'Z'
else:
  V = 'W'

### DEFINE PROCESSES ###
lo = root.Process('LO',root.kExtra1)
qcd = root.Process('QCD NLO',root.kExtra2); qcd.additionalWeight = kfactor
qcdewk = root.Process('QCD+EWK NLO',root.kExtra3); qcdewk.additionalWeight = ewkcorr

processes = [qcdewk,qcd,lo]

for p in processes:
  p.Init("Events")

### ASSIGN FILES TO PROCESSES ###
filename = baseDir+boson+'.root'
for p in processes:
  p.AddFile(filename)
  plot.AddProcess(p)

recoilBins = [250,300,350,400,500,1000] 
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{%s+jets}"%(V))

# define what "recoil" means
recoilLabel = 'p_{T}^{%s}'%(V)

recoil=None
recoil = root.Distribution("bst15_PUPPIgenVpt",nRecoilBins,recoilLabel,"Events/GeV")

setBins(recoil,recoilBins)
plot.AddDistribution(recoil)
plot.AddDistribution(recoil)
plot.AddDistribution(recoil)
plot.AddDistribution(recoil)
plot.AddDistribution(recoil)
plot.AddDistribution(root.Distribution("bst15_PUPPIjet1.Pt()",250,500,25,"p_{T}","Events/10 GeV"))

#plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
### DRAW AND CATALOGUE ###
try:
  mkdir(baseDir+'/figs_plots/'+boson)
except OSError:
  pass
plot.DrawAll(baseDir+'/figs_plots/'+boson)

