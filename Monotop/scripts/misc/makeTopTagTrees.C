void activateBranch(TTree *t, const char *bname, void *address) {
  t->SetBranchStatus(bname,1);
  t->SetBranchAddress(bname,address);
}

void skimTag(TTree *tIn, TFile *fOut) {
  fOut->cd();
  TTree *tOut = new TTree("Events","Events");

  tIn->SetBranchStatus("*",0);

  // output vars
  unsigned int runNum, lumiSec, evtNum, npv=0, pass;
  int qtag=0, qprobe=0;
  TLorentzVector *probe=0, *tag = new TLorentzVector(0,0,0,0);
  float mass, scale1fb, npu=0;

  // helper vars
  unsigned int nCA15, nLooseDR2, metfilter, nmu, nele, npho, ntau, triggerBits;
  float sjbtag, tau32;
  TLorentzVector *recoil=0;

  activateBranch(tIn,"runNum",&runNum);
  activateBranch(tIn,"lumiSec",&lumiSec);
  activateBranch(tIn,"evtNum",&evtNum);
  activateBranch(tIn,"bst15_PUPPIjet1",&probe);
  activateBranch(tIn,"bst15_PUPPIjet1msd",&mass);
  activateBranch(tIn,"scale1fb",&scale1fb);
  activateBranch(tIn,"nf15PUPPIjets",&nCA15);
  activateBranch(tIn,"nbPUPPIjetsLdR2",&nLooseDR2);
  activateBranch(tIn,"metfilter",&metfilter);
  activateBranch(tIn,"nmu",&nmu);
  activateBranch(tIn,"nele",&nele);
  activateBranch(tIn,"npho",&npho);
  activateBranch(tIn,"ntau",&ntau);
  activateBranch(tIn,"triggerBits",&triggerBits);
  activateBranch(tIn,"fakepuppet",&recoil);
  activateBranch(tIn,"bst15_PUPPIjet1tau32",&tau32);
  activateBranch(tIn,"bst15_PUPPIjet1maxsubcsv",&sjbtag);

  tOut->Branch("runNum",&runNum,"runNum/i");
  tOut->Branch("lumiSec",&lumiSec,"lumiSec/i");
  tOut->Branch("evtNum",&evtNum,"evtNum/i");
  tOut->Branch("pass",&pass,"pass/i");
  tOut->Branch("npv",&npv,"npv/i");
  tOut->Branch("mass",&mass,"mass/F");
  tOut->Branch("npu",&npu,"npu/F");
  tOut->Branch("scale1fb",&scale1fb,"scale1fb/F");
  tOut->Branch("qtag",&qtag,"qtag/I");
  tOut->Branch("qprobe",&qprobe,"qprobe/I");
  tOut->Branch("probe","TLorentzVector",&probe);
  tOut->Branch("tag","TLorentzVector",&tag);

  unsigned int nEntries = tIn->GetEntries();
  for (unsigned int iE=0; iE!=nEntries; ++iE) {
    if (iE%10000==0)
      printf("%u/%u\n",iE,nEntries);
    tIn->GetEntry(iE);
    if (nCA15==1 && nmu==1 && (nele+npho+ntau)==0 && nLooseDR2==1
        && probe->Pt()>250 && TMath::Abs(probe->Eta())<2.4
        && recoil->Pt()>200
  //      && recoil->Pt()<250 && recoil->Pt()>200
        && metfilter==0 && (triggerBits&2)!=0
        && sjbtag>0.76) {
      pass = (tau32<0.61) ? 1 : 0;
      tOut->Fill();
    }
  }
  fOut->WriteTObject(tOut);
  fOut->Close();
}

void skimTagE(TTree *tIn, TFile *fOut) {
  fOut->cd();
  TTree *tOut = new TTree("Events","Events");

  tIn->SetBranchStatus("*",0);

  // output vars
  unsigned int runNum, lumiSec, evtNum, npv=0, pass;
  int qtag=0, qprobe=0;
  TLorentzVector *probe=0, *tag = new TLorentzVector(0,0,0,0);
  float mass, scale1fb, npu=0;

  // helper vars
  unsigned int nCA15, nLooseDR2, metfilter, nmu, nele, npho, ntau, triggerBits;
  float sjbtag, tau32;
  TLorentzVector *recoil=0, *met=0;

  activateBranch(tIn,"runNum",&runNum);
  activateBranch(tIn,"lumiSec",&lumiSec);
  activateBranch(tIn,"evtNum",&evtNum);
  activateBranch(tIn,"bst15_PUPPIjet1",&probe);
  activateBranch(tIn,"bst15_PUPPIjet1msd",&mass);
  activateBranch(tIn,"scale1fb",&scale1fb);
  activateBranch(tIn,"nf15PUPPIjets",&nCA15);
  activateBranch(tIn,"nbPUPPIjetsLdR2",&nLooseDR2);
  activateBranch(tIn,"metfilter",&metfilter);
  activateBranch(tIn,"nmu",&nmu);
  activateBranch(tIn,"nele",&nele);
  activateBranch(tIn,"npho",&npho);
  activateBranch(tIn,"ntau",&ntau);
  activateBranch(tIn,"triggerBits",&triggerBits);
  activateBranch(tIn,"fakepuppet",&recoil);
  activateBranch(tIn,"puppet",&met);
  activateBranch(tIn,"bst15_PUPPIjet1tau32",&tau32);
  activateBranch(tIn,"bst15_PUPPIjet1maxsubcsv",&sjbtag);

  tOut->Branch("runNum",&runNum,"runNum/i");
  tOut->Branch("lumiSec",&lumiSec,"lumiSec/i");
  tOut->Branch("evtNum",&evtNum,"evtNum/i");
  tOut->Branch("pass",&pass,"pass/i");
  tOut->Branch("npv",&npv,"npv/i");
  tOut->Branch("mass",&mass,"mass/F");
  tOut->Branch("npu",&npu,"npu/F");
  tOut->Branch("scale1fb",&scale1fb,"scale1fb/F");
  tOut->Branch("qtag",&qtag,"qtag/I");
  tOut->Branch("qprobe",&qprobe,"qprobe/I");
  tOut->Branch("probe","TLorentzVector",&probe);
  tOut->Branch("tag","TLorentzVector",&tag);

  unsigned int nEntries = tIn->GetEntries();
  for (unsigned int iE=0; iE!=nEntries; ++iE) {
    if (iE%10000==0)
      printf("%u/%u\n",iE,nEntries);
    tIn->GetEntry(iE);
    if (nCA15==1 && nele==1 && (nmu+npho+ntau)==0 && nLooseDR2==1
        && probe->Pt()>250 && TMath::Abs(probe->Eta())<2.4
        && recoil->Pt()>200 && met->Pt()>50
//        && recoil->Pt()<250 && recoil->Pt()>200
        && metfilter==0 && (triggerBits&4)!=0
        && sjbtag>0.76) {
      pass = (tau32<0.61) ? 1 : 0;
      tOut->Fill();
    }
  }
  fOut->WriteTObject(tOut);
  fOut->Close();
}

void skimMistag(TTree *tIn, TFile *fOut) {
  fOut->cd();
  TTree *tOut = new TTree("Events","Events");

  tIn->SetBranchStatus("*",0);

  // output vars
  unsigned int runNum, lumiSec, evtNum, npv=0, pass;
  int qtag=0, qprobe=0;
  TLorentzVector *probe=0, *tag = new TLorentzVector(0,0,0,0);
  float mass, scale1fb, npu=0;

  // helper vars
  unsigned int nCA15, nLooseDR2, metfilter, nmu, nele, npho, ntau, triggerBits;
  float sjbtag, tau32;
  TLorentzVector *recoil=0;

  activateBranch(tIn,"runNum",&runNum);
  activateBranch(tIn,"lumiSec",&lumiSec);
  activateBranch(tIn,"evtNum",&evtNum);
  activateBranch(tIn,"bst15_PUPPIjet1",&probe);
  activateBranch(tIn,"bst15_PUPPIjet1msd",&mass);
  activateBranch(tIn,"scale1fb",&scale1fb);
  activateBranch(tIn,"nf15PUPPIjets",&nCA15);
  activateBranch(tIn,"nbPUPPIjetsLdR2",&nLooseDR2);
  activateBranch(tIn,"metfilter",&metfilter);
  activateBranch(tIn,"nmu",&nmu);
  activateBranch(tIn,"nele",&nele);
  activateBranch(tIn,"npho",&npho);
  activateBranch(tIn,"ntau",&ntau);
  activateBranch(tIn,"triggerBits",&triggerBits);
  activateBranch(tIn,"fakepuppet",&recoil);
  activateBranch(tIn,"bst15_PUPPIjet1tau32",&tau32);
  activateBranch(tIn,"bst15_PUPPIjet1maxsubcsv",&sjbtag);

  tOut->Branch("runNum",&runNum,"runNum/i");
  tOut->Branch("lumiSec",&lumiSec,"lumiSec/i");
  tOut->Branch("evtNum",&evtNum,"evtNum/i");
  tOut->Branch("pass",&pass,"pass/i");
  tOut->Branch("npv",&npv,"npv/i");
  tOut->Branch("mass",&mass,"mass/F");
  tOut->Branch("npu",&npu,"npu/F");
  tOut->Branch("scale1fb",&scale1fb,"scale1fb/F");
  tOut->Branch("qtag",&qtag,"qtag/I");
  tOut->Branch("qprobe",&qprobe,"qprobe/I");
  tOut->Branch("probe","TLorentzVector",&probe);
  tOut->Branch("tag","TLorentzVector",&tag);

  unsigned int nEntries = tIn->GetEntries();
  for (unsigned int iE=0; iE!=nEntries; ++iE) {
    if (iE%100000==0)
      printf("%u/%u\n",iE,nEntries);
    tIn->GetEntry(iE);
    if (nCA15==1 && npho==1 && (nele+nmu+ntau)==0 // && nLooseDR2==1
        && probe->Pt()>250 && TMath::Abs(probe->Eta())<2.4
        && recoil->Pt()>250
  //      && recoil->Pt()<250 && recoil->Pt()>200
        && metfilter==0 && (triggerBits&8)!=0) {
      pass = (tau32<0.61) ? 1 : 0; 
      tOut->Fill();
    }
  }
  fOut->WriteTObject(tOut);
  fOut->Close();
}

void makeTopTagTrees() {
  TChain *tIn;
  TFile *fOut;

  tIn = new TChain("Events");
  tIn->AddFile("SingleElectron.root");
  fOut = new TFile("tiny/tage_data.root","RECREATE");
  skimTagE(tIn,fOut);
  delete tIn;

  tIn = new TChain("Events");
  tIn->AddFile("TTBST_new.root");
  fOut = new TFile("tiny/sig_tage.root","RECREATE");
  skimTagE(tIn,fOut);
  delete tIn;

  tIn = new TChain("Events");
  tIn->AddFile("TTCOM_new.root");
  fOut = new TFile("tiny/bkg_unmatched_tage.root","RECREATE");
  skimTagE(tIn,fOut);
  delete tIn;

  tIn = new TChain("Events");
  tIn->AddFile("W.root");
  tIn->AddFile("QCD.root");
  tIn->AddFile("T.root");
  tIn->AddFile("DY.root");
  tIn->AddFile("Z.root");
  fOut = new TFile("tiny/bkg_tage.root","RECREATE");
  skimTagE(tIn,fOut);
  delete tIn;
  /*
  tIn = new TChain("Events");
  tIn->AddFile("MET.root");
  fOut = new TFile("tiny/tag_data.root","RECREATE");
  skimTag(tIn,fOut);
  delete tIn;

  tIn = new TChain("Events");
  tIn->AddFile("TTBST_new.root");
  fOut = new TFile("tiny/sig_tag.root","RECREATE");
  skimTag(tIn,fOut);
  delete tIn;

  tIn = new TChain("Events");
  tIn->AddFile("TTCOM_new.root");
  fOut = new TFile("tiny/bkg_unmatched_tag.root","RECREATE");
  skimTag(tIn,fOut);
  delete tIn;

  tIn = new TChain("Events");
  tIn->AddFile("W.root");
  tIn->AddFile("QCD.root");
  tIn->AddFile("T.root");
  tIn->AddFile("DY.root");
  tIn->AddFile("Z.root");
  fOut = new TFile("tiny/bkg_tag.root","RECREATE");
  skimTag(tIn,fOut);
  delete tIn;

  tIn = new TChain("Events");
  tIn->AddFile("SinglePhoton.root");
  fOut = new TFile("tiny/mistag_data.root","RECREATE");
  skimMistag(tIn,fOut);
  delete tIn;


  tIn = new TChain("Events");
  tIn->AddFile("G.root");
  tIn->AddFile("QCD.root");
  fOut = new TFile("tiny/sig_mistag.root","RECREATE");
  skimMistag(tIn,fOut);
  delete tIn;

  tIn = new TChain("Events");
  tIn->AddFile("herwig/TTherwig.root");
  fOut = new TFile("tiny/sig_tagherwig.root","RECREATE");
  skimTag(tIn,fOut);
  delete tIn;
  */

}

