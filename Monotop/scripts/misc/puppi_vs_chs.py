#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv

### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

### SET GLOBAL VARIABLES ###
region = argv[1]
baseDir = '/home/snarayan/cms/root/signalComparison/'
lumi = 1200.

### DEFINE REGIONS ###
cuts = [
          ' puppimet>250',
          ' && nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4',
          ' && (nLooseElectron+nLooseMuon)==0',
          ' && (nLoosePhoton)==0',
          ' && (nTau)==0',
          ' && dPhiMET>1.1'
        ]
### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(False)
plot.Logy(False)
plot.SetNormFactor(True)
plot.SetLumi(lumi)
fullCut=''
for i in xrange(len(cuts)):
  fullCut += cuts[i]

if not region=='tau32':
  fullCut += ' && CA15fj1_tau3/CA15fj1_tau2<0.78'
if not region=='btag':
  fullCut += ' && CA15fj1_maxsubjetbtag>0.89'
if not region=='msd':
  fullCut += ' && CA15fj1_mSD>150 && CA15fj1_mSD<240'

plot.SetCut(root.TCut(fullCut))
plot.SetMCWeight(root.TCut('mcWeight'))

### DEFINE PROCESSES ###
schs = root.Process('Signal [CHS]',root.kST,root.kWjets)
spuppi = root.Process('Signal [PUPPI]',root.kTTbar,root.kWjets)
bchs = root.Process('QCD-like [CHS]',root.kQCD,root.kZjets)
bpuppi = root.Process('QCD-like [PUPPI]',root.kZjets,root.kZjets)
schs.dashed=True
bchs.dashed=True
processes = [schs,spuppi,bchs,bpuppi]

### ASSIGN FILES TO PROCESSES ###
schs.AddFile(baseDir+'signal_chs.root')
spuppi.AddFile(baseDir+'signal_puppi.root')
bchs.AddFile(baseDir+'wjets_chs.root')
bpuppi.AddFile(baseDir+'wjets_puppi.root')
for p in processes:
  plot.AddProcess(p)

### CHOOSE DISTRIBUTIONS, LABELS ###
if region=='tau32':
  plot.AddDistribution(root.Distribution("CA15fj1_tau3/CA15fj1_tau2",0,1.2,12,"#tau_{3}/#tau_{3}","a.u."))
elif region=='btag':
  plot.AddDistribution(root.Distribution("CA15fj1_maxsubjetbtag",0,1,20,"max subjet b-tag","a.u."))
elif region=='msd':
  plot.AddDistribution(root.Distribution("CA15fj1_mSD",40,440,20,"soft drop mass","a.u."))

### DRAW ###
plot.DrawAll(baseDir+'/figs/')
# cataloging is now done dynamically
