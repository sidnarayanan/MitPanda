#!/usr/bin/env python

from MitPanda.Tools.MultiThreading import MacroRunner


if __name__ == "__main__":
  mr = MacroRunner('applyBDT.C',None,["libMitPhysicsUtils.so"])
  argList = [['/local/snarayan/monotop25ns_v2/'+x+'.root'] for x in 
                ['Diboson',
                 'TTbar',
                 'QCD',
                 'WJets',
                 'ZtoNuNu',
                 'ZJets',
                 'Signal1p1',
                 'Signal2p1',
                 'SingleTop',
                ]
            ]
  mr.setArgList(argList)
  mr.run(5)


