#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv
tcut = root.TCut
### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/NeroSkimmer.h")
gSystem.Load('libMitPandaTools.so')
ns = root.NeroSkimmer
### HELPER FUNCTIONS ###
def setBins(dist,bins):
  for b in bins:
    dist.AddBinEdge(b)

def contains(s1,s2):
  if type(s2)==type(""):
    return s1.find(s2)>=0
  else:
    for s in s2:
      if s1.find(s)>=0:
        return True
    return False

### SET GLOBAL VARIABLES ###
region = argv[1]
try:
  additionalcut = argv[2]
except:
  additionalcut = None
makePlots = True
baseDir = '/home/snarayan/cms/root/puppi/'
lumi = 2245.

### DEFINE REGIONS ###
cuts = {}
'''
metTrigger=tcut('trigger&1||trigger&2')
eleTrigger=tcut('trigger&4||trigger&8')
phoTrigger=tcut('trigger&32||trigger&64') # skip 120 trigger for now

massCut = tcut('CA15fj1_mSD>150 && CA15fj1_mSD<240')
tau32Cut = tcut('CA15fj1_tau3/CA15fj1_tau2<0.78')
btagCut = tcut('CA15fj1_maxsubjetbtag>0.89')
SRCut = tcut('puppimet>250 && dPhiPuppiMET>1.1 && (nLooseElectron+nlooseMuon+nLoosePhoton)==0')+metTrigger
MuTTCRCut = tcut('nLooseMuon==1 && looseLep1IsTight==1 && UWmag>250 && dPhiUW>1.1 && (nLooseElectron+nLoosePhoton)==0')+metTrigger
'''
# resolved

# basic CA15 selection
cuts['ca15']             = tcut('nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4')

#N-1 cuts
'''
cuts['ca15_nottau32SR']  = cuts['ca15'] + massCut + btagCut + SRCut
cuts['ca15_notmsdSR']  = cuts['ca15'] + tau32Cut + btagCut + SRCut
cuts['ca15_notbSR']  = cuts['ca15'] + massCut + tau32Cut + SRCut

cuts['ca15_nottau32CR']  = cuts['ca15'] + massCut + btagCut + MuTTCRCut
cuts['ca15_notmsdCR']  = cuts['ca15'] + tau32Cut + btagCut + MuTTCRCut
cuts['ca15_notbCR']  = cuts['ca15'] + massCut + tau32Cut + MuTTCRCut
'''

# regions 
cuts['ca15_SR']        = tcut('region&%i'%(ns.kMonotopSR))
cuts['ca15_muttCR']    = tcut('region&%i'%(ns.kMonotopMuTTCR))
cuts['ca15_muZCR']     = tcut('region&%i'%(ns.kMonotopMuZCR))
#cuts['ca15_muWCR']     = tcut('(region&%i!=0)'%(ns.kMonotopMuWCR))
cuts['ca15_muZCR']     = tcut('UZmag>250 && nCA15fj==1 && nLooseMuon==2 && (looseLep1IsTight==1||looseLep2IsTight==1) && (nLooseElectron+nLoosePhoton+nTau)==0 && dPhiUZ>1.1 && CA15fj1_tau3/CA15fj1_tau2<0.78')
cuts['ca15_muWCR']     = tcut('UWmag>250 && nCA15fj==1 && nLooseMuon==1 && looseLep1IsTight==1 && (nLooseElectron+nLoosePhoton+nTau)==0 && dPhiUW>1.1 && CA15fj1_tau3/CA15fj1_tau2>0.78')
#cuts['ca15_muWCR']     = tcut('UWmag>250 && nCA15fj==1 && nLooseMuon==1 && looseLep1IsTight==1 && (nLooseElectron+nLoosePhoton+nTau)==0 && dPhiUW>1.1 && CA15fj1_tau3/CA15fj1_tau2>0.78')
cuts['ca15_ACR']       = tcut('region&%i'%(ns.kMonotopACR))

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
plot.Logy(False)
plot.Ratio(True)
plot.SetNormFactor(False)
plot.SetLumi(lumi)
fullCut = region + ('_'+additionalcut if additionalcut!=None else '')
plot.SetCut(cuts[fullCut])
triggerEff = "(((met<240)*(0.975+(met-200)*0.000625))+(met>240))"
if contains(additionalcut,["SR","ttCR"]):
  plot.SetMCWeight("%f*mcWeight*kWFactor*ewkWCorr*kZFactor*ewkZCorr*lepSF*%s"%(lumi,triggerEff))
else:
  #plot.SetMCWeight("%f*mcWeight*kWFactor*ewkWCorr*lepSF"%(lumi))
  plot.SetMCWeight("%f*mcWeight*kWFactor*ewkWCorr*kZFactor*ewkZCorr*lepSF*%s"%(lumi,triggerEff))

### DEFINE PROCESSES ###
zjets     = root.Process('Z+jets',root.kZjets)
wjets     = root.Process('W+jets',root.kWjets)
diboson   = root.Process('Diboson',root.kDiboson)
ttbar     = root.Process('t#bar{t}',root.kTTbar)
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
gjets     = root.Process('#gamma+jets',root.kGjets)
signal    = root.Process("Signal",root.kSignal)
data      = root.Process("Data",root.kData)
processes = [zjets,wjets,diboson,singletop,ttbar,qcd]

### ASSIGN FILES TO PROCESSES ###
zjets.AddFile(baseDir+'ZJets.root')
zjets.AddFile(baseDir+'ZtoNuNu.root')
wjets.AddFile(baseDir+'WJets.root')
diboson.AddFile(baseDir+'Diboson.root')
ttbar.AddFile(baseDir+'TTbar.root')
singletop.AddFile(baseDir+'SingleTop.root')
if contains(additionalcut,"ACR"):
  processes.append(gjets)
  gjets.AddFile(baseDir+'GJets.root')
qcd.AddFile(baseDir+'QCD.root')

if not contains(additionalcut,"SR"):
  if contains(additionalcut,"ACR"):
    data.AddFile(baseDir+"SinglePhoton.root")
  elif contains(additionalcut,"e"):
    data.AddFile(baseDir+"SingleElectron.root")
  else:
    data.AddFile(baseDir+'MET.root')
  processes.append(data)
else:
  plot.Ratio(False)
  if not contains(additionalcut,["notag"]):
    plot.SetSignalScale(0.1)
  signal.AddFile(baseDir+'Signal1p1.root')
  processes.append(signal)

for p in processes:
  plot.AddProcess(p)

recoilBins = [250,300,350,400,450,500,600,700]
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
ptLabel = "p_{T}>250 GeV"
cutLabel = "cut-based top tag" if additionalcut=="SR" else ""
plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.1f fb^{-1} (13 TeV)}{%s}}"%(lumi/1000,cutLabel))

# define what "recoil" means
if contains(additionalcut,"mu"):
  lep='#mu'
else:
  lep='e'
# choose recoil
recoil=None
if additionalcut=='SR' or additionalcut=='testSR':
  recoil=root.Distribution("met",nRecoilBins,"MET","Events/GeV")
elif contains(additionalcut,'ZCR'):
  recoil=root.Distribution("UZmag",nRecoilBins,"Recoil (%s%s)"%(lep,lep),"Events/GeV")
elif contains(additionalcut,'WCR'):
  recoil=root.Distribution("UWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
elif contains(additionalcut,'ttCR'):
  recoil=root.Distribution("UWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
elif contains(additionalcut,'ACR'):
  recoil=root.Distribution("UAmag",nRecoilBins,"Recoil (#gamma)","Events/GeV")
if recoil:
  setBins(recoil,recoilBins)
  plot.AddDistribution(recoil)
#    plot.AddDistribution(root.Distribution("CA15fj1_mSD",0,400,20,"soft drop mass","Events/20 GeV"))

if contains(additionalcut,["notag","not"]):
  plot.AddDistribution(root.Distribution("CA15fj1_tau3/CA15fj1_tau2",0,1.2,12,"#tau_{3}/#tau_{3}","Events"))
  plot.AddDistribution(root.Distribution("CA15fj1_maxsubjetbtag",0,1,20,"max subjet b-tag","Events/0.05"))
  plot.AddDistribution(root.Distribution("CA15fj1_mSD",0,400,20,"soft drop mass","Events/20 GeV"))

plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))

### DRAW AND CATALOGUE ###
try:
  mkdir(baseDir+'/figs/'+fullCut)
except OSError:
  pass
plot.DrawAll(baseDir+'/figs/'+fullCut)
