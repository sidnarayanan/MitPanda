#!/usr/bin/env python

import ROOT as root
from sys import argv,exit
from os import system,getenv
from glob import glob
from re import sub

eos=getenv('EOS')+'/monotop/v1.6.0/'

datasetName=argv[1]

print eos+datasetName+'/*/*/*/NeroNtuples*root'
listOfFiles = glob(eos+datasetName+'/*/*/*/NeroNtuples*root')
if len(listOfFiles)==0:
  print 'NO FILES FOUND'
  exit(0)

histPath = sub(r'NeroNtuples_[0-9]*\.root','hist.root',listOfFiles[0])
#histPath = eos+datasetName+'/hist.root'
print histPath
histFile = root.TFile.Open(histPath,'RECREATE')

sumHist = root.TH1F("hDTotalMCWeight","hDTotalMCWeight",1,0,2);

counter=0
print "Adding: "
for f in listOfFiles:
  print "\t",f
  addFile = root.TFile.Open(f)
  addTree = addFile.Get('nero/all')
  addHist = root.TH1F('h%i'%counter,'h%i'%counter,1,0,2)
  addTree.Draw('1>>h%i'%counter,'mcWeight/TMath::Abs(mcWeight)')
#  addHist = addFile.Get('nero/hDTotalMcWeight')
  sumHist.Add(addHist)
  counter+=1

histFile.WriteTObject(sumHist,"hDTotalMCWeight")
histFile.Close()


