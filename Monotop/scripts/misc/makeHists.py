#!/usr/bin/env python

import ROOT as root
from array import array


#metbins = array('d',[250,300,350,400,450,700])
metbins = array('d',[400,450,700])
nbins = len(metbins)-1
baseDir = '/home/snarayan/cms/root/nero_pfchs_v3/'
lumi = 2245
triggerEff = "(((met<240)*(0.975+(met-200)*0.000625))+(met>240))"
weight = "%f*mcWeight*lepSF*puWeight*ewkWCorr*kWFactor*ewkZCorr*kZFactor*ewkACorr*kAFactor*%s"%(lumi,triggerEff)
processes = { 'QCD':['QCD'],
#              'Diboson':['Diboson'],
              'Signal':['Signal2p1'],
#              'SingleTop':['SingleTop'],
              'SingleLep':['TTbar','WJets','SingleTop','Diboson'],
              'ZJets':['ZtoNuNu','ZJets'],
              'Data':['MET']
            }
toptag = ' && nCA15fj==1 && CA15fj1_mSD>150 && CA15fj1_mSD<240 && CA15fj1_maxsubjetbtag>0.89 && CA15fj1_tau3/CA15fj1_tau2<0.78 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4'
cuts = { 'signal' : ('met','met>250 && dPhiMET>1.1 && (nLoosePhoton+nLooseMuon+nLooseElectron+nTau)==0' + toptag),
         'singlemu' : ('UWmag','UWmag>250 && dPhiUW>1.1 && (nLoosePhoton+nLooseElectron+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1' + toptag),
         'doublemu' : ('UZmag','UZmag>250 && dPhiUZ>1.1 && (nLoosePhoton+nLooseElectron+nTau)==0 && nLooseMuon==2 && (looseLep1IsTight==1||looseLep2IsTight==1) &&  nCA15fj==1 && CA15fj1_tau3/CA15fj1_tau2<0.78')
       }

triggers = ' && (trigger&1!=0||trigger&2!=0)'

fOut = root.TFile.Open(baseDir+'histogramsHighMet.root','RECREATE')

processFiles = {}

for process,names in processes.iteritems():
  processFiles[process] = []
  for name in names:
    newfile = root.TFile.Open(baseDir+name+'.root')
    newtree = newfile.Get('events')
    processFiles[process].append((newfile,newtree))

datacard = {}
# datacard[region] = [observed, {process:expected}]

for region in cuts:
  datacard[region] = [0,{}]

for region,cut in cuts.iteritems():
  var = cut[0]
  mccut = weight+'*('+cut[1]+triggers+')'
  datacut = cut[1]+triggers
  for process,files in processFiles.iteritems():
    if process=='Data':
      actualcut = datacut
    else:
      actualcut = mccut
    if region=='signal' and process=='Data':
      continue
    histname = 'h_'+region+'_'+process
    hnew = root.TH1F(histname,histname,nbins,metbins)
    filecounter=0
    for f in files:
      htmp = root.TH1F('htmp','htmp',nbins,metbins)
      t = f[1]
      t.Draw(var+'>>htmp',actualcut)
      hnew.Add(htmp)
      fOut.WriteTObject(htmp,'hIndividual_'+region+'_'+processes[process][filecounter])
      htmp.Delete()
      filecounter += 1
    if process=='Data':
      datacard[region][0] = hnew.Integral()
    else:
      datacard[region][1][process] = (hnew.Integral(),hnew.GetEntries())
    fOut.WriteTObject(hnew,histname)

fOut.Close()

binobsline  ='bin         '
obsrateline ='observation '

binline      ='bin        '
process1line ='process    '
process2line ='process    '
rateline     ='rate       '

statlines    =[]
nentries = []

lineCounter=0
regionCounter=1
for region,card in datacard.iteritems():
  binobsline += "%10i "%(regionCounter)
  obsrateline += "%10f "%(card[0])
  print region
  print '\tobserved:',card[0]
  processCounter=1
  for k,vals in card[1].iteritems():
    v,entries = vals
    if v<0.000001:
      processCounter += 1
      continue
    if k=='Signal' and not(region=='signal'):
      processCounter += 1
      continue
    print '\t%s:'%(k),v
    binline += "%10i "%(regionCounter)
    process1line += "%10s "%(k)
    process2line += "%10i "%( 0 if k=='Signal' else processCounter)
    rateline += "%10f "%(v)
    nentries.append((entries,v))
    lineCounter += 1
    processCounter += 1
  regionCounter += 1

for iL in xrange(lineCounter):
  entries,rate = nentries[iL]
  statlines.append('st%i  gmN %10i '%(iL,entries))
  for jL in xrange(lineCounter):
    if jL==iL:
      statlines[iL] += '%10f'%(rate/entries)
    else:
      statlines[iL] += '%10s'%('-')



print ''
print binobsline
print obsrateline
print ''
print binline
print process1line
print process2line
print rateline
print ''
for l in statlines:
  print l
