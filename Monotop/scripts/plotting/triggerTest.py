#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from sys import argv,exit
from array import array
from MitPanda.Tools.Misc import *
from math import sqrt
from ROOT import gROOT
from MitPanda.Monotop.Selection import *
from re import sub

baseDir = '/afs/cern.ch/work/s/snarayan/skims/monotop_trigger/'

### DEFINE REGIONS ###

cut = cuts['singleelectronw'] 
centweight = 'normalizedWeight*sf_eleTrig'
upweight = 'normalizedWeight*sf_eleTrigUp'
downweight = 'normalizedWeight*sf_eleTrigDown'

gROOT.LoadMacro('${CMSSW_BASE}/src/MitPanda/Tools/interface/HistogramDrawer.h')
gSystem.Load('libMitPandaTools.so')

recoilBins = [250,300,350,400,500,1000]
nRecoilBins = len(recoilBins)-1
recoilBins = array('f',recoilBins)

plotr = root.HistogramDrawer()
plotr.SetRatioStyle()
plotr.AddCMSLabel()
plotr.InitLegend(.7,.7,.9,.9)
plotr.SetAbsMin(0.99)
plotr.SetMaxScale(1.01)

counter=0

which='WJets'
fin = root.TFile(baseDir+which+'.root')
tin = fin.Get('events')

ctmp = root.TCanvas()

def getMETHist(tree,cut,weight):
  global counter
  ctmp.cd()
  h = root.TH1F('h%i'%counter,'h%i'%counter,nRecoilBins,recoilBins)
  h.GetXaxis().SetTitle('U [GeV]')
  h.GetYaxis().SetTitle('1+#delta#sigma/#sigma')
  tree.Draw('UWmag>>h%i'%counter,tTIMES(weight,cut))
  counter += 1
  h.SetFillStyle(0)
  return h

def plotWZ(label):
  hcent = getMETHist(tin,cut,centweight)
  hup = getMETHist(tin,cut,upweight)
  hdown = getMETHist(tin,cut,downweight)
  hup.Divide(hcent)
  hdown.Divide(hcent)
  for iB in xrange(1,hcent.GetNbinsX()+1):
    val = hcent.GetBinContent(iB)
    hcent.SetBinError(iB,hcent.GetBinError(iB)/val)
    hcent.SetBinContent(iB,1)
  hup.SetLineWidth(3); hdown.SetLineWidth(3);
  plotr.AddHistogram(hup,'e trigger up',root.kExtra2)
  plotr.AddHistogram(hdown,'e trigger down',root.kExtra1)
  plotr.AddPlotLabel(label,.18,.8,False,42,.05)

  hcent.SetFillStyle(3005)
  hcent.SetFillColor(root.kGray+1)
  hcent.SetLineWidth(0)

  #plotr.AddAdditional(hcent,'f','stat. unc.')
  plotr.Draw('~/www/figs/monotop_2016_13fb/triggerTest/',which)
  plotr.Reset()

labels = {
    'WJets':'W#rightarrowe#nu',
    }

plotWZ(labels[which])
