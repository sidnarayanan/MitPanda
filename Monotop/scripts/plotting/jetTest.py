#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from sys import argv,exit
from array import array
from MitPanda.Tools.Misc import *
from math import sqrt
from ROOT import gROOT
from MitPanda.Monotop.Selection import *
from re import sub

baseDir = '/afs/cern.ch/work/s/snarayan/skims/monotop_jecTest/'

### DEFINE REGIONS ###

centcut = removeCut(cuts['signal'],'metFilter')
upcut = sub('CA15fj1_pt','CA15fj1_ptUp',centcut)
downcut = sub('CA15fj1_pt','CA15fj1_ptDown',centcut)
weight = 'normalizedWeight'

gROOT.LoadMacro('${CMSSW_BASE}/src/MitPanda/Tools/interface/HistogramDrawer.h')
gSystem.Load('libMitPandaTools.so')

# recoilBins = [200., 260.0, 320.0,  390.0,  470.0, 550.0,  640.0, 740.0, 840.0, 1250.0]
recoilBins = [250,300,350,400,500,1000]
nRecoilBins = len(recoilBins)-1
recoilBins = array('f',recoilBins)

plotr = root.HistogramDrawer()
plotr.SetRatioStyle()
plotr.AddCMSLabel()
plotr.InitLegend(.75,.7,.9,.9)
plotr.SetAbsMin(0.9)
plotr.SetMaxScale(1.2)

counter=0

which=argv[1]
fin = root.TFile(baseDir+which+'.root')
tin = fin.Get('events')

ctmp = root.TCanvas()

def getMETHist(tree,cut,binwidth=False):
  global counter
  ctmp.cd()
  h = root.TH1F('h%i'%counter,'h%i'%counter,nRecoilBins,recoilBins)
  h.GetXaxis().SetTitle('U [GeV]')
  h.GetYaxis().SetTitle('1+#delta#sigma/#sigma')
  if binwidth:
    h.Scale(1,'width')
  tree.Draw('puppimet>>h%i'%counter,tTIMES(weight,cut))
  counter += 1
  h.SetFillStyle(0)
  return h

def plotWZ(label):
  hcent = getMETHist(tin,centcut)
  hup = getMETHist(tin,upcut)
  hdown = getMETHist(tin,downcut)
  hup.Divide(hcent)
  hdown.Divide(hcent)
  for iB in xrange(1,hcent.GetNbinsX()+1):
    val = hcent.GetBinContent(iB)
    hcent.SetBinError(iB,hcent.GetBinError(iB)/val)
    hcent.SetBinContent(iB,1)
  hup.SetLineWidth(3); hdown.SetLineWidth(3);
  plotr.AddHistogram(hup,'JEC Up',root.kExtra2)
  plotr.AddHistogram(hdown,'JEC Down',root.kExtra1)
  plotr.AddPlotLabel(label,.18,.8,False,42,.05)

  hcent.SetFillStyle(3005)
  hcent.SetFillColor(root.kGray+1)
  hcent.SetLineWidth(0)

  plotr.AddAdditional(hcent,'e2','stat. unc.')
  plotr.Draw('~/www/figs/monotop_2016_13fb/jecTest/',which)
  plotr.Reset()

labels = {
    'ZtoNuNu':'Z#rightarrow#nu#nu',
    'monotop_fcnc_mMed1100':'FCNC m_{V}=1.1 TeV',
    'monotop_res_mMed1100':'Resonant m_{S}=1.1 TeV',
    }

plotWZ(labels[which])
