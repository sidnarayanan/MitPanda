#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from sys import argv,exit
from array import array
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.Selection import *
from ROOT import gROOT

gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

plot = root.PlotUtility()
plot.SetTDRStyle()
plot.AddCMSLabel()
plot.Stack(False)

baseDir = '/afs/cern.ch/work/s/snarayan/skims/monotop_80_v8/'

weight = '%f*normalizedWeight'%(12918.)
cut = cuts['singlemuon']

plot.SetMCWeight(weight)
plot.SetCut(cut)
plot.Stack(True)
#plot.Logy(True)

hf=False

t = root.Process('t#bar{t} Monte Carlo',root.kTTbar)
t.AddFile(baseDir+'TTbar_Powheg.root')
w = root.Process('W+jets Monte Carlo',root.kWjets)
w.AddFile(baseDir+'WJets.root')

if hf:
  label = 'jethf_'
  flav=5
  jettype='b'
else:
  label = 'jetlf_'
  flav=0
  jettype='LF'

for p in [t,w]:
  plot.AddProcess(p)

jetpt = root.Distribution('jetIso1Pt*(jetIso1Flav==%i)'%flav,1,801,20,'leading isolated %s-jet p_{T}'%jettype,'Events/40 GeV')
jetpt.filename='jetIso1Pt'
plot.AddDistribution(jetpt)

sjpt = root.Distribution('sj1Pt*(sj1Flav==%i)'%flav,100,900,20,'leading %s-subjet p_{T}'%jettype,'Events/40 GeV')
sjpt.filename='sj1Pt'
plot.AddDistribution(sjpt)
plot.DrawAll('~/www/figs/monotop_2016_13fb/'+label)

