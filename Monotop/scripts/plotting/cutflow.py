#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv

### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

### SET GLOBAL VARIABLES ###
region = int(argv[1])
#baseDir = '/local/snarayan/monotop/'
baseDir = '/afs/cern.ch/user/s/snarayan/work/skims/monotop_80/'
lumi = 2300.

### DEFINE REGIONS ###
cuts = [
          '',
          ' puppimet>250 && met>175',
#          ' met>250',
          ' && nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4',
          ' && (nLooseElectron+nLooseMuon)==0',
          ' && (nLoosePhoton)==0',
          ' && (nTau)==0',
          ' && dPhiPuppiMET>1.1',
          #' && dPhiMET>1.1',
          ' && CA15fj1_maxsjbtag>0.89',
          ' && CA15fj1_mSD>150 && CA15fj1_mSD<240',
          ' && jetNIsoBtags==0',
          ' && CA15fj1_tau3/CA15fj1_tau2<0.78',
        ]
'''
cuts = [
          '',
          ' met>250 && (nLooseElectron+nLooseMuon)==0 && (nTau)==0 && (nLoosePhoton)==0',
          ' && dPhiMET>1.1',
          ' && nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4',
          ' && CA15fj1_tau3/CA15fj1_tau2<0.61 && CA15fj1_mSD>110 && CA15fj1_mSD<210',
          ' && CA15fj1_maxsjbtag>0.76',
          ' && jetNIsoBtags==0'
        ]
'''
### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
plot.Logy(True)
plot.SetNormFactor(False)
plot.SetLumi(lumi)
plot.DrawMCErrors(True)
fullCut=''
for i in xrange(region):
  fullCut += cuts[i]
if  region>0:
  print cuts[region-1]
plot.SetCut(fullCut)
print fullCut
plot.SetMCWeight('%f*mcWeight'%(lumi))

### DEFINE PROCESSES ###
sync = root.Process('Sync',root.kExtra1)
processes = [sync]

### ASSIGN FILES TO PROCESSES ###
#sync.AddFile(baseDir+'zeynep_TTbarDM.root')
sync.AddFile(baseDir+'/TTbarDM_fast.root')
#sync.AddFile(baseDir+'ZtoNuNuNLO.root')
#sync.AddFile(baseDir+'TTbar.root'); sync.additionalWeight = root.TCut("sf_tt")
#sync.AddFile(baseDir+'ZtoNuNu.root'); sync.additionalWeight = root.TCut("sf_qcdZ*sf_ewkZ")
for p in processes:
  plot.AddProcess(p)

### CHOOSE DISTRIBUTIONS, LABELS ###
#plot.AddDistribution(root.Distribution("CA15fj1_pt",250,750,50,"pT","fatjet pT"))
plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))

### DRAW ###
try:
  mkdir(baseDir+'/figs/cutflow_'+str(region))
except OSError:
  pass
plot.DrawAll(baseDir+'/figs/cutflow_%i_'%region)
# cataloging is now done dynamically
