#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv,exit
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.Selection import *

blind=True

### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

### SET GLOBAL VARIABLES ###
region = argv[1]
nminusone = None
if len(argv)>2:
  nminusone = argv[2]
makePlots = True
#baseDir = '/local/snarayan/nero_pfchs_v3/'
baseDir = '/local/snarayan/monotop_80/'
lumi = 2600.

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
if not region=='test' and nminusone==None:
  plot.Logy(True)
plot.Ratio(1)
plot.DrawMCErrors(True)
plot.SetTDRStyle()
plot.SetNormFactor(False)
plot.FixRatio(True)
plot.SetLumi(lumi/1000)
plot.AddCMSLabel()
plot.AddLumiLabel()

label = region
cut = cuts[region]
if nminusone!=None:
  if nminusone=='tau32':
    cut = removeCut(cut,'CA15fj1_tau3/CA15fj1_tau2')
  elif nminusone=='sjbtag':
    cut = removeCut(cut,'CA15fj1_maxsjbtag')
  elif nminusone=='mSD':
    cut = removeCut(cut,'CA15fj1_mSD')
  label = 'NMinus1_%s_%s'%(nminusone,label)

print cut
plot.SetCut(root.TCut(cut))

#if 'signal' in region or 'muon' in region:
#  triggerEff = "(((met<240)*(0.975+(met-200)*0.000625))+(met>240))"
#else:
#  triggerEff = '1'

weight = '%f*normalizedWeight'%lumi
if 'signal' in region:
  weight = weights['signal']%lumi
elif 'single' in region and 'w' in region:
  weight = weights['w']%lumi
elif 'single' in region and 'top' in region:
  weight = weights['top']%lumi
elif 'di' in region or 'photon' in region:
  weight = weights['notag']%lumi
print weight
plot.SetMCWeight(weight)

### DEFINE PROCESSES ###
zjets     = root.Process('Z+jets',root.kZjets)
wjets     = root.Process('W+jets',root.kWjets)
diboson   = root.Process('Diboson',root.kDiboson)
ttbar1lt     = root.Process('t#bar{t} [1L t]',root.kTTbar); ttbar1lt.additionalWeight = root.TCut('0.8')
ttbar1ltbar     = root.Process('t#bar{t} [1L #bar{t}]',root.kExtra2); ttbar1ltbar.additionalWeight = root.TCut('0.8')
ttbar2l     = root.Process('t#bar{t} [2L]',root.kExtra3)
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
gjets     = root.Process('#gamma+jets',root.kGjets)
signal    = root.Process("Signal",root.kSignal)
data      = root.Process("Data",root.kData)
processes = [qcd,diboson,singletop,wjets,ttbar2l,ttbar1ltbar,ttbar1lt,zjets]

### ASSIGN FILES TO PROCESSES ###
zjets.AddFile(baseDir+'ZJets.root')
zjets.AddFile(baseDir+'ZtoNuNu.root')
wjets.AddFile(baseDir+'WJets.root')
diboson.AddFile(baseDir+'Diboson.root')
ttbar1lt.AddFile(baseDir+'TTbar_1LT.root')
ttbar1ltbar.AddFile(baseDir+'TTbar_1LTbar.root')
ttbar2l.AddFile(baseDir+'TTbar_2L.root')
singletop.AddFile(baseDir+'SingleTop.root')
if contains(region,"photon"):
  processes.append(gjets)
  gjets.AddFile(baseDir+'GJets.root')
qcd.AddFile(baseDir+'QCD.root')

if contains(region,"photon"):
  print 'Using SinglePhoton'
  data.AddFile(baseDir+"SinglePhoton.root"); data.additionalCut = root.TCut(phoTrigger)
elif contains(region,"electron"):
  print 'Using SingleElectron'
  data.AddFile(baseDir+"SingleElectron.root"); data.additionalCut = root.TCut(eleTrigger)
else:
  print 'Using MET'
  data.AddFile(baseDir+'MET.root');  data.additionalCut = root.TCut(metTrigger)
if not(blind and 'signal' in region):
  processes.append(data)

for p in processes:
  plot.AddProcess(p)

recoilBins = [250,300,350,400,500,1000]
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
# define what "recoil" means
if contains(region,"electron"):
  lep='e'
else:
  lep='#mu'

recoil=None
if nminusone==None:
  plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
  if region=='signal':
    recoil=root.Distribution("met",nRecoilBins,"MET","Events/GeV")
  elif 'photon' in region:
    recoil=root.Distribution("UAmag",nRecoilBins,"Recoil (#gamma)","Events/GeV")
  elif 'di' in region:
    recoil=root.Distribution("UZmag",nRecoilBins,"Recoil (%s%s)"%(lep,lep),"Events/GeV")
  elif ('single' in region and 'w' in region) or region=='test':
    recoil=root.Distribution("UWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
  elif 'single' in region and 'top' in region:
    recoil=root.Distribution("UWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
if recoil:
  setBins(recoil,recoilBins)
  plot.AddDistribution(recoil)
  plot.AddDistribution(root.Distribution("CA15fj1_mSD",0,400,20,"soft drop mass","Events/20 GeV",0.1))

plotvars = (nminusone==None)

if nminusone=="tau32" or plotvars:
  plot.AddDistribution(root.Distribution("CA15fj1_tau3/CA15fj1_tau2",0,1.2,12,"#tau_{3}/#tau_{3}","Events"))
if nminusone=="sjbtag" or plotvars:
  plot.AddDistribution(root.Distribution("CA15fj1_maxsjbtag",0,1,20,"max subjet b-tag","Events/0.05"))
if nminusone=="mSD" or plotvars:
  plot.AddDistribution(root.Distribution("CA15fj1_mSD",40,440,20,"soft drop mass","Events/20 GeV"))

### DRAW AND CATALOGUE ###
plot.DrawAll('~/public_html/figs/monotop_debug/scaled_'+label+'_')

