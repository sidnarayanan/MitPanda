#!/usr/bin/env python

'''

Produce  trigger eff. plots

'''

from RecoLuminosity.LumiDB import argparse
import math
import fnmatch
import logging
import os
import ctypes
import ROOT
from ROOT import TFile, TAttMarker, TCanvas, TColor, gStyle, TStyle, TLegend, TLatex, TH1F, PyConfig, gROOT, TGraph, TGraphAsymmErrors, TMultiGraph, TEllipse, TApplication, PyROOT, THStack, TPad, gPad, RooHistError
import shutil
import sys
from array import array
import numpy as np
#from cmsstyle import style

PyConfig.IgnoreCommandLineOptions = True

from ROOT import TCanvas, TColor, gStyle, TStyle, TLatex, PyConfig, gROOT

def style():
    gROOT.SetStyle("Plain")
    gStyle.SetPalette(1)
    gStyle.SetPaintTextFormat("1.4f")
    gStyle.SetPaintTextFormat("1.4f")

    # For the canvas:
    gStyle.SetCanvasBorderMode(0)
    gStyle.SetCanvasColor(0)
    gStyle.SetCanvasDefX(0)   #Position on screen
    gStyle.SetCanvasDefY(0)

    # For the Pad:
    gStyle.SetPadBorderMode(0)
    gStyle.SetPadColor(0)
    gStyle.SetPadGridX(True)
    gStyle.SetPadGridY(True)
    gStyle.SetGridColor(0)
    gStyle.SetGridStyle(3)
    gStyle.SetGridWidth(1)

    # For the frame:
    gStyle.SetFrameBorderMode(0)
    gStyle.SetFrameBorderSize(1)
    gStyle.SetFrameFillColor(0)
    gStyle.SetFrameFillStyle(0)
    gStyle.SetFrameLineColor(1)
    gStyle.SetFrameLineStyle(1)
    gStyle.SetFrameLineWidth(1)

    # For the histo:
    gStyle.SetHistLineColor(1)
    gStyle.SetHistLineStyle(0)
    gStyle.SetHistLineWidth(1)
    gStyle.SetEndErrorSize(2)
    gStyle.SetMarkerStyle(20)
    gStyle.SetMarkerSize(4)
    gStyle.SetFuncColor(2)
    gStyle.SetFuncStyle(1)
    gStyle.SetFuncWidth(1)
    #For the date:
    gStyle.SetOptDate(0)

    # For the statistics box:
    gStyle.SetOptFile(0)
    gStyle.SetOptStat(0) # To display the mean and RMS:   SetOptStat("mr")
    gStyle.SetStatColor(0)
    gStyle.SetStatFont(42)
    gStyle.SetStatFontSize(0.05)
    gStyle.SetStatTextColor(1)
    gStyle.SetStatFormat("5.4g")
    gStyle.SetStatBorderSize(1)
    gStyle.SetStatH(0.5)
    gStyle.SetStatW(0.35)
    
    # Margins:
    gStyle.SetPadTopMargin(0.08)
    gStyle.SetPadBottomMargin(0.16)
    gStyle.SetPadLeftMargin(0.16)
    gStyle.SetPadRightMargin(0.15)

    # For the Global title:
    gStyle.SetOptTitle(0)
    gStyle.SetTitleFont(42)
    gStyle.SetTitleColor(1)
    gStyle.SetTitleTextColor(1)
    gStyle.SetTitleFillColor(10)
    gStyle.SetTitleFontSize(0.05)
    
    # For the axis titles:
    gStyle.SetTitleColor(1, "XYZ")
    gStyle.SetTitleFont(43, "XYZ")
    gStyle.SetTitleSize(48, "XYZ")
    gStyle.SetTitleOffset(1.2,"X")
    gStyle.SetTitleOffset(1.2,"Y")

    # For the axis labels:
    gStyle.SetLabelColor(1, "XYZ")
    gStyle.SetLabelFont(43, "XYZ")
    gStyle.SetLabelOffset(0.007, "XYZ")
    gStyle.SetLabelSize(55, "XYZ")
    gStyle.SetTitleSize(55, "XYZ")
    
    # For the axis:
    gStyle.SetAxisColor(1, "XYZ")
    gStyle.SetStripDecimals(True)
    gStyle.SetTickLength(0.03, "XYZ")
    gStyle.SetNdivisions(505, "YZ")
    gStyle.SetPadTickX(1)  # To get tick marks on the opposite side of the frame
    gStyle.SetPadTickY(1)

    # Change for log plots:
    gStyle.SetOptLogx(0)
    gStyle.SetOptLogy(0)
    gStyle.SetOptLogz(0)

    # Postscript options:
    gStyle.SetPaperSize(20.,20.)


def plot(inputroot, histogram, output, xtitle):
    gStyle.SetOptStat(0)
    gStyle.SetPalette(1)
    gStyle.SetErrorX(1);
    style()    

    is2D = False
    if histogram=="hEffEtaPt":
        is2D = True

    hTrig = inputroot.Get("%s" % histogram);
    hTrig = inputroot.Get("%s" % histogram);
  

    #rebinning = [0,100,200,300,400,600,1000]
    rebinning = [0,100,500,1000]
    nRebinning = len(rebinning)-1
    arrbins = array('d',rebinning)

    
    c = TCanvas("canvas","canvas", 900, 800)
    c.cd(0)
  
    hTrig.Draw()
    c.SaveAs(output+'_test.png')
    c.SaveAs(output+'_test.pdf')

    htemp = hTrig.Clone()
    for i in range(1,htemp.GetNbinsX()+1):
        htemp.SetBinContent(i,hTrig.GetBinContent(i)*hTrig.GetBinWidth(i))
        htemp.SetBinError(i,hTrig.GetBinError(i)*hTrig.GetBinWidth(i))
    

    hrebin = htemp.Rebin(nRebinning,"hrebin",arrbins)

    htemp2 = hrebin.Clone()
    for i in range(1,htemp2.GetNbinsX()+1):
        htemp2.SetBinContent(i,hrebin.GetBinContent(i)/hrebin.GetBinWidth(i))
        htemp2.SetBinError(i,hrebin.GetBinError(i)/hrebin.GetBinWidth(i))
    

    c.Clear()

    if not is2D:
#        for i in range(1,hTrig.GetNbinsX()+1):
        #print hTrig.GetBinError(i)
#            hTrig.SetBinError(i,0.00001)
#            hrebin.GetYaxis().SetRangeUser(0,1.3)
        htemp2.GetXaxis().SetTitle("%s (GeV)" % xtitle)
        htemp2.GetYaxis().SetTitle("Efficiency")
        htemp2.SetMarkerStyle(20);
        htemp2.SetMinimum(0)
        htemp2.Draw("E1")

    else:
        hrebin.Draw("colz")
        hrebin.GetYaxis().SetTitle("%s (GeV)" % xtitle)
        hrebin.GetXaxis().SetTitle("Probe electron #eta")

  

    label_kstest = TLatex()
    label_kstest.SetNDC(1)
    label_kstest.SetTextAlign(12)
    label_kstest.SetTextSize(0.06)
    label_kstest.SetTextFont(42)
    label_kstest.DrawLatex(0.2,0.85,"#bf{CMS} #scale[0.7]{#it{Preliminary}}")                                                                                                                 
    label_kstest.DrawLatex(0.7,0.95,"#scale[0.7]{13 TeV}")                                                                                                                 

    c.SaveAs(output+".pdf")
    c.SaveAs(output+".png")
            
def main(inputfilename, histogram, output):
    inputroot = TFile("%s.root" % inputfilename , 'READ')
    if not inputroot:
        raise IOError("Can't open input file: %s.root" % inputfilename)
    if (inputfilename.find("pho")>=0):
        xtitle = "Photon p_{T}"
    elif (inputfilename.find("ele")>=0):    
        xtitle = "Probe electron p_{T}"
    elif (inputfilename.find("met")>=0):    
        xtitle = "U"

    plot(inputroot, histogram, output, xtitle)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help='Input name of root file, without *.root')
    parser.add_argument('--histogram', default="tmp", help='Histogram name')
    parser.add_argument('--output', default="tmp", help='Output name')
    args = parser.parse_args()
    main(args.input, args.histogram, args.output)
