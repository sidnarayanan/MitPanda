#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv
tcut = root.TCut
### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')
### HELPER FUNCTIONS ###
def setBins(dist,bins):
  for b in bins:
    dist.AddBinEdge(b)

def contains(s1,s2):
  if type(s2)==type(""):
    return s1.find(s2)>=0
  else:
    for s in s2:
      if s1.find(s)>=0:
        return True
    return False

def tAND(s1,s2):
  return "(("+s1+")&&("+s2+"))"

def tOR(s1,s2):
  return "(("+s1+")||("+s2+"))"

def tTIMES(w,s):
  return "("+w+")*("+s+")"

### SET GLOBAL VARIABLES ###
region = argv[1]

makePlots = True
#baseDir = '/home/snarayan/local/BaconSkim/MonoXSelection/baconbits/ntuples/'
baseDir = '/home/snarayan/local/BaconSkim/ntuples_v7/'
lumi = 2.26

### DEFINE REGIONS ###
cuts = {}
# resolved

# basic CA15 selection
cuts['ca15']             = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0'
#cuts['ca15']             = 'nf15PUPPIjets==1'
topTagCut                = 'bst15_PUPPIjet1msd>110 && bst15_PUPPIjet1msd<210 && bst15_PUPPIjet1tau32<0.61' # used for evaluating
bTagCut                  = 'bst15_PUPPIjet1maxsubcsv>0.76'
antiBTagCut              = 'bst15_PUPPIjet1maxsubcsv<0.76'
#N-1 cut
# regions 
# regions 
cuts['validatetau32']    = tAND(bTagCut,tAND(tAND(cuts['ca15'],'bst15_PUPPIjet1msd>110 && bst15_PUPPIjet1msd<210'), 'nmu==1 && (nele+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&2)!=0) && nbPUPPIjetsLdR2>0'))
cuts['validatemsd']    = tAND(bTagCut,tAND(tAND(cuts['ca15'],'bst15_PUPPIjet1tau32<0.61'), 'nmu==1 && (nele+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&2)!=0) && nbPUPPIjetsLdR2>0'))
cuts['validatebtag']    = tAND(tAND(cuts['ca15'],topTagCut), 'nmu==1 && (nele+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&2)!=0) && nbPUPPIjetsLdR2>0')

cuts['signal']        = tAND(bTagCut,tAND(tAND(cuts['ca15'],topTagCut), '(nmu+nele+ntau+npho)==0 && puppet.Pt()>250 && TMath::Abs(min_dphijetspuppet)>1.1 && ((triggerBits&2)!=0) && nbPUPPIjetsLdR2==0'))

cuts['singlemuontop']    = tAND(bTagCut,tAND(tAND(cuts['ca15'],topTagCut), 'nmu==1 && (nele+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&2)!=0) && nbPUPPIjetsLdR2>0'))
cuts['singleelectrontop']   = tAND(bTagCut,tAND(tAND(cuts['ca15'],topTagCut), 'nele==1 && (nmu+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&4)!=0) && nbPUPPIjetsLdR2>0 && puppet.Pt()>50'))

cuts['singlemuonw']     = tAND(antiBTagCut,tAND(tAND(cuts['ca15'],topTagCut), 'nmu==1 && (nele+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&2)!=0) && nbPUPPIjetsLdR2==0'))
cuts['singleelectronw']    = tAND(antiBTagCut,tAND(tAND(cuts['ca15'],topTagCut), 'nele==1 && (nmu+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&4)!=0) && nbPUPPIjetsLdR2==0'))

cuts['dimuon']     = tAND(tAND(cuts['ca15'],topTagCut), 'nmu==2 && (nele+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&2)!=0)')
cuts['dielectron']    = tAND(tAND(cuts['ca15'],topTagCut), 'nele==2 && (nmu+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&4)!=0)')

cuts['photon']       = tAND(tAND(cuts['ca15'],topTagCut), 'npho==1 && (nmu+ntau+nele)==0 && fakepuppet.Pt()>250 && ((triggerBits&8)!=0)')
cuts['photon_ptcut']       = tAND(tAND(cuts['ca15'],topTagCut), 'npho==1 && (nmu+ntau+nele)==0 && fakepuppet.Pt()>250 && ((triggerBits&8)!=0) && bst15_PUPPIjet1.Pt()>350')

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
plot.Logy(True)
plot.DrawMCErrors(True)
#plot.Ratio(True)
#plot.SetNormFactor(False)
plot.SetLumi(lumi)
plot.SetCut(tcut(cuts[region]))
if contains(region,'top'):
  plot.SetMCWeight(tcut("%f*scale1fb*evtWeight*nloKfactor_CENT*ewkCorr_CENT*res_PUPPIbtagwL1_CENT*triggerEff*lepWeight"%(lumi)))
elif contains(region,['muonw','electronw','signal']):
  plot.SetMCWeight(tcut("%f*scale1fb*evtWeight*nloKfactor_CENT*ewkCorr_CENT*res_PUPPIbtagwL0_CENT*triggerEff*lepWeight"%(lumi)))
else:
  plot.SetMCWeight(tcut("%f*scale1fb*evtWeight*nloKfactor_CENT*ewkCorr_CENT*triggerEff*lepWeight"%(lumi)))

mistoptagWeight = "1.13"
toptagWeight = "1.13"

### DEFINE PROCESSES ###
zjets     = root.Process('Z+jets',root.kZjets); 
wjets     = root.Process('W+jets',root.kWjets); 
diboson   = root.Process('Diboson',root.kDiboson); 
#ttbar     = root.Process('t#bar{t} [t-matched]',root.kTTbar)
ttbar     = root.Process('t#bar{t}',root.kTTbar); 
fakettbar = root.Process('t#bar{t} [not t-matched]',root.kExtra3); 
singletop = root.Process('Single t',root.kST); 
qcd       = root.Process("QCD",root.kQCD); 
gjets     = root.Process('#gamma+jets',root.kGjets); 
#signal    = root.Process("M_{S}=1.1 TeV",root.kSignal); signal.additionalWeight = tcut(toptagWeight)
signal    = root.Process("M_{V}=0.9 TeV",root.kSignal); 
data      = root.Process("Data",root.kData)

'''
zjets.additionalWeight = tcut(mistoptagWeight)
wjets.additionalWeight = tcut(mistoptagWeight)
diboson.additionalWeight = tcut(mistoptagWeight)
ttbar.additionalWeight = tcut(toptagWeight)
fakettbar.additionalWeight = tcut(mistoptagWeight)
singletop.additionalWeight = tcut(mistoptagWeight)
gjets.additionalWeight = tcut(mistoptagWeight)
qcd.additionalWeight = tcut(mistoptagWeight)
signal.additionalWeight = tcut(toptagWeight)
'''
processes = [qcd,diboson,singletop,zjets,wjets,ttbar]
#processes = [qcd,diboson,singletop,zjets,wjets,fakettbar,ttbar]

for p in processes:
  p.Init("Events")
gjets.Init("Events")
data.Init("Events")
signal.Init("Events")

### ASSIGN FILES TO PROCESSES ###
zjets.AddFile(baseDir+'Z.root')
zjets.AddFile(baseDir+'DY.root')
wjets.AddFile(baseDir+'W.root')
diboson.AddFile(baseDir+'WW.root')
diboson.AddFile(baseDir+'WZ.root')
diboson.AddFile(baseDir+'ZZ.root')
ttbar.AddFile(baseDir+'TT1L.root')
ttbar.AddFile(baseDir+'TT2L.root')
ttbar.AddFile(baseDir+'TTHAD.root')
'''
ttbar.AddFile(baseDir+'TTBST_new.root')
fakettbar.AddFile(baseDir+'TTCOM_new.root')
'''
singletop.AddFile(baseDir+'T.root')
if contains(region,"photon"):
  processes = [qcd,gjets]
  gjets.AddFile(baseDir+'G.root')
qcd.AddFile(baseDir+'QCD.root')

if not contains(region,"signal"):
  if contains(region,"photon"):
    data.AddFile(baseDir+"SinglePhoton.root")
  elif contains(region,"electron"):
    data.AddFile(baseDir+"SingleElectron.root")
  else:
    data.AddFile(baseDir+'MET.root')
  processes.append(data)
else:
  plot.Ratio(False)
  #signal.AddFile(baseDir+'Spring15_a25ns_DMJetsMonotop_S1_Mres-1100_Mchi-100_13TeV-madgraph-pythia8_mc.root')
  signal.AddFile(baseDir+'Spring15_a25ns_DMJetsMonotop_S4_Mchi-900_13TeV-madgraph-pythia8_mc.root')
  processes.append(signal)

for p in processes:
  plot.AddProcess(p)

recoilBins = [250,300,350,400,500,1000] 
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
ptLabel = "p_{T}>250 GeV"
cutLabel=''
#plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.2f fb^{-1} (13 TeV)}{%s}}"%(lumi,cutLabel))
plot.SetTDRStyle()
plot.AddCMSLabel()
plot.AddLumiLabel()

# define what "recoil" means
recoilLabel='MET'
if region=='photon':
  recoilLabel = 'Recoil (#gamma)'
elif contains(region,'dielectron'):
  recoilLabel = 'Recoil (ee)'
elif contains(region,'dimuon'):
  recoilLabel = 'Recoil (#mu#mu)'
elif contains(region,'singleelectron'):
  recoilLabel = 'Recoil (e)'
elif contains(region,'singlemuon'):
  recoilLabel = 'Recoil (#mu)'

recoil=None
if region=='signal': # or region=='ACR':
  recoil=root.Distribution("puppet.Pt()",nRecoilBins,recoilLabel,"Events/GeV")
else:
  recoil = root.Distribution("fakepuppet.Pt()",nRecoilBins,recoilLabel,"Events/GeV")

if recoil and not contains(region,'validate'):
  setBins(recoil,recoilBins)
  plot.AddDistribution(recoil)

#plot.AddDistribution(root.Distribution("bst15_PUPPIjet1.Pt()",250,1000,25,"jet p_{T}","Events/30 GeV"))
if region=='validatetau32':
  plot.AddDistribution(root.Distribution("bst15_PUPPIjet1tau32",0,1.2,24,"#tau_{32}","Events"))
elif region=='validatemsd':
  plot.AddDistribution(root.Distribution("bst15_PUPPIjet1msd",0,400,20,"soft drop mass","Events/20 GeV"))
elif region=='validatebtag':
  plot.AddDistribution(root.Distribution("bst15_PUPPIjet1maxsubcsv",0,1,20,"max subjet btag","Events"))

#plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
### DRAW AND CATALOGUE ###
try:
  mkdir(baseDir+'/figs_plots/'+region)
except OSError:
  pass
plot.DrawAll(baseDir+'/figs_plots/'+region)

