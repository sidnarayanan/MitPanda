#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv,exit
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.MonojetSelection import *

blind=False
totalUnblind=False

### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

### SET GLOBAL VARIABLES ###
region = argv[1]
nminusone = None
if len(argv)>2:
  nminusone = argv[2]
makePlots = True
#baseDir = '/local/snarayan/nero_pfchs_v3/'
#baseDir = '/local/snarayan/monotop_80/'
baseDir = '/afs/cern.ch/work/s/snarayan/skims/monojet/'
dataBaseDir = baseDir
lumi = 7706.

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
plot.Logy(True)
if not(blind and 'signal' in region):
  plot.Ratio(1) 
if 'signal' in region and not blind and not totalUnblind:
  plot.SetEvtMod(3)
  plot.SetLumi(lumi/3000)
else:
  plot.SetLumi(lumi/1000)
plot.DrawMCErrors(True)
plot.SetTDRStyle()
plot.SetNormFactor(False)
plot.FixRatio(True)
plot.AddCMSLabel()
plot.AddLumiLabel()

label = region
cut = cuts[region]
if nminusone!=None:
  label = 'NMinus1_%s_%s'%(nminusone,label)

print cut
plot.SetCut(root.TCut(cut))

#if 'signal' in region or 'muon' in region:
#  triggerEff = "(((met<240)*(0.975+(met-200)*0.000625))+(met>240))"
#else:
#  triggerEff = '1'

weight = weights['notag']%lumi
plot.SetMCWeight(weight)
if 'electron' in region:
  weight = tTIMES(weight,'sf_eleTrig')
elif 'photon' in region:
  weight = tTIMES(weight,'sf_phoTrig')
else:
  weight = tTIMES(weight,'sf_metTrig')

### DEFINE PROCESSES ###
zjets     = root.Process('Z+jets',root.kZjets)
wjets     = root.Process('W+jets',root.kWjets)
diboson   = root.Process('Diboson',root.kDiboson)
if nminusone==None:
  ttbar     = root.Process('t#bar{t}',root.kTTbar)
else:
  ttbar     = root.Process('t#bar{t} [matched]',root.kTTbar); ttbar.additionalCut = root.TCut('CA15fj1_isMatched==1')
  ttbarunmatched     = root.Process('t#bar{t} [unmatched]',root.kExtra1); ttbarunmatched.additionalCut = root.TCut('CA15fj1_isMatched==0')
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
gjets     = root.Process('#gamma+jets',root.kGjets)
signal    = root.Process("Signal",root.kSignal)
data      = root.Process("Data",root.kData)
if nminusone==None:
  processes = [qcd,diboson,singletop,wjets,ttbar,zjets]
else:
  processes = [qcd,diboson,singletop,wjets,ttbarunmatched,ttbar,zjets]

### ASSIGN FILES TO PROCESSES ###
if 'signal' in region:
  zjets.AddFile(baseDir+'ZtoNuNu.root')
else:
  zjets.AddFile(baseDir+'ZJets.root')
wjets.AddFile(baseDir+'WJets.root')
diboson.AddFile(baseDir+'Diboson.root')
ttbarfile = 'TTbar_Powheg.root'
ttbar.AddFile(baseDir+ttbarfile)
if nminusone!=None:
  ttbarunmatched.AddFile(baseDir+ttbarfile)
singletop.AddFile(baseDir+'SingleTop.root')
if contains(region,"photon"):
  qcd.AddFile(dataBaseDir+'SinglePhoton.root')
  qcd.useCommonWeight=False
  qcd.additionalWeight = root.TCut('photonPurityWeight')
  qcd.additionalCut = root.TCut(tAND(metFilter,phoTrigger))
  gjets.AddFile(baseDir+'GJets.root'); gjets.additionalWeight = root.TCut('0.93')
  #processes = [gjets]
  processes = [qcd,gjets]
else:
  qcd.AddFile(baseDir+'QCD.root')

if contains(region,"photon"):
  print 'Using SinglePhoton'
  data.AddFile(dataBaseDir+"SinglePhoton.root") ; data.additionalCut = root.TCut(tAND(metFilter,phoTrigger))
elif contains(region,"electron"):
  print 'Using SingleElectron'
  data.AddFile(dataBaseDir+"SingleElectron.root"); 
  if 'di' in region:
    data.additionalCut = root.TCut(tAND(metFilter,tOR(eleTrigger,phoTrigger)))
  else:
    data.additionalCut = root.TCut(tAND(metFilter,eleTrigger))
else:
  print 'Using MET'
  data.AddFile(dataBaseDir+'MET.root') ;  data.additionalCut = root.TCut(tAND(metFilter,metTrigger))
if 'signal' in region and blind and False:
  print 'BLINDING'
else:
  processes.append(data)

for p in processes:
  plot.AddProcess(p)

recoilBins =  [150., 200., 230., 260.0, 290.0, 320.0, 350.0, 390.0, 430.0, 470.0, 510.0, 550.0, 590.0, 640.0, 690.0, 740.0, 790.0, 840.0, 900.0, 960.0, 1020.0, 1090.0, 1160.0, 1250.0]
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
# define what "recoil" means
if contains(region,"electron"):
  lep='e'
else:
  lep='#mu'

recoil=None
if nminusone==None:
#  plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
  if 'signal' in region or region=='qcdtest':
    recoil=root.Distribution("met",nRecoilBins,"MET","Events/GeV")
  elif 'photon' in region:
    recoil=root.Distribution("pfUAmag",nRecoilBins,"Recoil (#gamma)","Events/GeV")
  elif 'di' in region:
    recoil=root.Distribution("pfUZmag",nRecoilBins,"Recoil (%s%s)"%(lep,lep),"Events/GeV")
  elif 'single' in region:
    recoil=root.Distribution("pfUWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
if recoil and not(blind and 'signal' in region) and not totalUnblind:
  setBins(recoil,recoilBins)
  plot.AddDistribution(recoil)
  if 'electron' in region or 'muon' in region:
    plot.AddDistribution(root.Distribution("looseLep1Pt",30,930,30,"leading lepton p_{T}","Events/30 GeV",0.1)) 
    plot.AddDistribution(root.Distribution("looseLep1Eta",-5,5,20,"leading lepton #eta","Events",0.1)) 
  if 'photon' in region:
    plot.AddDistribution(root.Distribution("loosePho1Pt",100,1500,20,"leading photon p_{T}","Events",0.1)) 
  plot.AddDistribution(root.Distribution("jet1Pt",100,1500,20,"leading jet p_{T}","Events",0.1)) 
#  plot.AddDistribution(root.Distribution("fabs(dPhiMET)",0,3.14,30,"min #Delta #phi(MET,jet)","Events",0.1)) 
#  plot.AddDistribution(root.Distribution("fabs(dPhiPuppiMET)",0,3.14,30,"min #Delta #phi(MET,jet)","Events",0.1)) 
#  plot.AddDistribution(root.Distribution('(calomet-met)/calomet',-1,0.2,20,'(calo MET - PF MET)/(calo MET)','Events',0.01,10**4,'caloMinusPF'))
#  plot.AddDistribution(root.Distribution('(met-calomet)/met',-0.2,1,20,'(PF MET - calo MET)/(PF MET)','Events',0.01,10**4,'PFMinusCalo'))


### DRAW AND CATALOGUE ###
plot.DrawAll('~/www/figs/monojet_7fb/'+label+'_')

