import ROOT as root
from MitPanda.Monotop.Selection import *
from MitPanda.Tools.Misc import *

root.gROOT.LoadMacro('${CMSSW_BASE}/src/MitPanda/Tools/interface/CanvasDrawer.h')
root.gSystem.Load('libMitPandaTools.so')
root.gStyle.SetNumberContours(999)
root.gStyle.SetOptStat(0)
root.gStyle.SetPalette(56)
plot = root.CanvasDrawer()
plot.SetTDRStyle()
root.gStyle.SetPadRightMargin(0.15)

c = root.TCanvas()
c.SetLogz()

nBins=50

cut = cuts['singleelectrontop']

basedir = '/local/snarayan/monotop_80/'
fin = root.TFile(basedir+'TTbar.root')
#fin = root.TFile(basedir+'SingleElectron.root')
#fin = root.TFile(basedir+'MET.root')
tin = fin.Get('events')

hist = root.TH2D('hist','Calo vs PF MET',20,50,1000,20,50,1000)
hist.GetYaxis().SetTitle('Calo MET [GeV]')
hist.GetXaxis().SetTitle('PF MET [GeV]')
hist.GetYaxis().SetTitleOffset(1.5)
#tin.Draw('calomet:met>>hist',cut,'colz')
tin.Draw('calomet:met>>hist',tAND(cut,weights['top']%(2600)),'colz')

plot.SetCanvas(c)

plot.Draw(basedir+'figs/','singleelectrontop_caloVPF_MC')
