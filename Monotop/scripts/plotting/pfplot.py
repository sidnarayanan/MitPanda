#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv,exit
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.PFSelection import *

blind=False
totalUnblind=False

### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

### SET GLOBAL VARIABLES ###
region = argv[1]
nminusone = None
if len(argv)>2:
  nminusone = argv[2]
makePlots = True
#baseDir = '/local/snarayan/nero_pfchs_v3/'
#baseDir = '/local/snarayan/monotop_80/'
baseDir = '/afs/cern.ch/work/s/snarayan/skims/monotop_80_v3/'
lumi = 2600.

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
if nminusone!='tau32':
  plot.Logy(True)
if nminusone=='tau32' and False:
  plot.SetMaxScale(10**3)
if not(blind and 'signal' in region):
  plot.Ratio(1) 
if 'signal' in region and not blind and not totalUnblind:
  plot.SetEvtMod(3)
  plot.SetLumi(lumi/3000)
else:
  plot.SetLumi(lumi/1000)
plot.DrawMCErrors(True)
plot.SetTDRStyle()
plot.SetNormFactor(False)
plot.FixRatio(True)
plot.AddCMSLabel()
plot.AddLumiLabel()

label = 'PF_'+region
cut = cuts[region]
if nminusone!=None:
  if nminusone=='tau32':
    cut = removeCut(cut,'CA15fj1_tau3/CA15fj1_tau2')
  elif nminusone=='sjbtag':
    cut = removeCut(cut,'CA15fj1_maxsjbtag')
  elif nminusone=='mSD':
    cut = removeCut(cut,'CA15fj1_mSD')
  elif nminusone=='btag':
    cut = removeCut(cut,'jetNIsoBtags')
  elif nminusone=='allbtag':
    cut = removeCut(removeCut(cut,'jetNIsoBtags'),'CA15fj1_maxsjbtag')
  label = 'NMinus1_%s_%s'%(nminusone,label)

print cut
plot.SetCut(root.TCut(cut))

#if 'signal' in region or 'muon' in region:
#  triggerEff = "(((met<240)*(0.975+(met-200)*0.000625))+(met>240))"
#else:
#  triggerEff = '1'

weight = '%f*normalizedWeight'%lumi
if 'signal' in region:
  weight = weights['signal']%lumi
elif 'single' in region and 'w' in region:
  weight = weights['w']%lumi
elif ('single' in region and 'top' in region) or region=='nobtag':
  weight = weights['top']%lumi
elif 'di' in region or 'photon' in region:
  weight = weights['notag']%lumi
if 'electron' in region:
  weight = tTIMES(weight,'sf_eleTrig')
elif 'photon' in region:
  weight = tTIMES(weight,'sf_phoTrig')
else:
  weight = tTIMES(weight,'sf_metTrig')
print weight
plot.SetMCWeight(weight)

### DEFINE PROCESSES ###
zjets     = root.Process('Z+jets',root.kZjets)
wjets     = root.Process('W+jets',root.kWjets)
diboson   = root.Process('Diboson',root.kDiboson)
if nminusone==None:
  ttbar     = root.Process('t#bar{t}',root.kTTbar)
else:
  ttbar     = root.Process('t#bar{t} [matched]',root.kTTbar); ttbar.additionalCut = root.TCut('CA15fj1_isMatched==1')
  ttbarunmatched     = root.Process('t#bar{t} [unmatched]',root.kExtra1); ttbarunmatched.additionalCut = root.TCut('CA15fj1_isMatched==0')
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
gjets     = root.Process('#gamma+jets',root.kGjets)
signal    = root.Process("Signal",root.kSignal)
data      = root.Process("Data",root.kData)
if nminusone==None:
  processes = [qcd,diboson,singletop,wjets,ttbar,zjets]
else:
  processes = [qcd,diboson,singletop,wjets,ttbarunmatched,ttbar,zjets]

### ASSIGN FILES TO PROCESSES ###
if 'signal' in region:
  zjets.AddFile(baseDir+'ZtoNuNu.root')
else:
  zjets.AddFile(baseDir+'ZJets.root')
wjets.AddFile(baseDir+'WJets.root')
diboson.AddFile(baseDir+'Diboson.root')
ttbar.AddFile(baseDir+'TTbar.root')
if nminusone!=None:
  ttbarunmatched.AddFile(baseDir+'TTbar.root')
singletop.AddFile(baseDir+'SingleTop.root')
if contains(region,"photon"):
  qcd.AddFile(baseDir+'SinglePhoton.root')
  qcd.useCommonWeight=False
  qcd.additionalWeight = root.TCut('photonPurityWeight')
  qcd.additionalCut = root.TCut(tAND(metFilter,phoTrigger))
  gjets.AddFile(baseDir+'GJets.root')
  processes = [qcd,gjets]
else:
  qcd.AddFile(baseDir+'QCD.root')

if contains(region,"photon"):
  print 'Using SinglePhoton'
  data.AddFile(baseDir+"SinglePhoton.root") ; data.additionalCut = root.TCut(tAND(metFilter,phoTrigger))
elif contains(region,"electron"):
  print 'Using SingleElectron'
  data.AddFile(baseDir+"SingleElectron.root"); 
  if 'di' in region:
    data.additionalCut = root.TCut(tAND(metFilter,tOR(eleTrigger,phoTrigger)))
  else:
    data.additionalCut = root.TCut(tAND(metFilter,eleTrigger))
else:
  print 'Using MET'
  data.AddFile(baseDir+'MET.root') ;  data.additionalCut = root.TCut(tAND(metFilter,metTrigger))
if 'signal' in region and blind and False:
  print 'BLINDING'
else:
  processes.append(data)

for p in processes:
  plot.AddProcess(p)

recoilBins = [250,300,350,400,500,1000]
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
# define what "recoil" means
if contains(region,"electron"):
  lep='e'
else:
  lep='#mu'

recoil=None
if nminusone==None:
#  plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
  if 'signal' in region or region=='qcdtest':
    recoil=root.Distribution("met",nRecoilBins,"MET","Events/GeV")
  elif 'photon' in region:
    recoil=root.Distribution("pfUAmag",nRecoilBins,"Recoil (#gamma)","Events/GeV")
  elif 'di' in region:
    recoil=root.Distribution("pfUZmag",nRecoilBins,"Recoil (%s%s)"%(lep,lep),"Events/GeV")
  elif ('single' in region and 'w' in region) or region=='test':
    recoil=root.Distribution("pfUWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
  elif 'single' in region and 'top' in region:
    recoil=root.Distribution("pfUWmag",nRecoilBins,"Recoil (%s)"%(lep),"Events/GeV")
if recoil and not(blind and 'signal' in region) and not totalUnblind:
  setBins(recoil,recoilBins)
  plot.AddDistribution(recoil)
  pfmet=root.Distribution("met",0,200,20,"PF MET","Events")
#  plot.AddDistribution(pfmet)
  calomet=root.Distribution("calomet",50,1000,20,"Calo MET","Events")
  #plot.AddDistribution(calomet)
#  plot.AddDistribution(root.Distribution("nJet",-0.5,10.5,11,"N(AK4 jets)","Events",0.1)) 
  if 'electron' in region or 'muon' in region and False:
    plot.AddDistribution(root.Distribution("looseLep1Pt",30,930,30,"leading lepton p_{T}","Events/30 GeV",0.1)) 
    plot.AddDistribution(root.Distribution("looseLep1Eta",-5,5,20,"leading lepton #eta","Events",0.1)) 
  if 'photon' in region:
    plot.AddDistribution(root.Distribution("loosePho1Pt",100,1500,20,"leading photon p_{T}","Events",0.1)) 
  plot.AddDistribution(root.Distribution("CA15fj1_pt",250,1000,30,"fat jet p_{T}","Events/25 GeV",0.1)) 
#  plot.AddDistribution(root.Distribution("fabs(dPhiMET)",0,3.14,30,"min #Delta #phi(MET,jet)","Events",0.1)) 
#  plot.AddDistribution(root.Distribution("fabs(dPhiPuppiMET)",0,3.14,30,"min #Delta #phi(MET,jet)","Events",0.1)) 
#  plot.AddDistribution(root.Distribution('(calomet-met)/calomet',-1,0.2,20,'(calo MET - PF MET)/(calo MET)','Events',0.01,10**4,'caloMinusPF'))
#  plot.AddDistribution(root.Distribution('(met-calomet)/met',-0.2,1,20,'(PF MET - calo MET)/(PF MET)','Events',0.01,10**4,'PFMinusCalo'))

plotvars = (nminusone==None) and not('test' in region) and not(blind and 'signal' in region) and not totalUnblind and False

if nminusone=="tau32" or plotvars:
  plot.AddDistribution(root.Distribution("CA15fj1_tau3/CA15fj1_tau2",0,1.2,12,"#tau_{3}/#tau_{3}","Events",0.5))
if nminusone=="sjbtag" or nminusone=='allbtag' or plotvars:
  plot.AddDistribution(root.Distribution("CA15fj1_maxsjbtag",0,1,20,"max subjet b-tag","Events/0.05"))
if nminusone=="mSD" or plotvars:
  plot.AddDistribution(root.Distribution("CA15fj1_mSD",40,440,20,"soft drop mass","Events/20 GeV"))
if nminusone=="btag" or nminusone=='allbtag' or plotvars:
  plot.AddDistribution(root.Distribution("jetNIsoBtags",-0.5,3.5,4,"AK4 btags","Events"))

### DRAW AND CATALOGUE ###
plot.DrawAll(baseDir+'/figs/'+label+'_')

