#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv,exit
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.Selection import *

blind=False

### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

### SET GLOBAL VARIABLES ###
region = argv[1]
nminusone = None
if len(argv)>2:
  nminusone = argv[2]
makePlots = True
#baseDir = '/local/snarayan/nero_pfchs_v3/'
#baseDir = '/local/snarayan/monotop_80/'
baseDir = '/afs/cern.ch/work/s/snarayan/skims/monotop_80_v8/'
lumi = 12918.
logy=False

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
if nminusone==None:
  plot.Logy(True)
if nminusone=='tau32':
  if logy:
    plot.SetMaxScale(10**3)
  else:
    plot.SetMaxScale(2)
plot.Ratio(1) 
plot.SetLumi(lumi/1000)
plot.DrawMCErrors(True)
plot.SetTDRStyle()
plot.SetNormFactor(False)
plot.AddCMSLabel()
plot.AddLumiLabel()

label = region
cut = tt_cuts[region]
if nminusone!=None and 'pass' in region:
  if nminusone=='tau32':
    cut = removeCut(cut,'CA15fj1_tau3/CA15fj1_tau2')
  elif nminusone=='sjbtag':
    cut = removeCut(cut,'CA15fj1_maxsjbtag')
  elif nminusone=='mSD':
    cut = removeCut(cut,'CA15fj1_mSD')
print cut
plot.SetCut(root.TCut(cut))

#if 'signal' in region or 'muon' in region:
#  triggerEff = "(((met<240)*(0.975+(met-200)*0.000625))+(met>240))"
#else:
#  triggerEff = '1'

weight = '%f*normalizedWeight'%lumi
if 'mistag' in region: 
  weight = tTIMES(weights['notag']%lumi,'sf_phoTrig')
else:
  weight = weights['top_sf']%lumi
print weight
plot.SetMCWeight(weight)

### DEFINE PROCESSES ###
zjets     = root.Process('Z+jets',root.kZjets)
wjets     = root.Process('W+jets',root.kWjets)
diboson   = root.Process('Diboson',root.kDiboson)
ttbar     = root.Process('t#bar{t} [matched]',root.kTTbar); ttbar.additionalCut = root.TCut('CA15fj1_isMatched==1')
ttbarunmatched     = root.Process('t#bar{t} [unmatched]',root.kExtra1); ttbarunmatched.additionalCut = root.TCut('CA15fj1_isMatched==0')
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
gjets     = root.Process('#gamma+jets',root.kGjets)
signal    = root.Process("Signal",root.kSignal)
data      = root.Process("Data",root.kData)
processes = [qcd,diboson,singletop,wjets,ttbarunmatched,ttbar,zjets]

### ASSIGN FILES TO PROCESSES ###
zjets.AddFile(baseDir+'ZJets.root')
wjets.AddFile(baseDir+'WJets.root')
diboson.AddFile(baseDir+'Diboson.root')
ttbar.AddFile(baseDir+'TTbar_Powheg.root')
ttbarunmatched.AddFile(baseDir+'TTbar_Powheg.root')
singletop.AddFile(baseDir+'SingleTop.root')
if contains(region,"mistag"):
  processes = [qcd,gjets]
  gjets.AddFile(baseDir+'GJets.root')
  qcd.AddFile(baseDir+'SinglePhoton.root')
  qcd.useCommonWeight=False
  qcd.additionalWeight = root.TCut('photonPurityWeight')
  qcd.additionalCut = root.TCut(tAND(metFilter,phoTrigger))
  processes = [qcd,gjets]
else:
  qcd.AddFile(baseDir+'QCD.root')

if 'mistag' in region:
  print 'Using SinglePhoton'
  data.AddFile(baseDir+"SinglePhoton.root") ; data.additionalCut = root.TCut(tAND(metFilter,phoTrigger))
else:
  print 'Using MET'
  data.AddFile(baseDir+'METSingleElectron.root') 
#  data.AddFile(baseDir+'SingleElectron_ext.root')
  data.additionalCut = root.TCut(tAND(metFilter,tOR(eleTrigger,metTrigger)))
processes.append(data)

for p in processes:
  plot.AddProcess(p)

#plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
if nminusone=='tau32' or nminusone==None:
  plot.AddDistribution(root.Distribution("CA15fj1_tau3/CA15fj1_tau2",0,1.2,12,"#tau_{3}/#tau_{2}","Events",0.5))
if nminusone=='sjbtag':
  plot.AddDistribution(root.Distribution("CA15fj1_maxsjbtag",0,1,20,"max subjet b-tag","Events/0.05"))
if nminusone=='mSD' or nminusone==None:
  plot.AddDistribution(root.Distribution("CA15fj1_mSD",40,440,20,"soft drop mass [GeV]","Events/20 GeV"))

### DRAW AND CATALOGUE ###
plot.DrawAll('~/www/figs/monotop_2016_13fb/toptagSF/'+label+'_')
