#!/usr/bin/env python

import ROOT as root
from MitPanda.Tools.Load import Load

Load('Skimmers','SFSkimmer')

basedir = '/local/snarayan/monotop_80/'
outdir = basedir+'/tnp/'
def fpath(s):
  return basedir+s+'.root'

sig_tag = root.SFSkimmer()
sig_tag.SetOutFile(outdir+'sig_tag.root')
sig_tag.AddFile(fpath('TTbar_Powheg'),0,0,1,1)
sig_tag.Run()
sig_tag.Terminate()

bkg_unmatched_tag = root.SFSkimmer()
bkg_unmatched_tag.SetOutFile(outdir+'bkg_unmatched_tag.root')
bkg_unmatched_tag.AddFile(fpath('TTbar_Powheg'),0,0,0,1)
bkg_unmatched_tag.Run()
bkg_unmatched_tag.Terminate()

bkg_tag = root.SFSkimmer()
bkg_tag.SetOutFile(outdir+'bkg_tag.root')
for f in ['WJets','Diboson','QCD','SingleTop']:
  bkg_tag.AddFile(fpath(f),0,0,0,0)
bkg_tag.Run()
bkg_tag.Terminate()

tag_data = root.SFSkimmer()
tag_data.SetOutFile(outdir+'tag_data.root')
tag_data.AddFile(fpath('METSingleElectron'),1,2,0,0)
#tag_data.AddFile(fpath('MET'),1,1,0,0)
#tag_data.AddFile(fpath('SingleElectron'),1,0,0,0)
tag_data.Run()
tag_data.Terminate()
