#!/usr/bin/env python

from ROOT import gSystem,gROOT
import ROOT as root
from re import sub
from sys import argv,exit
from os import system,getenv
from json import load as loadJson
import cPickle as pickle

if __name__ == "__main__":

  panda=getenv('CMSSW_BASE')+'/src/MitPanda/'
  with open(panda+'/Monotop/data/Cert_271036-274443_13TeV_PromptReco_Collisions16_JSON.txt') as jsonFile:
    json=loadJson(jsonFile)

  gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Skimmers/interface/NeroSkimmer.h")
  gSystem.Load('libMitPandaSkimmers.so')
  gSystem.Load('libNeroProducerCore.so')
  
  def fn(fullPath):

    skimmer = root.NeroSkimmer()
   
    skimmer.usePuppiMET=True;
    skimmer.set_ak4Jetslabel("puppi")
    skimmer.set_ca15Jetslabel("CA15Puppi")
#    skimmer.set_ak8Jetslabel("AK8Puppi")
    skimmer.maxEvents = 10000

    isData=not('SIM' in fullPath)
    isData=False

    skimmer.isData=isData
    skimmer.applyJson=False
    skimmer.fromBambu=False
    #skimmer.fromBambu=not('eoscms' in fullPath or 'EOS' in fullPath)
    if not isData:
      processType=root.NeroSkimmer.kNone
      if 'ZJets' in fullPath or 'DY' in fullPath:
        processType=root.NeroSkimmer.kZ
      elif 'WJets' in fullPath:
        processType=root.NeroSkimmer.kW
      elif 'GJets' in fullPath:
        processType=root.NeroSkimmer.kA
      elif 'TTJets' in fullPath:
        processType=root.NeroSkimmer.kTT
      elif 'Monotop' in fullPath or 'res' in fullPath:
        processType=root.NeroSkimmer.kMonotopSignal
      skimmer.processType=processType
    
    if isData:
      for run,lumi in json.iteritems():
        for l in lumi:
          for lll in xrange(l[0],l[1]+1):
            skimmer.AddLumi(int(run),int(lll))

    skimmer.SetDataDir(panda+"/Monotop/data/")
#    skimmer.SetPreselectionBit(root.NeroSkimmer.kMonojet)
    skimmer.SetPreselectionBit(root.NeroSkimmer.kMonotopCA15)
#    skimmer.SetPreselectionBit(root.NeroSkimmer.kMonotopRes)
    eosPath = 'root://eoscms//eos/cms/store/user/%s'%(getenv('USER'))
    cernboxPath = 'root://eosuser//eos/user/%s/%s'%(getenv('USER')[0],getenv('USER'))
    cernboxBPath = 'root://eosuser//eos/user/b/bmaier'
    fullPath = sub(r'\${CERNBOXB}',cernboxBPath,sub(r'\${CERNBOX}',cernboxPath,sub(r'\${EOS}',eosPath,fullPath)))
    fin = root.TFile.Open(fullPath)
    print fullPath
    print fin
    tree = fin.FindObjectAny("events")
    alltree = fin.Get('nero/all')
    print tree
    skimmer.SetOutputFile('/tmp/%s/testskim.root'%getenv('USER'))
    skimmer.Init(tree,alltree)
    skimmer.Run(1)
    skimmer.Terminate()

fn(argv[1]) 
