#!/bin/bash
label=$1
cfgName=$2
nPerJob=$3
pwd

#executable=skimSingleCoreRemote.py
executable=skimSingleCore.py

scramdir=/home/$USER/cms/cmssw/043/CMSSW_7_6_3/
cd ${scramdir}/src/
eval `scramv1 runtime -sh`
source MitPanda/Monotop/setup.sh
cd -

cp -r ${scramdir}/src/MitPanda/Monotop/data/ .
cp ${scramdir}/src/MitPanda/Monotop/config/${cfgName}.cfg local.cfg
cp ${scramdir}/src/MitPanda/Monotop/scripts/skimming/$executable .

python $executable $label $nPerJob ${PANDA_FLATDIR}/condor/

rm nero.root
#cp *.root 
rm -rf *root *.py local.cfg data

exit $status
