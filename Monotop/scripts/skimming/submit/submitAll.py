#!/usr/bin/env python

from os import system,environ
from sys import exit,stdout

user = environ['USER']

#cfgName='ttjets'
cfgName='monotop_v1_rnd'
nPerJob = 30

def submit(l):
  print "submitting",l
  condorJDLString = '''Executable  = run_skim.sh
Universe  = vanilla
requirements            = UidDomain == "mit.edu" && Arch == "X86_64" && OpSysAndVer == "SL6"
Error = /scratch/%s/condor/logs/%s_$(Process).err
Output  = /scratch/%s/condor/logs/%s_$(Process).out
Log = /scratch/%s/condor/logs/%s_$(Process).log
Arguments = "$(Process) %s %i"
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
GetEnv = True
accounting_group = group_cmsuser.%s
Queue %i'''%(user,cfgName,
             user,cfgName,
             user,cfgName,
             cfgName,nPerJob,user,l)
  with open('condor.jdl','w') as jdlFile:
    jdlFile.write(condorJDLString)
  system('condor_submit condor.jdl')

with open("../../../config/%s.cfg"%(cfgName)) as cfgFile:
  nJobs = len(list(cfgFile))/nPerJob+1
  submit(nJobs)
