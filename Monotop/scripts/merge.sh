#!/bin/bash

rm -rf /tmp/${USER}/split/*
rm -rf /tmp/${USER}/merged/*

#echo MET SingleElectron SinglePhoton | xargs -n 1 -P 5 ./mergeLXPLUS.py 
#echo MET SingleElectron SinglePhoton TTbar_Powheg Diboson ZJets ZtoNuNu GJets WJets SingleTop QCD | xargs -n 1 -P 6 ./mergeLXPLUS.py 
#echo TTbar_Powheg Diboson ZJets ZtoNuNu GJets WJets SingleTop QCD | xargs -n 1 -P 6 ./mergeLXPLUS.py 
for f in $(seq 300 200 1500); do echo monotop_fcnc_mMed${f} ; done | xargs -n 1 -P 6 ./mergeLXPLUS.py
for f in $(seq 900 200 2100); do echo monotop_res_mMed${f} ; done | xargs -n 1 -P 6 ./mergeLXPLUS.py
#echo TTbarDM | xargs -n 1 -P 5 time ./mergeLXPLUS.py
