#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv
tcut = root.TCut
### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')
### HELPER FUNCTIONS ###
def setBins(dist,bins):
  for b in bins:
    dist.AddBinEdge(b)

def contains(s1,s2):
  if type(s2)==type(""):
    return s1.find(s2)>=0
  else:
    for s in s2:
      if s1.find(s)>=0:
        return True
    return False

def tAND(s1,s2):
  return "(("+s1+")&&("+s2+"))"

def tOR(s1,s2):
  return "(("+s1+")||("+s2+"))"

def tTIMES(w,s):
  return "("+w+")*("+s+")"

### SET GLOBAL VARIABLES ###
region = argv[1]
try:
  additionalcut = argv[2]
except:
  additionalcut = None

makePlots = True
baseDir = '/home/snarayan/local/BaconSkim/ntuples_v6/'
lumi = 2.26
recoilBins = [250,300,350,400,500,1000] 
nRecoilBins = len(recoilBins)-1
ptBins = [0,350,9999]

### DEFINE REGIONS ###
cuts = {}
# resolved

# basic CA15 selection
#cuts['tnp']             = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0'
#cuts['tnp']             = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && bst15_PUPPIjet1maxsubcsv>0.76'
#cuts['ttCR']             = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && nbPUPPIjetsLdR2>0'
cuts['ttCR']             = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && nbPUPPIjetsLdR2==1 &&  bst15_PUPPIjet1maxsubcsv>0.76 && bst15_PUPPIjet1msd>50 && bst15_PUPPIjet1msd<300'
cuts['ettCR']            = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && nbPUPPIjetsLdR2==1 &&  bst15_PUPPIjet1maxsubcsv>0.76 && bst15_PUPPIjet1msd>50 && bst15_PUPPIjet1msd<300'
cuts['aCR']              = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && bst15_PUPPIjet1msd>50 && bst15_PUPPIjet1msd<300'
topTagCut                = 'bst15_PUPPIjet1tau32<0.61' # used for evaluating SF and uncert
failTopTagCut            = 'bst15_PUPPIjet1tau32>0.61' # used for evaluating SF and uncert
#N-1 cuts

# regions 
#cuts['ttCR_notoptag']    = tAND(cuts['ttCR'], 'nmu==1 && (nele+ntau+npho)==0 && fakepuppet.Pt()<250 && fakepuppet.Pt()>200 && ((triggerBits&2)!=0) && bst15_PUPPIjet1msd>50 && bst15_PUPPIjet1msd<300')
cuts['ttCR_notoptag']    = tAND(cuts['ttCR'], 'nmu==1 && (nele+ntau+npho)==0 && fakepuppet.Pt()>200 && ((triggerBits&2)!=0) && bst15_PUPPIjet1msd>50 && bst15_PUPPIjet1msd<300')
cuts['ttCR_failtoptag']  = tAND(cuts['ttCR_notoptag'], failTopTagCut)
cuts['ttCR_withtoptag']  = tAND(cuts['ttCR_notoptag'], topTagCut)

cuts['ettCR_notoptag']    = tAND(cuts['ettCR'], 'nele==1 && (nmu+ntau+npho)==0 && fakepuppet.Pt()>200 && puppet.Pt()>50 && ((triggerBits&4)!=0) && bst15_PUPPIjet1msd>50 && bst15_PUPPIjet1msd<300')
cuts['ettCR_failtoptag']  = tAND(cuts['ettCR_notoptag'], failTopTagCut)
cuts['ettCR_withtoptag']  = tAND(cuts['ettCR_notoptag'], topTagCut)

#cuts['aCR_notoptag']    = tAND(cuts['aCR'], 'npho==1 && (nele+ntau+nmu)==0 && fakepuppet.Pt()<250 && fakepuppet.Pt()>200 && ((triggerBits&8)!=0)')
cuts['aCR_notoptag']    = tAND(cuts['aCR'], 'npho==1 && (nele+ntau+nmu)==0 && fakepuppet.Pt()>250 && ((triggerBits&8)!=0) && bst15_PUPPIjet1msd>50 && bst15_PUPPIjet1msd<300')
cuts['aCR_withtoptag']  = tAND(cuts['aCR_notoptag'], topTagCut)
cuts['aCR_failtoptag']  = tAND(cuts['aCR_notoptag'], failTopTagCut)

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
plot.Logy(False)
plot.CloneTrees(True)
#plot.Ratio(True)
plot.DrawMCErrors(True)
plot.SetNormFactor(False)
plot.SetLumi(lumi)
fullCut = region + ('_'+additionalcut if additionalcut!=None else '')
plot.SetCut(tcut(cuts[fullCut]))
if region.find("aCR")>=0:
  plot.SetMCWeight(tcut("%f*scale1fb*evtWeight*nloKfactor_CENT*ewkCorr_CENT*lepWeight"%(lumi)))
else:
  plot.SetMCWeight(tcut("%f*scale1fb*evtWeight*nloKfactor_CENT*ewkCorr_CENT*triggerEff*res_PUPPIbtagwL1_CENT*lepWeight"%(lumi)))

### DEFINE PROCESSES ###
zjets     = root.Process('Z+jets',root.kZjets)
wjets     = root.Process('W+jets',root.kWjets)
diboson   = root.Process('Diboson',root.kDiboson)
ttbar     = root.Process('t#bar{t} [matched]',root.kTTbar)
fakettbar = root.Process('t#bar{t} [unmatched]',root.kExtra2)
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
data      = root.Process("Data",root.kData)
gjets     = root.Process("#gamma+jets",root.kGjets)
processes = [qcd,diboson,singletop,zjets,wjets,fakettbar,ttbar,data]

if region.find("a")>=0:
  sigs = root.Process("non-top jets",root.kExtra2)
  processes = [sigs,data]
else:
  sigs = root.Process("top jets [matched]",root.kExtra1)
  unmatchedsigs = root.Process("top jets [unmatched]",root.kExtra3)
  bgs  = root.Process("non-top jets",root.kExtra2)
  processes = [bgs,unmatchedsigs,sigs,data]

for p in processes:
  p.Init("Events")
#data.Init("Events")
#gjets.Init("Events")

### ASSIGN FILES TO PROCESSES ###
qcd.AddFile(baseDir+'QCD.root')
if region.find('a')>=0:
  data.AddFile(baseDir+'SinglePhoton.root')
#  gjets.AddFile(baseDir+'G.root')
  sigs.AddFile(baseDir+'GHF.root')
  sigs.AddFile(baseDir+'GLF.root')
  sigs.AddFile(baseDir+'QCD.root')
#  processes = [qcd,gjets,data]
else:
  '''
  zjets.AddFile(baseDir+'Z.root')
  zjets.AddFile(baseDir+'DY.root')
  wjets.AddFile(baseDir+'W.root')
  diboson.AddFile(baseDir+'WW.root')
  diboson.AddFile(baseDir+'WZ.root')
  diboson.AddFile(baseDir+'ZZ.root')
  ttbar.AddFile(baseDir+'TTBST_new.root')
  fakettbar.AddFile(baseDir+'TTCOM_new.root')
  singletop.AddFile(baseDir+'T.root')
  '''
  bgs.AddFile(baseDir+'ZLF.root')
  bgs.AddFile(baseDir+'ZHF.root')
  bgs.AddFile(baseDir+'WLF.root')
  bgs.AddFile(baseDir+'WHF.root')
  bgs.AddFile(baseDir+'DYLF.root')
  bgs.AddFile(baseDir+'DYHF.root')
  bgs.AddFile(baseDir+'WW.root')
  bgs.AddFile(baseDir+'WZ.root')
  bgs.AddFile(baseDir+'ZZ.root')
  bgs.AddFile(baseDir+'T.root')
  unmatchedsigs.AddFile(baseDir+'TTCOM_new.root')
  sigs.AddFile(baseDir+'TTBST_new.root')
  if 'e' in region:
    data.AddFile(baseDir+'SingleElectron.root')
  else:
    data.AddFile(baseDir+'MET.root')

for p in processes:
  plot.AddProcess(p)

### CHOOSE DISTRIBUTIONS, LABELS ###
ptLabel = ''
if contains(additionalcut,'with'):
  ptLabel = 'Passing'
elif contains(additionalcut,'fail'):
  ptLabel = 'Failing'
if contains(additionalcut,'bin0'):
  ptLabel += ' p_{T} < %i GeV'%(ptBins[1])
elif contains(additionalcut,'bin1'):
  ptLabel += ' p_{T} > %i GeV'%(ptBins[1])
cutLabel = "cut-based top tag" if additionalcut=="SR" else ""
#plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.2f fb^{-1} (13 TeV)}{%s}}"%(lumi,ptLabel))
plot.SetTDRStyle()
plot.AddCMSLabel()
plot.AddLumiLabel()
plot.AddPlotLabel(ptLabel,.16,.75,False,42,.06);

# define what "recoil" means
# choose recoil
recoil=None
if region=='SR' or region=='testSR':
  recoil=root.Distribution("puppet.Pt()",nRecoilBins,"MET","Events/GeV")
else:
  recoil=root.Distribution("fakepuppet.Pt()",nRecoilBins,"Recoil (#gamma)","Events/GeV")
if recoil:
  setBins(recoil,recoilBins)
#  plot.AddDistribution(recoil)
if contains(region,"ttCR") and False:
  lowMass = 40
  massBins = 18
else:
  lowMass = 0
  massBins = 20
#plot.AddDistribution(root.Distribution("nPUPPIjets",0,6,6,"N jets","Events"))
plot.AddDistribution(root.Distribution("bst15_PUPPIjet1msd",50,300,25,"fajet mass [GeV]","Events/10 GeV"))
plot.AddDistribution(root.Distribution("bst15_PUPPIjet1tau32",0,1.2,24,"#tau_{32}","Events"))
pt = root.Distribution("bst15_PUPPIjet1.Pt()",nRecoilBins,"fat jet p_{T}","Events/75 GeV")
setBins(pt,recoilBins)
#plot.AddDistribution(pt)
plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","Events"))
### DRAW AND CATALOGUE ###
try:
  mkdir(baseDir+'/figs/'+fullCut)
except OSError:
  pass
plot.DrawAll(baseDir+'/figs/'+fullCut)
