#!/usr/bin/env python

import ROOT as root

def loadHists(fOut,region,isPass):
  path = '~/public_html/figs/monotop/tnp/%s_%sbtag/hists.root'%(region,'with' if isPass else 'fail')
  fIn = root.TFile.Open(path)
  h = fIn.Get('h_bst15_PUPPIjet1maxsubcsv_Heavy flavor')
  fOut.WriteTObject(h,'%s%s_%s'%(region,'Pass' if isPass else 'Fail','hf'))
  h = fIn.Get('h_bst15_PUPPIjet1maxsubcsv_Light flavor')
  fOut.WriteTObject(h,'%s%s_%s'%(region,'Pass' if isPass else 'Fail','lf'))
  h = fIn.Get('h_bst15_PUPPIjet1maxsubcsv_Data')
  fOut.WriteTObject(h,'%s%s_%s'%(region,'Pass' if isPass else 'Fail','data_obs'))

fOut = root.TFile.Open('btagsfs.root','RECREATE')
loadHists(fOut,'aCR',True)
loadHists(fOut,'ttCR',True)
loadHists(fOut,'aCR',False)
loadHists(fOut,'ttCR',False)
