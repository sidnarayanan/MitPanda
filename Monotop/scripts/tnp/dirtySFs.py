#!/usr/bin/env python

import ROOT as root
from array import array
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.Selection import *
from math import sqrt

counter=0

def calc(tsig,tbg,tdata,tag,sigcut,bgcut,mcweight,probe):
  global counter
  hsigpass = root.TH1F('hsigpass%i'%counter,'hsigpass%i'%counter,20,40,440)
  hsigfail = root.TH1F('hsigfail%i'%counter,'hsigfail%i'%counter,20,40,440)
  hbgpass = root.TH1F('hbgpass%i'%counter,'hbgpass%i'%counter,20,40,440)
  hbgfail = root.TH1F('hbgfail%i'%counter,'hbgfail%i'%counter,20,40,440)
  hdatapass = root.TH1F('hdatapass%i'%counter,'hdatapass%i'%counter,20,40,440)
  hdatafail = root.TH1F('hdatafail%i'%counter,'hdatafail%i'%counter,20,40,440)

  passcut = tAND(tag,probe)
  failcut = tAND(tag,tNOT(probe))

  tsig.Draw('CA15fj1_mSD>>hsigpass%i'%counter,tTIMES(mcweight,tAND(passcut,sigcut)))
  tsig.Draw('CA15fj1_mSD>>hsigfail%i'%counter,tTIMES(mcweight,tAND(failcut,sigcut)))
  if tbg:
    tbg.Draw('CA15fj1_mSD>>hbgpass%i'%counter,tTIMES(mcweight,tAND(passcut,bgcut)))
    tbg.Draw('CA15fj1_mSD>>hbgfail%i'%counter,tTIMES(mcweight,tAND(failcut,bgcut)))
  tdata.Draw('CA15fj1_mSD>>hdatapass%i'%counter,passcut)
  tdata.Draw('CA15fj1_mSD>>hdatafail%i'%counter,failcut)

  nsigpass = hsigpass.Integral(); nsigfail = hsigfail.Integral();
  if tbg:
    nbgpass = hbgpass.Integral(); nbgfail = hbgfail.Integral();
  else:
    nbgpass=0; nbgfail=0
  ndatapass = hdatapass.Integral(); ndatafail = hdatafail.Integral();

  avgweight = hsigpass.GetEntries()/hsigpass.Integral()
  emc = nsigpass/(nsigpass+nsigfail)
  emcerr = emc*sqrt(1./nsigpass + 1./(nsigpass+nsigfail)) 

  normalize = (ndatapass+ndatafail)/(nsigpass+nsigfail+nbgpass+nbgfail)
#  print ndatapass,nbgpass,normalize
#  print ndatafail,nbgfail,normalize
  ndatasigpass = ndatapass-normalize*nbgpass
  ndatasigfail = ndatafail-normalize*nbgfail
  edata = ndatasigpass/(ndatasigpass+ndatasigfail)
  edataerr = edata*sqrt(1./ndatasigpass + 1./(ndatasigpass+ndatasigfail)) 
  sf = edata/emc
  sferr = sf * sqrt( pow(edataerr/edata,2) + pow(emcerr/emc,2) )
  counter+=1
  return sf,sferr


basedir = '~/local/monotop_80/'
toptag = 'CA15fj1_tau3/CA15fj1_tau2<0.61 && 110<CA15fj1_mSD && CA15fj1_mSD<210'
'''
ctt = root.TChain('events')
ctt.AddFile(basedir+'TTbar.root')
cbg = root.TChain('events')
for f in ['TTbar','WJets','SingleTop']:
  cbg.AddFile(basedir+f+'.root')
cmet = root.TChain('events')
cmet.AddFile(basedir+'MET.root')
basecut = tt_cuts['tag'] 
tag,tagerr = calc(ctt,cbg,cmet,basecut,'CA15fj1_isMatched==1','CA15fj1_isMatched==0',weights['top']%2600,toptag)
print 'SF tag:    %.3f +/- %.3f'%(tag,tagerr)
'''

cgjets = root.TChain('events')
cgjets.AddFile(basedir+'GJets.root')
cgjets.AddFile(basedir+'QCD.root')
cspho = root.TChain('events')
cspho.AddFile(basedir+'SinglePhoton.root')
basecut = tt_cuts['mistag']
print basecut
#mistag,mistagerr = calc(cgjets,None,cspho,basecut,'1==1','1==1',tTIMES(weights['notag']%4330,'photonPurityWeight'),toptag)
mistag,mistagerr = calc(cgjets,None,cspho,basecut,'1==1','1==1',weights['notag']%4330,toptag)
print 'SF mistag: %.3f +/- %.3f'%(mistag,mistagerr)
