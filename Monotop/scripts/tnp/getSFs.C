#include "TH1F.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TMath.h"
#include "TROOT.h"

float mysqr (float f) { return TMath::Power(f,2); }

float mysqrt (float f) { return TMath::Sqrt(f); }

void calcSFs(int binNumber=-1) {
  TString binString="";
  if (binNumber>=0) {
    binString = TString::Format("_bin%i",binNumber);
  }
  TString baseDir("/home/snarayan/public_html/figs/monotop/tnp/");
  printf("%s\n",binString.Data());

  TFile *fNoTag = TFile::Open(baseDir+"ttCR_notoptag"+binString+"/hists.root");
  TFile *fTag = TFile::Open(baseDir+"ttCR_withtoptag"+binString+"/hists.root");

  TH1F *hDataNoTag = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1msd_Data");
  TH1F *hBGNoTag   = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1msd_non-top jets");
  TH1F *hSigNoTag  = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1msd_top jets");

  TH1F *hDataTag = (TH1F*)fTag->Get("h_bst15_PUPPIjet1msd_Data");
  TH1F *hBGTag   = (TH1F*)fTag->Get("h_bst15_PUPPIjet1msd_non-top jets");
  TH1F *hSigTag  = (TH1F*)fTag->Get("h_bst15_PUPPIjet1msd_top jets");

  double dataTagInt, dataTagErr, bgTagInt, bgTagErr, sigTagInt, sigTagErr; 
  double dataNoTagInt, dataNoTagErr, bgNoTagInt, bgNoTagErr, sigNoTagInt, sigNoTagErr; 

  int nBins = hDataNoTag->GetNbinsX(); // assume the same
  dataTagInt = hDataTag->IntegralAndError(1,nBins+1,dataTagErr);
  bgTagInt   = hBGTag->IntegralAndError(1,nBins+1,bgTagErr);
  sigTagInt  = hSigTag->IntegralAndError(1,nBins+1,sigTagErr);

  dataNoTagInt = hDataNoTag->IntegralAndError(1,nBins+1,dataNoTagErr);
  bgNoTagInt   = hBGNoTag->IntegralAndError(1,nBins+1,bgNoTagErr);
  sigNoTagInt  = hSigNoTag->IntegralAndError(1,nBins+1,sigNoTagErr);

/*  float sumNoTag = sigNoTagInt + bgNoTagInt;
  float sumTag = sigTagInt + bgTagInt;

  sigNoTagInt /= sumNoTag; bgNoTagInt /= sumNoTag;
  sigTagInt /= sumTag; bgTagInt /= sumTag;
  sigNoTagErr /= sumNoTag; bgNoTagErr /= sumNoTag;
  sigTagErr /= sumTag; bgTagErr /= sumTag;
*/
  if (binNumber==1 || true) { 
    float bgScale = (hDataNoTag->GetBinContent(1) + hDataNoTag->GetBinContent(2))
                      /(hBGNoTag->GetBinContent(1)+hBGNoTag->GetBinContent(2));
//  fprintf(stderr,"bg scale = %f\n",bgScale);

    bgTagInt *= bgScale; bgTagErr *= bgScale;
    bgNoTagInt *= bgScale; bgNoTagErr *= bgScale; 
  }

  float eMC =     sigTagInt             / (sigNoTagInt); 
  float errorMC = eMC*mysqrt(mysqr(sigTagErr/sigTagInt) + mysqr(sigNoTagErr/sigNoTagInt));

  float eDataNum = dataTagInt - bgTagInt; float eDataDenom = dataNoTagInt-bgNoTagInt;
  float errorDataNum = mysqrt(mysqr(dataTagErr)+mysqr(bgTagErr));
  float errorDataDenom = mysqrt(mysqr(dataNoTagErr)+mysqr(bgNoTagErr));
  float eData = eDataNum/eDataDenom;
  float errorData = eData*mysqrt(mysqr(errorDataNum/eDataNum) + mysqr(errorDataDenom/eDataDenom));

  float sf = eData/eMC; 
  float errorSf = sf*mysqrt(mysqr(errorData/eData)+mysqr(errorMC/eMC));
  float sfUp = errorSf; float sfDown = errorSf;
  
//  fprintf(stderr,"%f %f %f %f\n",dataTagInt,bgTagInt,dataNoTagInt,bgNoTagInt);
//  fprintf(stderr,"%f %f %f %f\n",sigTagInt,sigNoTagInt,eDataNum,eDataDenom);

  printf("Top tag SF %7s   : %8.4f +%6.4f -%6.4f\n",binString.Data(),sf,sfUp,sfDown);

  // top mistag
  fNoTag = TFile::Open(baseDir+"aCR_notoptag"+binString+"/hists.root");
  fTag = TFile::Open(baseDir+"aCR_withtoptag"+binString+"/hists.root");

  hDataNoTag = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1msd_Data");
  hBGNoTag   = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1msd_non-top jets");

  hDataTag = (TH1F*)fTag->Get("h_bst15_PUPPIjet1msd_Data");
  hBGTag   = (TH1F*)fTag->Get("h_bst15_PUPPIjet1msd_non-top jets");

  nBins = hDataNoTag->GetNbinsX(); // assume the same

  dataTagInt = hDataTag->IntegralAndError(1,nBins+1,dataTagErr);
  bgTagInt   = hBGTag->IntegralAndError(1,nBins+1,bgTagErr);

  dataNoTagInt = hDataNoTag->IntegralAndError(1,nBins+1,dataNoTagErr);
  bgNoTagInt   = hBGNoTag->IntegralAndError(1,nBins+1,bgNoTagErr);

  eMC =     bgTagInt             / (bgNoTagInt); 
  errorMC = eMC*mysqrt(mysqr(bgTagErr/bgTagInt) + mysqr(bgNoTagErr/bgNoTagInt));

  eDataNum = dataTagInt; eDataDenom = dataNoTagInt;
  errorDataNum = dataTagErr;
  errorDataDenom = dataNoTagErr;
  eData = eDataNum/eDataDenom;
  errorData = eData*mysqrt(mysqr(errorDataNum/eDataNum) + mysqr(errorDataDenom/eDataDenom));

  sf = eData/eMC; 
  errorSf = sf*mysqrt(mysqr(errorData/eData)+mysqr(errorMC/eMC));
  sfUp = errorSf; sfDown = errorSf;

  printf("Top mistag SF %7s: %8.4f +%6.4f -%6.4f\n",binString.Data(),sf,sfUp,sfDown);

  // btag 
  fNoTag = TFile::Open(baseDir+"ttCR_nobtag"+binString+"/hists.root");
  fTag = TFile::Open(baseDir+"ttCR_withbtag"+binString+"/hists.root");

  hDataNoTag = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1maxsubcsv_Data");
  hBGNoTag   = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1maxsubcsv_Light flavor");
  hSigNoTag  = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1maxsubcsv_Heavy flavor");

  hDataTag = (TH1F*)fTag->Get("h_bst15_PUPPIjet1maxsubcsv_Data");
  hBGTag   = (TH1F*)fTag->Get("h_bst15_PUPPIjet1maxsubcsv_Light flavor");
  hSigTag  = (TH1F*)fTag->Get("h_bst15_PUPPIjet1maxsubcsv_Heavy flavor");

  nBins = hDataNoTag->GetNbinsX(); // assume the same

  dataTagInt = hDataTag->IntegralAndError(1,nBins+1,dataTagErr);
  bgTagInt   = hBGTag->IntegralAndError(1,nBins+1,bgTagErr);
  sigTagInt   = hSigTag->IntegralAndError(1,nBins+1,sigTagErr);

  dataNoTagInt = hDataNoTag->IntegralAndError(1,nBins+1,dataNoTagErr);
  bgNoTagInt   = hBGNoTag->IntegralAndError(1,nBins+1,bgNoTagErr);
  sigNoTagInt   = hSigNoTag->IntegralAndError(1,nBins+1,sigNoTagErr);
/*
  sumNoTag = sigNoTagInt + bgNoTagInt;
  sumTag = sigTagInt + bgTagInt;

  sigNoTagInt /= sumNoTag; bgNoTagInt /= sumNoTag;
  sigTagInt /= sumTag; bgTagInt /= sumTag;
  sigNoTagErr /= sumNoTag; bgNoTagErr /= sumNoTag;
  sigTagErr /= sumTag; bgTagErr /= sumTag;
*/
  eMC =     sigTagInt / (sigNoTagInt); 
  errorMC = eMC*mysqrt(mysqr(sigTagErr/sigTagInt) + mysqr(sigNoTagErr/sigNoTagInt));

  eDataNum = dataTagInt - bgTagInt; eDataDenom = dataNoTagInt-bgNoTagInt;
  errorDataNum = mysqrt(mysqr(dataTagErr)+mysqr(bgTagErr));
  errorDataDenom = mysqrt(mysqr(dataNoTagErr)+mysqr(bgNoTagErr));
  eData = eDataNum/eDataDenom;
  errorData = eData*mysqrt(mysqr(errorDataNum/eDataNum) + mysqr(errorDataDenom/eDataDenom));

  sf = eData/eMC; 
  errorSf = sf*mysqrt(mysqr(errorData/eData)+mysqr(errorMC/eMC));
  sfUp = errorSf; sfDown = errorSf;

//  fprintf(stderr,"%f %f %f\n",sigTagInt,bgTagInt,dataTagInt);
//  fprintf(stderr,"%f %f %f\n",sigNoTagInt,bgNoTagInt,dataNoTagInt);

//  fprintf(stderr,"%f %f %f\n",sigTagErr,bgTagErr,dataTagErr);
//  fprintf(stderr,"%f %f %f\n",sigNoTagErr,bgNoTagErr,dataNoTagErr);

  printf("b tag SF %7s     : %8.4f +%6.4f -%6.4f\n",binString.Data(),sf,sfUp,sfDown);

  // b mistag 
  fNoTag = TFile::Open(baseDir+"aCR_nobtag"+binString+"/hists.root");
  fTag = TFile::Open(baseDir+"aCR_withbtag"+binString+"/hists.root");

  hDataNoTag = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1maxsubcsv_Data");
  hBGNoTag   = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1maxsubcsv_Light flavor");
  hSigNoTag  = (TH1F*)fNoTag->Get("h_bst15_PUPPIjet1maxsubcsv_Heavy flavor");

  hDataTag = (TH1F*)fTag->Get("h_bst15_PUPPIjet1maxsubcsv_Data");
  hBGTag   = (TH1F*)fTag->Get("h_bst15_PUPPIjet1maxsubcsv_Light flavor");
  hSigTag  = (TH1F*)fTag->Get("h_bst15_PUPPIjet1maxsubcsv_Heavy flavor");

  nBins = hDataNoTag->GetNbinsX(); // assume the same

  dataTagInt = hDataTag->IntegralAndError(1,nBins+1,dataTagErr);
  bgTagInt   = hBGTag->IntegralAndError(1,nBins+1,bgTagErr);
  sigTagInt   = hSigTag->IntegralAndError(1,nBins+1,sigTagErr);

  dataNoTagInt = hDataNoTag->IntegralAndError(1,nBins+1,dataNoTagErr);
  bgNoTagInt   = hBGNoTag->IntegralAndError(1,nBins+1,bgNoTagErr);
  sigNoTagInt   = hSigNoTag->IntegralAndError(1,nBins+1,sigNoTagErr);

  float hfUncert = .4; // uncertainty on sigNoTagInt/(sigNoTagInt+bgNoTagInt)

  eMC =     bgTagInt / (bgNoTagInt); 
  errorMC = eMC*mysqrt(mysqr(bgTagErr/bgTagInt) + mysqr(bgNoTagErr/bgNoTagInt));

  eDataNum = dataTagInt - sigTagInt; eDataDenom = dataNoTagInt-sigNoTagInt;
  errorDataNum = mysqrt(mysqr(dataTagErr)+mysqr(sigTagErr));
  errorDataDenom = mysqrt(mysqr(dataNoTagErr)+mysqr(sigNoTagErr));
  // errorDataNum = mysqrt(mysqr(dataTagErr)+mysqr(sigTagErr)+mysqr(hfUncert*sigTagInt));
  // errorDataDenom = mysqrt(mysqr(dataNoTagErr)+mysqr(sigNoTagErr)+mysqr(hfUncert*sigNoTagInt));
  eData = eDataNum/eDataDenom;
  errorData = eData*mysqrt(mysqr(errorDataNum/eDataNum) + mysqr(errorDataDenom/eDataDenom));

  sf = eData/eMC; 
  errorSf = sf*mysqrt(mysqr(errorData/eData)+mysqr(errorMC/eMC));
  sfUp = errorSf; sfDown = errorSf;
 
  float eDataHFUp = (dataTagInt-(1+hfUncert)*sigTagInt)/(dataNoTagInt-(1+hfUncert)*sigNoTagInt);
  float eDataHFDown = (dataTagInt-(1-hfUncert)*sigTagInt)/(dataNoTagInt-(1-hfUncert)*sigNoTagInt);

  float sfHFUp = eDataHFUp/eMC, sfHFDown = eDataHFDown/eMC;
  if (sfHFUp<sfHFDown) { sfHFUp += sfHFDown; sfHFDown = sfHFUp-sfHFDown; sfHFUp = sfHFUp-sfHFDown; }
  sfHFUp -= sf; sfHFDown = sf-sfHFDown;

  float sfTotalUp = TMath::Sqrt(TMath::Power(sfUp,2)+TMath::Power(sfHFUp,2));
  float sfTotalDown = TMath::Sqrt(TMath::Power(sfDown,2)+TMath::Power(sfHFDown,2));

  printf("b mistag SF %7s  : %8.4f +%6.4f -%6.4f\n",binString.Data(),sf,sfTotalUp,sfTotalDown);
  //printf("b mistag SF %7s  : %8.4f +%6.4f -%6.4f\n",binString.Data(),sf,sfUp,sfDown);

}

void getSFs() {
  calcSFs(-1);
  calcSFs(0);
  calcSFs(1);  
}
