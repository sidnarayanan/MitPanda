#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from os import system,mkdir
from sys import argv
tcut = root.TCut
### LOAD LIBRARIES ###
#gSystem.CompileMacro("${CMSSW_BASE}/src/MitPanda/Tools/src/PlotUtility.cc")
gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')
### HELPER FUNCTIONS ###
def setBins(dist,bins):
  for b in bins:
    dist.AddBinEdge(b)

def contains(s1,s2):
  if type(s2)==type(""):
    return s1.find(s2)>=0
  else:
    for s in s2:
      if s1.find(s)>=0:
        return True
    return False

def tAND(s1,s2):
  return "(("+s1+")&&("+s2+"))"

def tOR(s1,s2):
  return "(("+s1+")||("+s2+"))"

def tTIMES(w,s):
  return "("+w+")*("+s+")"

### SET GLOBAL VARIABLES ###
region = argv[1]
try:
  additionalcut = argv[2]
except:
  additionalcut = None

makePlots = True
baseDir = '/home/snarayan/local/BaconSkim/ntuples_v6/'
lumi = 2.26
recoilBins = [250,300,350,400,500,1000] 
nRecoilBins = len(recoilBins)-1
ptBins = [0,350,9999]

### DEFINE REGIONS ###
cuts = {}
# resolved

# basic CA15 selection
topTagCut                = 'bst15_PUPPIjet1msd>110 && bst15_PUPPIjet1msd<210 && bst15_PUPPIjet1tau32<0.61' # used for evaluating SF and uncert
#topTagCut                = 'bst15_PUPPIjet1tau32<0.61' # used for evaluating SF and uncert
antiTopTagCut                = '!(bst15_PUPPIjet1msd>110 && bst15_PUPPIjet1msd<210 && bst15_PUPPIjet1tau32<0.61)' # used for evaluating SF and uncert
cuts['ttCR']             = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && nbPUPPIjetsLdR2==1 && bst15_PUPPIjet1msd>50 && bst15_PUPPIjet1msd<300'
cuts['ettCR']            = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && nbPUPPIjetsLdR2==1 && bst15_PUPPIjet1msd>50 && bst15_PUPPIjet1msd<300'
#cuts['ttCR']             = tAND(topTagCut, 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && nbPUPPIjetsLdR2>0 ')
cuts['aCR']              = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && bst15_PUPPIjet1msd>50 && bst15_PUPPIjet1msd<300'
#cuts['wCR']              = tAND('nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0',antiTopTagCut)
cuts['wCR']              = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && nPUPPIjetsdR2==0'
cuts['zCR']              = 'nf15PUPPIjets==1 && bst15_PUPPIjet1.Pt()>250 && TMath::Abs(bst15_PUPPIjet1.Eta())<2.4 && metfilter==0 && bst15_PUPPIjet1msd>40'
#cuts['tnptoptag']             = 'nf15PUPPIjets==1'
btagCut                  = 'bst15_PUPPIjet1maxsubcsv>0.76'
failBtagCut                  = 'bst15_PUPPIjet1maxsubcsv<0.76'
#N-1 cuts

# regions 
#cuts['ttCR_nobtag']    = tAND(cuts['ttCR'], 'nmu==1 && (nele+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&2)!=0)')
#cuts['ttCR_nobtag']    = tAND(cuts['ttCR'], 'nele==1 && (nmu+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&4)!=0) && puppet.Pt()>50')
cuts['ttCR_nobtag']    = tAND(cuts['ttCR'], 'nmu==1 && (nele+ntau+npho)==0 && fakepuppet.Pt()>200 && ((triggerBits&2)!=0)')
cuts['ttCR_withbtag']  = tAND(cuts['ttCR_nobtag'], btagCut)
cuts['ttCR_failbtag']  = tAND(cuts['ttCR_nobtag'], failBtagCut)

cuts['ettCR_nobtag']    = tAND(cuts['ettCR'], 'nele==1 && (nmu+ntau+npho)==0 && fakepuppet.Pt()>200 && puppet.Pt()>50 && ((triggerBits&4)!=0)')
cuts['ettCR_withbtag']  = tAND(cuts['ettCR_nobtag'], btagCut)
cuts['ettCR_failbtag']  = tAND(cuts['ettCR_nobtag'], failBtagCut)

cuts['zCR_nobtag']    = tAND(cuts['zCR'], 'nmu==2 && (nele+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&2)!=0)')
cuts['zCR_withbtag']  = tAND(cuts['zCR_nobtag'], btagCut)
cuts['zCR_failbtag']  = tAND(cuts['zCR_nobtag'], failBtagCut)

cuts['aCR_nobtag']    = tAND(cuts['aCR'], 'npho==1 && (nele+ntau+nmu)==0 && fakepuppet.Pt()>250 && fakepuppet.Pt()>250 && ((triggerBits&8)!=0)')
#cuts['aCR_nobtag']    = tAND(cuts['aCR'], 'npho==1 && (nele+ntau+nmu)==0 && vpho.Pt()> 200 && ((triggerBits&8)!=0)')
cuts['aCR_withbtag']  = tAND(cuts['aCR_nobtag'], btagCut)
cuts['aCR_failbtag']  = tAND(cuts['aCR_nobtag'], failBtagCut)

cuts['wCR_nobtag']    = tAND(cuts['wCR'], 'nmu==1 && (nele+ntau+npho)==0 && fakepuppet.Pt()>250 && ((triggerBits&16)!=0)')
cuts['wCR_withbtag']  = tAND(cuts['wCR_nobtag'], btagCut)
cuts['wCR_failbtag']  = tAND(cuts['wCR_nobtag'], failBtagCut)

cuts['test_test'] = 'nmu==2 && fakepuppet.Pt()>0 && ((triggerBits&16)!=0)'

### LOAD PLOTTING UTILITY ###
plot = root.PlotUtility()
plot.Stack(True)
plot.Logy(False)
plot.CloneTrees(True)
#plot.Ratio(True)
plot.DrawMCErrors(True)
plot.SetNormFactor(False)
plot.SetLumi(lumi)
fullCut = region + ('_'+additionalcut if additionalcut!=None else '')
plot.SetCut(tcut(cuts[fullCut]))
print cuts[fullCut]
plot.SetMCWeight(tcut("%f*scale1fb*evtWeight*nloKfactor_CENT*ewkCorr_CENT*lepWeight"%(lumi)))
if contains(region,'ttCR'):
  plot.SetMCWeight(tcut("%f*scale1fb*evtWeight*nloKfactor_CENT*ewkCorr_CENT*lepWeight*res_PUPPIbtagwL1_CENT*triggerEff"%(lumi)))


### DEFINE PROCESSES ###
zjetshf   = root.Process('Z+jets HF',root.kExtra3)
zjets     = root.Process('Z+jets LF',root.kZjets)
wjetshf   = root.Process('W+jets HF',root.kExtra4)
wjets     = root.Process('W+jets',root.kWjets)
diboson   = root.Process('Diboson',root.kDiboson)
ttbar     = root.Process('t#bar{t}',root.kTTbar)
singletop = root.Process('Single t',root.kST)
qcd       = root.Process("QCD",root.kQCD)
data      = root.Process("Data",root.kData)
gjetshf   = root.Process("#gamma+jets HF",root.kExtra5)
gjets     = root.Process("#gamma+jets LF",root.kGjets)
sigs      = root.Process("Heavy flavor",root.kExtra1)
bgs       = root.Process("Light flavor",root.kExtra2)

#processes = [qcd,diboson,singletop,zjets,wjets,zjetshf, wjetshf, ttbar,data]
processes = [bgs,sigs,data]
'''
if region.find("a")>=0:
  sigs = root.Process("non-top jets",root.kExtra2)
  processes = [sigs,data]
else:
  sigs = root.Process("top jets",root.kExtra1)
  bgs  = root.Process("non-top jets",root.kExtra2)
  processes = [bgs,sigs,data]
'''
for p in processes:
  p.Init("Events")
#data.Init("Events")
#gjets.Init("Events")
#gjetshf.Init("Events")

### ASSIGN FILES TO PROCESSES ###
qcd.AddFile(baseDir+'QCD.root')
if region.find('aCR')>=0:
  data.AddFile(baseDir+'SinglePhoton.root')
#  gjets.AddFile(baseDir+'GLF.root')
#  gjetshf.AddFile(baseDir+'GHF.root')
#  processes = [gjetshf,qcd,gjets,data]
  sigs.AddFile(baseDir+'GHF.root')
  bgs.AddFile(baseDir+'GLF.root')
  bgs.AddFile(baseDir+'QCD.root')
  processes = [sigs,bgs,data]
else:
  bgs.AddFile(baseDir+'ZLF.root')
  bgs.AddFile(baseDir+'DYLF.root')
  bgs.AddFile(baseDir+'WLF.root')
  bgs.AddFile(baseDir+'WW.root')
  bgs.AddFile(baseDir+'WZ.root')
  bgs.AddFile(baseDir+'ZZ.root')
  bgs.AddFile(baseDir+'T.root')
  sigs.AddFile(baseDir+'TT.root')
  sigs.AddFile(baseDir+'WHF.root')
  sigs.AddFile(baseDir+'ZHF.root')
  sigs.AddFile(baseDir+'DYHF.root')
  if not contains(region,'ttCR'):
    processes = [sigs,bgs,data]
  '''
  zjets.AddFile(baseDir+'ZLF.root')
  zjets.AddFile(baseDir+'DYLF.root')
  wjets.AddFile(baseDir+'WLF.root')
  diboson.AddFile(baseDir+'WW.root')
  diboson.AddFile(baseDir+'WZ.root')
  diboson.AddFile(baseDir+'ZZ.root')
  singletop.AddFile(baseDir+'T.root')
  ttbar.AddFile(baseDir+'TT.root')
  wjetshf.AddFile(baseDir+'WHF.root')
  zjetshf.AddFile(baseDir+'ZHF.root')
  zjetshf.AddFile(baseDir+'DYHF.root')
  '''
  if 'e' in region:
    data.AddFile(baseDir+'SingleElectron.root')
  else:
    data.AddFile(baseDir+'MET.root')
    
  #data.AddFile(baseDir+'../ntuples_v3/MET.root')
  #data.AddFile(baseDir+'SingleElectron.root')
  #data.AddFile(baseDir+'SingleMuon.root')

for p in processes:
  plot.AddProcess(p)

recoilBins = [250,300,350,400,500,1000] 
nRecoilBins = len(recoilBins)-1

### CHOOSE DISTRIBUTIONS, LABELS ###
ptLabel = ''
if 'with' in additionalcut:
  ptLabel = 'Passing'
elif 'fail' in additionalcut:
  ptLabel = 'Failing'
if contains(additionalcut,'bin0'):
  ptLabel += ' p_{T} < %i GeV'%(ptBins[1])
elif contains(additionalcut,'bin1'):
  ptLabel += ' p_{T} > %i GeV'%(ptBins[1])
cutLabel = "cut-based top tag" if additionalcut=="SR" else ""
#plot.SetPlotLabel("#splitline{#it{CMS Preliminary}}{#splitline{%.2f fb^{-1} (13 TeV)}{%s}}"%(lumi,ptLabel))
plot.SetTDRStyle()
plot.AddCMSLabel()
plot.AddLumiLabel()
plot.AddPlotLabel(ptLabel,.16,.75,False,42,.06);

# define what "recoil" means
if contains(region,"zCR"):
  recoilType = "#mu#mu"
elif contains(region,["wCR",'ttCR']):
  recoilType = "#mu"
else:
  recoilType = "#gamma"
# choose recoil
recoil=None
if additionalcut=='SR':
  recoil=root.Distribution("puppet.Pt()",nRecoilBins,"MET","Events/GeV")
else:
  recoil=root.Distribution("fakepuppet.Pt()",nRecoilBins,"Recoil (%s)"%recoilType,"Events/GeV")
if recoil:
  setBins(recoil,recoilBins)
#plot.AddDistribution(recoil)
#plot.AddDistribution(root.Distribution("fakepuppet.Pt()",200,250,10,"Recoil","Events/5 GeV"))
#plot.AddDistribution(root.Distribution("bst15_PUPPIjet1.Pt()",250,1000,25,"jet p_{T}","Events/30 GeV"))
#plot.AddDistribution(root.Distribution("nPUPPIjetsdR2",0,10,10,"N jets dR>2","Events"))
#plot.AddDistribution(root.Distribution("nbPUPPIjetsTdR2",0,10,10,"N b (tight) jets dR>2","Events"))
plot.AddDistribution(root.Distribution("bst15_PUPPIjet1msd",50,300,25,"fatjet mass [GeV]","Events/10 GeV"))
#plot.AddDistribution(root.Distribution("bst15_PUPPIjet1maxsubcsv",0,1,20,"max subjet btag","Events"))
#plot.AddDistribution(root.Distribution("nPUPPIjets",0,6,7,"nJets","Events"))
plot.AddDistribution(root.Distribution("1",0,2,1,"dummy","dummy"))
### DRAW AND CATALOGUE ###
try:
  mkdir(baseDir+'/figs/'+fullCut)
except OSError:
  pass
plot.DrawAll(baseDir+'/figs/'+fullCut)
