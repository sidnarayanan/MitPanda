#!/usr/bin/env python

from ROOT import gSystem, gROOT
import ROOT as root
from array import array
from glob import glob
from re import sub
from sys import argv
from os import environ,system
from MitPanda.Tools.process import *

gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/Normalizer.h")
gSystem.Load('libMitPandaTools.so')

pds = {}
for k,v in processes.iteritems():
  if v[1]=='MC':
    pds[v[0]] = (k,v[2])  
  else:
    pds[v[0]] = (k,-1)

VERBOSE=False

user = environ['USER']
system('mkdir -p /tmp/%s/split'%user) # tmp dir
system('mkdir -p /tmp/%s/merged'%user) # tmp dir

base = environ['PANDA_FLATDIR']
inbase = base+'/condor/'

def hadd(inpath,outpath):
  if type(inpath)==type('str'):
    infiles = glob(inpath)
    print 'hadding %s into %s'%(inpath,outpath)
  else:
    infiles = inpath
  if len(infiles)==0:
    print 'WARNING: nothing hadded into',outpath
    return
  elif len(infiles)==1:
    cmd = 'cp %s %s'%(infiles[0],outpath)
  else:
    cmd = 'hadd -f %s '%outpath
    for f in infiles:
      cmd += '%s '%f
  if VERBOSE: print cmd
  system(cmd+' >/dev/null')

def normalize(fpath,opt):
  xsec=-1
  if type(opt)==type(1.) or type(opt)==type(1):
    xsec = opt
  else:
    try:
      xsec = processes[proc][2]
    except KeyError:
      for k,v in processes.iteritems():
        if proc in k:
          xsec = v[2]
  if xsec<0:
    print 'could not find xsec, skipping %s!'%opt
    return
  print 'normalizing %s (%s) ...'%(fpath,opt)
  nw = array('f',[0])
  w = array('f',[0])
  fin = root.TFile(fpath,'UPDATE')
  tree = fin.Get('events')
  total = fin.Get('hDTotalMCWeight').Integral()
  b = tree.Branch('normalizedWeight',nw,'normalizedWeight/F')
  tree.SetBranchAddress('mcWeight',w)
  nEntries = tree.GetEntries()
  for iE in xrange(nEntries):
    tree.GetEntry(iE)
    nw[0] = xsec*w[0]/total
    b.Fill()
  fin.WriteTObject(tree)
  fin.Close()

def normalizeFast(fpath,opt):
  xsec=-1
  if type(opt)==type(1.) or type(opt)==type(1):
    xsec = opt
  else:
    try:
      xsec = processes[proc][2]
    except KeyError:
      for k,v in processes.iteritems():
        if proc in k:
          xsec = v[2]
  if xsec<0:
    print 'could not find xsec, skipping %s!'%opt
    return
  print 'fast normalizing %s (%s) ...'%(fpath,opt)
  n = root.Normalizer();
  n.NormalizeTree(fpath,xsec)

def merge(shortnames,mergedname):
  for shortname in shortnames:
    pd = pds[shortname][0]
    xsec = pds[shortname][1]
    hadd(inbase+shortname+'_*.root','/tmp/%s/split/%s.root'%(user,shortname))
    if xsec>0:
      normalizeFast('/tmp/%s/split/%s.root'%(user,shortname),xsec)
  hadd(['/tmp/%s/split/%s.root'%(user,x) for x in shortnames],'/tmp/%s/merged/%s.root'%(user,mergedname))

d = {
  'test' : ['Diboson_ww'],
  'Diboson' : ['Diboson_ww','Diboson_wz','Diboson_zz'],
  'ZJets' : ['ZJets_ht200to400','ZJets_ht400to600','ZJets_ht600toinf'],
  'ZtoNuNu' : ['ZtoNuNu_ht200to400','ZtoNuNu_ht400to600','ZtoNuNu_ht600to800','ZtoNuNu_ht800to1200','ZtoNuNu_ht1200to2500','ZtoNuNu_ht2500toinf'],
  'GJets' : ['GJets_ht200to400','GJets_ht400to600','GJets_ht600toinf'],
  'WJets' : ['WJets_ht200to400','WJets_ht400to600','WJets_ht600to800','WJets_ht800to1200','WJets_ht1200to2500','WJets_ht2500toinf'],
  'TTbar' : ['TTbar'],
  'TTbarDM' : ['TTbarDM'],
  'SingleTop' : ['SingleTop_tchannel','SingleTop_tWantitop','SingleTop_tWtop'],
  'QCD' : ['QCD_ht200to300','QCD_ht300to500','QCD_ht500to700','QCD_ht700to100','QCD_ht1000to1500','QCD_ht1500to2000','QCD_ht2000toinf'],
  'MET' : ['MET'],
}

args = {}

for pd in argv[1:]:
  args[pd] = d[pd]

for pd in args:
  merge(args[pd],pd)
  system('cp -r /tmp/%s/merged/%s.root %s'%(user,pd,base))
  print 'finished with',pd

