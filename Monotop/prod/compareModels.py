#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from sys import argv,exit
from array import array
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.Selection import *
from ROOT import gROOT

gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

plot = root.PlotUtility()
plot.SetTDRStyle()
plot.AddCMSLabel()
plot.Stack(False)

lumi=12918

res=True
scale=False
plot76x=True
applycut=True
label=''

if res:
  label='resonant'

if scale:
  plot.SetNormFactor(True)
  label+='scaled'

models = argv[1:]
if len(models)==1:
  label += models[0]

if res:
  fin_80x = '${PANDA_PRODFLATDIR}/monotop_med-%s_dm-100.root'
  fin_76x = '~/local/monotop_80/monotop_res_mMed%s.root'
else:
  fin_80x = '${PANDA_PRODFLATDIR}/monotop-nr-v3-%s-10_med-%s_dm-10.root'
  fin_76x = '~/local/monotop_80/monotop_fcnc_mMed%s.root'

weight = 'normalizedWeight'
cut = ''
if applycut:
  weight = weights['signal']%lumi
  cut = removeCut(cuts['signal'],'metFilter')

plot.SetCut(cut)
plot.SetMCWeight(weight)

ntot = len(models)

counter=2
for m in models:
  p = root.Process(m+' 80x',counter,counter)
  if res:
    p.AddFile(fin_80x%m)
  else:
    p.AddFile(fin_80x%(m,m))
  plot.AddProcess(p)
  counter += 1
  if plot76x:
    p = root.Process(m+' 76x',counter,counter)
    p.dashed=True
    p.AddFile(fin_76x%m)
    plot.AddProcess(p)
    counter += 1

recoilBins = [250,300,350,400,500,1000]
nRecoilBins = len(recoilBins)-1
recoil=root.Distribution("puppimet",nRecoilBins,"MET [GeV]","Events/GeV")
setBins(recoil,recoilBins)
plot.AddDistribution(recoil)
pfmet=root.Distribution("met",nRecoilBins,"MET [GeV]","Events/GeV")
setBins(pfmet,recoilBins)
plot.AddDistribution(pfmet)
plot.AddDistribution(root.Distribution("CA15fj1_pt",250,1000,30,"fat jet p_{T} [GeV]","Events/25 GeV")) 
plot.AddDistribution(root.Distribution("genJet1Pt",0,1000,30,"leading gen jet p_{T} [GeV]","Events/33 GeV")) 
plot.AddDistribution(root.Distribution("jet1Pt",0,1000,30,"leading jet p_{T} [GeV]","Events/33 GeV")) 
plot.AddDistribution(root.Distribution("trueGenBosonPt",0,1000,20,"mediator p_{T} [GeV]","Events/50 GeV")) 
plot.AddDistribution(root.Distribution("genBosonMass",0,3000,3000,"mediator mass [GeV]","Events GeV")) 
plot.AddDistribution(root.Distribution("genTopPt",0,1000,20,"gen top p_{T} [GeV]","Events/50 GeV")) 

plot.DrawAll("~/public_html/figs/monotop_privateProd/comparison/"+label)

