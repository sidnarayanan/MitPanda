#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from sys import argv,exit
from array import array
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.Selection import *
from ROOT import gROOT

gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

plot = root.PlotUtility()
plot.SetTDRStyle()
plot.AddCMSLabel()
plot.Stack(False)

res=False
scale=False
label=''

if scale:
  plot.SetNormFactor(True)
  label='scaled'

masspoints = [int(m) for m in argv[1:]]

fin = '${PANDA_PRODFLATDIR}/non-res-%i.root'

'''
weight = weights['signal']%1
cut = cuts['signal']
'''

weight = 'normalizedWeight'
cut = ''

plot.SetCut(cut)
plot.SetMCWeight(weight)

counter=2
for m in masspoints:
  p = root.Process('m_{V} = %.1f TeV'%(m/1000.),counter)
  p.AddFile(fin%m)
  plot.AddProcess(p)
  counter += 1

recoilBins = [0,50,100,150,200,250,300,350,400,500,1000]
nRecoilBins = len(recoilBins)-1
recoil=root.Distribution("puppimet",nRecoilBins,"MET [GeV]","Events/GeV")
setBins(recoil,recoilBins)
plot.AddDistribution(recoil)
pfmet=root.Distribution("met",nRecoilBins,"MET [GeV]","Events/GeV")
setBins(pfmet,recoilBins)
plot.AddDistribution(pfmet)
plot.AddDistribution(root.Distribution("CA15fj1_pt",250,1000,30,"fat jet p_{T} [GeV]","Events/25 GeV")) 
plot.AddDistribution(root.Distribution("genJet1Pt",0,1000,30,"leading gen jet p_{T} [GeV]","Events/33 GeV")) 
plot.AddDistribution(root.Distribution("jet1Pt",0,1000,30,"leading jet p_{T} [GeV]","Events/33 GeV")) 
plot.AddDistribution(root.Distribution("trueGenBosonPt",0,1000,20,"mediator p_{T} [GeV]","Events/50 GeV")) 
plot.AddDistribution(root.Distribution("genBosonMass",0,3000,3000,"mediator mass [GeV]","Events GeV")) 
plot.AddDistribution(root.Distribution("genTopPt",0,1000,20,"gen top p_{T} [GeV]","Events/50 GeV")) 

plot.DrawAll("~/public_html/figs/monotop_2016_powheg/signalComparison/fcnc80x"+label+'_')

