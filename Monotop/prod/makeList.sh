#!/bin/bash

catalog=/home/cmsprod/catalog/t2mit/fullsm/044/monotop_med-${1}_dm-100/
xrd=$(grep -o "root.*-100" ${catalog}/Filesets | head -n 1)
echo $xrd
for f in $(grep -o "monotop.*root" ${catalog}/Files)
do
  ff=$(echo $f | sed 's/bambu/miniaodsim/')
  echo ${xrd}/$ff
  break
done
