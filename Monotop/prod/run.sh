#!/bin/bash

label=$1
cfgName=$2
outpath=$3

echo $label $cfgName $outpath

pwd
rm *root

executable=testNeroCondor.py

scramdir=/home/snarayan/cms/cmssw/CMSSW_8_0_11/src
cd ${scramdir}/
eval `scramv1 runtime -sh`
cd -

echo RUNNING ON $HOSTNAME
voms-proxy-init -voms cms
#cp ${scramdir}/MitPanda/Monotop/prod/x509up_u67051 .
#export X509_USER_PROXY=${PWD}/x509up_u67051
#echo "X509_USER_PROXY = $X509_USER_PROXY"

nerodir=${scramdir}/NeroProducer/Nero/test/

cp -r ${nerodir}/jec .
cp -r ${nerodir}/jer .
cp -r ${nerodir}/qg .

for f in $(cat $cfgName); do
  echo "copying $f"
  xrdcp ${f} .
done 

echo "##############################################################"

touch local.cfg
for f in $(ls *.root); do
  echo file:${PWD}/${f} >> local.cfg
done

cat local.cfg

echo "##############################################################"

ls

echo "##############################################################"

#cmsRun ${nerodir}/${executable} filelist=${cfgName} outfile=${PWD}/NeroNtuples_${label}.root
cmsRun ${nerodir}/${executable} filelist=local.cfg outfile=${PWD}/NeroNtuples_${label}.root

echo "##############################################################"

mv ${PWD}/NeroNtuples_${label}.root ${outpath}

rm -r jec jer qg *root local.cfg
