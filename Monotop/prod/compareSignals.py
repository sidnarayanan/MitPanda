#!/usr/bin/env python

from ROOT import gROOT, gSystem
import ROOT as root
from sys import argv,exit
from array import array
from MitPanda.Tools.Misc import *
from MitPanda.Monotop.Selection import *
from ROOT import gROOT

gROOT.LoadMacro("${CMSSW_BASE}/src/MitPanda/Tools/interface/PlotUtility.h")
gSystem.Load('libMitPandaTools.so')

plot = root.PlotUtility()
plot.SetTDRStyle()
plot.AddCMSLabel()
plot.Stack(False)

res=False
tW=False
chs=False
scale=False
applycut=False
label=''

if scale:
  plot.SetNormFactor(True)
  label='scaled_'
if chs:
  label += 'chs_'
if applycut:
  label += '_cut_'

fin80xv3=None
if tW:
  if chs:
    fin76x = '/local/snarayan/monotop_76/chs/SingleTop_tW.root'
    fin80x = '/local/snarayan/monotop_80/chs/SingleTop_tW.root'
  else:
    fin76x = '/local/snarayan/monotop_76/SingleTop_tW.root'
    fin80x = '/local/snarayan/monotop_80_4p3fb/SingleTop_tW_noSkim.root'
  med=None
elif not res:
  if chs:
    fin76x = '/local/snarayan/monotop_76/chs/monotop_fcnc_mMed1100.root'
    fin80x = '/scratch5/snarayan/root/monotop_pfprod/monotop-nr-1100_med-1100.root'
  else:
    fin76x = '/local/snarayan/monotop_76/monotop_fcnc_mMed1100.root'
    fin80x = '/scratch5/snarayan/root/monotop_prod/monotop-nr-50ns_med-1100.root'
    fin80xv3 = '/scratch5/snarayan/root/monotop_prod/monotop-nr-v3-50ns_med-1100_dm-10.root'
  med='V'
else:
  if chs:
    fin76x = '/local/snarayan/monotop_76/chs/monotop_res_mMed1100.root'
    fin80x = '/scratch5/snarayan/root/monotop_pfprod/monotop_med-1100_dm-100.root'
  else:
    fin76x = '/local/snarayan/monotop_76/monotop_res_mMed1100.root'
    fin80x = '/scratch5/snarayan/root/monotop_prod/monotop_med-1100_dm-100.root'
  med='S'

'''
weight = weights['signal']%1
cut = cuts['signal']
'''

weight = 'normalizedWeight'
cut = ''
if applycut:
  weight = weights['signal']%1
  cut = cuts['signal']

plot.SetCut(cut)
plot.SetMCWeight(weight)

plotlabel = ''
if tW:
  plotlabel = 'tW'
elif res:
  plotlabel = 'Resonant, m_{#phi}=1.1 TeV'
else:
  plotlabel = 'FCNC, m_{V}=1.1 TeV'
plot.AddPlotLabel(plotlabel,0.18,0.8,False,42,0.04)

x80v3=None
if tW:
  x76 = root.Process('76x',root.kSignal1); x76.AddFile(fin76x)
  x80 = root.Process('80x',root.kSignal2); x80.AddFile(fin80x)
else:
  x76 = root.Process('76x',root.kSignal1); x76.AddFile(fin76x)
  x80 = root.Process('80x',root.kSignal2); x80.AddFile(fin80x); #x80.additionalWeight = root.TCut('3')
  if fin80xv3:
    x80v3 = root.Process('80x v3 m_{#chi}=10 GeV',root.kSignal3); x80v3.AddFile(fin80xv3)

for p in [x80v3, x76, x80]:
  if p:
    plot.AddProcess(p)

recoilBins = [250,300,350,400,500,1000]
nRecoilBins = len(recoilBins)-1
plot.AddDistribution(root.Distribution("nGenJet",0,15,15,'N_{gen AK4}','Events'))
plot.AddDistribution(root.Distribution("looseLep1Pt",0,400,10,"leading lep p_{T}","Events/40 GeV"))
recoil=root.Distribution("puppimet",nRecoilBins,"Puppi MET [GeV]","Events/GeV")
pfrecoil=root.Distribution("met",nRecoilBins,"PF MET [GeV]","Events/GeV")
for r in [recoil,pfrecoil]:
  setBins(r,recoilBins)
  plot.AddDistribution(r)
plot.AddDistribution(root.Distribution("CA15fj1_pt",250,1000,15,"fat jet p_{T} [GeV]","Events/50 GeV")) 
plot.AddDistribution(root.Distribution("genJet1Pt",0,1000,20,"leading gen jet p_{T} [GeV]","Events/50 GeV")) 
if not tW:
  plot.AddDistribution(root.Distribution("genTopPt",0,1000,20,"top p_{T} [GeV]","Events/50 GeV")) 
plot.AddDistribution(root.Distribution("jet1Pt",0,1000,20,"leading jet p_{T} [GeV]","Events/50 GeV")) 
plot.AddDistribution(root.Distribution("nJet",0,6,6,'N_{AK4}','Events'))
plot.AddDistribution(root.Distribution("npv",0,40,10,'N_{PV}','Events'))
plot.AddDistribution(root.Distribution("CA15fj1_mSD",0,400,20,"mSD",'Events/20 GeV'))
plot.AddDistribution(root.Distribution("CA15fj1_tau3/CA15fj1_tau2*(nCA15fj>0)",0,1.2,12,"tau32",'Events'))
if not tW:
  if res:
    plot.AddDistribution(root.Distribution("trueGenBosonPt",0,1000,20, " DM fermion p_{T} [GeV]","Events/50 GeV")) 
  else:
    plot.AddDistribution(root.Distribution("trueGenBosonPt",0,1000,20,"mediator p_{T} [GeV]","Events/50 GeV")) 

if tW:
  plot.DrawAll("/home/snarayan/public_html/figs/monotop_privateProd/beamspot/tW"+label+'_')
elif res:
  plot.DrawAll("/home/snarayan/public_html/figs/monotop_privateProd/beamspot/res"+label+'_')
else:
  plot.DrawAll("/home/snarayan/public_html/figs/monotop_privateProd/beamspot/fcnc"+label+'_')

