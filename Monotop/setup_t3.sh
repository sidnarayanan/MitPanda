#!/bin/bash

#here
export PANDA=${CMSSW_BASE}/src/MitPanda/

#location of nero output (from bambu or miniaod)
export PANDA_HISTDIR=${HOME}/cms/hist/monotop_v2/

#which bambu setup we are running on
export PANDA_MITBOOK=t2mit/filefi/043

#location of flat trees
export PANDA_FLATDIR=${HOME}/local/monotop_80

#download metfilters
if [ ! -d ${PANDA}/Monotop/data/metfilters ]; then
  cd ${PANDA}/Monotop/data
  sh ${PANDA}/Monotop/data/getFilters.sh
  cd -
fi
