#!/usr/bin/env python

from sys import argv
from glob import glob
from os import stat,getenv
from MitPanda.Tools.process import *
from re import sub

histdir = getenv('PANDA_PRODDIR')
listOfFiles = glob(histdir+'/*/NeroNtuples_*.root')
print 'found %i files'%len(listOfFiles)

cfgFile = open(argv[1],'w')

couldNotFind = []

for f in sorted(listOfFiles):
  if stat(f).st_size==0:
    continue
  ff = f.split('/')
  nickname = ff[-2]
  fileName = '${PANDA_PRODDIR}/'
  fileName += nickname+'/'
  fileName += ff[-1]
  if len(argv)>2:
    if argv[2] in nickname:
      cfgFile.write('{0:<40}\t\tMC\t\t1\t\t{1:<180}\n'.format(nickname,fileName)) 
  else:
    cfgFile.write('{0:<40}\t\tMC\t\t1\t\t{1:<180}\n'.format(nickname,fileName)) 

