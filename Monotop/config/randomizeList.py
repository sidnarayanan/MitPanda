#!/usr/bin/env python

from random import shuffle
from sys import argv
from re import sub

fIn = list(open(argv[1],'r'))
fOut = open(sub('.cfg','_rnd.cfg',argv[1]),'w')
shuffle(fIn)
for l in fIn:
  fOut.write(l)

fOut.close()
