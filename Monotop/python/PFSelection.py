from MitPanda.Tools.Misc import *
from re import sub


presel = 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && CA15fj1_tau3/CA15fj1_tau2<0.61 && 110<CA15fj1_mSD && CA15fj1_mSD<210'
cuts = {
    'signal_nocalomet'            : tAND(presel,'met>175 && met>250 && dPhiMET>1.1 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==0 && CA15fj1_isTight==1'),
    'signal'            : tAND(presel,'met>175 && met>250 && dPhiMET>1.1 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==0 && CA15fj1_isTight==1'),
    'signal_nodphi'     : tAND(presel,'met>175 && met>250 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==0'),
    'qcdtest'           : 'nCA15fj==1 && CA15fj1_pt>250 && TMath::Abs(CA15fj1_eta)<2.4 && met>175 && met>250 && dPhiMET<0.5 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0',
    'singlemuontop_base'     : removeCut(removeCut(tAND(presel,'pfUWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && jetNIsoBtags==1 && CA15fj1_maxsjbtag>0.46'),'CA15fj1_tau3/CA15fj1_tau2'),'CA15fj1_mSD'),
    'singlemuontop'     : tAND(presel,'pfUWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==1'),
    'singlemuontop_nobtag'     : tAND(presel,'pfUWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && jetNIsoBtags==1'),
    'singleelectrontop' : tAND(presel,'pfUWmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag>0.46 && jetNIsoBtags==1 && met>40'),
    'singlemuonw'       : tAND(presel,'pfUWmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag<0.46 && jetNIsoBtags==0'),
    'singleelectronw'   : tAND(presel,'pfUWmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==1 && looseLep1IsTight==1 && CA15fj1_maxsjbtag<0.46 && jetNIsoBtags==0 && met>40'),
    'dimuon'            : tAND(presel,'pfUZmag>250 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==2 && looseLep1IsTight==1'),
    #'dielectron'        : tAND(presel,'pfUZmag>250 && (nLooseMuon+nTau)==0 && nLooseElectron==2 && looseLep1IsTight==1'),
    'dielectron'        : tAND(presel,'pfUZmag>250 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==2 && looseLep1IsTight==1'),
    'photon'            : tAND(presel,'pfUAmag>250 && (nLooseMuon+nLooseElectron+nTau)==0 && nLoosePhoton==1 && loosePho1IsTight==1'),
    'photon_base'       : removeCut(removeCut(tAND(presel,'pfUAmag>250 && (nLooseMuon+nLooseElectron+nTau)==0 && nLoosePhoton==1 && loosePho1IsTight==1'),'CA15fj1_tau3/CA15fj1_tau2'),'CA15fj1_mSD'),
    'test'              : 'nCA15fj==1 && CA15fj1_pt>250 && nLooseMuon==1 && looseLep1IsTight==1 && (nLooseElectron+nLoosePhoton+nTau)==0 && pfUWmag>250',
    'nobtag'            : 'nCA15fj==1 && CA15fj1_pt>250 && nLooseMuon==1 && looseLep1IsTight==1 && (nLooseElectron+nLoosePhoton+nTau)==0 && pfUWmag>250',
}

tag_presel = removeCut(removeCut(cuts['singlemuontop'],'CA15fj1_tau3/CA15fj1_tau2'),'CA15fj1_mSD')
mistag_presel = removeCut(removeCut(cuts['photon'],'CA15fj1_tau3/CA15fj1_tau2'),'CA15fj1_mSD')
tag = 'CA15fj1_tau3/CA15fj1_tau2<0.61 && 110<CA15fj1_mSD && CA15fj1_mSD<210'
tt_cuts = {
  'tag' : tag_presel,
  'tag_pass' : tAND(tag,tag_presel),
  'tag_fail' : tAND(tNOT(tag),tag_presel),
  'mistag' : mistag_presel,
  'mistag_pass' : tAND(tag,mistag_presel),
  'mistag_fail' : tAND(tNOT(tag),mistag_presel),
}

weights = {
  'signal' : '%f*normalizedWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_btag0*sf_sjbtag1',
  'top'    : '%f*normalizedWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_btag1*sf_sjbtag1',
  'w'      : '%f*normalizedWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_btag0*sf_sjbtag0',
#  'notag'  : '%f*normalizedWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*1.3*sf_tt',
  'notag'  : '%f*normalizedWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt',
}


for r in ['signal','top','w']:
  for shift in ['BUp','BDown','MUp','MDown']:
    for cent in ['sf_btag','sf_sjbtag']:
      weights[r+'_'+cent+shift] = sub(cent+'0',cent+'0'+shift,sub(cent+'1',cent+'1'+shift,weights[r]))

metTrigger='(trigger&1)!=0'
eleTrigger='(trigger&2)!=0'
phoTrigger='(trigger&4)!=0'
metFilter='metFilter==1'
