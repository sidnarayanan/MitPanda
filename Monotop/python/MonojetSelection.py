from MitPanda.Tools.Misc import *
from re import sub


#presel = 'nJet>0 && jet1IsTight==1 && jet1Pt>100 && TMath::Abs(jet1Eta)<2.4'
presel = 'nJet>0 && jet1Pt>100 && jet1IsTight==1 && TMath::Abs(jet1Eta)<2.4 && jetNBtags==0'
cuts = {
    'signal'            : tAND(presel,'met>150 && dPhiMET>1.1 && (nLooseMuon+nLooseElectron+nLoosePhoton+nTau)==0 '),
    'singlemuon'        : tAND(presel,'pfUWmag>150 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==1 && looseLep1IsTight==1'),
    'singleelectron'    : tAND(presel,'pfUWmag>150 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==1 && looseLep1IsTight==1 && puppimet>40'),
    'dimuon'            : tAND(presel,'pfUZmag>150 && (nLooseElectron+nLoosePhoton+nTau)==0 && nLooseMuon==2 && looseLep1IsTight==1'),
    'dielectron'        : tAND(presel,'pfUZmag>150 && (nLooseMuon+nLoosePhoton+nTau)==0 && nLooseElectron==2 && looseLep1IsTight==1'),
    'photon'            : tAND(presel,'pfUAmag>150 && (nLooseMuon+nLooseElectron+nTau)==0 && nLoosePhoton==1 && loosePho1IsTight==1'),
}


weights = {
  'signal' : '%f*normalizedWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_btag0*sf_sjbtag1',
  'top'    : '%f*normalizedWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_btag1*sf_sjbtag1',
  'w'      : '%f*normalizedWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt*sf_btag0*sf_sjbtag0',
  'notag'  : '%f*normalizedWeight*sf_lep*sf_ewkZ*sf_qcdZ*sf_ewkW*sf_qcdW*sf_ewkA*sf_qcdA*sf_tt',
}


metTrigger='(trigger&1)!=0'
eleTrigger='(trigger&2)!=0'
phoTrigger='(trigger&4)!=0'
metFilter='newMetFilter==1'
