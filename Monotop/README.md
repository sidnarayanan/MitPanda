# Monotop analysis code

## Running from Bambu
------


Do the usual set up for Bambu 043 or 044, depending on whether you want to use 76x or 80x

### Set up Nero

Clone NeroProducer:

```
cd $CMSSW_BASE/src
wget --no-check-certificate 'https://raw.githubusercontent.com/MiT-HEP/NeroProducer/master/Nero/script/setup.sh' -O /tmp/$USER/setup.sh
```

If you are using 043/76x, run the following steps:

```
source /tmp/$USER/setup.sh CMSSW_7_6_4 ## even though you are using 7_6_3, make sure to pass 7_6_4 here
git clone git@github.com:MiT-HEP/NeroProducer.git -b CMSSW_76X 
```

If you are using 044/80x:

```
source /tmp/$USER/setup.sh CMSSW_8_0_10
git clone git@github.com:MiT-HEP/NeroProducer.git 
```

Compile NeroProducer:

```
cd ${CMSSW_BASE}/src
scram b -j$(nproc)
cd NeroProducer/Bambu
make -j$(nproc)
```

### Set up analysis code

Clone MitPanda:

```
cd ${CMSSW_BASE}/src
git clone git@gitlab.com:sidnarayanan/MitPanda.git
```

Compile:

```
scram b -j$(nproc)
```

### Running Nero ntuplization

The analysis macro is contained in `NeroProducer/Bambu/macros/bambuToNero.py`. To run it locally (i.e. for testing), one can do:

```
cd ${CMSSW_BASE}/src/MitAna/bin
./analysis.py --book <book> --dataset <dataset> --nentries <nentries> /home/snarayan/cms/cmssw/043/CMSSW_7_6_3/src/NeroProducer/Bambu/macros/bambuToNero.py
```

To run on many datasets, one has to create a config file. An example is in `MitPanda/FlatNtupler/config/samples/monotop2016.cfg`. One should also make sure the condor configuration will actually export the files (not default behavior). Edit `NeroProducer/Bambu/config/condor_template.jdl` to look like:

```
Requirements            = Arch == "X86_64" && OpSysAndVer == "SL6" && HasFileTransfer
Notification            = Error
Rank                    = Mips
# Uncomment below if a local copy of ntuples are needed
transfer_output_files = {fileset}.root,nero.root
transfer_output_remaps = "nero.root = nero_{fileset}.root"
# transfer_output_files = {fileset}.root
accounting_group = group_cmsuser
```

Then, one can call:

```
./runOnDatasets.py --name <name> --book <book> --config <path/to/config> --analysis ${CMSSW_BASE}/src/NeroProducer/Bambu/macros/bambuToNero.py --condor-template ${CMSSW_BASE}/src/NeroProducer/Bambu/config/condor_template.jdl --num-files <num-files-per-job>
```

The output will land in `~/cms/hist/<name>`

### Running skims

First, set up your environment by editing the variables in `MitPanda/Monotop/setup.sh`. Then call `source setup.sh`. `PANDA_HISTDIR` should point to the location of the Nero ntuples made in the above step.

The core skimming object is defined in `MitPanda/Skimmers/src/NeroSkimmer.cc`. There are three main scripts to utilize it when running on Bambu. All are located in `MitPanda/Monotop/scripts/skimming/`. The first is used for testing things locally:
```
./skimLocal.py /path/to/input/file 
```
It runs on a single file and produces output in `/tmp/$USER/testskim.root`. 

To run on a large number of files, first create a config file:
```
cd MitPanda/Monotop/config/
./makeCfg.py monotop.cfg
```
The output is in `monotop.cfg`. Each line contains a single file, the category of the file, (i.e. ZJets, MET, etc.), whether it is data or MC, and the cross section if MC. These are read out of `MitPanda/Tools/python/processes.py`.

There are two ways to run on a config file: either on a single node using multiple cores or on many nodes using many cores. Especially if you are doing the former, I suggest you first test your setup using `skimLocal.py` first (python's `multiprocessing` obfuscates errors). To run on multiple cores:
```
./skimMultiCore.py monotop
```
The output will be put in `${PANDA_FLATDIR}/split/`. These files will then need to be merged (see below). 

If running on condor, the executable is `skimSingleCore.py`. To submit the jobs, first make sure `submit/run_skim.sh` points to the correct CMSSW location and `submit/submitAll.py` points to the config file you want to run on. Then, simply call:

```
./submitAll.py
```

The output will be put in `${PANDA_FLATDIR}/condor/`.

### Merging skims

The output files then need to be merged and the MC weights need to be normalized. Warning: the current setup assumes MC weights are +/- X, for some fixed value X. This is no longer true for Powheg samples and needs to be fixed. 

The workhorse is `MitPanda/Monotop/scripts/merge.py`. Make sure to edit line 29 to point to the correct subdirectory of `${PANDA_FLATDIR}`. Then, you can call multiple instances of `merge.py` by running `merge.sh` (simply passes a bunch of different physics process types through `xargs`).

## Running on MINIAOD
------

I typically keep separate environments for running on Bambu (T3) and MINIAOD (lxplus) because there are differences in CMSSW versions for NeroProducer. It is also useful to use EOS space on lxplus for staging out CRAB jobs.

### Set up Nero

Check out the appropriate release of CMSSW (7_6_4 or 8_0_10). The steps to setting up `NeroProducer` are identical to those detailed above, but you should not attempt compiling `NeroProducer/Bambu` without the `Mit*` packages installed.

### Set up analysis code

Again, the steps for `MitPanda` are identical to those above, but for one important difference. Immediately after cloning, call:
```
sh MitPanda/etc/lxplus.sh
```
It rearranges the setup a bit to avoid dependences on `Mit*` packages and switches some environment variables. If you want to commit any changes to git, make sure to call `sh etc/t3.sh` before committing, and `sh etc/lxplus.sh` to restore the lxplus-friendly environment.

### Running Nero

The CMSSW configuration file is in `NeroProducer/Nero/test/testNero.py`. Make sure to uncomment the lines loading `NeroMonojet_cfi` and `NeroMonotop_cfi` for this analysis (both must be enabled, and monojet must be loaded before monotop!). As usual, one can call `cmsRun testNero.py` or `python crabNero.py` to run on a single file or many datasets, respectively. A more detailed explanation is given here: https://github.com/MiT-HEP/NeroProducer/blob/master/README.md .

### Running skims

The set up to run skims on MINIAOD is very similar. Care should be made that the variables `metTriggers`, `eleTriggers`, and `phoTriggers` in `MitPanda/Skimmers/src/NeroSkimmer.cc` correctly map to the triggers in `NeroProducer/Nero/python/NeroMonojet_cfi`.

Make sure to set up your environment by sourcing `MitPanda/Monotop/setup.sh` (it should have been modified by `lxplus.sh`).

Once again, one can use `skimLocal.py` to test the skimmer on a single file. To run on multiple files, there are once again multi-threaded and batch submission options (there is also an option to run multicore jobs on lxbatch, but this is a bit broken for the moment). Both options assume the files are located on EOS in a directory structure compatible with CRAB output. 

First, make the configuration file using `Monotop/config/makeCfgEOS.py`. Then, call either `scripts/skimming/skimMultiCoreEOS.py <cfgname>` or edit and run the submission script at `scripts/skimming/submit/submitLxplus.py`

### Merging skims

The workhorse in this case is `mergeLXPLUS.py`. The only difference between it and `merge.py` is the input/output directory structure (I'll probably replace the two scripts with a single one soon). Make sure to edit `merge.sh` to point to the right executable, and then run.