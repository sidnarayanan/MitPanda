#!/bin/bash

# removes Mit* dependencies

panda=${CMSSW_BASE}/src/MitPanda/

mv ${panda}/PhysicsObjects/src/ ${panda}/PhysicsObjects/tmpsrc/
mv ${panda}/Fillers/src/ ${panda}/Fillers/tmpsrc/
mv ${panda}/FlatNtupler/src/ ${panda}/FlatNtupler/tmpsrc/
if [ ! -f ${panda}/Monotop/setup_t3.sh ]; then
  mv ${panda}/Monotop/setup.sh ${panda}/Monotop/setup_t3.sh
  mv ${panda}/Monotop/setup_lxplus.sh ${panda}/Monotop/setup.sh
fi
