#!/bin/bash

# replaces Mit* dependencies - necessary for git

panda=${CMSSW_BASE}/src/MitPanda/

mv ${panda}/PhysicsObjects/tmpsrc/ ${panda}/PhysicsObjects/src/
mv ${panda}/Fillers/tmpsrc/ ${panda}/Fillers/src/
mv ${panda}/FlatNtupler/tmpsrc/ ${panda}/FlatNtupler/src/
