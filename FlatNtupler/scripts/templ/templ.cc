#include <algorithm>
#include "MitPanda/FlatNtupler/interface/TEMPLATENAME.h"
#include "TLorentzVector.h"
#include "MitCommon/DataFormats/interface/Types.h"
#include "MitCommon/DataFormats/interface/Vect4M.h"
#include "MitAna/DataTree/interface/PFJet.h"
#include "MitAna/DataTree/interface/MCParticleCol.h"
#include "MitCommon/MathTools/interface/MathUtils.h"

using namespace mithep;

ClassImp(mithep::TEMPLATENAME)

//--------------------------------------------------------------------------------------------------
TEMPLATENAME::TEMPLATENAME(const char *name, const char *title):
  BaseMod(name,title)
{
  // STOP 1
}

TEMPLATENAME::~TEMPLATENAME()
{
  // STOP 2
  fFile->Delete();
}

void TEMPLATENAME::BeginRun() {

}

//--------------------------------------------------------------------------------------------------
void TEMPLATENAME::Process()
{
  unsigned int j;
  // STOP 3
  fTree->Fill();
  return;
}

//--------------------------------------------------------------------------------------------------
void TEMPLATENAME::SlaveBegin()
{
  fFile = new TFile(fFilePath,"RECREATE");
  fTree = new TTree("events","TEMPLATENAME");
  // STOP 4
}

//--------------------------------------------------------------------------------------------------
void TEMPLATENAME::SlaveTerminate()
{
  // STOP 5
  fFile->WriteTObject(fTree,fTree->GetName());
  fFile->Close();
}
