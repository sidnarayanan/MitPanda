#ifndef MITPANDA_FLAT_TEMPLATENAME_H
#define MITPANDA_FLAT_TEMPLATENAME_H

#include "MitPanda/PhysicsObjects/interface/XXLFatJetFwd.h"
#include "MitPanda/PhysicsObjects/interface/XXLFatJet.h"
#include "MitAna/DataTree/interface/PFTauFwd.h"
#include "MitAna/DataTree/interface/PFTau.h"
#include "MitAna/DataTree/interface/ElectronFwd.h"
#include "MitAna/DataTree/interface/Electron.h"
#include "MitAna/DataTree/interface/MuonFwd.h"
#include "MitAna/DataTree/interface/Muon.h"
#include "MitAna/DataTree/interface/PhotonFwd.h"
#include "MitAna/DataTree/interface/Photon.h"
#include "MitAna/DataTree/interface/PFMetFwd.h"
#include "MitAna/DataTree/interface/PFMet.h"
#include "MitAna/DataTree/interface/MetFwd.h"
#include "MitAna/DataTree/interface/Met.h"
#include "MitAna/TreeMod/interface/BaseMod.h"
#include "TTree.h"
#include "TFile.h"
#include "MitAna/DataTree/interface/MCEventInfo.h"
#include "MitAna/DataTree/interface/VertexCol.h"
#include "MitAna/DataTree/interface/TriggerName.h"
#include "MitAna/DataTree/interface/TriggerMask.h"
#include "MitAna/DataTree/interface/TriggerTable.h"
#include "MitAna/DataTree/interface/EventHeader.h"
#include "MitAna/DataTree/interface/MCParticleFwd.h"
#include "algorithm"

namespace mithep
{
  class TEMPLATENAME : public BaseMod
  {
    public:
      typedef FatJet::TrackData TrackData;
      typedef FatJet::SVData SVData;
      typedef FatJet::LeptonData LeptonData;

      TEMPLATENAME(const char*, const char*);
      ~TEMPLATENAME();
      // STOP 1
      void SetFilePath(TString p)  { fFilePath = p; }

    protected:
      void Process();
      void SlaveBegin();
      void SlaveTerminate();
      void BeginRun();

      TString fFilePath = "buffer.root";
      TFile *fFile;
      TTree *fTree;


    private:

      ClassDef(TEMPLATENAME, 0)         // TEMPLATENAME filler
  };
}
#endif
