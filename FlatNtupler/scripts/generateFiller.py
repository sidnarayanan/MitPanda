#!/usr/bin/env python

import sys,re

outFileName = sys.argv[1]
templName = "templ" if (len(sys.argv)<3) else sys.argv[2]

def makeCaps(s):
    return s[0].upper()+s[1:]

class fillerVar(object):
    def __init__(self,source,name,cType,fillFromInput = False):
        self.name = "%s_%s"%(source,name)
        self.baseName = name
        self.cType = cType
        self.fillFromInput = fillFromInput
    def __str__(self):
        return "%s %s %i"%(self.name,self.cType, 1 if self.fillFromInput else 0)
    def fill(self,source,counter):
        outString = ""
        if self.fillFromInput:
            if self.cType.find("TClonesArray") >= 0:
              outString += "\t\tnew ( (*f%s)[%s] ) TLorentzVector();\n"%(self.name,counter)
              outString += "\t\tconst FourVectorM &mom = _%s->Mom();\n"%(source)
              outString += "\t\tstatic_cast<TLorentzVector*>((*f%s)[%s])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());\n"%(self.name,counter)
            else:
              outString += "\t\tf%s[%s]=_%s->%s();\n"%(self.name,counter,source,self.baseName)
        return outString
    def fillSpecial(self,prefix,counter):
        if self.baseName.find(prefix)==0:
            return '\t\t\tf%s[%s]=%s.%s;\n'%(self.name,counter,prefix,self.baseName.split('_')[1])
        else:
            return ''
    def clear(self):
        outString = ""
        if self.cType.find("vector") >= 0:
            outString += "\tf%s->clear();\n"%(self.name)
        elif self.cType.find("TClonesArray") >= 0:
            outString += "\tf%s->Clear();\n"%(self.name)
        elif self.cType.find("[") >= 0:
            return outString
        else:
            outString += "\tf%s = -999;\n"%(self.name)
        return outString
    def declare(self):
        if self.name.find("track_") >= 0:
            return '\t\t%s f%s[N_MAX_TRACKS];\n'%(self.cType.strip('[]'),self.name)
        elif self.name.find("SV_") >= 0:
            return '\t\t%s f%s[N_MAX_SVS];\n'%(self.cType.strip('[]'),self.name)
        elif self.name.find("softElectron_") >= 0:
            return '\t\t%s f%s[N_MAX_SOFTELECTRONS];\n'%(self.cType.strip('[]'),self.name)
        elif self.name.find("softMuon_") >= 0:
            return '\t\t%s f%s[N_MAX_SOFTMUONS];\n'%(self.cType.strip('[]'),self.name)
        elif self.cType.find("[")>=0:
            return '\t\t%s f%s[10];\n'%(self.cType.strip('[]'),self.name)
        else:
            return '\t\t%s f%s;\n'%(self.cType,self.name)
    def construct(self):
        if self.cType.find('*') >= 0:
            if self.cType.find('TClonesArray') >= 0:
                # only reason to use a TClonesArray is for TLorentzVector right now
                return '\tf%s = new TClonesArray("TLorentzVector",20);\n'%(self.name)
            return '\tf%s = new %s;\n'%(self.name,self.cType[:-1])
        else:
            return ''
    def delete(self):
        if self.cType.find('*') >= 0:
            return '\tdelete f%s;\n'%(self.name)
        else:
            return ''
    def makeBranch(self,sourceNickname=""):
        size="_%sCounter"%(sourceNickname)
        for info in ['track','SV','softElectron','softMuon']:
            if self.name.find(info) >= 0:
                size = "%s_%sCounter"%(sourceNickname,info)
                continue
        outString = ''
        if self.cType.find("[") >= 0:
            if self.cType=="float[]":
                outString += '\t\tfTree->Branch("%s",f%s,"%s[%s]/F");\n'%(self.name,self.name,self.name,size)
            if self.cType=="int[]":
                outString += '\t\tfTree->Branch("%s",f%s,"%s[%s]/I");\n'%(self.name,self.name,self.name,size)
        elif self.cType.find("vector") >= 0:
            outString += '\t\tfTree->Branch("%s","%s",&f%s);\n'%(self.name,self.cType[:-1],self.name)
        elif self.cType.find("TClonesArray") >= 0:
            outString += '\t\tfTree->Branch("%s","%s",&f%s,128000, 0);\n'%(self.name,self.cType[:-1],self.name)
        elif self.cType=="float":
            outString += '\t\tfTree->Branch("%s",&f%s,"%s/F");\n'%(self.name,self.name,self.name)
        elif self.cType=="int":
            outString += '\t\tfTree->Branch("%s",&f%s,"%s/I");\n'%(self.name,self.name,self.name)
        return outString


def writePublic(varDict):
    outString = ''
    for k,v in varDict.iteritems():
        outString += "\t\tconst %sCol * f%s;\n"%(k[1],k[0])
        outString += "\t\tunsigned int nMax%s = %i;\n"%(k[0],k[2])
        outString += "\t\tunsigned int _%sCounter;\n"%(k[3])
        if k[1]=="XlFatJet":
            outString += '\t\tint f%s_nTracks[10];\n'%(k[3])
            outString += '\t\tint f%s_firstTrack[10];\n'%(k[3])
            outString += '\t\tint f%s_nSVs[10];\n'%(k[3])
            outString += '\t\tint f%s_firstSV[10];\n'%(k[3])
            outString += '\t\tint f%s_nSoftElectrons[10];\n'%(k[3])
            outString += '\t\tint f%s_firstSoftElectron[10];\n'%(k[3])
            outString += '\t\tint f%s_nSoftMuons[10];\n'%(k[3])
            outString += '\t\tint f%s_firstSoftMuon[10];\n'%(k[3])
            outString += '\t\tunsigned int %s_trackCounter;\n'%(k[3])
            outString += '\t\tunsigned int %s_SVCounter;\n'%(k[3])
            outString += '\t\tunsigned int %s_softElectronCounter;\n'%(k[3])
            outString += '\t\tunsigned int %s_softMuonCounter;\n'%(k[3])
        for vv in v:
            outString += vv.declare()
    return outString

def writeConstructor(varDict):
    outString = ''
    for k,v in varDict.iteritems():
        outString += "\tf%s = NULL;\n"%(k[0])
        for vv in v:
            outString += vv.construct()
    return outString

def writeDestructor(varDict):
    outString = ''
    for k,v in varDict.iteritems():
#        outString += '\tf%s->Delete();\n'%(k[0])
        for vv in v:
            outString += vv.delete()
    return outString

def writeFatJetInfoLoop(prefix,varList,source,fatjetCounter):
    outString = ''
    outString += '\t\tf%s_n%ss[%s] = %ss.size();\n'%(source,makeCaps(prefix),fatjetCounter,prefix)
    outString += '\t\tf%s_first%s[%s] = %s_%sCounter;\n'%(source,makeCaps(prefix),fatjetCounter,source,prefix)
    outString += '\t\tj=0;\n'
    outString += '\t\tfor (const auto & %s : %ss) {\n'%(prefix,prefix)
    for vv in varList:
        outString += vv.fillSpecial(prefix,"%s_%sCounter+j"%(source,prefix))
    outString += '\t\t\t++j;\n'
    outString += '\t\t}\n'
    outString += '\t\t%s_%sCounter += %ss.size();\n'%(source,prefix,prefix)
    return outString

def writeProcess(varDict):
    outString = ''
    for k,v in varDict.iteritems():
        # if k[1]=='XlFatJet':
        #     for info in ['track','SV','softElectron','softMuon']:
        #         outString += '\tf%s_n%ss->clear();\n'%(k[3],makeCaps(info))
        #         outString += '\tf%s_first%s->clear();\n'%(k[3],makeCaps(info))
        for vv in v:
            outString += vv.clear()
        outString += '\tf%s = GetObject<%sCol>("%s");\n'%(k[0],k[1],k[0])
        outString += '\tassert(f%s!=NULL);\n'%(k[0])
        if k[1]=="XlFatJet": # keep track of number of tracks, etc
            outString += '\t%s_trackCounter = 0;\n'%(k[3])
            outString += '\t%s_SVCounter = 0;\n'%(k[3])
            outString += '\t%s_softElectronCounter = 0;\n'%(k[3])
            outString += '\t%s_softMuonCounter = 0;\n'%(k[3])
        outString += '\t_%sCounter = 0;\n'%(k[3]) # in case some custom code needs the index
        outString += '\tfor (UInt_t i=0; i<std::min(f%s->GetEntries(),(UInt_t)nMax%s); ++i) {\n'%(k[0],k[0])
        outString += '\t\tconst %s * _%s = f%s->At(i);\n'%(k[1],k[0],k[0])
        for vv in v:
            outString += vv.fill(k[0],"_%sCounter"%(k[3]))
        if k[1]=="XlFatJet": # generate handles for tracks, secondary vertices, maybe do loops?
            outString += '\t\tconst std::vector<TrackData> & tracks = _%s->GetTrackData();\n'%(k[0])
            outString += writeFatJetInfoLoop("track",v,k[3],"_%sCounter"%(k[3]))
            outString += '\t\tconst std::vector<SVData> & SVs = _%s->GetSVData();\n'%(k[0])
            outString += writeFatJetInfoLoop("SV",v,k[3],"_%sCounter"%(k[3]))
            outString += '\t\tconst std::vector<LeptonData> & softElectrons = _%s->GetElectronData();\n'%(k[0])
            outString += writeFatJetInfoLoop("softElectron",v,k[3],"_%sCounter"%(k[3]))
            outString += '\t\tconst std::vector<LeptonData> & softMuons = _%s->GetMuonData();\n'%(k[0])
            outString += writeFatJetInfoLoop("softMuon",v,k[3],"_%sCounter"%(k[3]))
        outString += '\t\t++_%sCounter;\n'%(k[3])
        outString += '\t}\n'
    return outString

def writeSlaveBegin(varDict):
    outString = ''
    for k,v in varDict.iteritems():
        outString += '\t\tfTree->Branch("_%sCounter",&_%sCounter,"_%sCounter/I");\n'%(k[3],k[3],k[3])
        if k[1]=='XlFatJet':
            for info in ['track','SV','softElectron','softMuon']:
                outString += '\t\tfTree->Branch("%s_n%ss",f%s_n%ss,"%s_n%ss[_%sCounter]/I");\n'%(k[3],makeCaps(info),k[3],makeCaps(info),k[3],makeCaps(info),k[3])
                outString += '\t\tfTree->Branch("%s_first%s",f%s_first%s,"%s_first%s[_%sCounter]/I");\n'%(k[3],makeCaps(info),k[3],makeCaps(info),k[3],makeCaps(info),k[3])
                outString += '\t\tfTree->Branch("%s_%sCounter",&%s_%sCounter,"%s_%sCounter/I");\n'%(k[3],info,k[3],info,k[3],info)
        for vv in v:
            outString += vv.makeBranch(k[3])
    return outString

# load all of the relevant files
cfgFile = open('../config/generator/'+outFileName+'.cfg','r')
ccFile = open('../src/'+outFileName+'.cc','w')
hFile = open('../interface/'+outFileName+'.h','w')
pyFile = open('../python/'+outFileName+'.py','w')
dictFile = open('../dict/'+outFileName+'LinkDef.h','w')
# ccFile = sys.stdout
# hFile = sys.stdout
# pyFile = sys.stdout
# dictFile = sys.stdout
if templName=="templ":
  ccTemplateFile = open('templ/templ.cc','r')
  hTemplateFile = open('templ/templ.h','r')
  dictTemplateFile = open('templ/templLinkDef.h','r')
  pyTemplateFile = open('templ/templ.py','r')
  TEMPLATENAME="TEMPLATENAME"
else:
  ccTemplateFile = open('../src/'+templName+'.cc','r')
  hTemplateFile = open('../interface/'+templName+'.h','r')
  dictTemplateFile = open('../dict/'+templName+'LinkDef.h','r')
  pyTemplateFile = open('../python/'+templName+'.py','r')
  TEMPLATENAME=templName

varDict = {}

inputState = False
currentInput = ""
varState = False
for l in cfgFile:
    if inputState:
        ll = l.strip().split()
        if ll[0] in varDict:
            print "Warning: tried to add %s multiple times"%(ll)
        else:
            currentInput = (ll[0],ll[1],int(ll[2]),ll[3])
            varDict[currentInput] = []
        inputState = False
        continue
    if l.find("INPUT") >= 0:
        inputState = True
        varState = False
        continue
    if varState:
        varProps = l.strip().split()
        varDict[currentInput].append(fillerVar(currentInput[3],varProps[0]
                                    ,varProps[1],(varProps[2]=="True")))
    if l.find("VAR") >= 0:
        inputState = False
        varState = True
        continue

for tmplLine in hTemplateFile:
    if tmplLine.find("STOP") >= 0:
        nStop = int(tmplLine.strip().split()[-1])
        if nStop==1:
            hFile.write(writePublic(varDict))
    hFile.write(re.sub(TEMPLATENAME,outFileName,tmplLine)) # keep STOP in output so output can be used as templ/template if need be

for tmplLine in ccTemplateFile:
    if tmplLine.find("STOP") >= 0:
        nStop = int(tmplLine.strip().split()[-1])
        if nStop==1:
            ccFile.write(writeConstructor(varDict))
        elif nStop==2:
            ccFile.write(writeDestructor(varDict))
        elif nStop==3:
            ccFile.write(writeProcess(varDict))
        elif nStop==4:
            ccFile.write(writeSlaveBegin(varDict))
    ccFile.write(re.sub(TEMPLATENAME,outFileName,tmplLine))

for tmplLine in pyTemplateFile:
    if tmplLine.find("STOP") >= 0:
        pass
    pyFile.write(re.sub(TEMPLATENAME,outFileName,tmplLine)) # keep STOP in output so output can be used as templ/template if need be

for tmplLine in dictTemplateFile:
    if tmplLine.find("STOP") >= 0:
        nStop = int(tmplLine.strip().split()[-1])
        if nStop==1:
            dictFile.write('#include "MitPanda/FlatNtupler/interface/%s.h"\n'%(outFileName))
        elif nStop==2:
            dictFile.write('#pragma link C++ class mithep::%s+;\n'%(outFileName))
    dictFile.write(re.sub(TEMPLATENAME,outFileName,tmplLine)) # keep STOP in output so output can be used as templ/template if need be
