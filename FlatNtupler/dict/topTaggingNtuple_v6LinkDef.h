#ifndef MITPANDA_FLAT_LINKDEF_H
#define MITPANDA_FLAT_LINKDEF_H

#include "MitPanda/FlatNtupler/interface/topTaggingNtuple_v6.h"
// STOP 1

#endif

#ifdef __CLING__

#pragma link C++ class mithep::topTaggingNtuple_v6+;
// STOP 2

#endif
