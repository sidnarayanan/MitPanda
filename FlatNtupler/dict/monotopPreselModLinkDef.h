#ifndef MITPANDA_FLAT_LINKDEF_H
#define MITPANDA_FLAT_LINKDEF_H

#include "MitPanda/FlatNtupler/interface/monotopPreselMod.h"
// STOP 1

#endif

#ifdef __CLING__

#pragma link C++ class mithep::monotopPreselMod+;
// STOP 2

#endif
