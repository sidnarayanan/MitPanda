#ifndef MITPANDA_FLAT_LINKDEF_H
#define MITPANDA_FLAT_LINKDEF_H

#include "MitPanda/FlatNtupler/interface/tnpPreselMod.h"
// STOP 1

#endif

#ifdef __CLING__

#pragma link C++ class mithep::tnpPreselMod+;
// STOP 2

#endif
