#ifndef MITPANDA_FLAT_LINKDEF_H
#define MITPANDA_FLAT_LINKDEF_H

#include "MitPanda/FlatNtupler/interface/dijetPreselMod.h"
// STOP 1

#endif

#ifdef __CLING__

#pragma link C++ class mithep::dijetPreselMod+;
// STOP 2

#endif
