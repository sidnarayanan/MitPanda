from MitAna.TreeMod.bambu import mithep, analysis
import os

mitdata=os.environ['MIT_DATA']

from MitPhysics.Mods.GoodPVFilterMod import goodPVFilterMod
from MitPhysics.Mods.PuppiMod import puppiMod

goodPVFilterModClone=goodPVFilterMod.clone()

### PUPPI

puppiModClone=puppiMod.clone()

generatorMod=mithep.GeneratorMod(
        IsData=False,
        CopyArrays=False,
        MCMETName="GenMet"
)


head='HEAD'
tag='BAMBU_042'

sequence=goodPVFilterModClone
#sequence *= generatorMod
sequence *= puppiModClone

analysis.SetAllowNoHLTTree(True)

analysis.setSequence(sequence)
