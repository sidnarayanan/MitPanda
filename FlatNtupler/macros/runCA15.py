from MitAna.TreeMod.bambu import mithep, analysis
import os

mitdata=os.environ['MIT_DATA']

from MitPhysics.Mods.GoodPVFilterMod import goodPVFilterMod
from MitPhysics.Mods.PuppiMod import puppiMod
from MitPhysics.Mods.PuppiFatJetMod import puppiFatJetMod
# from MitPhysics.Mods.FatJetExtenderMod import fatJetExtenderMod
from MitPanda.Fillers.XXLFatJetFiller import xxlFatJetFiller as fatJetExtenderMod
from MitPanda.FlatNtupler.topTaggingNtuple_v6 import topTaggingNtuple_v6

goodPVFilterModClone=goodPVFilterMod.clone()
ntupleMaker=topTaggingNtuple_v6.clone()

### PUPPI

puppiModClone=puppiMod.clone()

puppiCA15Jets=puppiFatJetMod.clone('puppiCA15Jets',
                                     InputName       =puppiModClone.GetOutputName(),
                                     OutputName      ='CA15FatJetsPuppi',
                                     R0              =1.5,
                                     JetAlgorithm    =mithep.PuppiFatJetMod.kCambridgeAachen,
                                     DoMatching      =True,
                                     MatchingJetsName='CA15FatJetsCHS')

fatjecSources=["/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L1FastJet_AK8PFPuppi.txt",
                   "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L2Relative_AK8PFPuppi.txt",
                   "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L3Absolute_AK8PFPuppi.txt"]

generatorMod=mithep.GeneratorMod(
        IsData=False,
        CopyArrays=False,
        MCMETName="GenMet"
)


  #####################################################
 ##                  C A 1.5                        ##
#####################################################

CA15CorrectionMod=mithep.JetCorrectionMod('CA15Correction',
    InputName=puppiCA15Jets.GetOutputName(),
    CorrectedJetsName='CorrectedCA15Jets',
    RhoAlgo=mithep.PileupEnergyDensity.kFixedGridFastjetAll
)
for jec in fatjecSources:
  CA15CorrectionMod.AddCorrectionFromFile(mitdata + jec)

CA15IdMod=mithep.JetIdMod('CA15JetId',
        MinChargedHadronFraction = 0,
        MaxNeutralHadronFraction = 0.99,
        MaxNeutralEMFraction = 0.99,
        MinNPFCandidates = 2,
        MinNChargedPFCandidates = 1,
        MaxChargedEMFraction = 0.99,
    InputName=CA15CorrectionMod.GetOutputName(),
    OutputName='GoodCA15Jets',
    MVATrainingSet=mithep.JetIDMVA.nMVATypes
)
CA15ExtenderMod=fatJetExtenderMod.clone('CA15Extender',
    ConeSize=1.5,
    InputName=CA15IdMod.GetOutputName(),
    OutputName="XlCA15Jets",
    QGTaggerCHS=True,
    PFCandsName=puppiModClone.GetOutputName(),
    VertexesName=goodPVFilterMod.GetOutputName(),
    SoftDropR0=1.5,
    DoShowerDeconstruction=True,
    NMaxMicrojets=9,
    BeVerbose=False,
    DoECF=False,
    DoQjets=True,
    NQjets=10,
    SoftDropZCut=0.2,
    UseSoftDropLib=False,
    DoIota=True,
    NIota=10,
    JetAlgo=mithep.XXLFatJetFiller.kCambridgeAachen,
    DoCMSandHTT=False,
    ReApplyJEC=False
)
CA15ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kSoftDrop)
# CA15ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kTrimmed)
# CA15ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kPruned)

head='HEAD'
tag='BAMBU_042'

sequence=goodPVFilterModClone
sequence *= generatorMod
sequence *= puppiModClone
sequence *= puppiCA15Jets
sequence *= CA15CorrectionMod
sequence *= CA15IdMod
sequence *= CA15ExtenderMod
sequence *= ntupleMaker

analysis.SetAllowNoHLTTree(True)

analysis.setSequence(sequence)
