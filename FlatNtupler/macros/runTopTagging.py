from MitAna.TreeMod.bambu import mithep, analysis
import os

mitdata=os.environ['MIT_DATA']

from MitPhysics.Mods.GoodPVFilterMod import goodPVFilterMod
from MitPhysics.Mods.PuppiMod import puppiMod
from MitPhysics.Mods.PuppiFatJetMod import puppiFatJetMod
# from MitPhysics.Mods.FatJetExtenderMod import fatJetExtenderMod
from MitPanda.Fillers.XXLFatJetFiller import xxlFatJetFiller as fatJetExtenderMod
from MitPanda.FlatNtupler.topTaggingNtuple_v5 import topTaggingNtuple_v5

goodPVFilterModClone=goodPVFilterMod.clone()
ntupleMaker=topTaggingNtuple_v5.clone()

### PUPPI

puppiModClone=puppiMod.clone()
puppiAK8Jets=puppiFatJetMod.clone('puppiAK8Jets',
                                    InputName       =puppiModClone.GetOutputName(),
                                    OutputName      ='AK8FatJetsPuppi',
                                    R0              =0.8,
                                    JetAlgorithm    =mithep.PuppiFatJetMod.kAntiKT,
                                    DoMatching      =True,
                                    MatchingJetsName='AKt8FatJetsCHS')

puppiCA15Jets=puppiFatJetMod.clone('puppiCA15Jets',
                                     InputName       =puppiModClone.GetOutputName(),
                                     OutputName      ='CA15FatJetsPuppi',
                                     R0              =1.5,
                                     JetAlgorithm    =mithep.PuppiFatJetMod.kCambridgeAachen,
                                     DoMatching      =True,
                                     MatchingJetsName='CA15FatJetsCHS')

fatjecSources=["/JEC/Summer15_25nsV5/Summer15_25nsV5_DATA_L1FastJet_AK8PFPuppi.txt",
                   "/JEC/Summer15_25nsV5/Summer15_25nsV5_DATA_L2Relative_AK8PFPuppi.txt",
                   "/JEC/Summer15_25nsV5/Summer15_25nsV5_DATA_L3Absolute_AK8PFPuppi.txt"]
  #####################################################
 ##                  A K 0.8                        ##
#####################################################

generatorMod=mithep.GeneratorMod(
        IsData=False,
        CopyArrays=False,
        MCMETName="GenMet"
)

AK8CorrectionMod=mithep.JetCorrectionMod('AK8Correction',
    InputName=puppiAK8Jets.GetOutputName(),
    CorrectedJetsName='CorrectedAK8Jets',
    RhoAlgo=mithep.PileupEnergyDensity.kFixedGridFastjetAll
)
for jec in fatjecSources:
  AK8CorrectionMod.AddCorrectionFromFile(mitdata + jec)

AK8IdMod=mithep.JetIdMod('AK8JetId',
        MinChargedHadronFraction = 0,
        MaxNeutralHadronFraction = 0.99,
        MaxNeutralEMFraction = 0.99,
        MinNPFCandidates = 2,
        MinNChargedPFCandidates = 1,
        MaxChargedEMFraction = 0.99,
    InputName=AK8CorrectionMod.GetOutputName(),
    OutputName='GoodAK8Jets',
    MVATrainingSet=mithep.JetIDMVA.nMVATypes
)
AK8ExtenderMod=fatJetExtenderMod.clone('AK8Extender',
    ConeSize=0.8,
    # InputName='AKt8FatJetsCHS',
    InputName=AK8IdMod.GetOutputName(),
    OutputName="XlAK8Jets",
    QGTaggerCHS=True,
    PFCandsName=puppiModClone.GetOutputName(),
    VertexesName=goodPVFilterMod.GetOutputName(),
    SoftDropR0=0.8,
    DoShowerDeconstruction=True,
    NMaxMicrojets=7,
    BeVerbose=False,
    DoECF=False,
    DoQjets=True,
    NQjets=10,
    SoftDropZCut=0.1,
    UseSoftDropLib=False,
    DoIota=True,
    JetAlgo=mithep.XXLFatJetFiller.kAntiKt,
    DoCMSandHTT=True,
    ReApplyJEC=True,
)
AK8ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kSoftDrop)
# AK8ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kTrimmed)
# AK8ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kPruned)

  #####################################################
 ##                  C A 1.5                        ##
#####################################################

CA15CorrectionMod=mithep.JetCorrectionMod('CA15Correction',
    InputName=puppiCA15Jets.GetOutputName(),
    CorrectedJetsName='CorrectedCA15Jets',
    RhoAlgo=mithep.PileupEnergyDensity.kFixedGridFastjetAll
)
for jec in fatjecSources:
  CA15CorrectionMod.AddCorrectionFromFile(mitdata + jec)

CA15IdMod=mithep.JetIdMod('CA15JetId',
        MinChargedHadronFraction = 0,
        MaxNeutralHadronFraction = 0.99,
        MaxNeutralEMFraction = 0.99,
        MinNPFCandidates = 2,
        MinNChargedPFCandidates = 1,
        MaxChargedEMFraction = 0.99,
    InputName=CA15CorrectionMod.GetOutputName(),
    OutputName='GoodCA15Jets',
    MVATrainingSet=mithep.JetIDMVA.nMVATypes
)
CA15ExtenderMod=fatJetExtenderMod.clone('CA15Extender',
    ConeSize=1.5,
    InputName=CA15IdMod.GetOutputName(),
    OutputName="XlCA15Jets",
    QGTaggerCHS=True,
    PFCandsName=puppiModClone.GetOutputName(),
    VertexesName=goodPVFilterMod.GetOutputName(),
    SoftDropR0=1.5,
    DoShowerDeconstruction=True,
    NMaxMicrojets=6,
    BeVerbose=False,
    DoECF=False,
    DoQjets=True,
    NQjets=10,
    SoftDropZCut=0.2,
    UseSoftDropLib=False,
    DoIota=True,
    JetAlgo=mithep.XXLFatJetFiller.kCambridgeAachen,
    DoCMSandHTT=True,
    ReApplyJEC=True
)
CA15ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kSoftDrop)
# CA15ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kTrimmed)
# CA15ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kPruned)

head='HEAD'
tag='BAMBU_042'

sequence=goodPVFilterModClone
sequence *= generatorMod
sequence *= puppiModClone
sequence *= puppiAK8Jets
sequence *= puppiCA15Jets
sequence *= AK8CorrectionMod
sequence *= AK8IdMod
sequence *= AK8ExtenderMod
sequence *= CA15CorrectionMod
sequence *= CA15IdMod
sequence *= CA15ExtenderMod
sequence *= ntupleMaker

analysis.SetAllowNoHLTTree(True)

analysis.setSequence(sequence)
