from MitAna.TreeMod.bambu import mithep, analysis
import os

mitdata=os.environ['MIT_DATA']

from MitPhysics.Mods.GoodPVFilterMod import goodPVFilterMod
from MitPhysics.Mods.SeparatePileUpMod import separatePileUpMod

from MitPhysics.Mods.PuppiMod import puppiMod
from MitPhysics.Mods.PuppiPFJetMod import puppiPFJetMod
from MitPhysics.Mods.PuppiFatJetMod import puppiFatJetMod

from MitPhysics.Mods.PFTauIdMod import pfTauIdMod
from MitPhysics.Mods.ElectronIdMod import electronIdMod
from MitPhysics.Mods.MuonIdMod import muonIdMod
from MitPhysics.Mods.PhotonIdMod import photonIdMod
#from MitPhysics.Mods.FatJetExtenderMod import fatJetExtenderMod
from MitPanda.Fillers.XXLFatJetFiller import xxlFatJetFiller as fatJetExtenderMod
from MitPanda.Fillers.FillPuppiMET import fillPuppiMET
from MitPanda.FlatNtupler.monotopPreselMod import monotopPreselMod
from MitPanda.FlatNtupler.monotopNtuple_v3 import monotopNtuple_v3

### event-wide mods

presel = monotopPreselMod.clone()

ntupleMaker=monotopNtuple_v3.clone()
ntupleMaker.SetIsData(analysis.isRealData)
triggers=[ 'HLT_Ele27_eta2p1_WPLoose_Gsf_LooseIsoPFTau20_v*' if analysis.isRealData else 'HLT_Ele27_eta2p1_WP75_Gsf_LooseIsoPFTau20_v*',
              'HLT_IsoMu27_v*',
              'HLT_PFMETNoMu90_JetIdCleaned_PFMHTNoMu90_IDTight_v*' if analysis.isRealData else 'HLT_PFMETNoMu90_NoiseCleaned_PFMHTNoMu90_IDTight_v*',
              'HLT_MonoCentralPFJet80_PFMETNoMu90_JetIdCleaned_PFMHTNoMu90_IDTight_v*' if analysis.isRealData else 'HLT_MonoCentralPFJet80_PFMETNoMu90_NoiseCleaned_PFMHTNoMu90_IDTight_v*',
              'HLT_DiPFJetAve200_v*'
              'HLT_PFMETNoMu120_JetIdCleaned_PFMHTNoMu120_IDTight_v*' if analysis.isRealData else 'HLT_PFMETNoMu120_NoiseCleaned_PFMHTNoMu120_IDTight_v*',
              ]

for trigger in triggers:
  ntupleMaker.AddTrigger(trigger) # this is to store trigger info
if analysis.isRealData:
  hltMod=mithep.HLTMod(ExportTrigObjects=False)
  for trigger in triggers:
    hltMod.AddTrigger(trigger)      # this is to kill events failing all triggers
goodPVFilterModClone=goodPVFilterMod.clone()
separatePileUpModClone=separatePileUpMod.clone()

generatorMod=mithep.GeneratorMod(
        IsData=analysis.isRealData,
        CopyArrays=False,
        MCMETName="GenMet"
)


### PUPPI

puppiModClone=puppiMod.clone()
puppiAK4Jets=puppiPFJetMod.clone('puppiAK4Jets',
                                   InputName       =puppiModClone.GetOutputName(),
                                   OutputName      ='AK4PFJetsPuppi',
                                   R0              =0.4,
                                   JetAlgorithm     =mithep.PuppiPFJetMod.kAntiKT,
                                   DoMatching      =True,
                                   MatchingJetsName='AKt4PFJetsCHS')
puppiAK8Jets=puppiFatJetMod.clone('puppiAK8Jets',
                                    InputName       =puppiModClone.GetOutputName(),
                                    OutputName      ='AK8FatJetsPuppi',
                                    R0              =0.8,
                                    JetAlgorithm    =mithep.PuppiFatJetMod.kAntiKT,
                                    DoMatching      =True,
                                    MatchingJetsName='AKt8FatJetsCHS')

puppiCA15Jets=puppiFatJetMod.clone('puppiCA15Jets',
                                     InputName       =puppiModClone.GetOutputName(),
                                     OutputName      ='CA15FatJetsPuppi',
                                     R0              =1.5,
                                     JetAlgorithm    =mithep.PuppiFatJetMod.kCambridgeAachen,
                                     DoMatching      =True,
                                     MatchingJetsName='CA15FatJetsCHS')

puppiMET=fillPuppiMET.clone('puppiMET',
                              InputName    =puppiModClone.GetOutputName(),
                              OutputName   ='PuppiMET',
                              ApplyEtaCut  =False)

### MET and JET correction and ID

if analysis.isRealData:
  jecSources=["/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L1FastJet_AK4PFPuppi.txt",
                "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L2Relative_AK4PFPuppi.txt",
                "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L3Absolute_AK4PFPuppi.txt",
                "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L2L3Residual_AK4PFPuppi.txt"]
  pfJECSources=["/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L1FastJet_AK4PFchs.txt",
                  "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L2Relative_AK4PFchs.txt",
                  "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L3Absolute_AK4PFchs.txt",
                  "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L2L3Residual_AK4PFchs.txt"]
  fatjecSources=["/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L1FastJet_AK8PFPuppi.txt",
                   "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L2Relative_AK8PFPuppi.txt",
                   "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L3Absolute_AK8PFPuppi.txt",
                   "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L2L3Residual_AK8PFPuppi.txt"]
else:
  jecSources=["/JEC/Summer15_25nsV6/Summer15_25nsV6_MC_L1FastJet_AK4PFPuppi.txt",
                "/JEC/Summer15_25nsV6/Summer15_25nsV6_MC_L2Relative_AK4PFPuppi.txt",
                "/JEC/Summer15_25nsV6/Summer15_25nsV6_MC_L3Absolute_AK4PFPuppi.txt"]
  pfJECSources=["/JEC/Summer15_25nsV6/Summer15_25nsV6_MC_L1FastJet_AK4PFchs.txt",
                  "/JEC/Summer15_25nsV6/Summer15_25nsV6_MC_L2Relative_AK4PFchs.txt",
                  "/JEC/Summer15_25nsV6/Summer15_25nsV6_MC_L3Absolute_AK4PFchs.txt"]
  fatjecSources=["/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L1FastJet_AK8PFPuppi.txt",
                   "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L2Relative_AK8PFPuppi.txt",
                   "/JEC/Summer15_25nsV6/Summer15_25nsV6_DATA_L3Absolute_AK8PFPuppi.txt"]

jetCorrection=mithep.JetCorrectionMod(
#                                         InputName        ='AKt4PFJetsCHS',
                                         InputName        =puppiAK4Jets.GetOutputName(),
                                         CorrectedJetsName='CorrectedJets',
                                         RhoAlgo          =mithep.PileupEnergyDensity.kFixedGridFastjetAll)

metCorrection=mithep.MetCorrectionMod('METCorrection',
                                          InputName    ='PFMet',
                                          OutputName   ='PFType1CorrectedMET',
                                          JetsName     ='AKt4PFJets',
                                          RhoAlgo      =mithep.PileupEnergyDensity.kFixedGridFastjetAll,
                                          MaxEMFraction=0.9,
                                          SkipMuons    =True)
metCorrection.ApplyType0(False)
metCorrection.ApplyType1(True)
metCorrection.ApplyShift(False)
metCorrection.IsData(analysis.isRealData)

puppiMETCorrection=metCorrection.clone('puppiMETCorrection',
                                          InputName      =puppiMET.GetOutputName(),
                                          OutputName     ='PuppiType1MET',
                                          JetsName       ='AK4PFJetsPuppi')
puppiMETCorrection.ApplyType0(False)
puppiMETCorrection.ApplyType1(True)
puppiMETCorrection.ApplyShift(False)
puppiMETCorrection.IsData(analysis.isRealData)

for jec in pfJECSources:
  metCorrection.AddJetCorrectionFromFile(mitdata+jec)
for jec in jecSources:
  puppiMETCorrection.AddJetCorrectionFromFile(mitdata+jec)
  jetCorrection.AddCorrectionFromFile(mitdata+jec)

jetId=mithep.JetIdMod(
        MinChargedHadronFraction = 0,
        MaxNeutralHadronFraction = 0.99,
        MaxNeutralEMFraction = 0.99,
        MinNPFCandidates = 2,
        MinNChargedPFCandidates = 1,
        MaxChargedEMFraction = 0.99,
        MVATrainingSet =mithep.JetIDMVA.nMVATypes,
        PtMin          =15.,
        InputName      ='CorrectedJets',
        OutputName     ='SelectedJets')

### FATJETS
CA15CorrectionMod=mithep.JetCorrectionMod('CA15Correction',
    InputName=puppiCA15Jets.GetOutputName(),
    CorrectedJetsName='CorrectedCA15Jets',
    RhoAlgo=mithep.PileupEnergyDensity.kFixedGridFastjetAll
)
for jec in fatjecSources:
  CA15CorrectionMod.AddCorrectionFromFile(mitdata+jec)

CA15IdMod=mithep.JetIdMod('CA15JetId',
        MinChargedHadronFraction = 0,
        MaxNeutralHadronFraction = 0.99,
        MaxNeutralEMFraction = 0.99,
        MinNPFCandidates = 2,
        MinNChargedPFCandidates = 1,
        MaxChargedEMFraction = 0.99,
    InputName=CA15CorrectionMod.GetOutputName(),
    OutputName='GoodCA15Jets',
    MVATrainingSet=mithep.JetIDMVA.nMVATypes
)
CA15ExtenderMod=fatJetExtenderMod.clone('CA15Extender',
    ConeSize=1.5,
    InputName=CA15IdMod.GetOutputName(),
    OutputName="XlCA15Jets",
    QGTaggerCHS=True,
    PFCandsName=puppiModClone.GetOutputName(),
    VertexesName=goodPVFilterMod.GetOutputName(),
    SoftDropR0=1.5,
    DoShowerDeconstruction=False,
    NMaxMicrojets=7,
    BeVerbose=False,
    DoECF=False,
    DoQjets=False,
    NQjets=10,
    SoftDropZCut=0.2,
    UseSoftDropLib=False,
    DoIota=True,
    DoCMSandHTT=False,
    ReApplyJEC=True
)
CA15ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kSoftDrop)

AK8CorrectionMod=mithep.JetCorrectionMod('AK8Correction',
    InputName=puppiAK8Jets.GetOutputName(),
    CorrectedJetsName='CorrectedAK8Jets',
    RhoAlgo=mithep.PileupEnergyDensity.kFixedGridFastjetAll
)
for jec in fatjecSources:
  AK8CorrectionMod.AddCorrectionFromFile(mitdata+jec)

AK8IdMod=mithep.JetIdMod('AK8JetId',
        MinChargedHadronFraction = 0,
        MaxNeutralHadronFraction = 0.99,
        MaxNeutralEMFraction = 0.99,
        MinNPFCandidates = 2,
        MinNChargedPFCandidates = 1,
        MaxChargedEMFraction = 0.99,
    InputName=AK8CorrectionMod.GetOutputName(),
    OutputName='GoodAK8Jets',
    MVATrainingSet=mithep.JetIDMVA.nMVATypes
)
AK8ExtenderMod=fatJetExtenderMod.clone('AK8Extender',
    ConeSize=.8,
    InputName=AK8IdMod.GetOutputName(),
    OutputName="XlAK8Jets",
    QGTaggerCHS=True,
    PFCandsName=puppiModClone.GetOutputName(),
    VertexesName=goodPVFilterMod.GetOutputName(),
    SoftDropR0=.8,
    DoShowerDeconstruction=False,
    NMaxMicrojets=7,
    BeVerbose=False,
    DoECF=False,
    DoQjets=False,
    NQjets=10,
    SoftDropZCut=0.1,
    UseSoftDropLib=False,
    DoIota=True,
    DoCMSandHTT=False,
    ReApplyJEC=True

)
AK8ExtenderMod.SetSubJetTypeOn(mithep.XlSubJet.kSoftDrop)

### LEPTONS
pfTauId=pfTauIdMod.clone('PFTauId', OutputName="selectedTaus", PtMin = 18, EtaMax = 2.3)
pfTauId.AddCutDiscriminator(mithep.PFTau.kDiscriminationByDecayModeFinding, 5., False)

electronFiducialId=electronIdMod.clone('ElectronFiducialId',
                                      OutputName='fiducialElectrons')

electronVetoId=electronIdMod.clone('ElectronVetoId',
                                      InputName=electronFiducialId.GetOutputName(),
                                      IdType= mithep.ElectronTools.kSummer15Veto,
                                      IsoType=mithep.ElectronTools.kSummer15VetoIso,
                                      OutputName='vetoElectrons')

electronLooseId=electronIdMod.clone('ElectronLooseId',
                                      InputName=electronFiducialId.GetOutputName(),
                                      IdType= mithep.ElectronTools.kSummer15Loose,
                                      IsoType=mithep.ElectronTools.kSummer15LooseIso,
                                      PtMin = 10,
                                      EtaMax = 2.5,
                                      OutputName='looseElectrons')

electronTightId=electronIdMod.clone('ElectronTightId',
                                      IdType= mithep.ElectronTools.kSummer15Tight,
                                      IsoType=mithep.ElectronTools.kSummer15TightIso,
                                      InputName=electronLooseId.GetOutputName(),
                                      PtMin = 40,
                                      EtaMax = 2.5,
                                      OutputName='tightElectrons')

muonLooseId=muonIdMod.clone('MuonLooseId', OutputName='looseMuons',
                                MuonClassType=mithep.MuonTools.kPFGlobalorTracker,
                                ApplyD0Cut=False,
                                ApplyDZCut=False,
                                PtMin = 10,
                                EtaMax = 2.4,
                                IsoType=mithep.MuonTools.kPFIsoBetaPUCorrectedLoose,
                                IdType = mithep.MuonTools.kLoose)

muonTightId=muonIdMod.clone('MuonTightId', OutputName='tightMuons',
                              IdType=mithep.MuonTools.kTight,
                              IsoType=mithep.MuonTools.kPFIsoBetaPUCorrectedTight,
                              InputName=muonLooseId.GetOutputName(),
                              MuonClassType=mithep.MuonTools.kPFGlobal,
                              ApplyD0Cut=True,
                              ApplyDZCut=True)

### PHOTONS
photonLooseId=photonIdMod.clone('PhotonLooseId', OutputName='loosePhotons',
                                  IdType   =mithep.PhotonTools.kSpring15Loose,
                                  IsoType  =mithep.PhotonTools.kSpring15LooseIso,
                                  PtMin = 15,
                                  EtaMax = 2.5,
                                  ApplyCSafeElectronVeto=True)
photonTightId=photonIdMod.clone('PhotonTightId', OutputName='tightPhotons', 
                                  IdType=mithep.PhotonTools.kSpring15Medium,
                                  IsoType=mithep.PhotonTools.kSpring15MediumIso,
                                  InputName=photonLooseId.GetOutputName(),
                                  PtMin = 175,
                                  EtaMax = 1.4442,
                                  ApplyCSafeElectronVeto=True)


### SETUP AND RUN
head='HEAD'
tag='BAMBU_042'

if analysis.isRealData:
  sequence =hltMod
  sequence *= goodPVFilterModClone
else:
  sequence=goodPVFilterModClone

sequence *= generatorMod
sequence *= separatePileUpModClone
sequence *= puppiModClone
sequence *= puppiAK4Jets
sequence *= puppiMET
sequence *= metCorrection
sequence *= puppiMETCorrection
sequence *= electronFiducialId
sequence *= electronLooseId
sequence *= electronVetoId
sequence *= electronTightId
sequence *= muonLooseId
sequence *= muonTightId
sequence *= photonLooseId
sequence *= photonTightId
sequence *= presel
sequence *= jetCorrection
sequence *= jetId
sequence *= pfTauId
sequence *= puppiAK8Jets
sequence *= puppiCA15Jets
sequence *= AK8CorrectionMod
sequence *= AK8IdMod
sequence *= AK8ExtenderMod
sequence *= CA15CorrectionMod
sequence *= CA15IdMod
sequence *= CA15ExtenderMod
sequence *= ntupleMaker

analysis.setSequence(sequence)
