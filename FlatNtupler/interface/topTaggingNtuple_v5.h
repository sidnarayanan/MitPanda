#ifndef MITPANDA_FLAT_topTaggingNtuple_v5_H
#define MITPANDA_FLAT_topTaggingNtuple_v5_H

//#include "MitAna/DataTree/interface/XXLFatJetFwd.h"
//#include "MitAna/DataTree/interface/XXLFatJet.h"
#include "MitAna/DataCont/interface/RefArray.h"
#include "MitAna/DataTree/interface/XlSubJet.h"
#include "MitAna/DataTree/interface/GenJetFwd.h"
#include "MitAna/DataTree/interface/GenJet.h"
#include "MitPanda/PhysicsObjects/interface/XXLFatJetFwd.h"
#include "MitPanda/PhysicsObjects/interface/XXLFatJet.h"
#include "MitAna/TreeMod/interface/BaseMod.h"
#include "TFile.h"
#include "MitAna/DataTree/interface/MCParticleFwd.h"
#include "MitAna/DataTree/interface/MCEventInfo.h"
#include "TTree.h"
#include "algorithm"

#define N_MAX_TRACKS 250
#define N_MAX_SVS 50
#define N_MAX_SOFTELECTRONS 20  
#define N_MAX_SOFTMUONS 20  
#define DOBTAG 0

namespace mithep
{
  class topTaggingNtuple_v5 : public BaseMod
  {
    public:
      typedef FatJet::TrackData TrackData;
      typedef FatJet::SVData SVData;
      typedef FatJet::LeptonData LeptonData;

      topTaggingNtuple_v5(const char*, const char*);
      ~topTaggingNtuple_v5();
		const XXLFatJetCol * fXlAK8Jets;
		unsigned int nMaxXlAK8Jets = 6;
		unsigned int _AK8fjCounter;
#if DOBTAG
		int fAK8fj_nTracks[100];
		int fAK8fj_firstTrack[100];
		int fAK8fj_nSVs[100];
		int fAK8fj_firstSV[100];
		int fAK8fj_nSoftElectrons[100];
		int fAK8fj_firstSoftElectron[100];
		int fAK8fj_nSoftMuons[100];
		int fAK8fj_firstSoftMuon[100];
		unsigned int AK8fj_trackCounter;
		unsigned int AK8fj_SVCounter;
		unsigned int AK8fj_softElectronCounter;
		unsigned int AK8fj_softMuonCounter;
#endif
		float fAK8fj_JEC[100];
		float fAK8fj_MassSDb0[100];
		float fAK8fj_MassSDb1[100];
		float fAK8fj_MassSDb2[100];
		float fAK8fj_MassSDbm1[100];
#if DOBTAG
		float fAK8fj_iota0[100];
		float fAK8fj_iota1[100];
		float fAK8fj_iota2[100];
		float fAK8fj_iota3[100];
		float fAK8fj_iota4[100];
		float fAK8fj_iota5[100];
		float fAK8fj_iotaAngle0[100];
		float fAK8fj_iotaAngle1[100];
		float fAK8fj_iotaAngle2[100];
		float fAK8fj_iotaAngle3[100];
		float fAK8fj_iotaAngle4[100];
		float fAK8fj_iotaAngle5[100];
#endif
		float fAK8fj_Tau1[100];
		float fAK8fj_Tau2[100];
		float fAK8fj_Tau3[100];
		float fAK8fj_Tau4[100];
		float fAK8fj_Tau1IVF[100];
		float fAK8fj_Tau2IVF[100];
		float fAK8fj_Tau3IVF[100];
		float fAK8fj_Charge[100];
		float fAK8fj_tauDot[100];
		float fAK8fj_zRatio[100];
		float fAK8fj_QJetVol[100];
		float fAK8fj_QGTag[100];
		float fAK8fj_MassSoftDrop[100];
		float fAK8fj_MassPruned[100];
		float fAK8fj_MassTrimmed[100];
		float fAK8fj_chi[100];
		float fAK8fj_nMicrojets[100];
		float fAK8fj_subJetBTag0[100];
		float fAK8fj_subJetBTag1[100];
		int fAK8fj_nSubJets[100];
    float fAK8fj_sjQGTag0[100];
    float fAK8fj_sjQGTag1[100];
    float fAK8fj_sjQGTag2[100];
    float fAK8fj_sjQGTag3[100];
		TClonesArray* fAK8fj_Momentum;
		TClonesArray* fAK8fj_SubJet0Momentum;
		TClonesArray* fAK8fj_SubJet1Momentum;
		TClonesArray* fAK8fj_SubJet2Momentum;
		TClonesArray* fAK8fj_SubJet3Momentum;
		TClonesArray* fAK8fj_SoftDropMomentum;
#if DOBTAG
		float fAK8fj_track_pt[N_MAX_TRACKS];
		float fAK8fj_track_eta[N_MAX_TRACKS];
		float fAK8fj_track_phi[N_MAX_TRACKS];
		float fAK8fj_track_length[N_MAX_TRACKS];
		float fAK8fj_track_dist[N_MAX_TRACKS];
		float fAK8fj_track_dxy[N_MAX_TRACKS];
		float fAK8fj_track_dz[N_MAX_TRACKS];
		float fAK8fj_track_IP2D[N_MAX_TRACKS];
		float fAK8fj_track_IP2Dsig[N_MAX_TRACKS];
		float fAK8fj_track_IP[N_MAX_TRACKS];
		float fAK8fj_track_IPsig[N_MAX_TRACKS];
		float fAK8fj_track_IP2Derr[N_MAX_TRACKS];
		float fAK8fj_track_IPerr[N_MAX_TRACKS];
		float fAK8fj_track_prob[N_MAX_TRACKS];
		float fAK8fj_track_PVWeight[N_MAX_TRACKS];
		float fAK8fj_track_SVWeight[N_MAX_TRACKS];
		int fAK8fj_track_PV[N_MAX_TRACKS];
		int fAK8fj_track_fromSV[N_MAX_TRACKS];
		int fAK8fj_track_SV[N_MAX_TRACKS];
		int fAK8fj_SV_nTrk[N_MAX_SVS];
		float fAK8fj_SV_flight[N_MAX_SVS];
		float fAK8fj_SV_flightErr[N_MAX_SVS];
		float fAK8fj_SV_deltaRJet[N_MAX_SVS];
		float fAK8fj_SV_deltaRSumJet[N_MAX_SVS];
		float fAK8fj_SV_deltaRSumDir[N_MAX_SVS];
		float fAK8fj_SV_flight2D[N_MAX_SVS];
		float fAK8fj_SV_flight2DErr[N_MAX_SVS];
		float fAK8fj_SV_totCharge[N_MAX_SVS];
		float fAK8fj_SV_vtxDistJetAxis[N_MAX_SVS];
		float fAK8fj_SV_mass[N_MAX_SVS];
		float fAK8fj_SV_energyRatio[N_MAX_SVS];
		float fAK8fj_SV_pt[N_MAX_SVS];
		float fAK8fj_SV_eta[N_MAX_SVS];
		float fAK8fj_SV_phi[N_MAX_SVS];
		float fAK8fj_SV_dirX[N_MAX_SVS];
		float fAK8fj_SV_dirY[N_MAX_SVS];
		float fAK8fj_SV_dirZ[N_MAX_SVS];
		float fAK8fj_softElectron_pt[N_MAX_SOFTELECTRONS];
		float fAK8fj_softElectron_eta[N_MAX_SOFTELECTRONS];
		float fAK8fj_softElectron_phi[N_MAX_SOFTELECTRONS];
		float fAK8fj_softElectron_ptRel[N_MAX_SOFTELECTRONS];
		float fAK8fj_softElectron_ratio[N_MAX_SOFTELECTRONS];
		float fAK8fj_softElectron_ratioRel[N_MAX_SOFTELECTRONS];
		float fAK8fj_softElectron_IP[N_MAX_SOFTELECTRONS];
		float fAK8fj_softElectron_IP2D[N_MAX_SOFTELECTRONS];
		float fAK8fj_softMuon_pt[N_MAX_SOFTMUONS];
		float fAK8fj_softMuon_eta[N_MAX_SOFTMUONS];
		float fAK8fj_softMuon_phi[N_MAX_SOFTMUONS];
		float fAK8fj_softMuon_ptRel[N_MAX_SOFTMUONS];
		float fAK8fj_softMuon_ratio[N_MAX_SOFTMUONS];
		float fAK8fj_softMuon_ratioRel[N_MAX_SOFTMUONS];
		float fAK8fj_softMuon_IP[N_MAX_SOFTMUONS];
		float fAK8fj_softMuon_IP2D[N_MAX_SOFTMUONS];
#endif
		float fAK8fj_GroomedTau1[100];
		float fAK8fj_GroomedTau2[100];
		float fAK8fj_GroomedTau3[100];
		float fAK8fj_MassCMSTT[100];
		float fAK8fj_MassHTT[100];
		float fAK8fj_FMin[100];
		float fAK8fj_ROpt[100];
		float fAK8fj_ROptCalc[100];
		float fAK8fj_PtROptCalc[100];
		const MCParticleCol * fMCQuarks;
		unsigned int nMaxMCQuarks = 1000;
		unsigned int _mcQuarkCounter;
		TClonesArray* fmcQuark_Momentum;
		int fmcQuark_PdgId[100];
		int fmcQuark_Status[100];
		int fmcQuark_StatusFlag[100];
		int fmcQuark_MotherPdgId[100];
    int fmcQuark_IsHadronicTop[100];
    float fmcQuark_TopSize[100];
		const XXLFatJetCol * fXlCA15Jets;
		unsigned int nMaxXlCA15Jets = 6;
		unsigned int _CA15fjCounter;
#if DOBTAG
		int fCA15fj_nTracks[100];
		int fCA15fj_firstTrack[100];
		int fCA15fj_nSVs[100];
		int fCA15fj_firstSV[100];
		int fCA15fj_nSoftElectrons[100];
		int fCA15fj_firstSoftElectron[100];
		int fCA15fj_nSoftMuons[100];
		int fCA15fj_firstSoftMuon[100];
		unsigned int CA15fj_trackCounter;
		unsigned int CA15fj_SVCounter;
		unsigned int CA15fj_softElectronCounter;
		unsigned int CA15fj_softMuonCounter;
#endif
		float fCA15fj_JEC[100];
		float fCA15fj_MassSDb0[100];
		float fCA15fj_MassSDb1[100];
		float fCA15fj_MassSDb2[100];
		float fCA15fj_MassSDbm1[100];
#if DOBTAG
		float fCA15fj_iota0[100];
		float fCA15fj_iota1[100];
		float fCA15fj_iota2[100];
		float fCA15fj_iota3[100];
		float fCA15fj_iota4[100];
		float fCA15fj_iota5[100];
		float fCA15fj_iotaAngle0[100];
		float fCA15fj_iotaAngle1[100];
		float fCA15fj_iotaAngle2[100];
		float fCA15fj_iotaAngle3[100];
		float fCA15fj_iotaAngle4[100];
		float fCA15fj_iotaAngle5[100];
#endif
    float fCA15fj_Tau1[100];
		float fCA15fj_Tau2[100];
		float fCA15fj_Tau3[100];
		float fCA15fj_Tau4[100];
		float fCA15fj_Tau1IVF[100];
		float fCA15fj_Tau2IVF[100];
		float fCA15fj_Tau3IVF[100];
		float fCA15fj_Charge[100];
		float fCA15fj_tauDot[100];
		float fCA15fj_zRatio[100];
		float fCA15fj_QJetVol[100];
		float fCA15fj_QGTag[100];
		float fCA15fj_MassSoftDrop[100];
		float fCA15fj_MassPruned[100];
		float fCA15fj_MassTrimmed[100];
		float fCA15fj_chi[100];
		float fCA15fj_nMicrojets[100];
		float fCA15fj_subJetBTag0[100];
		float fCA15fj_subJetBTag1[100];
		int fCA15fj_nSubJets[100];
    float fCA15fj_sjQGTag0[100];
    float fCA15fj_sjQGTag1[100];
    float fCA15fj_sjQGTag2[100];
    float fCA15fj_sjQGTag3[100];
		TClonesArray* fCA15fj_Momentum;
		TClonesArray* fCA15fj_SubJet0Momentum;
		TClonesArray* fCA15fj_SubJet1Momentum;
		TClonesArray* fCA15fj_SubJet2Momentum;
		TClonesArray* fCA15fj_SubJet3Momentum;
		TClonesArray* fCA15fj_SoftDropMomentum;
#if DOBTAG
		float fCA15fj_track_pt[N_MAX_TRACKS];
		float fCA15fj_track_eta[N_MAX_TRACKS];
		float fCA15fj_track_phi[N_MAX_TRACKS];
		float fCA15fj_track_length[N_MAX_TRACKS];
		float fCA15fj_track_dist[N_MAX_TRACKS];
		float fCA15fj_track_dxy[N_MAX_TRACKS];
		float fCA15fj_track_dz[N_MAX_TRACKS];
		float fCA15fj_track_IP2D[N_MAX_TRACKS];
		float fCA15fj_track_IP2Dsig[N_MAX_TRACKS];
		float fCA15fj_track_IP[N_MAX_TRACKS];
		float fCA15fj_track_IPsig[N_MAX_TRACKS];
		float fCA15fj_track_IP2Derr[N_MAX_TRACKS];
		float fCA15fj_track_IPerr[N_MAX_TRACKS];
		float fCA15fj_track_prob[N_MAX_TRACKS];
		float fCA15fj_track_PVWeight[N_MAX_TRACKS];
		float fCA15fj_track_SVWeight[N_MAX_TRACKS];
		int fCA15fj_track_PV[N_MAX_TRACKS];
		int fCA15fj_track_fromSV[N_MAX_TRACKS];
		int fCA15fj_track_SV[N_MAX_TRACKS];
		int fCA15fj_SV_nTrk[N_MAX_SVS];
		float fCA15fj_SV_flight[N_MAX_SVS];
		float fCA15fj_SV_flightErr[N_MAX_SVS];
		float fCA15fj_SV_deltaRJet[N_MAX_SVS];
		float fCA15fj_SV_deltaRSumJet[N_MAX_SVS];
		float fCA15fj_SV_deltaRSumDir[N_MAX_SVS];
		float fCA15fj_SV_flight2D[N_MAX_SVS];
		float fCA15fj_SV_flight2DErr[N_MAX_SVS];
		float fCA15fj_SV_totCharge[N_MAX_SVS];
		float fCA15fj_SV_vtxDistJetAxis[N_MAX_SVS];
		float fCA15fj_SV_mass[N_MAX_SVS];
		float fCA15fj_SV_energyRatio[N_MAX_SVS];
		float fCA15fj_SV_pt[N_MAX_SVS];
		float fCA15fj_SV_eta[N_MAX_SVS];
		float fCA15fj_SV_phi[N_MAX_SVS];
		float fCA15fj_SV_dirX[N_MAX_SVS];
		float fCA15fj_SV_dirY[N_MAX_SVS];
		float fCA15fj_SV_dirZ[N_MAX_SVS];
		float fCA15fj_softElectron_pt[N_MAX_SOFTELECTRONS];
		float fCA15fj_softElectron_eta[N_MAX_SOFTELECTRONS];
		float fCA15fj_softElectron_phi[N_MAX_SOFTELECTRONS];
		float fCA15fj_softElectron_ptRel[N_MAX_SOFTELECTRONS];
		float fCA15fj_softElectron_ratio[N_MAX_SOFTELECTRONS];
		float fCA15fj_softElectron_ratioRel[N_MAX_SOFTELECTRONS];
		float fCA15fj_softElectron_IP[N_MAX_SOFTELECTRONS];
		float fCA15fj_softElectron_IP2D[N_MAX_SOFTELECTRONS];
		float fCA15fj_softMuon_pt[N_MAX_SOFTMUONS];
		float fCA15fj_softMuon_eta[N_MAX_SOFTMUONS];
		float fCA15fj_softMuon_phi[N_MAX_SOFTMUONS];
		float fCA15fj_softMuon_ptRel[N_MAX_SOFTMUONS];
		float fCA15fj_softMuon_ratio[N_MAX_SOFTMUONS];
		float fCA15fj_softMuon_ratioRel[N_MAX_SOFTMUONS];
		float fCA15fj_softMuon_IP[N_MAX_SOFTMUONS];
		float fCA15fj_softMuon_IP2D[N_MAX_SOFTMUONS];
#endif
		float fCA15fj_GroomedTau1[100];
		float fCA15fj_GroomedTau2[100];
		float fCA15fj_GroomedTau3[100];
		float fCA15fj_MassCMSTT[100];
		float fCA15fj_MassHTT[100];
		float fCA15fj_FMin[100];
		float fCA15fj_ROpt[100];
		float fCA15fj_ROptCalc[100];
		float fCA15fj_PtROptCalc[100];
		const GenJetCol * fAKT4GenJets;
		unsigned int nMaxAKT4GenJets = 20;
		unsigned int _genJetCounter;
		TClonesArray* fgenJet_Momentum;
      // STOP 1
      void SetFilePath(TString p)  { fFilePath = p; }
    protected:
      void Process();
      void SlaveBegin();
      void SlaveTerminate();

      TString fFilePath = "buffer.root";
      TFile *fFile;
      TTree *fTree;

    private:

      ClassDef(topTaggingNtuple_v5, 0)         // topTaggingNtuple_v5 filler
  };
}
#endif
