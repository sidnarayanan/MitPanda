#ifndef MITPANDA_FLAT_tnpPreselMod_H
#define MITPANDA_FLAT_tnpPreselMod_H

#include "MitAna/TreeMod/interface/BaseMod.h"
#include "MitAna/DataTree/interface/PFJetFwd.h"
#include "MitAna/DataTree/interface/JetFwd.h"
#include "MitAna/DataTree/interface/PFJet.h"
#include "MitAna/DataTree/interface/Jet.h"
#include "MitAna/DataTree/interface/PFMetFwd.h"
#include "MitAna/DataTree/interface/PFMet.h"
#include "MitAna/DataTree/interface/MetFwd.h"
#include "MitAna/DataTree/interface/Met.h"
#include "MitAna/DataTree/interface/Electron.h"
#include "MitAna/DataTree/interface/Muon.h"
#include "MitAna/DataTree/interface/ElectronFwd.h"
#include "MitAna/DataTree/interface/MuonFwd.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "algorithm"

namespace mithep
{
  class tnpPreselMod : public BaseMod
  {
    public:
      tnpPreselMod(const char*, const char*);
      ~tnpPreselMod();
		const MuonCol * flooseMuons;
		unsigned int nMaxlooseMuons = 6;
		unsigned int _muonCounter;
		TClonesArray* fmuon_Momentum;
		const PFMetCol * fPuppiType1MET;
		unsigned int nMaxPuppiType1MET = 1;
		unsigned int _metCounter;
		TClonesArray* fmet_Momentum;
		const ElectronCol * flooseElectrons;
		unsigned int nMaxlooseElectrons = 6;
		unsigned int _electronCounter;
		TClonesArray* felectron_Momentum;
		const JetCol * fSelectedJets;
		unsigned int nMaxSelectedJets = 6;
		unsigned int _jetCounter;
		TClonesArray* fjet_Momentum;
		float fjet_btag[10];
      // STOP 1
      void SetFilePath(TString p)  { fFilePath = p; }

    protected:
      void Process();
      void SlaveBegin();
      void SlaveTerminate();

      TString fFilePath = "buffer.root";
      TFile *fFile;
      TTree *fTree;


    private:

      ClassDef(tnpPreselMod, 0)         // tnpPreselMod filler
  };
}
#endif
