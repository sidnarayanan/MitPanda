#ifndef MITPANDA_FLAT_monotopPreselMod_H
#define MITPANDA_FLAT_monotopPreselMod_H

#include "MitPanda/PhysicsObjects/interface/XXLFatJetFwd.h"
#include "MitPanda/PhysicsObjects/interface/XXLFatJet.h"
#include "MitAna/DataTree/interface/ElectronFwd.h"
#include "MitAna/DataTree/interface/Electron.h"
#include "MitAna/DataTree/interface/MuonFwd.h"
#include "MitAna/DataTree/interface/Muon.h"
#include "MitAna/DataTree/interface/PhotonFwd.h"
#include "MitAna/DataTree/interface/Photon.h"
#include "MitAna/DataTree/interface/PFMetFwd.h"
#include "MitAna/DataTree/interface/PFMet.h"
#include "MitAna/TreeMod/interface/BaseMod.h"
#include "TTree.h"
#include "TFile.h"
#include "algorithm"

namespace mithep
{
  class monotopPreselMod : public BaseMod
  {
    public:
      monotopPreselMod(const char*, const char*);
      ~monotopPreselMod();
		const PhotonCol * floosePhotons;
		unsigned int nMaxloosePhotons = 6;
		unsigned int _photonCounter;
		TClonesArray* fphoton_Momentum;
		const MuonCol * flooseMuons;
		unsigned int nMaxlooseMuons = 6;
		unsigned int _muonCounter;
		TClonesArray* fmuon_Momentum;
		const PFMetCol * fPuppiType1MET;
		unsigned int nMaxPuppiType1MET = 1;
		unsigned int _metCounter;
		TClonesArray* fmet_Momentum;
		const ElectronCol * flooseElectrons;
		unsigned int nMaxlooseElectrons = 6;
		unsigned int _electronCounter;
		TClonesArray* felectron_Momentum;
      // STOP 1

    protected:
      void Process();
      void SlaveBegin();
      void SlaveTerminate();
      void BeginRun();

    private:

      ClassDef(monotopPreselMod, 0)         // monotopPreselMod filler
  };
}
#endif
