#ifndef MITPANDA_FLAT_unitTest_H
#define MITPANDA_FLAT_unitTest_H

#include "MitAna/DataTree/interface/FatJetFwd.h"
#include "MitAna/DataTree/interface/FatJet.h"
#include "MitAna/DataTree/interface/PFTauFwd.h"
#include "MitAna/DataTree/interface/PFTau.h"
#include "MitAna/DataTree/interface/ElectronFwd.h"
#include "MitAna/DataTree/interface/Electron.h"
#include "MitAna/DataTree/interface/MuonFwd.h"
#include "MitAna/DataTree/interface/Muon.h"
#include "MitAna/DataTree/interface/PhotonFwd.h"
#include "MitAna/DataTree/interface/Photon.h"
#include "MitAna/DataTree/interface/PFMetFwd.h"
#include "MitAna/DataTree/interface/PFMet.h"
#include "MitAna/DataTree/interface/MetFwd.h"
#include "MitAna/DataTree/interface/Met.h"
#include "MitAna/TreeMod/interface/BaseMod.h"
#include "TTree.h"
#include "TFile.h"
#include "MitAna/DataTree/interface/MCEventInfo.h"
#include "MitAna/DataTree/interface/VertexCol.h"
#include "MitAna/DataTree/interface/TriggerName.h"
#include "MitAna/DataTree/interface/TriggerMask.h"
#include "MitAna/DataTree/interface/TriggerTable.h"
#include "MitAna/DataTree/interface/EventHeader.h"
#include "MitAna/DataTree/interface/MCParticleFwd.h"
#include "algorithm"

namespace mithep
{
  class unitTest : public BaseMod
  {
    public:
      typedef FatJet::TrackData TrackData;
      typedef FatJet::SVData SVData;
      typedef FatJet::LeptonData LeptonData;

      unitTest(const char*, const char*);
      ~unitTest();
    const EventHeader *fEventHeader;
    unsigned int feventNumber;
		const FatJetCol * fCA15FatJetsPuppi;
		unsigned int nMaxCA15FatJetsPuppi = 6;
		unsigned int _fatjetCounter;
		float ffatjet_Tau1[10];
		float ffatjet_Tau2[10];
		float ffatjet_Tau3[10];
		float ffatjet_Tau4[10];
		float ffatjet_MassSoftDrop[10];
		TClonesArray* ffatjet_Momentum;
		const PFMetCol * fPuppiMet;
		unsigned int nMaxPuppiMet = 1;
		unsigned int _metCounter;
		TClonesArray* fmet_Momentum;
      // STOP 1
      void SetFilePath(TString p)  { fFilePath = p; }

    protected:
      void Process();
      void SlaveBegin();
      void SlaveTerminate();
      void BeginRun();

      TString fFilePath = "buffer.root";
      TFile *fFile;
      TTree *fTree;


    private:

      ClassDef(unitTest, 0)         // unitTest filler
  };
}
#endif
