#ifndef MITPANDA_FLAT_dijetPreselMod_H
#define MITPANDA_FLAT_dijetPreselMod_H

#include "MitAna/DataTree/interface/PFJetFwd.h"
#include "MitAna/DataTree/interface/JetFwd.h"
#include "MitAna/DataTree/interface/PFJet.h"
#include "MitAna/DataTree/interface/Jet.h"
#include "MitAna/DataTree/interface/PFMetFwd.h"
#include "MitAna/DataTree/interface/PFMet.h"
#include "MitAna/DataTree/interface/MetFwd.h"
#include "MitAna/DataTree/interface/Met.h"
#include "MitAna/TreeMod/interface/BaseMod.h"
#include "TFile.h"
#include "TString.h"
#include "vector"
#include "algorithm"

namespace mithep
{
  class dijetPreselMod : public BaseMod
  {
    public:
      dijetPreselMod(const char*, const char*);
      ~dijetPreselMod();
		const PFMetCol * fPuppiType1MET;
		unsigned int nMaxPuppiType1MET = 1;
		unsigned int _metCounter;
		TClonesArray* fmet_Momentum;
		const JetCol * fSelectedJets;
		unsigned int nMaxSelectedJets = 2;
		unsigned int _jetCounter;
		TClonesArray* fjet_Momentum;
      // STOP 1
      void SetFilePath(TString p)  { fFilePath = p; }

    protected:
      void Process();
      void SlaveBegin();
      void SlaveTerminate();

      TString fFilePath = "buffer.root";
      TFile *fFile;
      TTree *fTree;


    private:

      ClassDef(dijetPreselMod, 0)         // dijetPreselMod filler
  };
}
#endif
