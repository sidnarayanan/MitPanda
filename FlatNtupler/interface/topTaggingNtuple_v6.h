#ifndef MITPANDA_FLAT_topTaggingNtuple_v6_H
#define MITPANDA_FLAT_topTaggingNtuple_v6_H

//#include "MitAna/DataTree/interface/XXLFatJetFwd.h"
//#include "MitAna/DataTree/interface/XXLFatJet.h"
#include "MitAna/DataCont/interface/RefArray.h"
#include "MitAna/DataTree/interface/XlSubJet.h"
#include "MitAna/DataTree/interface/GenJetFwd.h"
#include "MitAna/DataTree/interface/GenJet.h"
#include "MitPanda/PhysicsObjects/interface/XXLFatJetFwd.h"
#include "MitPanda/PhysicsObjects/interface/XXLFatJet.h"
#include "MitAna/TreeMod/interface/BaseMod.h"
#include "TFile.h"
#include "MitAna/DataTree/interface/MCParticleFwd.h"
#include "MitAna/DataTree/interface/MCEventInfo.h"
#include "TTree.h"
#include "algorithm"

#define N_MAX_TRACKS 250
#define N_MAX_SVS 50
#define N_MAX_SOFTELECTRONS 20  
#define N_MAX_SOFTMUONS 20  
#define DOBTAG 0

namespace mithep
{
  class topTaggingNtuple_v6 : public BaseMod
  {
    public:
      typedef FatJet::TrackData TrackData;
      typedef FatJet::SVData SVData;
      typedef FatJet::LeptonData LeptonData;

      topTaggingNtuple_v6(const char*, const char*);
      ~topTaggingNtuple_v6();
		const MCParticleCol * fMCQuarks;
		unsigned int nMaxMCQuarks = 100;
		unsigned int _mcQuarkCounter;
		TClonesArray* fmcQuark_Momentum;
		int fmcQuark_PdgId[100];
		int fmcQuark_Status[100];
		int fmcQuark_StatusFlag[100];
		int fmcQuark_MotherPdgId[100];
    int fmcQuark_IsHadronicTop[100];
    float fmcQuark_TopSize[100];
		const XXLFatJetCol * fXlCA15Jets;
		unsigned int nMaxXlCA15Jets = 6;
		unsigned int _CA15fjCounter;
		float fCA15fj_JEC[8];
		float fCA15fj_MassSDb0[8];
		float fCA15fj_MassSDb1[8];
		float fCA15fj_MassSDb2[8];
		float fCA15fj_MassSDbm1[8];
		float fCA15fj_iota0[8];
		float fCA15fj_iota1[8];
		float fCA15fj_iota2[8];
		float fCA15fj_iota3[8];
		float fCA15fj_iota4[8];
		float fCA15fj_iota5[8];
		float fCA15fj_iota6[8];
		float fCA15fj_iota7[8];
		float fCA15fj_iota8[8];
		float fCA15fj_iota9[8];
		float fCA15fj_iotaDR0[8];
		float fCA15fj_iotaDR1[8];
		float fCA15fj_iotaDR2[8];
		float fCA15fj_iotaDR3[8];
		float fCA15fj_iotaDR4[8];
		float fCA15fj_iotaDR5[8];
		float fCA15fj_iotaDR6[8];
		float fCA15fj_iotaDR7[8];
		float fCA15fj_iotaDR8[8];
		float fCA15fj_iotaDR9[8];
		float fCA15fj_iotaM0[8];
		float fCA15fj_iotaM1[8];
		float fCA15fj_iotaM2[8];
		float fCA15fj_iotaM3[8];
		float fCA15fj_iotaM4[8];
		float fCA15fj_iotaM5[8];
		float fCA15fj_iotaM6[8];
		float fCA15fj_iotaM7[8];
		float fCA15fj_iotaM8[8];
		float fCA15fj_iotaM9[8];
    float fCA15fj_Tau1[8];
		float fCA15fj_Tau2[8];
		float fCA15fj_Tau3[8];
		float fCA15fj_Tau4[8];
    float fCA15fj_QJetVol[8];
		float fCA15fj_QGTag[8];
		float fCA15fj_MassSoftDrop[8];
		float fCA15fj_MassPruned[8];
		float fCA15fj_MassTrimmed[8];
		float fCA15fj_chi[8];
		float fCA15fj_nMicrojets[8];
		float fCA15fj_subJetBTag0[8];
		float fCA15fj_subJetBTag1[8];
		int fCA15fj_nSubJets[8];
    float fCA15fj_sjQGTag0[8];
    float fCA15fj_sjQGTag1[8];
    float fCA15fj_sjQGTag2[8];
    float fCA15fj_sjQGTag3[8];
		TClonesArray* fCA15fj_Momentum;
		TClonesArray* fCA15fj_SubJet0Momentum;
		TClonesArray* fCA15fj_SubJet1Momentum;
		TClonesArray* fCA15fj_SubJet2Momentum;
		TClonesArray* fCA15fj_SubJet3Momentum;
		TClonesArray* fCA15fj_SoftDropMomentum;
		float fCA15fj_GroomedTau1[8];
		float fCA15fj_GroomedTau2[8];
		float fCA15fj_GroomedTau3[8];
		float fCA15fj_MassCMSTT[8];
		float fCA15fj_MassHTT[8];
		float fCA15fj_FMin[8];
		float fCA15fj_ROpt[8];
		float fCA15fj_ROptCalc[8];
		float fCA15fj_PtROptCalc[8];
		const GenJetCol * fAKT4GenJets;
		unsigned int nMaxAKT4GenJets = 20;
		unsigned int _genJetCounter;
		TClonesArray* fgenJet_Momentum;
      // STOP 1
      void SetFilePath(TString p)  { fFilePath = p; }
    protected:
      void Process();
      void SlaveBegin();
      void SlaveTerminate();

      TString fFilePath = "buffer.root";
      TFile *fFile;
      TTree *fTree;

    private:

      ClassDef(topTaggingNtuple_v6, 0)         // topTaggingNtuple_v6 filler
  };
}
#endif
