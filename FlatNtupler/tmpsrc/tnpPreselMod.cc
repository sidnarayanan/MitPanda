#include <algorithm>
#include "MitPanda/FlatNtupler/interface/tnpPreselMod.h"
#include "TLorentzVector.h"
#include "MitCommon/DataFormats/interface/Types.h"
#include "MitCommon/DataFormats/interface/Vect4M.h"
#include "TMath.h"

using namespace mithep;

ClassImp(mithep::tnpPreselMod)

//--------------------------------------------------------------------------------------------------
tnpPreselMod::tnpPreselMod(const char *name, const char *title):
  BaseMod(name,title)
{
	flooseMuons = NULL;
	fmuon_Momentum = new TClonesArray("TLorentzVector",20);
	fPuppiType1MET = NULL;
	fmet_Momentum = new TClonesArray("TLorentzVector",20);
	flooseElectrons = NULL;
	felectron_Momentum = new TClonesArray("TLorentzVector",20);
	fSelectedJets = NULL;
	fjet_Momentum = new TClonesArray("TLorentzVector",20);
  // STOP 1
}

tnpPreselMod::~tnpPreselMod()
{
	delete fmuon_Momentum;
	delete fmet_Momentum;
	delete felectron_Momentum;
	delete fjet_Momentum;
  // STOP 2
}

//--------------------------------------------------------------------------------------------------
void tnpPreselMod::Process()
{

	flooseElectrons = GetObject<ElectronCol>("looseElectrons");
	assert(flooseElectrons!=NULL);
	_electronCounter = 0;
  float elePt=0, elePhi=0;
	for (UInt_t i=0; i<std::min(flooseElectrons->GetEntries(),(UInt_t)nMaxlooseElectrons); ++i) {
		const Electron * _looseElectrons = flooseElectrons->At(i);
		const FourVectorM &mom = _looseElectrons->Mom();
    if (TMath::Abs(mom.Eta()) > 2.5)
      continue;
    elePt = mom.Pt();
    elePhi = mom.Phi();
//    fprintf(stderr,"ele %f %f\n",elePt,elePhi);
    if (elePt<30)
      break;
		++_electronCounter;
    break;
	}

	flooseMuons = GetObject<MuonCol>("looseMuons");
	assert(flooseMuons!=NULL);
	_muonCounter = 0;
  float muPt=0;
  float muPhi=0;
	for (UInt_t i=0; i<std::min(flooseMuons->GetEntries(),(UInt_t)nMaxlooseMuons); ++i) {
		const Muon * _looseMuons = flooseMuons->At(i);
		const FourVectorM &mom = _looseMuons->Mom();
    if (TMath::Abs(mom.Eta()) > 2.5)
      continue;
		muPt = mom.Pt();
    muPhi = mom.Phi();
//    fprintf(stderr,"mu %f %f\n",muPt,muPhi);
    if (muPt<30)
      break;
    ++_muonCounter;
    break;
	}
  if (_muonCounter==0 && _electronCounter==0) {
//    fprintf(stderr,"2\n");
    SkipEvent();
    return;
  }

	fSelectedJets = GetObject<JetCol>("SelectedJets");
	assert(fSelectedJets!=NULL);
	_jetCounter = 0;
  Double_t highBtag = -1;
	for (UInt_t i=0; i<std::min(fSelectedJets->GetEntries(),(UInt_t)nMaxSelectedJets); ++i) {
		const Jet * _SelectedJets = fSelectedJets->At(i);
		const FourVectorM &mom = _SelectedJets->Mom();
    if (mom.Pt()<30)
      break;
		highBtag = std::max(highBtag,_SelectedJets->BJetTagsDisc(Jet::kCombinedInclusiveSecondaryVertexV2));
    if (mom.Pt()<100)
      continue;
		++_jetCounter;
	}
  if (highBtag<0.6 || _jetCounter==0) {
//    fprintf(stderr,"3\n");
    SkipEvent();
    return;
  }

	fPuppiType1MET = GetObject<PFMetCol>("PuppiType1MET");
	assert(fPuppiType1MET!=NULL);
	_metCounter = 0;
  float met=0,metPhi=0;
	for (UInt_t i=0; i<std::min(fPuppiType1MET->GetEntries(),(UInt_t)nMaxPuppiType1MET); ++i) {
		const PFMet * _PuppiType1MET = fPuppiType1MET->At(i);
		const FourVectorM &mom = _PuppiType1MET->Mom();
    met = mom.Pt();
    metPhi = mom.Phi();
		++_metCounter;
	}

  // compute fake MET
  bool passed = false;
  float fakeMETSquared;
  if (_muonCounter>0) {
    fakeMETSquared = TMath::Power(met*TMath::Cos(metPhi)+muPt*TMath::Cos(muPhi),2.) + TMath::Power(met*TMath::Sin(metPhi)+muPt*TMath::Sin(muPhi),2.);
    passed = (fakeMETSquared>900);
  }
  if (!passed && _electronCounter>0) {
    fakeMETSquared = TMath::Power(met*TMath::Cos(metPhi)+elePt*TMath::Cos(elePhi),2.) + TMath::Power(met*TMath::Sin(metPhi)+elePt*TMath::Sin(elePhi),2.);
    passed = (fakeMETSquared>900);
  }
  if (!passed) {
//    fprintf(stderr,"4\n");
    SkipEvent();
    return;
  }
//  fprintf(stderr,"passed!\n");
  // STOP 3
  return;
}

//--------------------------------------------------------------------------------------------------
void tnpPreselMod::SlaveBegin()
{
}

//--------------------------------------------------------------------------------------------------
void tnpPreselMod::SlaveTerminate()
{
}
