#include <algorithm>
#include "MitPanda/FlatNtupler/interface/unitTest.h"
#include "TLorentzVector.h"
#include "MitCommon/DataFormats/interface/Types.h"
#include "MitCommon/DataFormats/interface/Vect4M.h"
#include "MitAna/DataTree/interface/PFJet.h"
#include "MitAna/DataTree/interface/MCParticleCol.h"
#include "MitCommon/MathTools/interface/MathUtils.h"

using namespace mithep;

ClassImp(mithep::unitTest)

//--------------------------------------------------------------------------------------------------
unitTest::unitTest(const char *name, const char *title):
  BaseMod(name,title)
{
	fCA15FatJetsPuppi = NULL;
	ffatjet_Momentum = new TClonesArray("TLorentzVector",20);
	fPuppiMet = NULL;
	fmet_Momentum = new TClonesArray("TLorentzVector",20);
  // STOP 1
}

unitTest::~unitTest()
{
	delete ffatjet_Momentum;
	delete fmet_Momentum;
  // STOP 2
  fFile->Delete();
}

void unitTest::BeginRun() {

}

//--------------------------------------------------------------------------------------------------
void unitTest::Process()
{
  fEventHeader = GetObject<EventHeader>("EventHeader");
  assert(fEventHeader!=NULL);
  feventNumber = fEventHeader->EvtNum();
	ffatjet_Momentum->Clear();
	fCA15FatJetsPuppi = GetObject<FatJetCol>("CA15FatJetsPuppi");
	assert(fCA15FatJetsPuppi!=NULL);
	_fatjetCounter = 0;
	for (UInt_t i=0; i<std::min(fCA15FatJetsPuppi->GetEntries(),(UInt_t)nMaxCA15FatJetsPuppi); ++i) {
		const FatJet * _CA15FatJetsPuppi = fCA15FatJetsPuppi->At(i);
		ffatjet_Tau1[_fatjetCounter]=_CA15FatJetsPuppi->Tau1();
		ffatjet_Tau2[_fatjetCounter]=_CA15FatJetsPuppi->Tau2();
		ffatjet_Tau3[_fatjetCounter]=_CA15FatJetsPuppi->Tau3();
		ffatjet_Tau4[_fatjetCounter]=_CA15FatJetsPuppi->Tau4();
		ffatjet_MassSoftDrop[_fatjetCounter]=_CA15FatJetsPuppi->MassSoftDrop();
		new ( (*ffatjet_Momentum)[_fatjetCounter] ) TLorentzVector();
		const FourVectorM &mom = _CA15FatJetsPuppi->Mom();
		static_cast<TLorentzVector*>((*ffatjet_Momentum)[_fatjetCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
		++_fatjetCounter;
	}
	fmet_Momentum->Clear();
	fPuppiMet = GetObject<PFMetCol>("PuppiMet");
	assert(fPuppiMet!=NULL);
	_metCounter = 0;
	for (UInt_t i=0; i<std::min(fPuppiMet->GetEntries(),(UInt_t)nMaxPuppiMet); ++i) {
		const PFMet * _PuppiMet = fPuppiMet->At(i);
		new ( (*fmet_Momentum)[_metCounter] ) TLorentzVector();
		const FourVectorM &mom = _PuppiMet->Mom();
		static_cast<TLorentzVector*>((*fmet_Momentum)[_metCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
		++_metCounter;
	}
  // STOP 3
  fTree->Fill();
  return;
}

//--------------------------------------------------------------------------------------------------
void unitTest::SlaveBegin()
{
  fFile = new TFile(fFilePath,"RECREATE");
  fTree = new TTree("events","unitTest");
    fTree->Branch("eventNumber",&feventNumber,"eventNumber/I");
		fTree->Branch("_fatjetCounter",&_fatjetCounter,"_fatjetCounter/I");
		fTree->Branch("fatjet_Tau1",ffatjet_Tau1,"fatjet_Tau1[_fatjetCounter]/F");
		fTree->Branch("fatjet_Tau2",ffatjet_Tau2,"fatjet_Tau2[_fatjetCounter]/F");
		fTree->Branch("fatjet_Tau3",ffatjet_Tau3,"fatjet_Tau3[_fatjetCounter]/F");
		fTree->Branch("fatjet_Tau4",ffatjet_Tau4,"fatjet_Tau4[_fatjetCounter]/F");
		fTree->Branch("fatjet_MassSoftDrop",ffatjet_MassSoftDrop,"fatjet_MassSoftDrop[_fatjetCounter]/F");
		fTree->Branch("fatjet_Momentum","TClonesArray",&ffatjet_Momentum,128000, 0);
		fTree->Branch("_metCounter",&_metCounter,"_metCounter/I");
		fTree->Branch("met_Momentum","TClonesArray",&fmet_Momentum,128000, 0);
  // STOP 4
}

//--------------------------------------------------------------------------------------------------
void unitTest::SlaveTerminate()
{
  // STOP 5
  fFile->WriteTObject(fTree,fTree->GetName());
  fFile->Close();
}
