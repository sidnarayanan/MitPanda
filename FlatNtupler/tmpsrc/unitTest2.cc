#include <algorithm>
#include "MitPanda/FlatNtupler/interface/unitTest2.h"
#include "TLorentzVector.h"
#include "MitCommon/DataFormats/interface/Types.h"
#include "MitCommon/DataFormats/interface/Vect4M.h"
#include "MitAna/DataTree/interface/PFJet.h"
#include "MitAna/DataTree/interface/MCParticleCol.h"
#include "MitCommon/MathTools/interface/MathUtils.h"

using namespace mithep;

ClassImp(mithep::unitTest2)

//--------------------------------------------------------------------------------------------------
unitTest2::unitTest2(const char *name, const char *title):
  BaseMod(name,title)
{
	fCA15FatJetsPuppi = NULL;
	ffatjet_Momentum = new TClonesArray("TLorentzVector",20);
	fPuppiMet = NULL;
	fmet_Momentum = new TClonesArray("TLorentzVector",20);
	fPFMet = NULL;
	fpfmet_Momentum = new TClonesArray("TLorentzVector",20);
  fVertices = NULL;
	fSelectedJets = NULL;
	fjet_Momentum = new TClonesArray("TLorentzVector",20);
  // STOP 1
}

unitTest2::~unitTest2()
{
	delete ffatjet_Momentum;
	delete fmet_Momentum;
	delete fpfmet_Momentum;
  // STOP 2
  fFile->Delete();
}

void unitTest2::BeginRun() {

}

//--------------------------------------------------------------------------------------------------
void unitTest2::Process()
{
  fVertices = GetObject<VertexCol>("PrimaryVertexes");
  nPV = fVertices->GetEntries();

  fEventHeader = GetObject<EventHeader>("EventHeader");
  assert(fEventHeader!=NULL);
  feventNumber = fEventHeader->EvtNum();
	ffatjet_Momentum->Clear();
	fCA15FatJetsPuppi = GetObject<FatJetCol>("AKt8FatJetsCHS");
	assert(fCA15FatJetsPuppi!=NULL);
	_fatjetCounter = 0;
	for (UInt_t i=0; i<std::min(fCA15FatJetsPuppi->GetEntries(),(UInt_t)nMaxCA15FatJetsPuppi); ++i) {
		const FatJet * _CA15FatJetsPuppi = fCA15FatJetsPuppi->At(i);
		ffatjet_Tau1[_fatjetCounter]=_CA15FatJetsPuppi->Tau1();
		ffatjet_Tau2[_fatjetCounter]=_CA15FatJetsPuppi->Tau2();
		ffatjet_Tau3[_fatjetCounter]=_CA15FatJetsPuppi->Tau3();
		ffatjet_Tau4[_fatjetCounter]=_CA15FatJetsPuppi->Tau4();
		ffatjet_MassSoftDrop[_fatjetCounter]=_CA15FatJetsPuppi->MassSoftDrop();
		new ( (*ffatjet_Momentum)[_fatjetCounter] ) TLorentzVector();
		const FourVectorM &mom = _CA15FatJetsPuppi->Mom();
		static_cast<TLorentzVector*>((*ffatjet_Momentum)[_fatjetCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());

    std::vector<float> const &subjetBtags = _CA15FatJetsPuppi->GetSubJetBtags();
    float maxBtag=-10, secondMaxBtag=-10;
    for (auto & btag : subjetBtags) {
        if (btag > maxBtag) {
          secondMaxBtag = maxBtag;
          maxBtag = btag;
        } else if (btag > secondMaxBtag) {
          secondMaxBtag = btag;
        }
    }
    ffatjet_subJetBTag0[_fatjetCounter] = maxBtag;
    ffatjet_subJetBTag1[_fatjetCounter] = secondMaxBtag;
		++_fatjetCounter;
	}
	fmet_Momentum->Clear();
	fPuppiMet = GetObject<PFMetCol>("PuppiMet");
	assert(fPuppiMet!=NULL);
	_metCounter = 0;
	for (UInt_t i=0; i<std::min(fPuppiMet->GetEntries(),(UInt_t)nMaxPuppiMet); ++i) {
		const PFMet * _PuppiMet = fPuppiMet->At(i);
		new ( (*fmet_Momentum)[_metCounter] ) TLorentzVector();
		const FourVectorM &mom = _PuppiMet->Mom();
		static_cast<TLorentzVector*>((*fmet_Momentum)[_metCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
		++_metCounter;
	}
	fpfmet_Momentum->Clear();
	fPFMet = GetObject<PFMetCol>("PFMet");
	assert(fPFMet!=NULL);
	_pfmetCounter = 0;
	for (UInt_t i=0; i<std::min(fPFMet->GetEntries(),(UInt_t)nMaxPFMet); ++i) {
		const PFMet * _PFMet = fPFMet->At(i);
		new ( (*fpfmet_Momentum)[_pfmetCounter] ) TLorentzVector();
		const FourVectorM &mom = _PFMet->Mom();
		static_cast<TLorentzVector*>((*fpfmet_Momentum)[_pfmetCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
		++_pfmetCounter;
	}
	fjet_Momentum->Clear();
	fSelectedJets = GetObject<JetCol>("AKt4PuppiJets");
	assert(fSelectedJets!=NULL);
	_jetCounter = 0;
	for (UInt_t i=0; i<std::min(fSelectedJets->GetEntries(),(UInt_t)nMaxSelectedJets); ++i) {
		const PFJet * _SelectedJets = dynamic_cast<const PFJet*>(fSelectedJets->At(i));
    new ( (*fjet_Momentum)[_jetCounter] ) TLorentzVector();
    const FourVectorM &mom = _SelectedJets->Mom();
    static_cast<TLorentzVector*>((*fjet_Momentum)[_jetCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
		fjet_btag[_jetCounter]=_SelectedJets->BJetTagsDisc(Jet::kCombinedInclusiveSecondaryVertexV2);
		++_jetCounter;
	}
  // STOP 3
  fTree->Fill();
  return;
}

//--------------------------------------------------------------------------------------------------
void unitTest2::SlaveBegin()
{
  fFile = new TFile(fFilePath,"RECREATE");
  fTree = new TTree("events","unitTest2");
    fTree->Branch("eventNumber",&feventNumber,"eventNumber/I");
    fTree->Branch("nPV",&nPV,"nPV/I");
		fTree->Branch("_fatjetCounter",&_fatjetCounter,"_fatjetCounter/I");
		fTree->Branch("fatjet_Tau1",ffatjet_Tau1,"fatjet_Tau1[_fatjetCounter]/F");
		fTree->Branch("fatjet_Tau2",ffatjet_Tau2,"fatjet_Tau2[_fatjetCounter]/F");
		fTree->Branch("fatjet_Tau3",ffatjet_Tau3,"fatjet_Tau3[_fatjetCounter]/F");
		fTree->Branch("fatjet_Tau4",ffatjet_Tau4,"fatjet_Tau4[_fatjetCounter]/F");
		fTree->Branch("fatjet_subJetBTag0",ffatjet_subJetBTag0,"fatjet_subJetBTag0[_fatjetCounter]/F");
		fTree->Branch("fatjet_subJetBTag1",ffatjet_subJetBTag1,"fatjet_subJetBTag1[_fatjetCounter]/F");
		fTree->Branch("fatjet_MassSoftDrop",ffatjet_MassSoftDrop,"fatjet_MassSoftDrop[_fatjetCounter]/F");
		fTree->Branch("fatjet_Momentum","TClonesArray",&ffatjet_Momentum,128000, 0);
		fTree->Branch("_metCounter",&_metCounter,"_metCounter/I");
		fTree->Branch("met_Momentum","TClonesArray",&fmet_Momentum,128000, 0);
		fTree->Branch("_pfmetCounter",&_pfmetCounter,"_pfmetCounter/I");
		fTree->Branch("pfmet_Momentum","TClonesArray",&fpfmet_Momentum,128000, 0);
		fTree->Branch("_jetCounter",&_jetCounter,"_jetCounter/I");
		fTree->Branch("jet_Momentum","TClonesArray",&fjet_Momentum,128000, 0);
		fTree->Branch("jet_btag",fjet_btag,"jet_btag[_jetCounter]/F");
  // STOP 4
}

//--------------------------------------------------------------------------------------------------
void unitTest2::SlaveTerminate()
{
  // STOP 5
  fFile->WriteTObject(fTree,fTree->GetName());
  fFile->Close();
}
