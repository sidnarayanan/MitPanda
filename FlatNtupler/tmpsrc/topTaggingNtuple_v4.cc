#include <algorithm>
#include "MitPanda/FlatNtupler/interface/topTaggingNtuple_v4.h"
#include "TLorentzVector.h"
#include "MitCommon/DataFormats/interface/Types.h"
#include "MitCommon/DataFormats/interface/Vect4M.h"
#include "MitAna/DataTree/interface/MCParticleCol.h"
#include "MitCommon/MathTools/interface/MathUtils.h"


using namespace mithep;

ClassImp(mithep::topTaggingNtuple_v4)

//--------------------------------------------------------------------------------------------------
topTaggingNtuple_v4::topTaggingNtuple_v4(const char *name, const char *title)
{
	fXlAK8Jets = NULL;
	fAK8fj_Momentum = new TClonesArray("TLorentzVector",20);
	fAK8fj_SoftDropMomentum = new TClonesArray("TLorentzVector",20);
	fMCQuarks = NULL;
	fmcQuark_Momentum = new TClonesArray("TLorentzVector",20);
	fXlCA15Jets = NULL;
	fCA15fj_Momentum = new TClonesArray("TLorentzVector",20);
	fCA15fj_SoftDropMomentum = new TClonesArray("TLorentzVector",20);
	fAKT4GenJets = NULL;
	fgenJet_Momentum = new TClonesArray("TLorentzVector",20);
  // STOP 1
}

topTaggingNtuple_v4::~topTaggingNtuple_v4()
{
	delete fAK8fj_Momentum;
	delete fAK8fj_SoftDropMomentum;
	delete fmcQuark_Momentum;
	delete fCA15fj_Momentum;
	delete fCA15fj_SoftDropMomentum;
	delete fgenJet_Momentum;
  // STOP 2
  fFile->Close();
  fFile->Delete();
}

//--------------------------------------------------------------------------------------------------
void topTaggingNtuple_v4::Process()
{
  unsigned int j;
	fAK8fj_Momentum->Clear();
	fAK8fj_SoftDropMomentum->Clear();
	fXlAK8Jets = GetObject<XXLFatJetCol>("XlAK8Jets");
	assert(fXlAK8Jets!=NULL);
	AK8fj_trackCounter = 0;
	AK8fj_SVCounter = 0;
	AK8fj_softElectronCounter = 0;
	AK8fj_softMuonCounter = 0;
	_AK8fjCounter = 0;
	for (UInt_t i=0; i<std::min(fXlAK8Jets->GetEntries(),(UInt_t)nMaxXlAK8Jets); ++i) {
		const XXLFatJet * _XlAK8Jets = fXlAK8Jets->At(i);
		fAK8fj_Tau1[_AK8fjCounter]=_XlAK8Jets->Tau1();
		fAK8fj_Tau2[_AK8fjCounter]=_XlAK8Jets->Tau2();
		fAK8fj_Tau3[_AK8fjCounter]=_XlAK8Jets->Tau3();
		fAK8fj_Tau4[_AK8fjCounter]=_XlAK8Jets->Tau4();
		fAK8fj_Tau1IVF[_AK8fjCounter]=_XlAK8Jets->Tau1IVF();
		fAK8fj_Tau2IVF[_AK8fjCounter]=_XlAK8Jets->Tau2IVF();
		fAK8fj_Tau3IVF[_AK8fjCounter]=_XlAK8Jets->Tau3IVF();
		fAK8fj_Charge[_AK8fjCounter]=_XlAK8Jets->Charge();
		fAK8fj_tauDot[_AK8fjCounter]=_XlAK8Jets->tauDot();
		fAK8fj_zRatio[_AK8fjCounter]=_XlAK8Jets->zRatio();
		fAK8fj_QJetVol[_AK8fjCounter]=_XlAK8Jets->QJetVol();
		fAK8fj_QGTag[_AK8fjCounter]=_XlAK8Jets->QGTag();
		fAK8fj_MassSoftDrop[_AK8fjCounter]=_XlAK8Jets->MassSDb0();
		fAK8fj_MassPruned[_AK8fjCounter]=_XlAK8Jets->MassPruned();
		fAK8fj_MassTrimmed[_AK8fjCounter]=_XlAK8Jets->MassTrimmed();
		fAK8fj_chi[_AK8fjCounter]=_XlAK8Jets->chi();
		fAK8fj_nMicrojets[_AK8fjCounter]=_XlAK8Jets->nMicrojets();
		const std::vector<TrackData> & tracks = _XlAK8Jets->GetTrackData();
		fAK8fj_nTracks[_AK8fjCounter] = tracks.size();
		fAK8fj_firstTrack[_AK8fjCounter] = AK8fj_trackCounter;
		j=0;
		for (const auto & track : tracks) {
			fAK8fj_track_pt[AK8fj_trackCounter+j]=track.pt;
			fAK8fj_track_eta[AK8fj_trackCounter+j]=track.eta;
			fAK8fj_track_phi[AK8fj_trackCounter+j]=track.phi;
			fAK8fj_track_length[AK8fj_trackCounter+j]=track.length;
			fAK8fj_track_dist[AK8fj_trackCounter+j]=track.dist;
			fAK8fj_track_dxy[AK8fj_trackCounter+j]=track.dxy;
			fAK8fj_track_dz[AK8fj_trackCounter+j]=track.dz;
			fAK8fj_track_IP2D[AK8fj_trackCounter+j]=track.IP2D;
			fAK8fj_track_IP2Dsig[AK8fj_trackCounter+j]=track.IP2Dsig;
			fAK8fj_track_IP[AK8fj_trackCounter+j]=track.IP;
			fAK8fj_track_IPsig[AK8fj_trackCounter+j]=track.IPsig;
			fAK8fj_track_IP2Derr[AK8fj_trackCounter+j]=track.IP2Derr;
			fAK8fj_track_IPerr[AK8fj_trackCounter+j]=track.IPerr;
			fAK8fj_track_prob[AK8fj_trackCounter+j]=track.prob;
			fAK8fj_track_PVWeight[AK8fj_trackCounter+j]=track.PVWeight;
			fAK8fj_track_SVWeight[AK8fj_trackCounter+j]=track.SVWeight;
			fAK8fj_track_PV[AK8fj_trackCounter+j]=track.PV;
			fAK8fj_track_fromSV[AK8fj_trackCounter+j]=track.fromSV;
			fAK8fj_track_SV[AK8fj_trackCounter+j]=track.SV;
			++j;
		}
		AK8fj_trackCounter += tracks.size();
		const std::vector<SVData> & SVs = _XlAK8Jets->GetSVData();
		fAK8fj_nSVs[_AK8fjCounter] = SVs.size();
		fAK8fj_firstSV[_AK8fjCounter] = AK8fj_SVCounter;
		j=0;
		for (const auto & SV : SVs) {
			fAK8fj_SV_nTrk[AK8fj_SVCounter+j]=SV.nTrk;
			fAK8fj_SV_flight[AK8fj_SVCounter+j]=SV.flight;
			fAK8fj_SV_flightErr[AK8fj_SVCounter+j]=SV.flightErr;
			fAK8fj_SV_deltaRJet[AK8fj_SVCounter+j]=SV.deltaRJet;
			fAK8fj_SV_deltaRSumJet[AK8fj_SVCounter+j]=SV.deltaRSumJet;
			fAK8fj_SV_deltaRSumDir[AK8fj_SVCounter+j]=SV.deltaRSumDir;
			fAK8fj_SV_flight2D[AK8fj_SVCounter+j]=SV.flight2D;
			fAK8fj_SV_flight2DErr[AK8fj_SVCounter+j]=SV.flight2DErr;
			fAK8fj_SV_totCharge[AK8fj_SVCounter+j]=SV.totCharge;
			fAK8fj_SV_vtxDistJetAxis[AK8fj_SVCounter+j]=SV.vtxDistJetAxis;
			fAK8fj_SV_mass[AK8fj_SVCounter+j]=SV.mass;
			fAK8fj_SV_energyRatio[AK8fj_SVCounter+j]=SV.energyRatio;
			fAK8fj_SV_pt[AK8fj_SVCounter+j]=SV.pt;
			fAK8fj_SV_eta[AK8fj_SVCounter+j]=SV.eta;
			fAK8fj_SV_phi[AK8fj_SVCounter+j]=SV.phi;
			fAK8fj_SV_dirX[AK8fj_SVCounter+j]=SV.dirX;
			fAK8fj_SV_dirY[AK8fj_SVCounter+j]=SV.dirY;
			fAK8fj_SV_dirZ[AK8fj_SVCounter+j]=SV.dirZ;
			++j;
		}
		AK8fj_SVCounter += SVs.size();
		const std::vector<LeptonData> & softElectrons = _XlAK8Jets->GetElectronData();
		fAK8fj_nSoftElectrons[_AK8fjCounter] = softElectrons.size();
		fAK8fj_firstSoftElectron[_AK8fjCounter] = AK8fj_softElectronCounter;
		j=0;
		for (const auto & softElectron : softElectrons) {
			fAK8fj_softElectron_pt[AK8fj_softElectronCounter+j]=softElectron.pt;
			fAK8fj_softElectron_eta[AK8fj_softElectronCounter+j]=softElectron.eta;
			fAK8fj_softElectron_phi[AK8fj_softElectronCounter+j]=softElectron.phi;
			fAK8fj_softElectron_ptRel[AK8fj_softElectronCounter+j]=softElectron.ptRel;
			fAK8fj_softElectron_ratio[AK8fj_softElectronCounter+j]=softElectron.ratio;
			fAK8fj_softElectron_ratioRel[AK8fj_softElectronCounter+j]=softElectron.ratioRel;
			fAK8fj_softElectron_IP[AK8fj_softElectronCounter+j]=softElectron.IP;
			fAK8fj_softElectron_IP2D[AK8fj_softElectronCounter+j]=softElectron.IP2D;
			++j;
		}
		AK8fj_softElectronCounter += softElectrons.size();
		const std::vector<LeptonData> & softMuons = _XlAK8Jets->GetMuonData();
		fAK8fj_nSoftMuons[_AK8fjCounter] = softMuons.size();
		fAK8fj_firstSoftMuon[_AK8fjCounter] = AK8fj_softMuonCounter;
		j=0;
		for (const auto & softMuon : softMuons) {
			fAK8fj_softMuon_pt[AK8fj_softMuonCounter+j]=softMuon.pt;
			fAK8fj_softMuon_eta[AK8fj_softMuonCounter+j]=softMuon.eta;
			fAK8fj_softMuon_phi[AK8fj_softMuonCounter+j]=softMuon.phi;
			fAK8fj_softMuon_ptRel[AK8fj_softMuonCounter+j]=softMuon.ptRel;
			fAK8fj_softMuon_ratio[AK8fj_softMuonCounter+j]=softMuon.ratio;
			fAK8fj_softMuon_ratioRel[AK8fj_softMuonCounter+j]=softMuon.ratioRel;
			fAK8fj_softMuon_IP[AK8fj_softMuonCounter+j]=softMuon.IP;
			fAK8fj_softMuon_IP2D[AK8fj_softMuonCounter+j]=softMuon.IP2D;
			++j;
		}
		AK8fj_softMuonCounter += softMuons.size();
		std::vector<float> const &subjetBtags = _XlAK8Jets->GetSubJetBtags();
		float maxBtag = -10;
		float secondMaxBtag = -10;
		for (auto & btag : subjetBtags) {
				if (btag > maxBtag) {
					secondMaxBtag = maxBtag;
					maxBtag = btag;
				} else if (btag > secondMaxBtag) {
					secondMaxBtag = btag;
				}
		}
		fAK8fj_subJetBTag0[_AK8fjCounter] = maxBtag;
		fAK8fj_subJetBTag1[_AK8fjCounter] = secondMaxBtag;

    const RefArray<XlSubJet>& fXlAK8SubJets = _XlAK8Jets->GetSubJets(XlSubJet::kSoftDrop);
		fAK8fj_nSubJets[_AK8fjCounter] = fXlAK8SubJets.GetEntries();
    if (fAK8fj_nSubJets[_AK8fjCounter]>0) 
      fAK8fj_sjQGTag0[_AK8fjCounter] = fXlAK8SubJets.At(0)->QGTag();
    else 
      fAK8fj_sjQGTag0[_AK8fjCounter] = -1;
    if (fAK8fj_nSubJets[_AK8fjCounter]>1) 
      fAK8fj_sjQGTag1[_AK8fjCounter] = fXlAK8SubJets.At(1)->QGTag();
    else 
      fAK8fj_sjQGTag1[_AK8fjCounter] = -1;
    if (fAK8fj_nSubJets[_AK8fjCounter]>2) 
      fAK8fj_sjQGTag2[_AK8fjCounter] = fXlAK8SubJets.At(2)->QGTag();
    else 
      fAK8fj_sjQGTag2[_AK8fjCounter] = -1;

		fAK8fj_MassSDb0[_AK8fjCounter]=_XlAK8Jets->MassSDb0();
		fAK8fj_MassSDb1[_AK8fjCounter]=_XlAK8Jets->MassSDb1();
		fAK8fj_MassSDb2[_AK8fjCounter]=_XlAK8Jets->MassSDb2();
		fAK8fj_MassSDbm1[_AK8fjCounter]=_XlAK8Jets->MassSDbm1();

    const std::vector<Double32_t> &iotas = _XlAK8Jets->Iotas();
    fAK8fj_iota0[_AK8fjCounter] = iotas[0];
    fAK8fj_iota1[_AK8fjCounter] = iotas[1];
    fAK8fj_iota2[_AK8fjCounter] = iotas[2];
    fAK8fj_iota3[_AK8fjCounter] = iotas[3];
    fAK8fj_iota4[_AK8fjCounter] = iotas[4];
    fAK8fj_iota5[_AK8fjCounter] = iotas[5];
    const std::vector<Double32_t> &iotaAngles = _XlAK8Jets->IotaAngles();
    fAK8fj_iotaAngle0[_AK8fjCounter] = iotaAngles[0];
    fAK8fj_iotaAngle1[_AK8fjCounter] = iotaAngles[1];
    fAK8fj_iotaAngle2[_AK8fjCounter] = iotaAngles[2];
    fAK8fj_iotaAngle3[_AK8fjCounter] = iotaAngles[3];
    fAK8fj_iotaAngle4[_AK8fjCounter] = iotaAngles[4];
    fAK8fj_iotaAngle5[_AK8fjCounter] = iotaAngles[5];

		new ( (*fAK8fj_Momentum)[_AK8fjCounter] ) TLorentzVector();
		const FourVectorM &mom = _XlAK8Jets->Mom();
		static_cast<TLorentzVector*>((*fAK8fj_Momentum)[_AK8fjCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());

		new ( (*fAK8fj_SoftDropMomentum)[_AK8fjCounter] ) TLorentzVector();
		const Vect4M &pMom = _XlAK8Jets->SoftDropP();
		static_cast<TLorentzVector*>((*fAK8fj_SoftDropMomentum)[_AK8fjCounter])->SetPtEtaPhiM(pMom.Pt(),pMom.Eta(),pMom.Phi(),pMom.M());

    ++_AK8fjCounter;
	}

	fCA15fj_Momentum->Clear();
	fCA15fj_SoftDropMomentum->Clear();
	fXlCA15Jets = GetObject<XXLFatJetCol>("XlCA15Jets");
	assert(fXlCA15Jets!=NULL);
	CA15fj_trackCounter = 0;
	CA15fj_SVCounter = 0;
	CA15fj_softElectronCounter = 0;
	CA15fj_softMuonCounter = 0;
	_CA15fjCounter = 0;
	for (UInt_t i=0; i<std::min(fXlCA15Jets->GetEntries(),(UInt_t)nMaxXlCA15Jets); ++i) {
		const XXLFatJet * _XlCA15Jets = fXlCA15Jets->At(i);
		fCA15fj_Tau1[_CA15fjCounter]=_XlCA15Jets->Tau1();
		fCA15fj_Tau2[_CA15fjCounter]=_XlCA15Jets->Tau2();
		fCA15fj_Tau3[_CA15fjCounter]=_XlCA15Jets->Tau3();
		fCA15fj_Tau4[_CA15fjCounter]=_XlCA15Jets->Tau4();
		fCA15fj_Tau1IVF[_CA15fjCounter]=_XlCA15Jets->Tau1IVF();
		fCA15fj_Tau2IVF[_CA15fjCounter]=_XlCA15Jets->Tau2IVF();
		fCA15fj_Tau3IVF[_CA15fjCounter]=_XlCA15Jets->Tau3IVF();
		fCA15fj_Charge[_CA15fjCounter]=_XlCA15Jets->Charge();
		fCA15fj_tauDot[_CA15fjCounter]=_XlCA15Jets->tauDot();
		fCA15fj_zRatio[_CA15fjCounter]=_XlCA15Jets->zRatio();
		fCA15fj_QJetVol[_CA15fjCounter]=_XlCA15Jets->QJetVol();
		fCA15fj_QGTag[_CA15fjCounter]=_XlCA15Jets->QGTag();
		fCA15fj_MassSoftDrop[_CA15fjCounter]=_XlCA15Jets->MassSDb1();
		fCA15fj_MassPruned[_CA15fjCounter]=_XlCA15Jets->MassPruned();
		fCA15fj_MassTrimmed[_CA15fjCounter]=_XlCA15Jets->MassTrimmed();
		fCA15fj_chi[_CA15fjCounter]=_XlCA15Jets->chi();
		fCA15fj_nMicrojets[_CA15fjCounter]=_XlCA15Jets->nMicrojets();
		const std::vector<TrackData> & tracks = _XlCA15Jets->GetTrackData();
		fCA15fj_nTracks[_CA15fjCounter] = tracks.size();
		fCA15fj_firstTrack[_CA15fjCounter] = CA15fj_trackCounter;
		j=0;
		for (const auto & track : tracks) {
			fCA15fj_track_pt[CA15fj_trackCounter+j]=track.pt;
			fCA15fj_track_eta[CA15fj_trackCounter+j]=track.eta;
			fCA15fj_track_phi[CA15fj_trackCounter+j]=track.phi;
			fCA15fj_track_length[CA15fj_trackCounter+j]=track.length;
			fCA15fj_track_dist[CA15fj_trackCounter+j]=track.dist;
			fCA15fj_track_dxy[CA15fj_trackCounter+j]=track.dxy;
			fCA15fj_track_dz[CA15fj_trackCounter+j]=track.dz;
			fCA15fj_track_IP2D[CA15fj_trackCounter+j]=track.IP2D;
			fCA15fj_track_IP2Dsig[CA15fj_trackCounter+j]=track.IP2Dsig;
			fCA15fj_track_IP[CA15fj_trackCounter+j]=track.IP;
			fCA15fj_track_IPsig[CA15fj_trackCounter+j]=track.IPsig;
			fCA15fj_track_IP2Derr[CA15fj_trackCounter+j]=track.IP2Derr;
			fCA15fj_track_IPerr[CA15fj_trackCounter+j]=track.IPerr;
			fCA15fj_track_prob[CA15fj_trackCounter+j]=track.prob;
			fCA15fj_track_PVWeight[CA15fj_trackCounter+j]=track.PVWeight;
			fCA15fj_track_SVWeight[CA15fj_trackCounter+j]=track.SVWeight;
			fCA15fj_track_PV[CA15fj_trackCounter+j]=track.PV;
			fCA15fj_track_fromSV[CA15fj_trackCounter+j]=track.fromSV;
			fCA15fj_track_SV[CA15fj_trackCounter+j]=track.SV;
			++j;
		}
		CA15fj_trackCounter += tracks.size();
		const std::vector<SVData> & SVs = _XlCA15Jets->GetSVData();
		fCA15fj_nSVs[_CA15fjCounter] = SVs.size();
		fCA15fj_firstSV[_CA15fjCounter] = CA15fj_SVCounter;
		j=0;
		for (const auto & SV : SVs) {
			fCA15fj_SV_nTrk[CA15fj_SVCounter+j]=SV.nTrk;
			fCA15fj_SV_flight[CA15fj_SVCounter+j]=SV.flight;
			fCA15fj_SV_flightErr[CA15fj_SVCounter+j]=SV.flightErr;
			fCA15fj_SV_deltaRJet[CA15fj_SVCounter+j]=SV.deltaRJet;
			fCA15fj_SV_deltaRSumJet[CA15fj_SVCounter+j]=SV.deltaRSumJet;
			fCA15fj_SV_deltaRSumDir[CA15fj_SVCounter+j]=SV.deltaRSumDir;
			fCA15fj_SV_flight2D[CA15fj_SVCounter+j]=SV.flight2D;
			fCA15fj_SV_flight2DErr[CA15fj_SVCounter+j]=SV.flight2DErr;
			fCA15fj_SV_totCharge[CA15fj_SVCounter+j]=SV.totCharge;
			fCA15fj_SV_vtxDistJetAxis[CA15fj_SVCounter+j]=SV.vtxDistJetAxis;
			fCA15fj_SV_mass[CA15fj_SVCounter+j]=SV.mass;
			fCA15fj_SV_energyRatio[CA15fj_SVCounter+j]=SV.energyRatio;
			fCA15fj_SV_pt[CA15fj_SVCounter+j]=SV.pt;
			fCA15fj_SV_eta[CA15fj_SVCounter+j]=SV.eta;
			fCA15fj_SV_phi[CA15fj_SVCounter+j]=SV.phi;
			fCA15fj_SV_dirX[CA15fj_SVCounter+j]=SV.dirX;
			fCA15fj_SV_dirY[CA15fj_SVCounter+j]=SV.dirY;
			fCA15fj_SV_dirZ[CA15fj_SVCounter+j]=SV.dirZ;
			++j;
		}
		CA15fj_SVCounter += SVs.size();
		const std::vector<LeptonData> & softElectrons = _XlCA15Jets->GetElectronData();
		fCA15fj_nSoftElectrons[_CA15fjCounter] = softElectrons.size();
		fCA15fj_firstSoftElectron[_CA15fjCounter] = CA15fj_softElectronCounter;
		j=0;
		for (const auto & softElectron : softElectrons) {
			fCA15fj_softElectron_pt[CA15fj_softElectronCounter+j]=softElectron.pt;
			fCA15fj_softElectron_eta[CA15fj_softElectronCounter+j]=softElectron.eta;
			fCA15fj_softElectron_phi[CA15fj_softElectronCounter+j]=softElectron.phi;
			fCA15fj_softElectron_ptRel[CA15fj_softElectronCounter+j]=softElectron.ptRel;
			fCA15fj_softElectron_ratio[CA15fj_softElectronCounter+j]=softElectron.ratio;
			fCA15fj_softElectron_ratioRel[CA15fj_softElectronCounter+j]=softElectron.ratioRel;
			fCA15fj_softElectron_IP[CA15fj_softElectronCounter+j]=softElectron.IP;
			fCA15fj_softElectron_IP2D[CA15fj_softElectronCounter+j]=softElectron.IP2D;
			++j;
		}
		CA15fj_softElectronCounter += softElectrons.size();
		const std::vector<LeptonData> & softMuons = _XlCA15Jets->GetMuonData();
		fCA15fj_nSoftMuons[_CA15fjCounter] = softMuons.size();
		fCA15fj_firstSoftMuon[_CA15fjCounter] = CA15fj_softMuonCounter;
		j=0;
		for (const auto & softMuon : softMuons) {
			fCA15fj_softMuon_pt[CA15fj_softMuonCounter+j]=softMuon.pt;
			fCA15fj_softMuon_eta[CA15fj_softMuonCounter+j]=softMuon.eta;
			fCA15fj_softMuon_phi[CA15fj_softMuonCounter+j]=softMuon.phi;
			fCA15fj_softMuon_ptRel[CA15fj_softMuonCounter+j]=softMuon.ptRel;
			fCA15fj_softMuon_ratio[CA15fj_softMuonCounter+j]=softMuon.ratio;
			fCA15fj_softMuon_ratioRel[CA15fj_softMuonCounter+j]=softMuon.ratioRel;
			fCA15fj_softMuon_IP[CA15fj_softMuonCounter+j]=softMuon.IP;
			fCA15fj_softMuon_IP2D[CA15fj_softMuonCounter+j]=softMuon.IP2D;
			++j;
		}
		CA15fj_softMuonCounter += softMuons.size();
		std::vector<float> const &subjetBtags = _XlCA15Jets->GetSubJetBtags();
		float maxBtag = -10;
		float secondMaxBtag = -10;
		for (auto & btag : subjetBtags) {
				if (btag > maxBtag) {
					secondMaxBtag = maxBtag;
					maxBtag = btag;
				} else if (btag > secondMaxBtag) {
					secondMaxBtag = btag;
				}
		}
		fCA15fj_subJetBTag0[_CA15fjCounter] = maxBtag;
		fCA15fj_subJetBTag1[_CA15fjCounter] = secondMaxBtag;

    const RefArray<XlSubJet>& fXlCA15SubJets = _XlCA15Jets->GetSubJets(XlSubJet::kSoftDrop);
		fCA15fj_nSubJets[_CA15fjCounter] = fXlCA15SubJets.GetEntries();
    if (fCA15fj_nSubJets[_CA15fjCounter]>0) 
      fCA15fj_sjQGTag0[_CA15fjCounter] = fXlCA15SubJets.At(0)->QGTag();
    else 
      fCA15fj_sjQGTag0[_CA15fjCounter] = -1;
    if (fCA15fj_nSubJets[_CA15fjCounter]>1) 
      fCA15fj_sjQGTag1[_CA15fjCounter] = fXlCA15SubJets.At(1)->QGTag();
    else 
      fCA15fj_sjQGTag1[_CA15fjCounter] = -1;
    if (fCA15fj_nSubJets[_CA15fjCounter]>2) 
      fCA15fj_sjQGTag2[_CA15fjCounter] = fXlCA15SubJets.At(2)->QGTag();
    else 
      fCA15fj_sjQGTag2[_CA15fjCounter] = -1;

		fCA15fj_MassSDb0[_CA15fjCounter]=_XlCA15Jets->MassSDb0();
		fCA15fj_MassSDb1[_CA15fjCounter]=_XlCA15Jets->MassSDb1();
		fCA15fj_MassSDb2[_CA15fjCounter]=_XlCA15Jets->MassSDb2();
		fCA15fj_MassSDbm1[_CA15fjCounter]=_XlCA15Jets->MassSDbm1();

    const std::vector<Double32_t> &iotas = _XlCA15Jets->Iotas();
    fCA15fj_iota0[_CA15fjCounter] = iotas[0];
    fCA15fj_iota1[_CA15fjCounter] = iotas[1];
    fCA15fj_iota2[_CA15fjCounter] = iotas[2];
    fCA15fj_iota3[_CA15fjCounter] = iotas[3];
    fCA15fj_iota4[_CA15fjCounter] = iotas[4];
    fCA15fj_iota5[_CA15fjCounter] = iotas[5];
    const std::vector<Double32_t> &iotaAngles = _XlCA15Jets->IotaAngles();
    fCA15fj_iotaAngle0[_CA15fjCounter] = iotaAngles[0];
    fCA15fj_iotaAngle1[_CA15fjCounter] = iotaAngles[1];
    fCA15fj_iotaAngle2[_CA15fjCounter] = iotaAngles[2];
    fCA15fj_iotaAngle3[_CA15fjCounter] = iotaAngles[3];
    fCA15fj_iotaAngle4[_CA15fjCounter] = iotaAngles[4];
    fCA15fj_iotaAngle5[_CA15fjCounter] = iotaAngles[5];

		new ( (*fCA15fj_Momentum)[_CA15fjCounter] ) TLorentzVector();
		const FourVectorM &mom = _XlCA15Jets->Mom();
		static_cast<TLorentzVector*>((*fCA15fj_Momentum)[_CA15fjCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());

		new ( (*fCA15fj_SoftDropMomentum)[_CA15fjCounter] ) TLorentzVector();
		const Vect4M &pMom = _XlCA15Jets->SoftDropP();
		static_cast<TLorentzVector*>((*fCA15fj_SoftDropMomentum)[_CA15fjCounter])->SetPtEtaPhiM(pMom.Pt(),pMom.Eta(),pMom.Phi(),pMom.M());

		++_CA15fjCounter;
	}
	
	fmcQuark_Momentum->Clear();
	fMCQuarks = GetObject<MCParticleCol>("MCQuarks");
	assert(fMCQuarks!=NULL);
	_mcQuarkCounter = 0;
	for (UInt_t i=0; i<std::min(fMCQuarks->GetEntries(),(UInt_t)nMaxMCQuarks); ++i) {
		const MCParticle * _MCQuarks = fMCQuarks->At(i);
    if (_MCQuarks->HasDaughter(_MCQuarks->PdgId(),kTRUE))
      continue;
    int shortStatusFlag = 0;
    for (int iF=0; iF!=15; ++iF) {
    	if (_MCQuarks->StatusFlag(iF))
    		shortStatusFlag |= 1<<iF;
    }
		fmcQuark_StatusFlag[_mcQuarkCounter]=shortStatusFlag;
		fmcQuark_PdgId[_mcQuarkCounter]=_MCQuarks->PdgId();
		fmcQuark_Status[_mcQuarkCounter]=_MCQuarks->Status();
    if (TMath::Abs(fmcQuark_PdgId[_mcQuarkCounter])==6) {
      const MCParticle * mcW = _MCQuarks->FindDaughter(24);
      const MCParticle * mcB = _MCQuarks->FindDaughter(5);
      if (mcW==NULL || mcB==NULL) {
        // this shouldn't happen
        fmcQuark_IsHadronicTop[_mcQuarkCounter] = -1;
        fmcQuark_TopSize[_mcQuarkCounter] = -1;
      } else {
        Bool_t foundFinalW = kFALSE;
        int pdgIdW = mcW->PdgId();
        const MCParticle *tmpW=NULL;
        while (!foundFinalW) {
          tmpW = mcW->FindDaughter(pdgIdW,kTRUE);
          if (tmpW!=NULL)  mcW = tmpW;
          else             foundFinalW = kTRUE;
        }
      	fmcQuark_TopSize[_mcQuarkCounter] = TMath::Max(MathUtils::DeltaR2(mcB,_MCQuarks),
      																								 TMath::Max(MathUtils::DeltaR2(mcW->Daughter(0),_MCQuarks),
      																														MathUtils::DeltaR2(mcW->Daughter(1),_MCQuarks)));
        if (mcW->Daughter(0)->PdgId()<6 && mcW->Daughter(1)->PdgId()<6) 
          fmcQuark_IsHadronicTop[_mcQuarkCounter] = 1;
        else 
          fmcQuark_IsHadronicTop[_mcQuarkCounter] = 0;
      }
    } else {
    	fmcQuark_TopSize[_mcQuarkCounter] = 0;	
      fmcQuark_IsHadronicTop[_mcQuarkCounter] = 0;
    }
		if (_MCQuarks->HasMother())
			fmcQuark_MotherPdgId[_mcQuarkCounter] = _MCQuarks->DistinctMother()->PdgId();
		else
			fmcQuark_MotherPdgId[_mcQuarkCounter] = 0;
  	new ( (*fmcQuark_Momentum)[_mcQuarkCounter] ) TLorentzVector();
		const FourVectorM &mom = _MCQuarks->Mom();
		static_cast<TLorentzVector*>((*fmcQuark_Momentum)[_mcQuarkCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
		++_mcQuarkCounter;
	}

	fgenJet_Momentum->Clear();
	fAKT4GenJets = GetObject<GenJetCol>("AKT4GenJets");
	assert(fAKT4GenJets!=NULL);
	_genJetCounter = 0;
	for (UInt_t i=0; i<std::min(fAKT4GenJets->GetEntries(),(UInt_t)nMaxAKT4GenJets); ++i) {
		const GenJet * _AKT4GenJets = fAKT4GenJets->At(i);
		const FourVectorM &mom = _AKT4GenJets->Mom();
    if (mom.Pt() < 200)
      continue; // kill anything too soft
		new ( (*fgenJet_Momentum)[_genJetCounter] ) TLorentzVector();
		static_cast<TLorentzVector*>((*fgenJet_Momentum)[_genJetCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
		++_genJetCounter;
	}
  // STOP 3
  fTree->Fill();
  return;
}

//--------------------------------------------------------------------------------------------------
void topTaggingNtuple_v4::SlaveBegin()
{
  fFile = new TFile(fFilePath,"RECREATE");
  fTree = new TTree("events","topTaggingNtuple_v4");
  fTree->SetMaxVirtualSize(5000000);
		fTree->Branch("_AK8fjCounter",&_AK8fjCounter,"_AK8fjCounter/I");
		fTree->Branch("AK8fj_nTracks",fAK8fj_nTracks,"AK8fj_nTracks[_AK8fjCounter]/I");
		fTree->Branch("AK8fj_firstTrack",fAK8fj_firstTrack,"AK8fj_firstTrack[_AK8fjCounter]/I");
		fTree->Branch("AK8fj_trackCounter",&AK8fj_trackCounter,"AK8fj_trackCounter/I");
		fTree->Branch("AK8fj_nSVs",fAK8fj_nSVs,"AK8fj_nSVs[_AK8fjCounter]/I");
		fTree->Branch("AK8fj_firstSV",fAK8fj_firstSV,"AK8fj_firstSV[_AK8fjCounter]/I");
		fTree->Branch("AK8fj_SVCounter",&AK8fj_SVCounter,"AK8fj_SVCounter/I");
		fTree->Branch("AK8fj_nSoftElectrons",fAK8fj_nSoftElectrons,"AK8fj_nSoftElectrons[_AK8fjCounter]/I");
		fTree->Branch("AK8fj_firstSoftElectron",fAK8fj_firstSoftElectron,"AK8fj_firstSoftElectron[_AK8fjCounter]/I");
		fTree->Branch("AK8fj_softElectronCounter",&AK8fj_softElectronCounter,"AK8fj_softElectronCounter/I");
		fTree->Branch("AK8fj_nSoftMuons",fAK8fj_nSoftMuons,"AK8fj_nSoftMuons[_AK8fjCounter]/I");
		fTree->Branch("AK8fj_firstSoftMuon",fAK8fj_firstSoftMuon,"AK8fj_firstSoftMuon[_AK8fjCounter]/I");
		fTree->Branch("AK8fj_softMuonCounter",&AK8fj_softMuonCounter,"AK8fj_softMuonCounter/I");
		fTree->Branch("AK8fj_MassSDb0",fAK8fj_MassSDb0,"AK8fj_MassSDb0[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_MassSDb1",fAK8fj_MassSDb1,"AK8fj_MassSDb1[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_MassSDb2",fAK8fj_MassSDb2,"AK8fj_MassSDb2[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_MassSDbm1",fAK8fj_MassSDbm1,"AK8fj_MassSDbm1[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iota0",fAK8fj_iota0,"AK8fj_iota0[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iota1",fAK8fj_iota1,"AK8fj_iota1[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iota2",fAK8fj_iota2,"AK8fj_iota2[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iota3",fAK8fj_iota3,"AK8fj_iota3[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iota4",fAK8fj_iota4,"AK8fj_iota4[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iota5",fAK8fj_iota5,"AK8fj_iota5[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iotaAngle0",fAK8fj_iotaAngle0,"AK8fj_iotaAngle0[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iotaAngle1",fAK8fj_iotaAngle1,"AK8fj_iotaAngle1[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iotaAngle2",fAK8fj_iotaAngle2,"AK8fj_iotaAngle2[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iotaAngle3",fAK8fj_iotaAngle3,"AK8fj_iotaAngle3[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iotaAngle4",fAK8fj_iotaAngle4,"AK8fj_iotaAngle4[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_iotaAngle5",fAK8fj_iotaAngle5,"AK8fj_iotaAngle5[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_Tau1",fAK8fj_Tau1,"AK8fj_Tau1[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_Tau2",fAK8fj_Tau2,"AK8fj_Tau2[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_Tau3",fAK8fj_Tau3,"AK8fj_Tau3[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_Tau4",fAK8fj_Tau4,"AK8fj_Tau4[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_Tau1IVF",fAK8fj_Tau1IVF,"AK8fj_Tau1IVF[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_Tau2IVF",fAK8fj_Tau2IVF,"AK8fj_Tau2IVF[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_Tau3IVF",fAK8fj_Tau3IVF,"AK8fj_Tau3IVF[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_Charge",fAK8fj_Charge,"AK8fj_Charge[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_tauDot",fAK8fj_tauDot,"AK8fj_tauDot[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_zRatio",fAK8fj_zRatio,"AK8fj_zRatio[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_QJetVol",fAK8fj_QJetVol,"AK8fj_QJetVol[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_QGTag",fAK8fj_QGTag,"AK8fj_QGTag[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_MassSoftDrop",fAK8fj_MassSoftDrop,"AK8fj_MassSoftDrop[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_MassPruned",fAK8fj_MassPruned,"AK8fj_MassPruned[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_MassTrimmed",fAK8fj_MassTrimmed,"AK8fj_MassTrimmed[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_chi",fAK8fj_chi,"AK8fj_chi[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_nMicrojets",fAK8fj_nMicrojets,"AK8fj_nMicrojets[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_subJetBTag0",fAK8fj_subJetBTag0,"AK8fj_subJetBTag0[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_subJetBTag1",fAK8fj_subJetBTag1,"AK8fj_subJetBTag1[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_nSubJets",fAK8fj_nSubJets,"AK8fj_nSubJets[_AK8fjCounter]/I");
		fTree->Branch("AK8fj_sjQGTag0",fAK8fj_sjQGTag0,"AK8fj_sjQGTag0[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_sjQGTag1",fAK8fj_sjQGTag1,"AK8fj_sjQGTag1[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_sjQGTag2",fAK8fj_sjQGTag2,"AK8fj_sjQGTag2[_AK8fjCounter]/F");
		fTree->Branch("AK8fj_Momentum","TClonesArray",&fAK8fj_Momentum,128000, 0);
		fTree->Branch("AK8fj_SoftDropMomentum","TClonesArray",&fAK8fj_SoftDropMomentum,128000, 0);
		fTree->Branch("AK8fj_track_pt",fAK8fj_track_pt,"AK8fj_track_pt[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_eta",fAK8fj_track_eta,"AK8fj_track_eta[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_phi",fAK8fj_track_phi,"AK8fj_track_phi[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_length",fAK8fj_track_length,"AK8fj_track_length[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_dist",fAK8fj_track_dist,"AK8fj_track_dist[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_dxy",fAK8fj_track_dxy,"AK8fj_track_dxy[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_dz",fAK8fj_track_dz,"AK8fj_track_dz[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_IP2D",fAK8fj_track_IP2D,"AK8fj_track_IP2D[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_IP2Dsig",fAK8fj_track_IP2Dsig,"AK8fj_track_IP2Dsig[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_IP",fAK8fj_track_IP,"AK8fj_track_IP[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_IPsig",fAK8fj_track_IPsig,"AK8fj_track_IPsig[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_IP2Derr",fAK8fj_track_IP2Derr,"AK8fj_track_IP2Derr[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_IPerr",fAK8fj_track_IPerr,"AK8fj_track_IPerr[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_prob",fAK8fj_track_prob,"AK8fj_track_prob[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_PVWeight",fAK8fj_track_PVWeight,"AK8fj_track_PVWeight[AK8fj_trackCounter]/F");
		fTree->Branch("AK8fj_track_SVWeight",fAK8fj_track_SVWeight,"AK8fj_track_SVWeight[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_track_PV",fAK8fj_track_PV,"AK8fj_track_PV[AK8fj_trackCounter]/I");
		fTree->Branch("AK8fj_track_fromSV",fAK8fj_track_fromSV,"AK8fj_track_fromSV[AK8fj_SVCounter]/I");
		fTree->Branch("AK8fj_track_SV",fAK8fj_track_SV,"AK8fj_track_SV[AK8fj_SVCounter]/I");
		fTree->Branch("AK8fj_SV_nTrk",fAK8fj_SV_nTrk,"AK8fj_SV_nTrk[AK8fj_SVCounter]/I");
		fTree->Branch("AK8fj_SV_flight",fAK8fj_SV_flight,"AK8fj_SV_flight[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_flightErr",fAK8fj_SV_flightErr,"AK8fj_SV_flightErr[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_deltaRJet",fAK8fj_SV_deltaRJet,"AK8fj_SV_deltaRJet[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_deltaRSumJet",fAK8fj_SV_deltaRSumJet,"AK8fj_SV_deltaRSumJet[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_deltaRSumDir",fAK8fj_SV_deltaRSumDir,"AK8fj_SV_deltaRSumDir[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_flight2D",fAK8fj_SV_flight2D,"AK8fj_SV_flight2D[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_flight2DErr",fAK8fj_SV_flight2DErr,"AK8fj_SV_flight2DErr[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_totCharge",fAK8fj_SV_totCharge,"AK8fj_SV_totCharge[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_vtxDistJetAxis",fAK8fj_SV_vtxDistJetAxis,"AK8fj_SV_vtxDistJetAxis[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_mass",fAK8fj_SV_mass,"AK8fj_SV_mass[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_energyRatio",fAK8fj_SV_energyRatio,"AK8fj_SV_energyRatio[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_pt",fAK8fj_SV_pt,"AK8fj_SV_pt[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_eta",fAK8fj_SV_eta,"AK8fj_SV_eta[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_phi",fAK8fj_SV_phi,"AK8fj_SV_phi[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_dirX",fAK8fj_SV_dirX,"AK8fj_SV_dirX[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_dirY",fAK8fj_SV_dirY,"AK8fj_SV_dirY[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_SV_dirZ",fAK8fj_SV_dirZ,"AK8fj_SV_dirZ[AK8fj_SVCounter]/F");
		fTree->Branch("AK8fj_softElectron_pt",fAK8fj_softElectron_pt,"AK8fj_softElectron_pt[AK8fj_softElectronCounter]/F");
		fTree->Branch("AK8fj_softElectron_eta",fAK8fj_softElectron_eta,"AK8fj_softElectron_eta[AK8fj_softElectronCounter]/F");
		fTree->Branch("AK8fj_softElectron_phi",fAK8fj_softElectron_phi,"AK8fj_softElectron_phi[AK8fj_softElectronCounter]/F");
		fTree->Branch("AK8fj_softElectron_ptRel",fAK8fj_softElectron_ptRel,"AK8fj_softElectron_ptRel[AK8fj_softElectronCounter]/F");
		fTree->Branch("AK8fj_softElectron_ratio",fAK8fj_softElectron_ratio,"AK8fj_softElectron_ratio[AK8fj_softElectronCounter]/F");
		fTree->Branch("AK8fj_softElectron_ratioRel",fAK8fj_softElectron_ratioRel,"AK8fj_softElectron_ratioRel[AK8fj_softElectronCounter]/F");
		fTree->Branch("AK8fj_softElectron_IP",fAK8fj_softElectron_IP,"AK8fj_softElectron_IP[AK8fj_softElectronCounter]/F");
		fTree->Branch("AK8fj_softElectron_IP2D",fAK8fj_softElectron_IP2D,"AK8fj_softElectron_IP2D[AK8fj_softElectronCounter]/F");
		fTree->Branch("AK8fj_softMuon_pt",fAK8fj_softMuon_pt,"AK8fj_softMuon_pt[AK8fj_softMuonCounter]/F");
		fTree->Branch("AK8fj_softMuon_eta",fAK8fj_softMuon_eta,"AK8fj_softMuon_eta[AK8fj_softMuonCounter]/F");
		fTree->Branch("AK8fj_softMuon_phi",fAK8fj_softMuon_phi,"AK8fj_softMuon_phi[AK8fj_softMuonCounter]/F");
		fTree->Branch("AK8fj_softMuon_ptRel",fAK8fj_softMuon_ptRel,"AK8fj_softMuon_ptRel[AK8fj_softMuonCounter]/F");
		fTree->Branch("AK8fj_softMuon_ratio",fAK8fj_softMuon_ratio,"AK8fj_softMuon_ratio[AK8fj_softMuonCounter]/F");
		fTree->Branch("AK8fj_softMuon_ratioRel",fAK8fj_softMuon_ratioRel,"AK8fj_softMuon_ratioRel[AK8fj_softMuonCounter]/F");
		fTree->Branch("AK8fj_softMuon_IP",fAK8fj_softMuon_IP,"AK8fj_softMuon_IP[AK8fj_softMuonCounter]/F");
		fTree->Branch("AK8fj_softMuon_IP2D",fAK8fj_softMuon_IP2D,"AK8fj_softMuon_IP2D[AK8fj_softMuonCounter]/F");
		fTree->Branch("_mcQuarkCounter",&_mcQuarkCounter,"_mcQuarkCounter/I");
		fTree->Branch("mcQuark_Momentum","TClonesArray",&fmcQuark_Momentum,128000, 0);
		fTree->Branch("mcQuark_PdgId",fmcQuark_PdgId,"mcQuark_PdgId[_mcQuarkCounter]/I");
		fTree->Branch("mcQuark_Status",fmcQuark_Status,"mcQuark_Status[_mcQuarkCounter]/I");
		fTree->Branch("mcQuark_StatusFlag",fmcQuark_StatusFlag,"mcQuark_StatusFlag[_mcQuarkCounter]/I");
		fTree->Branch("mcQuark_MotherPdgId",fmcQuark_MotherPdgId,"mcQuark_MotherPdgId[_mcQuarkCounter]/I");
		fTree->Branch("mcQuark_IsHadronicTop",fmcQuark_IsHadronicTop,"mcQuark_IsHadronicTop[_mcQuarkCounter]/I");
		fTree->Branch("mcQuark_TopSize",fmcQuark_TopSize,"mcQuark_TopSize[_mcQuarkCounter]/F");
		fTree->Branch("_CA15fjCounter",&_CA15fjCounter,"_CA15fjCounter/I");
		fTree->Branch("CA15fj_nTracks",fCA15fj_nTracks,"CA15fj_nTracks[_CA15fjCounter]/I");
		fTree->Branch("CA15fj_firstTrack",fCA15fj_firstTrack,"CA15fj_firstTrack[_CA15fjCounter]/I");
		fTree->Branch("CA15fj_trackCounter",&CA15fj_trackCounter,"CA15fj_trackCounter/I");
		fTree->Branch("CA15fj_nSVs",fCA15fj_nSVs,"CA15fj_nSVs[_CA15fjCounter]/I");
		fTree->Branch("CA15fj_firstSV",fCA15fj_firstSV,"CA15fj_firstSV[_CA15fjCounter]/I");
		fTree->Branch("CA15fj_SVCounter",&CA15fj_SVCounter,"CA15fj_SVCounter/I");
		fTree->Branch("CA15fj_nSoftElectrons",fCA15fj_nSoftElectrons,"CA15fj_nSoftElectrons[_CA15fjCounter]/I");
		fTree->Branch("CA15fj_firstSoftElectron",fCA15fj_firstSoftElectron,"CA15fj_firstSoftElectron[_CA15fjCounter]/I");
		fTree->Branch("CA15fj_softElectronCounter",&CA15fj_softElectronCounter,"CA15fj_softElectronCounter/I");
		fTree->Branch("CA15fj_nSoftMuons",fCA15fj_nSoftMuons,"CA15fj_nSoftMuons[_CA15fjCounter]/I");
		fTree->Branch("CA15fj_firstSoftMuon",fCA15fj_firstSoftMuon,"CA15fj_firstSoftMuon[_CA15fjCounter]/I");
		fTree->Branch("CA15fj_softMuonCounter",&CA15fj_softMuonCounter,"CA15fj_softMuonCounter/I");
		fTree->Branch("CA15fj_MassSDb0",fCA15fj_MassSDb0,"CA15fj_MassSDb0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassSDb1",fCA15fj_MassSDb1,"CA15fj_MassSDb1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassSDb2",fCA15fj_MassSDb2,"CA15fj_MassSDb2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassSDbm1",fCA15fj_MassSDbm1,"CA15fj_MassSDbm1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota0",fCA15fj_iota0,"CA15fj_iota0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota1",fCA15fj_iota1,"CA15fj_iota1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota2",fCA15fj_iota2,"CA15fj_iota2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota3",fCA15fj_iota3,"CA15fj_iota3[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota4",fCA15fj_iota4,"CA15fj_iota4[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota5",fCA15fj_iota5,"CA15fj_iota5[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaAngle0",fCA15fj_iotaAngle0,"CA15fj_iotaAngle0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaAngle1",fCA15fj_iotaAngle1,"CA15fj_iotaAngle1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaAngle2",fCA15fj_iotaAngle2,"CA15fj_iotaAngle2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaAngle3",fCA15fj_iotaAngle3,"CA15fj_iotaAngle3[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaAngle4",fCA15fj_iotaAngle4,"CA15fj_iotaAngle4[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaAngle5",fCA15fj_iotaAngle5,"CA15fj_iotaAngle5[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau1",fCA15fj_Tau1,"CA15fj_Tau1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau2",fCA15fj_Tau2,"CA15fj_Tau2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau3",fCA15fj_Tau3,"CA15fj_Tau3[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau4",fCA15fj_Tau4,"CA15fj_Tau4[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau1IVF",fCA15fj_Tau1IVF,"CA15fj_Tau1IVF[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau2IVF",fCA15fj_Tau2IVF,"CA15fj_Tau2IVF[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau3IVF",fCA15fj_Tau3IVF,"CA15fj_Tau3IVF[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Charge",fCA15fj_Charge,"CA15fj_Charge[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_tauDot",fCA15fj_tauDot,"CA15fj_tauDot[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_zRatio",fCA15fj_zRatio,"CA15fj_zRatio[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_QJetVol",fCA15fj_QJetVol,"CA15fj_QJetVol[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_QGTag",fCA15fj_QGTag,"CA15fj_QGTag[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassSoftDrop",fCA15fj_MassSoftDrop,"CA15fj_MassSoftDrop[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassPruned",fCA15fj_MassPruned,"CA15fj_MassPruned[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassTrimmed",fCA15fj_MassTrimmed,"CA15fj_MassTrimmed[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_chi",fCA15fj_chi,"CA15fj_chi[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_nMicrojets",fCA15fj_nMicrojets,"CA15fj_nMicrojets[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_subJetBTag0",fCA15fj_subJetBTag0,"CA15fj_subJetBTag0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_subJetBTag1",fCA15fj_subJetBTag1,"CA15fj_subJetBTag1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_nSubJets",fCA15fj_nSubJets,"CA15fj_nSubJets[_CA15fjCounter]/I");
		fTree->Branch("CA15fj_sjQGTag0",fCA15fj_sjQGTag0,"CA15fj_sjQGTag0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_sjQGTag1",fCA15fj_sjQGTag1,"CA15fj_sjQGTag1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_sjQGTag2",fCA15fj_sjQGTag2,"CA15fj_sjQGTag2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Momentum","TClonesArray",&fCA15fj_Momentum,128000, 0);
		fTree->Branch("CA15fj_SoftDropMomentum","TClonesArray",&fCA15fj_SoftDropMomentum,128000, 0);
		fTree->Branch("CA15fj_track_pt",fCA15fj_track_pt,"CA15fj_track_pt[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_eta",fCA15fj_track_eta,"CA15fj_track_eta[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_phi",fCA15fj_track_phi,"CA15fj_track_phi[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_length",fCA15fj_track_length,"CA15fj_track_length[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_dist",fCA15fj_track_dist,"CA15fj_track_dist[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_dxy",fCA15fj_track_dxy,"CA15fj_track_dxy[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_dz",fCA15fj_track_dz,"CA15fj_track_dz[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_IP2D",fCA15fj_track_IP2D,"CA15fj_track_IP2D[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_IP2Dsig",fCA15fj_track_IP2Dsig,"CA15fj_track_IP2Dsig[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_IP",fCA15fj_track_IP,"CA15fj_track_IP[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_IPsig",fCA15fj_track_IPsig,"CA15fj_track_IPsig[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_IP2Derr",fCA15fj_track_IP2Derr,"CA15fj_track_IP2Derr[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_IPerr",fCA15fj_track_IPerr,"CA15fj_track_IPerr[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_prob",fCA15fj_track_prob,"CA15fj_track_prob[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_PVWeight",fCA15fj_track_PVWeight,"CA15fj_track_PVWeight[CA15fj_trackCounter]/F");
		fTree->Branch("CA15fj_track_SVWeight",fCA15fj_track_SVWeight,"CA15fj_track_SVWeight[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_track_PV",fCA15fj_track_PV,"CA15fj_track_PV[CA15fj_trackCounter]/I");
		fTree->Branch("CA15fj_track_fromSV",fCA15fj_track_fromSV,"CA15fj_track_fromSV[CA15fj_SVCounter]/I");
		fTree->Branch("CA15fj_track_SV",fCA15fj_track_SV,"CA15fj_track_SV[CA15fj_SVCounter]/I");
		fTree->Branch("CA15fj_SV_nTrk",fCA15fj_SV_nTrk,"CA15fj_SV_nTrk[CA15fj_SVCounter]/I");
		fTree->Branch("CA15fj_SV_flight",fCA15fj_SV_flight,"CA15fj_SV_flight[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_flightErr",fCA15fj_SV_flightErr,"CA15fj_SV_flightErr[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_deltaRJet",fCA15fj_SV_deltaRJet,"CA15fj_SV_deltaRJet[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_deltaRSumJet",fCA15fj_SV_deltaRSumJet,"CA15fj_SV_deltaRSumJet[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_deltaRSumDir",fCA15fj_SV_deltaRSumDir,"CA15fj_SV_deltaRSumDir[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_flight2D",fCA15fj_SV_flight2D,"CA15fj_SV_flight2D[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_flight2DErr",fCA15fj_SV_flight2DErr,"CA15fj_SV_flight2DErr[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_totCharge",fCA15fj_SV_totCharge,"CA15fj_SV_totCharge[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_vtxDistJetAxis",fCA15fj_SV_vtxDistJetAxis,"CA15fj_SV_vtxDistJetAxis[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_mass",fCA15fj_SV_mass,"CA15fj_SV_mass[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_energyRatio",fCA15fj_SV_energyRatio,"CA15fj_SV_energyRatio[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_pt",fCA15fj_SV_pt,"CA15fj_SV_pt[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_eta",fCA15fj_SV_eta,"CA15fj_SV_eta[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_phi",fCA15fj_SV_phi,"CA15fj_SV_phi[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_dirX",fCA15fj_SV_dirX,"CA15fj_SV_dirX[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_dirY",fCA15fj_SV_dirY,"CA15fj_SV_dirY[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_SV_dirZ",fCA15fj_SV_dirZ,"CA15fj_SV_dirZ[CA15fj_SVCounter]/F");
		fTree->Branch("CA15fj_softElectron_pt",fCA15fj_softElectron_pt,"CA15fj_softElectron_pt[CA15fj_softElectronCounter]/F");
		fTree->Branch("CA15fj_softElectron_eta",fCA15fj_softElectron_eta,"CA15fj_softElectron_eta[CA15fj_softElectronCounter]/F");
		fTree->Branch("CA15fj_softElectron_phi",fCA15fj_softElectron_phi,"CA15fj_softElectron_phi[CA15fj_softElectronCounter]/F");
		fTree->Branch("CA15fj_softElectron_ptRel",fCA15fj_softElectron_ptRel,"CA15fj_softElectron_ptRel[CA15fj_softElectronCounter]/F");
		fTree->Branch("CA15fj_softElectron_ratio",fCA15fj_softElectron_ratio,"CA15fj_softElectron_ratio[CA15fj_softElectronCounter]/F");
		fTree->Branch("CA15fj_softElectron_ratioRel",fCA15fj_softElectron_ratioRel,"CA15fj_softElectron_ratioRel[CA15fj_softElectronCounter]/F");
		fTree->Branch("CA15fj_softElectron_IP",fCA15fj_softElectron_IP,"CA15fj_softElectron_IP[CA15fj_softElectronCounter]/F");
		fTree->Branch("CA15fj_softElectron_IP2D",fCA15fj_softElectron_IP2D,"CA15fj_softElectron_IP2D[CA15fj_softElectronCounter]/F");
		fTree->Branch("CA15fj_softMuon_pt",fCA15fj_softMuon_pt,"CA15fj_softMuon_pt[CA15fj_softMuonCounter]/F");
		fTree->Branch("CA15fj_softMuon_eta",fCA15fj_softMuon_eta,"CA15fj_softMuon_eta[CA15fj_softMuonCounter]/F");
		fTree->Branch("CA15fj_softMuon_phi",fCA15fj_softMuon_phi,"CA15fj_softMuon_phi[CA15fj_softMuonCounter]/F");
		fTree->Branch("CA15fj_softMuon_ptRel",fCA15fj_softMuon_ptRel,"CA15fj_softMuon_ptRel[CA15fj_softMuonCounter]/F");
		fTree->Branch("CA15fj_softMuon_ratio",fCA15fj_softMuon_ratio,"CA15fj_softMuon_ratio[CA15fj_softMuonCounter]/F");
		fTree->Branch("CA15fj_softMuon_ratioRel",fCA15fj_softMuon_ratioRel,"CA15fj_softMuon_ratioRel[CA15fj_softMuonCounter]/F");
		fTree->Branch("CA15fj_softMuon_IP",fCA15fj_softMuon_IP,"CA15fj_softMuon_IP[CA15fj_softMuonCounter]/F");
		fTree->Branch("CA15fj_softMuon_IP2D",fCA15fj_softMuon_IP2D,"CA15fj_softMuon_IP2D[CA15fj_softMuonCounter]/F");
		fTree->Branch("_genJetCounter",&_genJetCounter,"_genJetCounter/I");
		fTree->Branch("genJet_Momentum","TClonesArray",&fgenJet_Momentum,128000, 0);
  // STOP 4
  //  AddOutput(fTree);
}

//--------------------------------------------------------------------------------------------------
void topTaggingNtuple_v4::SlaveTerminate()
{
  // STOP 5
  fFile->WriteTObject(fTree,fTree->GetName());
//  fFile->Close();
}
