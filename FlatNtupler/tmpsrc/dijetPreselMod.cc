#include <algorithm>
#include "MitPanda/FlatNtupler/interface/dijetPreselMod.h"
#include "TLorentzVector.h"
#include "MitCommon/DataFormats/interface/Types.h"
#include "MitCommon/DataFormats/interface/Vect4M.h"

using namespace mithep;

ClassImp(mithep::dijetPreselMod)

//--------------------------------------------------------------------------------------------------
dijetPreselMod::dijetPreselMod(const char *name, const char *title):
  BaseMod(name,title)
{
	fPuppiType1MET = NULL;
	fmet_Momentum = new TClonesArray("TLorentzVector",2);
	fSelectedJets = NULL;
	fjet_Momentum = new TClonesArray("TLorentzVector",20);
  // STOP 1
}

dijetPreselMod::~dijetPreselMod()
{
	delete fmet_Momentum;
	delete fjet_Momentum;
  // STOP 2
}

//--------------------------------------------------------------------------------------------------
void dijetPreselMod::Process()
{
	fPuppiType1MET = GetObject<PFMetCol>("PuppiType1MET");
	assert(fPuppiType1MET!=NULL);
	_metCounter = 0;
	for (UInt_t i=0; i<std::min(fPuppiType1MET->GetEntries(),(UInt_t)nMaxPuppiType1MET); ++i) {
		const PFMet * _PuppiType1MET = fPuppiType1MET->At(i);
		const FourVectorM &mom = _PuppiType1MET->Mom();
    if (mom.Pt()>50) {
      SkipEvent();
      return;
    } else {
      break;
    }
	}


	fSelectedJets = GetObject<JetCol>("SelectedJets");
	assert(fSelectedJets!=NULL);
	_jetCounter = 0;
  if (fSelectedJets->GetEntries()<2) {
    SkipEvent();
    return;
  }
	for (UInt_t i=0; i<2; ++i) {
		const PFJet * _SelectedJets = dynamic_cast<const PFJet*>(fSelectedJets->At(i));
		const FourVectorM &mom = _SelectedJets->Mom();
    if (mom.Pt()<175) {
      SkipEvent();
      return;
    }
   ++_jetCounter; 
  }
  // STOP 3
  return;
}

//--------------------------------------------------------------------------------------------------
void dijetPreselMod::SlaveBegin()
{
}

//--------------------------------------------------------------------------------------------------
void dijetPreselMod::SlaveTerminate()
{
}
