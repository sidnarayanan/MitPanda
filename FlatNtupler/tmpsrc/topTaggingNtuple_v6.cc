#include <algorithm>
#include "MitPanda/FlatNtupler/interface/topTaggingNtuple_v6.h"
#include "TLorentzVector.h"
#include "MitCommon/DataFormats/interface/Types.h"
#include "MitCommon/DataFormats/interface/Vect4M.h"
#include "MitAna/DataTree/interface/MCParticleCol.h"
#include "MitCommon/MathTools/interface/MathUtils.h"


using namespace mithep;

ClassImp(mithep::topTaggingNtuple_v6)

//--------------------------------------------------------------------------------------------------
topTaggingNtuple_v6::topTaggingNtuple_v6(const char *name, const char *title)
{
	fMCQuarks = NULL;
	fmcQuark_Momentum = new TClonesArray("TLorentzVector",20);
	fXlCA15Jets = NULL;
	fCA15fj_Momentum = new TClonesArray("TLorentzVector",20);
	fCA15fj_SubJet0Momentum = new TClonesArray("TLorentzVector",20);
	fCA15fj_SubJet1Momentum = new TClonesArray("TLorentzVector",20);
	fCA15fj_SubJet2Momentum = new TClonesArray("TLorentzVector",20);
	fCA15fj_SubJet3Momentum = new TClonesArray("TLorentzVector",20);
	fCA15fj_SoftDropMomentum = new TClonesArray("TLorentzVector",20);
	fAKT4GenJets = NULL;
//	fgenJet_Momentum = new TClonesArray("TLorentzVector",20);
  // STOP 1
}

topTaggingNtuple_v6::~topTaggingNtuple_v6()
{
	delete fCA15fj_Momentum;
	delete fCA15fj_SubJet0Momentum;
	delete fCA15fj_SubJet1Momentum;
	delete fCA15fj_SubJet2Momentum;
	delete fCA15fj_SubJet3Momentum;
	delete fCA15fj_SoftDropMomentum;
	delete fmcQuark_Momentum;
//	delete fgenJet_Momentum;
  // STOP 2
  fFile->Close();
  fFile->Delete();
}

//--------------------------------------------------------------------------------------------------
void topTaggingNtuple_v6::Process()
{

	fCA15fj_Momentum->Clear();
	fCA15fj_SoftDropMomentum->Clear();
	fXlCA15Jets = GetObject<XXLFatJetCol>("XlCA15Jets");
	assert(fXlCA15Jets!=NULL);
#if DOBTAG
	CA15fj_trackCounter = 0;
	CA15fj_SVCounter = 0;
	CA15fj_softElectronCounter = 0;
	CA15fj_softMuonCounter = 0;
#endif
	_CA15fjCounter = 0;
	for (UInt_t i=0; i<std::min(fXlCA15Jets->GetEntries(),(UInt_t)nMaxXlCA15Jets); ++i) {
		const XXLFatJet * _XlCA15Jets = fXlCA15Jets->At(i);
		fCA15fj_JEC[_CA15fjCounter]=_XlCA15Jets->JEC();
		fCA15fj_Tau1[_CA15fjCounter]=_XlCA15Jets->Tau1();
		fCA15fj_Tau2[_CA15fjCounter]=_XlCA15Jets->Tau2();
		fCA15fj_Tau3[_CA15fjCounter]=_XlCA15Jets->Tau3();
		fCA15fj_Tau4[_CA15fjCounter]=_XlCA15Jets->Tau4();
#if DOBTAG
		fCA15fj_Tau1IVF[_CA15fjCounter]=_XlCA15Jets->Tau1IVF();
		fCA15fj_Tau2IVF[_CA15fjCounter]=_XlCA15Jets->Tau2IVF();
		fCA15fj_Tau3IVF[_CA15fjCounter]=_XlCA15Jets->Tau3IVF();
		fCA15fj_Charge[_CA15fjCounter]=_XlCA15Jets->Charge();
		fCA15fj_tauDot[_CA15fjCounter]=_XlCA15Jets->tauDot();
		fCA15fj_zRatio[_CA15fjCounter]=_XlCA15Jets->zRatio();
#endif
		fCA15fj_QJetVol[_CA15fjCounter]=_XlCA15Jets->QJetVol();
		fCA15fj_QGTag[_CA15fjCounter]=_XlCA15Jets->QGTag();
		fCA15fj_MassSoftDrop[_CA15fjCounter]=_XlCA15Jets->MassSDb1();
		fCA15fj_MassPruned[_CA15fjCounter]=_XlCA15Jets->MassPruned();
		fCA15fj_MassTrimmed[_CA15fjCounter]=_XlCA15Jets->MassTrimmed();
		fCA15fj_chi[_CA15fjCounter]=_XlCA15Jets->chi();
		fCA15fj_nMicrojets[_CA15fjCounter]=_XlCA15Jets->nMicrojets();
		fCA15fj_GroomedTau1[_CA15fjCounter]=_XlCA15Jets->GroomedTau1();
		fCA15fj_GroomedTau2[_CA15fjCounter]=_XlCA15Jets->GroomedTau2();
		fCA15fj_GroomedTau3[_CA15fjCounter]=_XlCA15Jets->GroomedTau3();
		fCA15fj_MassCMSTT[_CA15fjCounter]=_XlCA15Jets->MassCMSTT();
		fCA15fj_MassHTT[_CA15fjCounter]=_XlCA15Jets->MassHTT();
		fCA15fj_FMin[_CA15fjCounter]=_XlCA15Jets->FMin();
		fCA15fj_ROpt[_CA15fjCounter]=_XlCA15Jets->ROpt();
		fCA15fj_ROptCalc[_CA15fjCounter]=_XlCA15Jets->ROptCalc();
		fCA15fj_PtROptCalc[_CA15fjCounter]=_XlCA15Jets->PtROptCalc();
		std::vector<float> const &subjetBtags = _XlCA15Jets->GetSubJetBtags();
		float maxBtag = -10;
		float secondMaxBtag = -10;
		for (auto & btag : subjetBtags) {
				if (btag > maxBtag) {
					secondMaxBtag = maxBtag;
					maxBtag = btag;
				} else if (btag > secondMaxBtag) {
					secondMaxBtag = btag;
				}
		}
		fCA15fj_subJetBTag0[_CA15fjCounter] = maxBtag;
		fCA15fj_subJetBTag1[_CA15fjCounter] = secondMaxBtag;

    const RefArray<XlSubJet>& fXlCA15SubJets = _XlCA15Jets->GetSubJets(XlSubJet::kSoftDrop);
    int nSubJets = fXlCA15SubJets.GetEntries();
		fCA15fj_nSubJets[_CA15fjCounter] = nSubJets; 
    
    fCA15fj_sjQGTag0[_CA15fjCounter] = -1;
    fCA15fj_sjQGTag1[_CA15fjCounter] = -1;
    fCA15fj_sjQGTag2[_CA15fjCounter] = -1;
    fCA15fj_sjQGTag3[_CA15fjCounter] = -1;
		
    new ( (*fCA15fj_SubJet0Momentum)[_CA15fjCounter] ) TLorentzVector();
    new ( (*fCA15fj_SubJet1Momentum)[_CA15fjCounter] ) TLorentzVector();
    new ( (*fCA15fj_SubJet2Momentum)[_CA15fjCounter] ) TLorentzVector();
    new ( (*fCA15fj_SubJet3Momentum)[_CA15fjCounter] ) TLorentzVector();
    
    if (nSubJets>0) {
  		const FourVectorM &mom = fXlCA15SubJets.At(0)->Mom();
	  	static_cast<TLorentzVector*>((*fCA15fj_SubJet0Momentum)[_CA15fjCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
      fCA15fj_sjQGTag0[_CA15fjCounter] = fXlCA15SubJets.At(0)->QGTag();
    }
    if (nSubJets>1) {
      const FourVectorM &mom = fXlCA15SubJets.At(1)->Mom();
	    static_cast<TLorentzVector*>((*fCA15fj_SubJet1Momentum)[_CA15fjCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
      fCA15fj_sjQGTag1[_CA15fjCounter] = fXlCA15SubJets.At(1)->QGTag();
    }
    if (nSubJets>2) { 
      const FourVectorM &mom = fXlCA15SubJets.At(2)->Mom();
  	  static_cast<TLorentzVector*>((*fCA15fj_SubJet2Momentum)[_CA15fjCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
	    fCA15fj_sjQGTag2[_CA15fjCounter] = fXlCA15SubJets.At(2)->QGTag();
    }
    if (nSubJets>3) {
      const FourVectorM &mom = fXlCA15SubJets.At(3)->Mom();
    	static_cast<TLorentzVector*>((*fCA15fj_SubJet3Momentum)[_CA15fjCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
		  fCA15fj_sjQGTag3[_CA15fjCounter] = fXlCA15SubJets.At(3)->QGTag();
    }


		fCA15fj_MassSDb0[_CA15fjCounter]=_XlCA15Jets->MassSDb0();
		fCA15fj_MassSDb1[_CA15fjCounter]=_XlCA15Jets->MassSDb1();
		fCA15fj_MassSDb2[_CA15fjCounter]=_XlCA15Jets->MassSDb2();
		fCA15fj_MassSDbm1[_CA15fjCounter]=_XlCA15Jets->MassSDbm1();
    
    const std::vector<Double32_t> &iotas = _XlCA15Jets->Iotas();
    fCA15fj_iota0[_CA15fjCounter] = iotas[0];
    fCA15fj_iota1[_CA15fjCounter] = iotas[1];
    fCA15fj_iota2[_CA15fjCounter] = iotas[2];
    fCA15fj_iota3[_CA15fjCounter] = iotas[3];
    fCA15fj_iota4[_CA15fjCounter] = iotas[4];
    fCA15fj_iota5[_CA15fjCounter] = iotas[5];
    fCA15fj_iota6[_CA15fjCounter] = iotas[6];
    fCA15fj_iota7[_CA15fjCounter] = iotas[7];
    fCA15fj_iota8[_CA15fjCounter] = iotas[8];
    fCA15fj_iota9[_CA15fjCounter] = iotas[9];
    
    const std::vector<Double32_t> &iotaDRs = _XlCA15Jets->IotaAngles();
    fCA15fj_iotaDR0[_CA15fjCounter] = iotaDRs[0];
    fCA15fj_iotaDR1[_CA15fjCounter] = iotaDRs[1];
    fCA15fj_iotaDR2[_CA15fjCounter] = iotaDRs[2];
    fCA15fj_iotaDR3[_CA15fjCounter] = iotaDRs[3];
    fCA15fj_iotaDR4[_CA15fjCounter] = iotaDRs[4];
    fCA15fj_iotaDR5[_CA15fjCounter] = iotaDRs[5];
    fCA15fj_iotaDR6[_CA15fjCounter] = iotaDRs[6];
    fCA15fj_iotaDR7[_CA15fjCounter] = iotaDRs[7];
    fCA15fj_iotaDR8[_CA15fjCounter] = iotaDRs[8];
    fCA15fj_iotaDR9[_CA15fjCounter] = iotaDRs[9];
    
    const std::vector<Double32_t> &iotaMs = _XlCA15Jets->IotaMasses();
    fCA15fj_iotaM0[_CA15fjCounter] = iotaMs[0];
    fCA15fj_iotaM1[_CA15fjCounter] = iotaMs[1];
    fCA15fj_iotaM2[_CA15fjCounter] = iotaMs[2];
    fCA15fj_iotaM3[_CA15fjCounter] = iotaMs[3];
    fCA15fj_iotaM4[_CA15fjCounter] = iotaMs[4];
    fCA15fj_iotaM5[_CA15fjCounter] = iotaMs[5];
    fCA15fj_iotaM6[_CA15fjCounter] = iotaMs[6];
    fCA15fj_iotaM7[_CA15fjCounter] = iotaMs[7];
    fCA15fj_iotaM8[_CA15fjCounter] = iotaMs[8];
    fCA15fj_iotaM9[_CA15fjCounter] = iotaMs[9];
    
		new ( (*fCA15fj_Momentum)[_CA15fjCounter] ) TLorentzVector();
		const FourVectorM &mom = _XlCA15Jets->Mom();
		static_cast<TLorentzVector*>((*fCA15fj_Momentum)[_CA15fjCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());

		new ( (*fCA15fj_SoftDropMomentum)[_CA15fjCounter] ) TLorentzVector();
		const Vect4M &pMom = _XlCA15Jets->SoftDropP();
		static_cast<TLorentzVector*>((*fCA15fj_SoftDropMomentum)[_CA15fjCounter])->SetPtEtaPhiM(pMom.Pt(),pMom.Eta(),pMom.Phi(),pMom.M());

		++_CA15fjCounter;
	}
	
	fmcQuark_Momentum->Clear();
	fMCQuarks = GetObject<MCParticleCol>("MCQuarks");
	assert(fMCQuarks!=NULL);
	_mcQuarkCounter = 0;
	for (UInt_t i=0; i<std::min(fMCQuarks->GetEntries(),(UInt_t)nMaxMCQuarks); ++i) {
		const MCParticle * _MCQuarks = fMCQuarks->At(i);
    if (_MCQuarks->HasDaughter(_MCQuarks->PdgId(),kTRUE))
      continue;
    int shortStatusFlag = 0;
    for (int iF=0; iF!=15; ++iF) {
    	if (_MCQuarks->StatusFlag(iF))
    		shortStatusFlag |= 1<<iF;
    }
		fmcQuark_StatusFlag[_mcQuarkCounter]=shortStatusFlag;
		fmcQuark_PdgId[_mcQuarkCounter]=_MCQuarks->PdgId();
		fmcQuark_Status[_mcQuarkCounter]=_MCQuarks->Status();
    if (TMath::Abs(fmcQuark_PdgId[_mcQuarkCounter])==6) {
      const MCParticle * mcW = _MCQuarks->FindDaughter(24);
      const MCParticle * mcB = _MCQuarks->FindDaughter(5);
      if (mcW==NULL || mcB==NULL) {
        // this shouldn't happen
        fmcQuark_IsHadronicTop[_mcQuarkCounter] = -1;
        fmcQuark_TopSize[_mcQuarkCounter] = -1;
      } else {
        Bool_t foundFinalW = kFALSE;
        int pdgIdW = mcW->PdgId();
        const MCParticle *tmpW=NULL;
        while (!foundFinalW) {
          tmpW = mcW->FindDaughter(pdgIdW,kTRUE);
          if (tmpW!=NULL)  mcW = tmpW;
          else             foundFinalW = kTRUE;
        }
      	fmcQuark_TopSize[_mcQuarkCounter] = TMath::Max(MathUtils::DeltaR2(mcB,_MCQuarks),
      																								 TMath::Max(MathUtils::DeltaR2(mcW->Daughter(0),_MCQuarks),
      																														MathUtils::DeltaR2(mcW->Daughter(1),_MCQuarks)));
        if (mcW->Daughter(0)->PdgId()<6 && mcW->Daughter(1)->PdgId()<6) 
          fmcQuark_IsHadronicTop[_mcQuarkCounter] = 1;
        else 
          fmcQuark_IsHadronicTop[_mcQuarkCounter] = 0;
      }
    } else {
    	fmcQuark_TopSize[_mcQuarkCounter] = 0;	
      fmcQuark_IsHadronicTop[_mcQuarkCounter] = 0;
    }
		if (_MCQuarks->HasMother())
			fmcQuark_MotherPdgId[_mcQuarkCounter] = _MCQuarks->DistinctMother()->PdgId();
		else
			fmcQuark_MotherPdgId[_mcQuarkCounter] = 0;
  	new ( (*fmcQuark_Momentum)[_mcQuarkCounter] ) TLorentzVector();
		const FourVectorM &mom = _MCQuarks->Mom();
		static_cast<TLorentzVector*>((*fmcQuark_Momentum)[_mcQuarkCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
		++_mcQuarkCounter;
	}

  /*
	fgenJet_Momentum->Clear();
	fAKT4GenJets = GetObject<GenJetCol>("AKT4GenJets");
	assert(fAKT4GenJets!=NULL);
	_genJetCounter = 0;
	for (UInt_t i=0; i<std::min(fAKT4GenJets->GetEntries(),(UInt_t)nMaxAKT4GenJets); ++i) {
		const GenJet * _AKT4GenJets = fAKT4GenJets->At(i);
		const FourVectorM &mom = _AKT4GenJets->Mom();
    if (mom.Pt() < 200)
      continue; // kill anything too soft
		new ( (*fgenJet_Momentum)[_genJetCounter] ) TLorentzVector();
		static_cast<TLorentzVector*>((*fgenJet_Momentum)[_genJetCounter])->SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
		++_genJetCounter;
	}
  */
  // STOP 3
  fTree->Fill();
  return;
}

//--------------------------------------------------------------------------------------------------
void topTaggingNtuple_v6::SlaveBegin()
{
  fFile = new TFile(fFilePath,"RECREATE");
  fTree = new TTree("events","topTaggingNtuple_v6");
  fTree->SetMaxVirtualSize(5000000);
		fTree->Branch("_mcQuarkCounter",&_mcQuarkCounter,"_mcQuarkCounter/I");
		fTree->Branch("mcQuark_Momentum","TClonesArray",&fmcQuark_Momentum,128000, 0);
		fTree->Branch("mcQuark_PdgId",fmcQuark_PdgId,"mcQuark_PdgId[_mcQuarkCounter]/I");
		fTree->Branch("mcQuark_Status",fmcQuark_Status,"mcQuark_Status[_mcQuarkCounter]/I");
		fTree->Branch("mcQuark_StatusFlag",fmcQuark_StatusFlag,"mcQuark_StatusFlag[_mcQuarkCounter]/I");
		fTree->Branch("mcQuark_MotherPdgId",fmcQuark_MotherPdgId,"mcQuark_MotherPdgId[_mcQuarkCounter]/I");
		fTree->Branch("mcQuark_IsHadronicTop",fmcQuark_IsHadronicTop,"mcQuark_IsHadronicTop[_mcQuarkCounter]/I");
		fTree->Branch("mcQuark_TopSize",fmcQuark_TopSize,"mcQuark_TopSize[_mcQuarkCounter]/F");
		fTree->Branch("_CA15fjCounter",&_CA15fjCounter,"_CA15fjCounter/I");
		fTree->Branch("CA15fj_MassSDb0",fCA15fj_MassSDb0,"CA15fj_MassSDb0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassSDb1",fCA15fj_MassSDb1,"CA15fj_MassSDb1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassSDb2",fCA15fj_MassSDb2,"CA15fj_MassSDb2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassSDbm1",fCA15fj_MassSDbm1,"CA15fj_MassSDbm1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota0",fCA15fj_iota0,"CA15fj_iota0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota1",fCA15fj_iota1,"CA15fj_iota1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota2",fCA15fj_iota2,"CA15fj_iota2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota3",fCA15fj_iota3,"CA15fj_iota3[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota4",fCA15fj_iota4,"CA15fj_iota4[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota5",fCA15fj_iota5,"CA15fj_iota5[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota6",fCA15fj_iota6,"CA15fj_iota6[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota7",fCA15fj_iota7,"CA15fj_iota7[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota8",fCA15fj_iota8,"CA15fj_iota8[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iota9",fCA15fj_iota9,"CA15fj_iota9[_CA15fjCounter]/F");

		fTree->Branch("CA15fj_iotaDR0",fCA15fj_iotaDR0,"CA15fj_iotaDR0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaDR1",fCA15fj_iotaDR1,"CA15fj_iotaDR1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaDR2",fCA15fj_iotaDR2,"CA15fj_iotaDR2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaDR3",fCA15fj_iotaDR3,"CA15fj_iotaDR3[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaDR4",fCA15fj_iotaDR4,"CA15fj_iotaDR4[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaDR5",fCA15fj_iotaDR5,"CA15fj_iotaDR5[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaDR6",fCA15fj_iotaDR6,"CA15fj_iotaDR6[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaDR7",fCA15fj_iotaDR7,"CA15fj_iotaDR7[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaDR8",fCA15fj_iotaDR8,"CA15fj_iotaDR8[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaDR9",fCA15fj_iotaDR9,"CA15fj_iotaDR9[_CA15fjCounter]/F");

		fTree->Branch("CA15fj_iotaM0",fCA15fj_iotaM0,"CA15fj_iotaM0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaM1",fCA15fj_iotaM1,"CA15fj_iotaM1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaM2",fCA15fj_iotaM2,"CA15fj_iotaM2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaM3",fCA15fj_iotaM3,"CA15fj_iotaM3[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaM4",fCA15fj_iotaM4,"CA15fj_iotaM4[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaM5",fCA15fj_iotaM5,"CA15fj_iotaM5[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaM6",fCA15fj_iotaM6,"CA15fj_iotaM6[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaM7",fCA15fj_iotaM7,"CA15fj_iotaM7[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaM8",fCA15fj_iotaM8,"CA15fj_iotaM8[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_iotaM9",fCA15fj_iotaM9,"CA15fj_iotaM9[_CA15fjCounter]/F");

		fTree->Branch("CA15fj_JEC",fCA15fj_JEC,"CA15fj_JEC[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau1",fCA15fj_Tau1,"CA15fj_Tau1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau2",fCA15fj_Tau2,"CA15fj_Tau2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau3",fCA15fj_Tau3,"CA15fj_Tau3[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Tau4",fCA15fj_Tau4,"CA15fj_Tau4[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_QJetVol",fCA15fj_QJetVol,"CA15fj_QJetVol[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_QGTag",fCA15fj_QGTag,"CA15fj_QGTag[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassSoftDrop",fCA15fj_MassSoftDrop,"CA15fj_MassSoftDrop[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassPruned",fCA15fj_MassPruned,"CA15fj_MassPruned[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassTrimmed",fCA15fj_MassTrimmed,"CA15fj_MassTrimmed[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_chi",fCA15fj_chi,"CA15fj_chi[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_nMicrojets",fCA15fj_nMicrojets,"CA15fj_nMicrojets[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_subJetBTag0",fCA15fj_subJetBTag0,"CA15fj_subJetBTag0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_subJetBTag1",fCA15fj_subJetBTag1,"CA15fj_subJetBTag1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_nSubJets",fCA15fj_nSubJets,"CA15fj_nSubJets[_CA15fjCounter]/I");
		fTree->Branch("CA15fj_sjQGTag0",fCA15fj_sjQGTag0,"CA15fj_sjQGTag0[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_sjQGTag1",fCA15fj_sjQGTag1,"CA15fj_sjQGTag1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_sjQGTag2",fCA15fj_sjQGTag2,"CA15fj_sjQGTag2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_sjQGTag3",fCA15fj_sjQGTag3,"CA15fj_sjQGTag3[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_Momentum","TClonesArray",&fCA15fj_Momentum,128000, 0);
		fTree->Branch("CA15fj_SubJet0Momentum","TClonesArray",&fCA15fj_SubJet0Momentum,128000, 0);
		fTree->Branch("CA15fj_SubJet1Momentum","TClonesArray",&fCA15fj_SubJet1Momentum,128000, 0);
		fTree->Branch("CA15fj_SubJet2Momentum","TClonesArray",&fCA15fj_SubJet2Momentum,128000, 0);
		fTree->Branch("CA15fj_SubJet3Momentum","TClonesArray",&fCA15fj_SubJet3Momentum,128000, 0);
		fTree->Branch("CA15fj_SoftDropMomentum","TClonesArray",&fCA15fj_SoftDropMomentum,128000, 0);
//		fTree->Branch("_genJetCounter",&_genJetCounter,"_genJetCounter/I");
//		fTree->Branch("genJet_Momentum","TClonesArray",&fgenJet_Momentum,128000, 0);
		fTree->Branch("CA15fj_GroomedTau1",fCA15fj_GroomedTau1,"CA15fj_GroomedTau1[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_GroomedTau2",fCA15fj_GroomedTau2,"CA15fj_GroomedTau2[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_GroomedTau3",fCA15fj_GroomedTau3,"CA15fj_GroomedTau3[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassCMSTT",fCA15fj_MassCMSTT,"CA15fj_MassCMSTT[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_MassHTT",fCA15fj_MassHTT,"CA15fj_MassHTT[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_FMin",fCA15fj_FMin,"CA15fj_FMin[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_ROpt",fCA15fj_ROpt,"CA15fj_ROpt[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_ROptCalc",fCA15fj_ROptCalc,"CA15fj_ROptCalc[_CA15fjCounter]/F");
		fTree->Branch("CA15fj_PtROptCalc",fCA15fj_PtROptCalc,"CA15fj_PtROptCalc[_CA15fjCounter]/F");
  // STOP 4
  //  AddOutput(fTree);
}

//--------------------------------------------------------------------------------------------------
void topTaggingNtuple_v6::SlaveTerminate()
{
  // STOP 5
  fFile->WriteTObject(fTree,fTree->GetName());
//  fFile->Close();
}
