#include <algorithm>
#include "MitPanda/FlatNtupler/interface/monotopPreselMod.h"
#include "TLorentzVector.h"
#include "MitCommon/DataFormats/interface/Types.h"
#include "MitCommon/DataFormats/interface/Vect4M.h"
#include "MitCommon/MathTools/interface/MathUtils.h"

using namespace mithep;

ClassImp(mithep::monotopPreselMod)

//--------------------------------------------------------------------------------------------------
monotopPreselMod::monotopPreselMod(const char *name, const char *title):
  BaseMod(name,title)
{
	floosePhotons = NULL;
	flooseMuons = NULL;
	fPuppiType1MET = NULL;
	flooseElectrons = NULL;
  // STOP 1
}

monotopPreselMod::~monotopPreselMod()
{
  // STOP 2
}

void monotopPreselMod::BeginRun() {

}

//--------------------------------------------------------------------------------------------------
void monotopPreselMod::Process()
{
  // get objects
  TLorentzVector aMom, e1Mom, e2Mom, mu1Mom, mu2Mom, met;

	floosePhotons = GetObject<PhotonCol>("loosePhotons");
	assert(floosePhotons!=NULL);
  bool aGood=false;
  if (floosePhotons->GetEntries()>0) {
		const Photon * _loosePhotons = floosePhotons->At(0);
		const FourVectorM &mom = _loosePhotons->Mom();
    if (mom.Pt()>10) {
  		aGood=true;
      aMom.SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
    }
	}

	flooseMuons = GetObject<MuonCol>("looseMuons");
	assert(flooseMuons!=NULL);
  bool mu1Good=false, mu2Good=false;
  int nMu = flooseMuons->GetEntries();
  if (nMu>0) {
		const Muon * _looseMuons = flooseMuons->At(0);
		const FourVectorM &mom = _looseMuons->Mom();
    if (mom.Pt()>20 && TMath::Abs(mom.Eta())<2.4) {
  		mu1Good=true;
      mu1Mom.SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
    }
	}
  if (nMu>1) {
		const Muon * _looseMuons = flooseMuons->At(1);
		const FourVectorM &mom = _looseMuons->Mom();
    if (mom.Pt()>20 && TMath::Abs(mom.Eta())<2.4) {
  		mu2Good=true;
      mu2Mom.SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
    }
	}

	flooseElectrons = GetObject<ElectronCol>("looseElectrons");
	assert(flooseElectrons!=NULL);
  bool e1Good=false, e2Good=false;
  int nE = flooseElectrons->GetEntries();
  if (nE>0) {
		const Electron * _looseElectrons = flooseElectrons->At(0);
		const FourVectorM &mom = _looseElectrons->Mom();
    if (mom.Pt()>10 && TMath::Abs(mom.Eta())<2.4) {
  		e1Good=true;
      e1Mom.SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
    }
	}
  if (nE>1) {
		const Electron * _looseElectrons = flooseElectrons->At(1);
		const FourVectorM &mom = _looseElectrons->Mom();
    if (mom.Pt()>10 && TMath::Abs(mom.Eta())<2.4) {
  		e2Good=true;
      e2Mom.SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
    }
	}
	
  fPuppiType1MET = GetObject<PFMetCol>("PuppiType1MET");
	assert(fPuppiType1MET!=NULL);
	const PFMet * _PuppiType1MET = fPuppiType1MET->At(0);
	const FourVectorM &mom = _PuppiType1MET->Mom();
	met.SetPtEtaPhiM(mom.Pt(),mom.Eta(),mom.Phi(),mom.M());
  
  // signal region
  if (met.Pt() > 90)
    return;

  // OF control region - dileptonic ttbar
  if (mu1Good && e1Good)
    return;

  // single lep control region - W+jets and semileptonic ttbar
  if (mu1Good && (mu1Mom+met).Pt()>40)
    return;
  if (e1Good && (e1Mom+met).Pt()>40)
    return;

  // dilepton control region - Z->ll
  if (mu1Good && mu2Good && ((mu1Mom+mu2Mom).Pt()>50 || (mu1Mom+mu2Mom+met).Pt()>50))
    return;
  if (e1Good && e2Good && ((e1Mom+e2Mom).Pt()>50 || (e1Mom+e2Mom+met).Pt()>50))
    return;

  // photon control region - gamma + jets
  if (aGood && (aMom.Pt()>50 || (aMom+met).Pt()>50))
    return;

  // event has failed all preselection
  SkipEvent();

  // STOP 3
  return;
}

//--------------------------------------------------------------------------------------------------
void monotopPreselMod::SlaveBegin()
{
  // STOP 4
}

//--------------------------------------------------------------------------------------------------
void monotopPreselMod::SlaveTerminate()
{
  // STOP 5
}
