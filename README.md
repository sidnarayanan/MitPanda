# MitPanda
A set of tools for analyzing CMS data in the MIT Bambu data format

## Fillers
Mods to fill data formats with features specific to my analyses

## FlatNtupler
Mods to produce flat ntuples, a script to generate said mods, and macros to run said mods

## Monotop
Analysis code for top+MET search

## PerformanceStudies
Collection of tools for studying top-tagging

## PhysicsObjects
Contains custom data formats not part of MitAna/DataTree

## Skimmers
Definitions of tree formats (and a script to generate said definitions from a config file). Objects to go from one tree definition to another._
