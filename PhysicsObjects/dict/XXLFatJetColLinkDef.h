#ifndef MITANA_DATATREE_XXLFATJETCOLLINKDEF_H
#define MITANA_DATATREE_XXLFATJETCOLLINKDEF_H

#include "MitAna/DataCont/interface/Ref.h"
#include "MitPanda/PhysicsObjects/interface/XXLFatJetCol.h"
#endif

#ifndef __CINT__
# define _R__UNIQUEIDENTIFIER_ XXLFatJetCol
# define _R__JOIN3_(F,X,Y) _NAME3_(F,X,Y)
# undef _R__UNIQUE_
# define _R__UNIQUE_(X) _R__JOIN3_( _R__UNIQUEIDENTIFIER_,X,__LINE__)
#endif

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;
#pragma link C++ namespace mithep;

#pragma link C++ class mithep::XXLFatJet+;
#pragma link C++ class mithep::Collection<mithep::XXLFatJet>+;
#pragma link C++ class mithep::Array<mithep::XXLFatJet>+;
#pragma link C++ class mithep::ObjArray<mithep::XXLFatJet>+;
#pragma link C++ class mithep::Ref<mithep::XXLFatJet>+;
#pragma link C++ typedef mithep::XXLFatJetCol;
#pragma link C++ typedef mithep::XXLFatJetArr;
#pragma link C++ typedef mithep::XXLFatJetOArr;
#endif
