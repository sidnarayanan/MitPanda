#ifndef MITANA_DATATREE_XXLFATJETCOL_H
#define MITANA_DATATREE_XXLFATJETCOL_H

#include "MitAna/DataCont/interface/Collection.h"
#include "MitAna/DataCont/interface/Array.h"
#include "MitAna/DataCont/interface/ObjArray.h"
#include "MitPanda/PhysicsObjects/interface/XXLFatJetFwd.h"
#include "MitPanda/PhysicsObjects/interface/XXLFatJet.h"
#endif
