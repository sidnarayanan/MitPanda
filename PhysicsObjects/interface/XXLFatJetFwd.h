#ifndef MITANA_DATATREE_XXLFATJETFWD_H
#define MITANA_DATATREE_XXLFATJETFWD_H

#include "MitAna/DataCont/interface/CollectionFwd.h"
#include "MitAna/DataCont/interface/ArrayFwd.h"
#include "MitAna/DataCont/interface/ObjArrayFwd.h"

namespace mithep {
  class XXLFatJet;
  typedef Collection<XXLFatJet> XXLFatJetCol;
  typedef Array<XXLFatJet> XXLFatJetArr;
  typedef ObjArray<XXLFatJet> XXLFatJetOArr;
}
#endif
