#!/usr/bin/env python

from Misc import *

objects = {
  'Tools' : {
    'CanvasDrawer':['CanvasDrawer'],
    'DuplicateRemover':['DuplicateRemover'],
    'GraphDrawer':['GraphDrawer','GraphErrDrawer','GraphAsymmErrDrawer'],
    'HistogramDrawer':['HistogramDrawer'],
    'Normalizer':['Normalizer'],
    'PlotUtility':['PlotUtility','Process','Distribution'],
    'ROCTool':['ROCTool'],
    'BranchAdder':['BranchAdder']
  },
  'Skimmers' : {
    'LimitTreeBuilder':['VariableMap','Process','Region','LimitTreeBuilder'],
    'NeroSkimmer':['NeroSkimmer'],
    'SFSkimmer':['SFSkimmer'],
  }
}

from ROOT import gROOT, gSystem
import ROOT as root

def Load(lib,obj):
  includepath=None
  for inc in objects[lib]:
    if obj in objects[lib][inc]:
      includepath = "${CMSSW_BASE}/src/MitPanda/%s/interface/%s.h"%(lib,inc)
      break
  if includepath==None:
    PError('MitPanda.Tools.Load','Could not load %s from %s'%(obj,lib))
    return
  PInfo('MitPanda.Tools.Load','Including %s'%includepath)
  gROOT.LoadMacro(includepath)
  libpath = "libMitPanda%s.so"%(lib)
  PInfo('MitPanda.Tools.Load','Loading %s'%libpath)
  gSystem.Load(libpath)
  if 'Nero' in obj:
    neropath = 'libNeroProducerCore.so'
    PInfo('MitPanda.Tools.Load','Loading %s'%neropath)
    gSystem.Load(neropath)
