#include "../interface/LimitTreeBuilder.h"

Process::Process(TString n, TTree *in, VariableMap *vPtr, TString sel, TString w) {
  VariableMap v = *vPtr;
  limitTree = new LimitTree();
  metvar = v("met");
  metphivar = v("metphi");
  // mtvar = v("mt");
  genvptvar = v("genVpt");
  genvphivar = v("genVphi");
  name = n;

  inputTree = in;
  TTree *outTree = new TTree(TString::Format("tree%i",treeCounter++),TString::Format("limit tree for %s%s",n.Data(),syst.Data()));
  limitTree->WriteTree(outTree);

  selection = sel;
  weight = w;

}

Process::~Process() {
  delete limitTree; 
  delete inputTree;
}

void Process::Run() {
//  fprintf(stderr,selection.Data());
  printf("%s%s\n",name.Data(),syst.Data());
  TTree *clonedTree = (TTree*)inputTree->CopyTree(selection.Data());
  // fprintf(stdout,"variable map:\n\tmet:%s\n\tmetphi:%s\n\tgenvpt:%s\n\tgenvphi:%s\n",metvar.Data(),metphivar.Data(),genvptvar.Data(),genvphivar.Data());
  clonedTree->SetBranchAddress(metvar.Data(),&limitTree->met);
  clonedTree->SetBranchAddress(metphivar.Data(),&limitTree->metphi);
  // clonedTree->SetBranchAddress(mtvar.Data(),&limitTree->mt);
  clonedTree->SetBranchAddress(genvptvar.Data(),&limitTree->genVpt);
  clonedTree->SetBranchAddress(genvphivar.Data(),&limitTree->genVphi);

  TTreeFormula fweight(TString::Format("w_%s",name.Data()).Data(),weight.Data(),clonedTree);
  fweight.SetQuickLoad(true);
  unsigned int nEntries = clonedTree->GetEntries();
  printf("%u\n",nEntries);
  for (unsigned int iE=0; iE!=nEntries; ++iE) {
    if (iE%10000==0) printf("%10u/%u\n",iE,nEntries);
    clonedTree->GetEntry(iE);
    limitTree->weight = fweight.EvalInstance();
    limitTree->treePtr->Fill();
  }
}

void LimitTreeBuilder::Output() {
  for (auto r : regions) {
    const char *rname = r->name.Data();
    for (auto p : r->GetProcesses()) {
      const char *pname = p->name.Data();
      const char *systname = p->syst.Data();
      fOut->WriteTObject(p->GetTree(),TString::Format("%s_%s%s",pname,rname,systname));
    }
  }
  fOut->Close();
  fOut=0;
}
