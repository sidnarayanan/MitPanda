#include "../interface/TopTagSkimmer.h"
#include "TMath.h"
#include "TLorentzRotation.h"
#include <vector>
#include <iostream>

// some helper functions

TLorentzVector *getVec(TClonesArray *arr, unsigned int idx) {
  return (TLorentzVector *) ((*arr)[idx]);
}

float clean(float x, float def=-1) {
  if (!(x==x)) return def;
  else return x;
}


class sjpair {
  public:
    sjpair (TLorentzVector *sj0, TLorentzVector *sj1, float qgtag0, float qgtag1, TLorentzRotation &lambda) {
        if (sj0&&sj1 && sj0->Pt()>0 && sj1->Pt()>0) {
          TLorentzVector sum = *sj1+*sj0;
          mjj = clean(sum.M());
          //mj0 = clean((sj0->M()>sj1->M()) ? sj0->M() : sj1->M());
          mj0 = clean((sj0->Pt()>sj1->Pt()) ? sj0->M() : sj1->M());

          dR = clean(sj0->DeltaR(*sj1));          
          dR = (dR>0) ? dR : 999;
          dij = dR*TMath::Min(sj0->Pt(),sj1->Pt());

          TLorentzVector sjboosted0 = lambda*(*sj0);
          TLorentzVector sjboosted1 = lambda*(*sj1);
          if (sjboosted0.Pt()>0 && sjboosted1.Pt()>1) {
            dRTRF = clean(sjboosted0.DeltaR(sjboosted1));
            dPhiTRF = clean(std::min(TMath::Abs(sum.DeltaPhi(sjboosted0)),
                                     TMath::Abs(sum.DeltaPhi(sjboosted0))));
          }
          if (qgtag1>qgtag0) {
            qg0 = qgtag0; qg1 = qgtag1;
          } else {
            qg1 = qgtag0; qg0 = qgtag1;
          }
          sumqg = clean(TMath::TanH((TMath::ATanH(2*qg0-1)+TMath::ATanH(2*qg1-1))/2.),-2);
        }
    }
    ~sjpair() { }
    float dR=999;
    float mjj=-1;
    float mj0=-1;
    float qg0=-1;
    float qg1=-1;
    float dij=999;
    float sumqg=-1;
    float dRTRF=999;
    float dPhiTRF=999;
};


bool dijComp(sjpair p1, sjpair p2) {
  return p1.dij<p2.dij;
}

bool dRComp(sjpair p1, sjpair p2) {
  return p1.dR<p2.dR;
}

bool dPhiTRFComp(sjpair p1, sjpair p2) {
  return p1.dPhiTRF<p2.dPhiTRF;
}

bool sumQGComp(sjpair p1, sjpair p2) {
  return p1.sumqg>p2.sumqg;
}

// TopTagSkimmer
TopTagSkimmer::TopTagSkimmer() {
  tIn = new topTagTree();
  tOut = new flatTagTree;
}

TopTagSkimmer::~TopTagSkimmer() {
  delete tIn;
  delete tOut;
  delete fIn;
  delete fOut;
//  delete outTreePtr;
}

void TopTagSkimmer::Init(TTree *inTree, TTree *outTree) {
  tIn->ReadTree(inTree);
  tOut->WriteTree(outTree);
}

void TopTagSkimmer::Init(TString inFile, TString outFile) {
  fIn = new TFile(inFile); fOut = new TFile(outFile,"RECREATE");
  outTreePtr = new TTree("events","events"); 
  Init((TTree*)fIn->Get("events"),outTreePtr);
}

void TopTagSkimmer::ResetBranches() {
  tIn->Reset();
  tOut->Reset();
}

void TopTagSkimmer::Terminate() {
  if (fOut) {
    fOut->WriteTObject(outTreePtr,outTreePtr->GetName(),"Overwrite");
  }
  fOut->Close();
}

void TopTagSkimmer::Run() {
  float R0=1.2, topSize=0.8, minPt=250;
  topSize *= topSize;
  unsigned int nTotal=0, nAccepted=0;
  TLorentzVector *mcMom, *fjMom, *sj0Mom, *sj1Mom, *sj2Mom, *sj3Mom;

  unsigned int nEntries = tIn->treePtr->GetEntries();
//  nEntries = 100;
  for (unsigned int iE=0; iE!=nEntries; ++iE) {
    tIn->treePtr->GetEntry(iE);
    std::vector<unsigned int>goodPartonIndices;
    if (isSignal) {
      for (unsigned int iQ=0; iQ!=(unsigned int)tIn->_mcQuarkCounter; ++iQ) {
        if (tIn->mcQuark_IsHadronicTop[iQ]==1 && tIn->mcQuark_TopSize[iQ]<topSize) {
          goodPartonIndices.push_back(iQ);
        }
      }
    }
    
    for (unsigned int iJ=0; iJ!=(unsigned int)tIn->_CA15fjCounter; ++iJ) {
    
      ++nTotal;
      
      fjMom = getVec(tIn->CA15fj_Momentum,iJ);  
      if (fjMom->Pt()<minPt || TMath::Abs(fjMom->Eta())>2.4) {
//        fprintf(stderr,"rejecting because of kinematics %f %f\n",fjMom->Pt(),fjMom->Eta());
        continue;
      }
      bool foundMCParton=!isSignal;
      for (unsigned int iQ : goodPartonIndices) {
        mcMom = getVec(tIn->mcQuark_Momentum,iQ);  
        if (!(mcMom->Pt()>10))
          continue;
        foundMCParton = mcMom->DeltaR(*fjMom)<R0 ;
        if (foundMCParton)
          break;
      }
      if (!foundMCParton) {
//        fprintf(stderr,"rejecting because could not find parton\n");
        continue;
      }
      ++nAccepted;
      
      tOut->pt = fjMom->Pt();
      tOut->eta = fjMom->Eta();

      tOut->QjetVol = clean(tIn->CA15fj_QJetVol[iJ]);
      float chi = clean(tIn->CA15fj_chi[iJ]);
      if (chi>0) {
        tOut->logchi = TMath::Log(chi);
        if (tOut->logchi>15)
          tOut->logchi = 15;
      } else {
        tOut->logchi = -30;
      }

      tOut->tau1 = clean(tIn->CA15fj_Tau1[iJ]);
      tOut->tau2 = clean(tIn->CA15fj_Tau2[iJ]);
      tOut->tau3 = clean(tIn->CA15fj_Tau3[iJ]);

      tOut->mSD = clean(tIn->CA15fj_MassSoftDrop[iJ]);
      tOut->rho = clean(2*TMath::Log(tOut->mSD/tOut->pt),999);
      tOut->rhoprime = clean(TMath::Log(TMath::Power(tOut->mSD,2)/tOut->pt),999);
      tOut->maxsjbtag = clean(tIn->CA15fj_subJetBTag0[iJ]);

      TLorentzVector *sdMom = getVec(tIn->CA15fj_SoftDropMomentum,iJ);  
      TLorentzRotation lambdaInv(sdMom->BoostVector());
      TLorentzRotation lambda = lambdaInv.Inverse();
      
      tOut->nSubjets = (unsigned int)tIn->CA15fj_nSubJets[iJ];
      unsigned int nsubjets = tOut->nSubjets;
      float subjetSumQG = 0;
      sj0Mom=0; sj1Mom=0; sj2Mom=0; sj3Mom=0;
      
      if (nsubjets>0) {
        subjetSumQG += TMath::ATanH(2*tIn->CA15fj_sjQGTag0[iJ]-1);
        sj0Mom = getVec(tIn->CA15fj_SubJet0Momentum,iJ);
        if (nsubjets>1) {
          subjetSumQG += TMath::ATanH(2*tIn->CA15fj_sjQGTag1[iJ]-1);
          sj1Mom = getVec(tIn->CA15fj_SubJet1Momentum,iJ);
          if (nsubjets>2) {
            subjetSumQG += TMath::ATanH(2*tIn->CA15fj_sjQGTag2[iJ]-1);
            sj2Mom = getVec(tIn->CA15fj_SubJet2Momentum,iJ);
            if (nsubjets>3) {
              subjetSumQG += TMath::ATanH(2*tIn->CA15fj_sjQGTag3[iJ]-1);
              sj3Mom = getVec(tIn->CA15fj_SubJet3Momentum,iJ);
            }
          }
        }
      }  

      if (nsubjets)
        subjetSumQG /= (1.*nsubjets);    
      subjetSumQG = TMath::TanH(subjetSumQG);
      tOut->sumQG = subjetSumQG;

      std::vector<sjpair> sjpairs;
      sjpairs.reserve(6);

      if (nsubjets>1) {
        sjpairs.push_back(sjpair(sj0Mom,sj1Mom,tIn->CA15fj_sjQGTag0[iJ],tIn->CA15fj_sjQGTag1[iJ],lambda));
      } else {
        sjpairs.push_back(sjpair(0,0,-1,-1,lambda));
      } 
      if (nsubjets>2) {
        sjpairs.push_back(sjpair(sj0Mom,sj2Mom,tIn->CA15fj_sjQGTag0[iJ],tIn->CA15fj_sjQGTag2[iJ],lambda));
        sjpairs.push_back(sjpair(sj1Mom,sj2Mom,tIn->CA15fj_sjQGTag1[iJ],tIn->CA15fj_sjQGTag2[iJ],lambda));
      } else {
        sjpairs.push_back(sjpair(0,0,-1,-1,lambda));
        sjpairs.push_back(sjpair(0,0,-1,-1,lambda));
      }
      if (nsubjets>3) {
        sjpairs.push_back(sjpair(sj0Mom,sj3Mom,tIn->CA15fj_sjQGTag0[iJ],tIn->CA15fj_sjQGTag3[iJ],lambda));
        sjpairs.push_back(sjpair(sj1Mom,sj3Mom,tIn->CA15fj_sjQGTag1[iJ],tIn->CA15fj_sjQGTag3[iJ],lambda));
        sjpairs.push_back(sjpair(sj2Mom,sj3Mom,tIn->CA15fj_sjQGTag2[iJ],tIn->CA15fj_sjQGTag3[iJ],lambda));
      } else {
        sjpairs.push_back(sjpair(0,0,-1,-1,lambda));
        sjpairs.push_back(sjpair(0,0,-1,-1,lambda));
        sjpairs.push_back(sjpair(0,0,-1,-1,lambda));
      }
      
      // dR sorting
      std::sort(sjpairs.begin(),sjpairs.end(),dRComp);
      tOut->dR_dR0 = sjpairs[0].dR;
      tOut->d23 = sjpairs[0].dij;
      if (nsubjets<2)
        tOut->mW_dR0 = -1;
      else
        //tOut->mW_dR0 = (sjpairs[0].dRTRF>3.14) ? sjpairs[0].mj0 : sjpairs[0].mjj;
        tOut->mW_dR0 = (nsubjets==2 || sjpairs[0].dRTRF>3.14) ? sjpairs[0].mj0 : sjpairs[0].mjj;
        //tOut->mW_dR0 = (nsubjets==2) ? sjpairs[0].mj0 : sjpairs[0].mjj;

      //dPhiTRF
      std::sort(sjpairs.begin(),sjpairs.end(),dPhiTRFComp);
      tOut->dR_dPhiTRF0 = sjpairs[0].dR;

      // sumQG sorting
      std::sort(sjpairs.begin(),sjpairs.end(),sumQGComp);
      tOut->sumQG_sumQG0 = sjpairs[0].sumqg; 
      if (nsubjets<2) 
        tOut->mW_sumQG0 = -1;
      else
        //tOut->mW_sumQG0 = (sjpairs[0].dRTRF>3.14) ? sjpairs[0].mj0 : sjpairs[0].mjj;
        tOut->mW_sumQG0 = (nsubjets==2 || sjpairs[0].dRTRF>3.14) ? sjpairs[0].mj0 : sjpairs[0].mjj;
        //tOut->mW_sumQG0 = (nsubjets==2) ? sjpairs[0].mj0 : sjpairs[0].mjj;
      
      // dij sorting
      std::sort(sjpairs.begin(),sjpairs.end(),dijComp);
      tOut->dij_d0 = sjpairs[0].dij;


      sjpairs.clear();

      tOut->mcweight = xsec; // should improve later
      tOut->jec = 1;

      tOut->iota0 = clean(tIn->CA15fj_iota0[iJ]);
      tOut->iota1 = clean(tIn->CA15fj_iota1[iJ]);
      tOut->iota2 = clean(tIn->CA15fj_iota2[iJ]);
      tOut->iota3 = clean(tIn->CA15fj_iota3[iJ]);
      tOut->iota4 = clean(tIn->CA15fj_iota4[iJ]);
      tOut->iota5 = clean(tIn->CA15fj_iota5[iJ]);
      tOut->iota6 = clean(tIn->CA15fj_iota6[iJ]);
      tOut->iota7 = clean(tIn->CA15fj_iota7[iJ]);
      tOut->iota8 = clean(tIn->CA15fj_iota8[iJ]);
      tOut->iota9 = clean(tIn->CA15fj_iota9[iJ]);

      float iotas[10]={ tOut->iota0,
                        tOut->iota1,
                        tOut->iota2,
                        tOut->iota3,
                        tOut->iota4,
                        tOut->iota5,
                        tOut->iota6,
                        tOut->iota7,
                        tOut->iota8,
                        tOut->iota9 };
      tOut->iotaRMS = TMath::RMS(10,iotas);

      tOut->iotaDR0 = clean(tIn->CA15fj_iotaDR0[iJ]);
      tOut->iotaDR1 = clean(tIn->CA15fj_iotaDR1[iJ]);
      tOut->iotaDR2 = clean(tIn->CA15fj_iotaDR2[iJ]);
      tOut->iotaDR3 = clean(tIn->CA15fj_iotaDR3[iJ]);
      tOut->iotaDR4 = clean(tIn->CA15fj_iotaDR4[iJ]);
      tOut->iotaDR5 = clean(tIn->CA15fj_iotaDR5[iJ]);
      tOut->iotaDR6 = clean(tIn->CA15fj_iotaDR6[iJ]);
      tOut->iotaDR7 = clean(tIn->CA15fj_iotaDR7[iJ]);
      tOut->iotaDR8 = clean(tIn->CA15fj_iotaDR8[iJ]);
      tOut->iotaDR9 = clean(tIn->CA15fj_iotaDR9[iJ]);

      float iotasDR[10]= { tOut->iotaDR0,
                  tOut->iotaDR1,
                  tOut->iotaDR2,
                  tOut->iotaDR3,
                  tOut->iotaDR4,
                  tOut->iotaDR5,
                  tOut->iotaDR6,
                  tOut->iotaDR7,
                  tOut->iotaDR8,
                  tOut->iotaDR9 };
      tOut->iotaDRRMS = TMath::RMS(10,iotasDR);
      
      tOut->iotaM0 = clean(tIn->CA15fj_iotaM0[iJ]);
      tOut->iotaM1 = clean(tIn->CA15fj_iotaM1[iJ]);
      tOut->iotaM2 = clean(tIn->CA15fj_iotaM2[iJ]);
      tOut->iotaM3 = clean(tIn->CA15fj_iotaM3[iJ]);
      tOut->iotaM4 = clean(tIn->CA15fj_iotaM4[iJ]);
      tOut->iotaM5 = clean(tIn->CA15fj_iotaM5[iJ]);
      tOut->iotaM6 = clean(tIn->CA15fj_iotaM6[iJ]);
      tOut->iotaM7 = clean(tIn->CA15fj_iotaM7[iJ]);
      tOut->iotaM8 = clean(tIn->CA15fj_iotaM8[iJ]);
      tOut->iotaM9 = clean(tIn->CA15fj_iotaM9[iJ]);

      float iotasM[10] ={ tOut->iotaM0,
                  tOut->iotaM1,
                  tOut->iotaM2,
                  tOut->iotaM3,
                  tOut->iotaM4,
                  tOut->iotaM5,
                  tOut->iotaM6,
                  tOut->iotaM7,
                  tOut->iotaM8,
                  tOut->iotaM9 };
      tOut->iotaMRMS = TMath::RMS(10,iotasM);
      
      tOut->treePtr->Fill();
    }
  }
  printf("%u/%u\n",nAccepted,nTotal);
}
