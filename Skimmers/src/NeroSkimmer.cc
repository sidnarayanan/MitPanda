#include "../interface/NeroSkimmer.h"
#include "TVector2.h"
#include "TMath.h"
#include <algorithm>
#include <vector>
#include "TStopwatch.h"

#define MUON_MASS 0.106
#define ELECTRON_MASS 0.000511
#define DEBUG 0

bool NeroSkimmer::LeptonIsolation(double pt, double eta, double iso, int pdgId, LepIsoType isoType) {
  float maxIso=0;
  float abseta = TMath::Abs(eta);
  if (abs(pdgId)==13) {
    maxIso = (isoType == kIsoTight) ? 0.15 : 0.25;
  } else {
    switch (isoType) {
      case kIsoVeto:
        maxIso = (abseta<=1.479) ? 0.126 : 0.144;
        break;
      case kIsoLoose:
        maxIso = (abseta<=1.479) ? 0.0893 : 0.121;
        break;
      case kIsoMedium:
        maxIso = (abseta<=1.479) ? 0.0766 : 0.0678;
        break;
      case kIsoTight:
        maxIso = (abseta<=1.479) ? 0.0354 : 0.0646;
        break;
      default:
        break;
    }
  }
  return (iso < pt*maxIso);
}

// class definition

NeroSkimmer::NeroSkimmer() {
  event = new BareEvent(); collections.push_back(event);
  ak4Jets = new BareJets(); collections.push_back(ak4Jets);
  ak8Jets = new BareFatJets(); collections.push_back(ak8Jets);
  ca15Jets = new BareFatJets(); collections.push_back(ca15Jets);
  leptons = new BareLeptons(); collections.push_back(leptons);
  photons = new BarePhotons(); collections.push_back(photons);
  taus = new BareTaus(); taus->SetExtend(); collections.push_back(taus);
  mets = new BareMet(); mets->SetExtend(); collections.push_back(mets);
  triggers = new BareTrigger(); collections.push_back(triggers);
  genParticles = new BareMonteCarlo(); collections.push_back(genParticles);
  vertices = new BareVertex(); collections.push_back(vertices);

  for (auto c : collections)
    c->init();

}

NeroSkimmer::~NeroSkimmer() {
  for (auto c : collections)
    delete c;
}

void NeroSkimmer::ResetBranches() {
  mcWeight=-1; 
  puWeight=1;
  sf_ewkZ=1;
  sf_ewkW=1;
  sf_ewkA=1;
  sf_qcdZ=1;
  sf_qcdW=1;
  sf_qcdA=1;

  sf_btag0=1;
  sf_btag0BUp=1;
  sf_btag0BDown=1;
  sf_btag0MUp=1;
  sf_btag0MDown=1;
  sf_btag1=1;
  sf_btag1BUp=1;
  sf_btag1BDown=1;
  sf_btag1MUp=1;
  sf_btag1MDown=1;
  sf_sjbtag0=1;
  sf_sjbtag0BUp=1;
  sf_sjbtag0BDown=1;
  sf_sjbtag0MUp=1;
  sf_sjbtag0MDown=1;
  sf_sjbtag1=1;
  sf_sjbtag1BUp=1;
  sf_sjbtag1BDown=1;
  sf_sjbtag1MUp=1;
  sf_sjbtag1MDown=1;
  sf_sjbtag2=1;
  sf_sjbtag2BUp=1;
  sf_sjbtag2BDown=1;
  sf_sjbtag2MUp=1;
  sf_sjbtag2MDown=1;

  sf_newBtag=1;
  sf_newBtagHFUp=1;
  sf_newBtagHFDown=1;
  sf_newBtagLFUp=1;
  sf_newBtagLFDown=1;
  sf_newSjBtag=1;
  sf_newSjBtagHFUp=1;
  sf_newSjBtagHFDown=1;
  sf_newSjBtagLFUp=1;
  sf_newSjBtagLFDown=1;

  sf_lep=1;
  sf_lepTrack=1;
  sf_tt=1;
  sf_metTrig=1;
  sf_eleTrig=1;
//  sf_eleTrigUp=1;
//  sf_eleTrigDown=1;
  sf_phoTrig=1;
  photonPurityWeight=1;
  finalWeight=1;

  region=0;

  runNumber=-1; 
  eventNumber=-1; 
  lumiNumber=-1; 
  npv=-1;
  truepu=-1;
  metFilter=1;
  newMetFilter=1;
  met=-1; 
  metphi=999;
  metnomu=-1; 
  calomet=-1;
  calometphi=999;
  pfcalobalance=-999;
  puppimet=-1; 
  puppimetphi=999; 
  sumET=-1;
  trkmet=-1;
  UWmag=-1; 
  UWphi=999; 
  UZmag=-1; 
  UZphi=999; 
  UAmag=-1; 
  UAphi=999; 
  Uperp=-9999;
  Upara=-999;
  pfUWmag=-1; 
  pfUWphi=999; 
  pfUZmag=-1; 
  pfUZphi=999; 
  pfUAmag=-1; 
  pfUAphi=999; 
  pfUperp=-9999;
  pfUpara=-999;

  dphimet=999; 
  dphipuppimet=999;
  dphiUW=999;
  dphiUZ=999; 
  dphiUA=999; 
  dphipfUW=999;
  dphipfUZ=999; 
  dphipfUA=999; 

  trueGenBosonPt=-1;
  genBosonPt=-1;
  genBosonEta=-1;
  genTopPt=-1;
  genBosonMass=-1;
  genBosonPhi=999;

  recoBosonPt=-1;
  recoBosonPhi=999;

  nSelectedJet=0; 
  jet1Phi=999; 
  jet1Pt=-1; 
  genJet1Pt=-1; 
  genJet1Eta=-1; 
  jet2Pt=-1; 
  jet1Eta=-1; 
  jet1Btag=-1; 
  jet1IsTight=0;
  nGenJet=0;
  jet2Btag=-1; 
  jetIso1Pt=-1;
  sj1Pt=-1;
  jetIso1Flav=-1;
  sj1Flav=-1;
#if 0
  jet2Phi=999; 
  jet3Phi=999; 
  jet4Phi=999; 
  jet3Pt=-1; 
  jet4Pt=-1; 
  jet2Eta=-1; 
  jet3Eta=-1; 
  jet4Eta=-1; 
  jet3Btag=-1; 
  jet4Btag=-1; 
#endif
  jet123Mass=-1; 
  jet12Mass=-1; 
  jet12DEta=-1; 
  jetNBtags=0; 
  jetNIsoBtags=0;

  nSelectedCA15fj=0; 
  CA15fj1_tau1=-1; 
  CA15fj1_tau2=-1; 
  CA15fj1_tau3=-1; 
  CA15fj1_mSD=-1; 
  CA15fj1_pt=-1; 
  CA15fj1_ptUp=-1; 
  CA15fj1_ptDown=-1; 
  CA15fj1_phi=-1; 
  CA15fj1_eta=-1; 
  CA15fj1_maxsjbtag=-1; 
  CA15fj1_minsjbtag=-1; 
//  CA15fj1_NNResponse =-1; 
  CA15fj1_isMatched=-1; 
  CA15fj1_rawPt=-1;
  CA15fj1_selBits=-1;
  CA15fj1_mjID=-1;
  CA15fj1_isHF=0;
  CA15fj1_isTight=0;
  CA15fj1_isLoose=0;
  isHF=0;

#if 0
  nSelectedAK8fj=0; 
  AK8fj1_tau1=-1; 
  AK8fj1_tau2=-1; 
  AK8fj1_tau3=-1; 
  AK8fj1_mSD=-1; 
  AK8fj1_pt=-1; 
  AK8fj1_phi=-1; 
  AK8fj1_eta=-1; 
  AK8fj1_maxsjbtag=-1; 
  AK8fj1_NNResponse =-1; 
  AK8fj1_isMatched=-1; 
#endif 

  nLoosePhoton=0; 
  nTightPhoton=0; 
  nLooseLep=0; 
  nLooseElectron=0; 
  nLooseMuon=0; 
  nTightLep=0; 
  nTightElectron=0; 
  nTightMuon=0; 
  nSelectedTau=0; 
  diLepMass=-1;
  loosePho1IsTight=0;
  looseLep1IsTight=0;
  looseLep2IsTight=0;
  looseLep1PdgId = -0;
  looseLep2PdgId = -0;

  looseLep1Pt=-1;
  looseLep2Pt=-1;
  looseLep1Eta=-999;
  looseLep2Eta=-999;
  looseLep1Phi=-999;
  looseLep2Phi=-999;
  loosePho1Pt=-1;
  loosePho1Eta=-999;
  loosePho1Phi=-999;
 

  trigger=0;
}

void NeroSkimmer::SetOutputFile(TString fOutName) {
  fOut = new TFile(fOutName,"RECREATE");
  tOut = new TTree("events","events slimmed");

  tOut->Branch("mcWeight",&mcWeight,"mcWeight/F");
  tOut->Branch("puWeight",&puWeight,"puWeight/F");
  tOut->Branch("sf_lep",&sf_lep,"sf_lep/F");
  tOut->Branch("sf_lepTrack",&sf_lepTrack,"sf_lepTrack/F");
  tOut->Branch("sf_btag0",&sf_btag0,"sf_btag0/F");
  tOut->Branch("sf_btag0BUp",&sf_btag0BUp,"sf_btag0BUp/F");
  tOut->Branch("sf_btag0BDown",&sf_btag0BDown,"sf_btag0BDown/F");
  tOut->Branch("sf_btag0MUp",&sf_btag0MUp,"sf_btag0MUp/F");
  tOut->Branch("sf_btag0MDown",&sf_btag0MDown,"sf_btag0MDown/F");
  tOut->Branch("sf_btag1",&sf_btag1,"sf_btag1/F");
  tOut->Branch("sf_btag1BUp",&sf_btag1BUp,"sf_btag1BUp/F");
  tOut->Branch("sf_btag1BDown",&sf_btag1BDown,"sf_btag1BDown/F");
  tOut->Branch("sf_btag1MUp",&sf_btag1MUp,"sf_btag1MUp/F");
  tOut->Branch("sf_btag1MDown",&sf_btag1MDown,"sf_btag1MDown/F");
  tOut->Branch("sf_sjbtag0",&sf_sjbtag0,"sf_sjbtag0/F");
  tOut->Branch("sf_sjbtag0BUp",&sf_sjbtag0BUp,"sf_sjbtag0BUp/F");
  tOut->Branch("sf_sjbtag0BDown",&sf_sjbtag0BDown,"sf_sjbtag0BDown/F");
  tOut->Branch("sf_sjbtag0MUp",&sf_sjbtag0MUp,"sf_sjbtag0MUp/F");
  tOut->Branch("sf_sjbtag0MDown",&sf_sjbtag0MDown,"sf_sjbtag0MDown/F");
  tOut->Branch("sf_sjbtag1",&sf_sjbtag1,"sf_sjbtag1/F");
  tOut->Branch("sf_sjbtag1BUp",&sf_sjbtag1BUp,"sf_sjbtag1BUp/F");
  tOut->Branch("sf_sjbtag1BDown",&sf_sjbtag1BDown,"sf_sjbtag1BDown/F");
  tOut->Branch("sf_sjbtag1MUp",&sf_sjbtag1MUp,"sf_sjbtag1MUp/F");
  tOut->Branch("sf_sjbtag1MDown",&sf_sjbtag1MDown,"sf_sjbtag1MDown/F");

  tOut->Branch("sf_newBtag",&sf_newBtag,"sf_newBtag/F");
  tOut->Branch("sf_newBtagHFUp",&sf_newBtagHFUp,"sf_newBtagHFUp/F");
  tOut->Branch("sf_newBtagHFDown",&sf_newBtagHFDown,"sf_newBtagHFDown/F");
  tOut->Branch("sf_newBtagLFUp",&sf_newBtagLFUp,"sf_newBtagLFUp/F");
  tOut->Branch("sf_newBtagLFDown",&sf_newBtagLFDown,"sf_newBtagLFDown/F");
  tOut->Branch("sf_newSjBtag",&sf_newSjBtag,"sf_newSjBtag/F");
  tOut->Branch("sf_newSjBtagHFUp",&sf_newSjBtagHFUp,"sf_newSjBtagHFUp/F");
  tOut->Branch("sf_newSjBtagHFDown",&sf_newSjBtagHFDown,"sf_newSjBtagHFDown/F");
  tOut->Branch("sf_newSjBtagLFUp",&sf_newSjBtagLFUp,"sf_newSjBtagLFUp/F");
  tOut->Branch("sf_newSjBtagLFDown",&sf_newSjBtagLFDown,"sf_newSjBtagLFDown/F");

  tOut->Branch("sf_ewkZ",&sf_ewkZ,"sf_ewkZ/F");
  tOut->Branch("sf_ewkW",&sf_ewkW,"sf_ewkW/F");
  tOut->Branch("sf_ewkA",&sf_ewkA,"sf_ewkA/F");
  tOut->Branch("sf_qcdZ",&sf_qcdZ,"sf_qcdZ/F");
  tOut->Branch("sf_qcdW",&sf_qcdW,"sf_qcdW/F");
  tOut->Branch("sf_qcdA",&sf_qcdA,"sf_qcdA/F");
  tOut->Branch("sf_tt",&sf_tt,"sf_tt/F");
  tOut->Branch("sf_eleTrig",&sf_eleTrig,"sf_eleTrig/F");
//  tOut->Branch("sf_eleTrigUp",&sf_eleTrigUp,"sf_eleTrigUp/F");
//  tOut->Branch("sf_eleTrigDown",&sf_eleTrigDown,"sf_eleTrigDown/F");
  tOut->Branch("sf_phoTrig",&sf_phoTrig,"sf_phoTrig/F");
  tOut->Branch("sf_metTrig",&sf_metTrig,"sf_metTrig/F");
  tOut->Branch("finalWeight",&finalWeight,"finalWeight/F");
  tOut->Branch("photonPurityWeight",&photonPurityWeight,"photonPurityWeight/F");
  // branch will be added to SinglePhoton for QCD template
  // weight is 1 for all other MC predictions
   
  tOut->Branch("region",&region,"region/I");

  tOut->Branch("runNumber",&runNumber,"runNumber/I");
  tOut->Branch("eventNumber",&eventNumber,"eventNumber/l");
  tOut->Branch("lumiNumber",&lumiNumber,"lumiNumber/I");
  tOut->Branch("npv",&npv,"npv/I");
  tOut->Branch("truepu",&truepu,"truepu/I");
  tOut->Branch("metFilter",&metFilter,"metFilter/I");
  tOut->Branch("newMetFilter",&newMetFilter,"newMetFilter/I");

  tOut->Branch("met",&met,"met/F");
  tOut->Branch("metphi",&metphi,"metphi/F");
  tOut->Branch("calomet",&calomet,"calomet/F");
  tOut->Branch("calometphi",&calometphi,"calometphi/F");
  tOut->Branch("pfcalobalance",&pfcalobalance,"pfcalobalance/F");
  tOut->Branch("metnomu",&metnomu,"metnomu/F");
  tOut->Branch("puppimet",&puppimet,"puppimet/F");
  tOut->Branch("puppimetphi",&puppimetphi,"puppimetphi/F");
  tOut->Branch("sumET",&sumET,"sumET/F");
  tOut->Branch("trkmet",&trkmet,"trkmet/F");

  tOut->Branch("Uperp",&Uperp,"Uperp/F");
  tOut->Branch("Upara",&Upara,"Upara/F");
  tOut->Branch("UWmag",&UWmag,"UWmag/F");
  tOut->Branch("UWphi",&UWphi,"UWphi/F");
  tOut->Branch("UZmag",&UZmag,"UZmag/F");
  tOut->Branch("UZphi",&UZphi,"UZphi/F");
  tOut->Branch("UAmag",&UAmag,"UAmag/F");
  tOut->Branch("UAphi",&UAphi,"UAphi/F");

  tOut->Branch("pfUperp",&pfUperp,"pfUperp/F");
  tOut->Branch("pfUpara",&pfUpara,"pfUpara/F");
  tOut->Branch("pfUWmag",&pfUWmag,"pfUWmag/F");
  tOut->Branch("pfUWphi",&pfUWphi,"pfUWphi/F");
  tOut->Branch("pfUZmag",&pfUZmag,"pfUZmag/F");
  tOut->Branch("pfUZphi",&pfUZphi,"pfUZphi/F");
  tOut->Branch("pfUAmag",&pfUAmag,"pfUAmag/F");
  tOut->Branch("pfUAphi",&pfUAphi,"pfUAphi/F");

  tOut->Branch("dPhiMET",&dphimet,"dPhiMET/F");
  tOut->Branch("dPhiPuppiMET",&dphipuppimet,"dPhiPuppiMET/F");
  tOut->Branch("dPhiUW",&dphiUW,"dPhiUW/F");
  tOut->Branch("dPhiUZ",&dphiUZ,"dPhiUZ/F");
  tOut->Branch("dPhiUA",&dphiUA,"dPhiUA/F");
  tOut->Branch("dPhipfUW",&dphipfUW,"dPhipfUW/F");
  tOut->Branch("dPhipfUZ",&dphipfUZ,"dPhipfUZ/F");
  tOut->Branch("dPhipfUA",&dphipfUA,"dPhipfUA/F");

  tOut->Branch("trueGenBosonPt",&trueGenBosonPt,"trueGenBosonPt/F");
  tOut->Branch("genTopPt",&genTopPt,"genTopPt/F");
  tOut->Branch("genBosonPt",&genBosonPt,"genBosonPt/F");
  tOut->Branch("genBosonEta",&genBosonEta,"genBosonEta/F");
  tOut->Branch("genBosonMass",&genBosonMass,"genBosonMass/F");
  tOut->Branch("genBosonPhi",&genBosonPhi,"genBosonPhi/F");
  tOut->Branch("recoBosonPt",&recoBosonPt,"recoBosonPt/F");
  tOut->Branch("recoBosonPhi",&recoBosonPhi,"recoBosonPhi/F");

  tOut->Branch("nGenJet",&nGenJet,"nGenJet/I");
  tOut->Branch("nJet",&nSelectedJet,"nJet/I");
  tOut->Branch("genJet1Pt",&genJet1Pt,"genJet1Pt/F");
  tOut->Branch("genJet1Eta",&genJet1Eta,"genJet1Eta/F");
  tOut->Branch("jet1Pt",&jet1Pt,"jet1Pt/F");
  tOut->Branch("jet2Pt",&jet2Pt,"jet2Pt/F");
  tOut->Branch("jet1Eta",&jet1Eta,"jet1Eta/F");
  tOut->Branch("jet2Eta",&jet2Eta,"jet2Eta/F");
  tOut->Branch("jetIso1Pt",&jetIso1Pt,"jetIso1Pt/F");
  tOut->Branch("sj1Pt",&sj1Pt,"sj1Pt/F");
  tOut->Branch("jetIso1Flav",&jetIso1Flav,"jetIso1Flav/I");
  tOut->Branch("sj1Flav",&sj1Flav,"sj1Flav/I");
/*
  tOut->Branch("jet1Phi",&jet1Phi,"jet1Phi/F");
  tOut->Branch("jet2Phi",&jet2Phi,"jet2Phi/F");
  tOut->Branch("jet3Phi",&jet3Phi,"jet3Phi/F");
  tOut->Branch("jet4Phi",&jet4Phi,"jet4Phi/F");
  tOut->Branch("jet3Pt",&jet3Pt,"jet3Pt/F");
  tOut->Branch("jet4Pt",&jet4Pt,"jet4Pt/F");
  tOut->Branch("jet3Eta",&jet3Eta,"jet3Eta/F");
  tOut->Branch("jet4Eta",&jet4Eta,"jet4Eta/F");
*/
  tOut->Branch("jet1Btag",&jet1Btag,"jet1Btag/F");
  tOut->Branch("jet2Btag",&jet2Btag,"jet2Btag/F");
  //tOut->Branch("jet3Btag",&jet3Btag,"jet3Btag/F");
  //tOut->Branch("jet4Btag",&jet4Btag,"jet4Btag/F");
  tOut->Branch("jet123Mass",&jet123Mass,"jet123Mass/F");
  tOut->Branch("jet12Mass",&jet12Mass,"jet12Mass/F");
  tOut->Branch("jet12DEta",&jet12DEta,"jet12DEta/F");
  tOut->Branch("jet1IsTight",&jet1IsTight,"jet1IsTight/I");
  tOut->Branch("jetNBtags",&jetNBtags,"jetNBtags/I");
  tOut->Branch("jetNIsoBtags",&jetNIsoBtags,"jetNIsoBtags/I");

  tOut->Branch("nCA15fj",&nSelectedCA15fj,"nCA15fj/I");
  //tOut->Branch("CA15fj1_tau1",&CA15fj1_tau1,"CA15fj1_tau1/F");
  tOut->Branch("CA15fj1_tau2",&CA15fj1_tau2,"CA15fj1_tau2/F");
  tOut->Branch("CA15fj1_tau3",&CA15fj1_tau3,"CA15fj1_tau3/F");
  tOut->Branch("CA15fj1_mSD",&CA15fj1_mSD,"CA15fj1_mSD/F");
  tOut->Branch("CA15fj1_pt",&CA15fj1_pt,"CA15fj1_pt/F");
  tOut->Branch("CA15fj1_ptUp",&CA15fj1_ptUp,"CA15fj1_ptUp/F");
  tOut->Branch("CA15fj1_ptDown",&CA15fj1_ptDown,"CA15fj1_ptDown/F");
  //tOut->Branch("CA15fj1_phi",&CA15fj1_phi,"CA15fj1_phi/F");
  tOut->Branch("CA15fj1_eta",&CA15fj1_eta,"CA15fj1_eta/F");
  tOut->Branch("CA15fj1_maxsjbtag",&CA15fj1_maxsjbtag,"CA15fj1_maxsjbtag/F");
  tOut->Branch("CA15fj1_minsjbtag",&CA15fj1_minsjbtag,"CA15fj1_minsjbtag/F");
  //tOut->Branch("CA15fj1_NNResponse",&CA15fj1_NNResponse,"CA15fj1_NNResponse/F");
  tOut->Branch("CA15fj1_isMatched",&CA15fj1_isMatched,"CA15fj1_isMatched/I");
  tOut->Branch("CA15fj1_isHF",&CA15fj1_isHF,"CA15fj1_isHF/I");
  tOut->Branch("CA15fj1_isTight",&CA15fj1_isTight,"CA15fj1_isTight/I");
  tOut->Branch("CA15fj1_isLoose",&CA15fj1_isLoose,"CA15fj1_isLoose/I");
  tOut->Branch("CA15fj1_rawPt",&CA15fj1_rawPt,"CA15fj1_rawPt/F");
  tOut->Branch("isHF",&isHF,"isHF/I");
/*
  tOut->Branch("nAK8fj",&nSelectedAK8fj,"nAK8fj/I");
  tOut->Branch("AK8fj1_tau1",&AK8fj1_tau1,"AK8fj1_tau1/F");
  tOut->Branch("AK8fj1_tau2",&AK8fj1_tau2,"AK8fj1_tau2/F");
  tOut->Branch("AK8fj1_tau3",&AK8fj1_tau3,"AK8fj1_tau3/F");
  tOut->Branch("AK8fj1_mSD",&AK8fj1_mSD,"AK8fj1_mSD/F");
  tOut->Branch("AK8fj1_pt",&AK8fj1_pt,"AK8fj1_pt/F");
  tOut->Branch("AK8fj1_phi",&AK8fj1_phi,"AK8fj1_phi/F");
  tOut->Branch("AK8fj1_eta",&AK8fj1_eta,"AK8fj1_eta/F");
  tOut->Branch("AK8fj1_maxsjbtag",&AK8fj1_maxsjbtag,"AK8fj1_maxsjbtag/F");
  tOut->Branch("AK8fj1_NNResponse",&AK8fj1_NNResponse,"AK8fj1_NNResponse/F");
  tOut->Branch("AK8fj1_isMatched",&AK8fj1_isMatched,"AK8fj1_isMatched/I");
*/
  tOut->Branch("nLooseLep",&nLooseLep,"nLooseLep/I");
  tOut->Branch("nLooseElectron",&nLooseElectron,"nLooseElectron/I");
  tOut->Branch("nTightLep",&nTightLep,"nTightLep/I");
  tOut->Branch("nTightElectron",&nTightElectron,"nTightElectron/I");
  tOut->Branch("looseLep1IsTight",&looseLep1IsTight,"looseLep1IsTight/I");
  tOut->Branch("looseLep2IsTight",&looseLep2IsTight,"looseLep2IsTight/I");
  tOut->Branch("looseLep1PdgId",&looseLep1PdgId,"looseLep1PdgId/I");
  tOut->Branch("looseLep2PdgId",&looseLep2PdgId,"looseLep2PdgId/I");
  tOut->Branch("looseLep1Pt",&looseLep1Pt,"looseLep1Pt/F");
  tOut->Branch("looseLep1Eta",&looseLep1Eta,"looseLep1Eta/F");
  tOut->Branch("looseLep1Phi",&looseLep1Phi,"looseLep1Phi/F");
  tOut->Branch("looseLep2Pt",&looseLep2Pt,"looseLep2Pt/F");
  tOut->Branch("looseLep2Eta",&looseLep2Eta,"looseLep2Eta/F");
  tOut->Branch("looseLep2Phi",&looseLep2Phi,"looseLep2Phi/F");
/*
  tOut->Branch("looseEle1Phi",&looseEle1Phi,"looseEle1Phi/F");
  tOut->Branch("looseEle1Eta",&looseEle1Eta,"looseEle1Eta/F");
  tOut->Branch("looseEle2Pt",&looseEle2Pt,"looseEle2Pt/F");
  tOut->Branch("looseEle2Phi",&looseEle2Phi,"looseEle2Phi/F");
  tOut->Branch("looseEle2Eta",&looseEle2Eta,"looseEle2Eta/F");
  tOut->Branch("looseEle1pdgId",&looseEle1pdgId,"looseEle1pdgId/F");
  tOut->Branch("looseEle2pdgId",&looseEle2pdgId,"looseEle2pdgId/F");
*/
  tOut->Branch("nLooseMuon",&nLooseMuon,"nLooseMuon/I");
  tOut->Branch("nTightMuon",&nTightMuon,"nTightMuon/I");
/*  
  tOut->Branch("looseMu1Pt",&looseMu1Pt,"looseMu1Pt/F");
  tOut->Branch("looseMu1Phi",&looseMu1Phi,"looseMu1Phi/F");
  tOut->Branch("looseMu1Eta",&looseMu1Eta,"looseMu1Eta/F");
  tOut->Branch("looseMu2Pt",&looseMu2Pt,"looseMu2Pt/F");
  tOut->Branch("looseMu2Phi",&looseMu2Phi,"looseMu2Phi/F");
  tOut->Branch("looseMu2Eta",&looseMu2Eta,"looseMu2Eta/F");
  tOut->Branch("looseMu1pdgId",&looseMu1pdgId,"looseMu1pdgId/F");
  tOut->Branch("looseMu2pdgId",&looseMu2pdgId,"looseMu2pdgId/F");
*/
  
  tOut->Branch("diLepMass",&diLepMass,"diLepMass/F");  
  
  tOut->Branch("nLoosePhoton",&nLoosePhoton,"nLoosePhoton/I");
  tOut->Branch("nTightPhoton",&nTightPhoton,"nTightPhoton/I");
  tOut->Branch("loosePho1IsTight",&loosePho1IsTight,"loosePho1IsTight/I");
  tOut->Branch("loosePho1Pt",&loosePho1Pt,"loosePho1Pt/F");
  tOut->Branch("loosePho1Eta",&loosePho1Eta,"loosePho1Eta/F");
  tOut->Branch("loosePho1Phi",&loosePho1Phi,"loosePho1Phi/F");
  
  tOut->Branch("nTau",&nSelectedTau,"nTau/I");

  tOut->Branch("trigger",&trigger,"trigger/I");
}


void NeroSkimmer::Init(TTree *t, TTree *allTree)
{
   // Set branch addresses and branch pointers
   if (!t) return;
   tIn = t;

  event->setBranchAddresses(t);
  ak4Jets->setBranchAddresses(t,label_ak4Jets);
  ak8Jets->setBranchAddresses(t,label_ak8Jets);
  ca15Jets->setBranchAddresses(t,label_ca15Jets);
  leptons->setBranchAddresses(t,label_leptons);
  photons->setBranchAddresses(t,label_photons);
  taus->setBranchAddresses(t,label_taus);
  triggers->setBranchAddresses(t);
  mets->setBranchAddresses(t);
  if (!isData)
    genParticles->setBranchAddresses(t);
  vertices->setBranchAddresses(t);

  if (!isData) {
    TH1F *hDTotalMCWeight = new TH1F("hDTotalMCWeight","hDTotalMCWeight",4,-2,2);
    allTree->Draw("fabs(mcWeight)/mcWeight>>hDTotalMCWeight","fabs(mcWeight)/mcWeight");
    fOut->WriteTObject(hDTotalMCWeight,"hDTotalMCWeight");
  }
}

void NeroSkimmer::Terminate() {
  fOut->WriteTObject(tOut,"events");
  fOut->Close();
  fEleTrigB->Close();
  fEleTrigE->Close();
  fPhoTrig->Close();
  fEleSF->Close();
  fMuSF->Close();
  fEleSFTight->Close();
  fMuSFTight->Close();
  fEleSFTrack->Close();
  fMuSFTrack->Close();
  if (fPU!=NULL) fPU->Close();
  fKFactor->Close();
  fHFNew->Close();
  fLFNew->Close();
  delete btagCalib;
  delete hfReader;
  delete lfReader;
  delete hfUpReader;
  delete lfUpReader;
  delete hfDownReader;
  delete lfDownReader;
//  delete ak8jec;
//  delete ak8unc;
}

void NeroSkimmer::SetDataDir(const char *s) {
  TString dirPath(s);

  fEleTrigB    = new TFile(dirPath+"/trigger_eff/ele_trig_lowpt_rebinned.root");
  fEleTrigE    = new TFile(dirPath+"/trigger_eff/ele_trig_lowpt_rebinned.root");
  fPhoTrig     = new TFile(dirPath+"/trigger_eff/pho_trig.root");
  fMetTrig     = new TFile(dirPath+"/trigger_eff/met_trig.root");
  fEleTrigLow  = new TFile(dirPath+"/trigger_eff/ele_trig_lowpt.root");

  fEleSF        = new TFile(dirPath+"/scaleFactor_electron_vetoid_12p9.root");
  fEleSFTight   = new TFile(dirPath+"/scaleFactor_electron_tightid_12p9.root");
  fEleSFTrack   = new TFile(dirPath+"/scaleFactor_electron_track.root");

  fMuSF         = new TFile(dirPath+"/scaleFactor_muon_looseid_12p9.root");
  fMuSFTight    = new TFile(dirPath+"/scaleFactor_muon_tightid_12p9.root");
  fMuSFTrack    = new TFile(dirPath+"/scaleFactor_muon_track.root");

  fPU      = new TFile(dirPath+"/puWeight_13invfb.root");

  fKFactor = new TFile(dirPath+"/kfactors.root");

  fHFNew = new TFile(dirPath+"/btag/csv_rwt_fit_hf_v2_final_2016_07_15test.root");
  fLFNew = new TFile(dirPath+"/btag/csv_rwt_fit_lf_v2_final_2016_07_15test.root");

  const char *bsys[NBSYS] = {"","_JESUp","_JESDown","_LFUp","_LFDown","_Stats1Up","_Stats1Down","_Stats2Up","_Stats2Down"};
  const char *lfsys[NBSYS] = {"","_JESUp","_JESDown","_HFUp","_HFDown","_Stats1Up","_Stats1Down","_Stats2Up","_Stats2Down"};
  const char *csys[NCSYS] = {"","_cErr1Up","_cErr1Down","_cErr2Up","_cErr2Down"};

  for (unsigned int iPt=0; iPt!=NHFPTBINS; ++iPt) {
    for (unsigned int iSys=0; iSys!=NBSYS; ++iSys) {
      hBNew[iPt][iSys] = (TH1D*)fHFNew->Get(TString::Format("csv_ratio_Pt%u_Eta0_final%s",iPt,bsys[iSys]).Data());
    }
    for (unsigned int iSys=0; iSys!=NCSYS; ++iSys) {
      hCNew[iPt][iSys] = (TH1D*)fHFNew->Get(TString::Format("c_csv_ratio_Pt%u_Eta0_final%s",iPt,csys[iSys]).Data());
    }
    if (iPt<NLFPTBINS) {
      for (unsigned int iEta=0; iEta!=NLFETABINS; ++iEta) {
        for (unsigned int iSys=0; iSys!=NBSYS; ++iSys) {
          hLFNew[iPt][iEta][iSys] = (TH1D*)fLFNew->Get(TString::Format("csv_ratio_Pt%u_Eta%u_final%s",iPt,iEta,lfsys[iSys]).Data());
        }
      }
    }
  }
 

  hEleTrigB = (TH1D*) fEleTrigB->Get("h_num");
  hEleTrigE = (TH1D*) fEleTrigE->Get("h_num_endcap");

  hPhoTrig = (TH1D*) fPhoTrig->Get("h_num");
  hMetTrig = (TH1D*) fMetTrig->Get("numer");
  hEleTrigLow = (TH2D*) fEleTrigLow->Get("hEffEtaPt");

  hEleVeto  = (TH2D*) fEleSF->Get("scaleFactor_electron_vetoid_RooCMSShape");
  hEleTight = (TH2D*) fEleSFTight->Get("scaleFactor_electron_tightid_RooCMSShape");

  hMuLoose = (TH2D*) fMuSF->Get("scaleFactor_muon_looseid_RooCMSShape");
  hMuTight = (TH2D*) fMuSFTight->Get("scaleFactor_muon_tightid_RooCMSShape");

  hMuTrack = (TH1D*) fMuSFTrack->Get("htrack2");
  hEleTrack = (TH2D*) fEleSFTrack->Get("EGamma_SF2D");

  if (fPU!=NULL)
    hPUWeight = (TH1D*)fPU->Get("hPU");

  hZNLO = (TH1D*)fKFactor->Get("ZJets_012j_NLO/nominal");
  hWNLO = (TH1D*)fKFactor->Get("WJets_012j_NLO/nominal");
  hANLO = (TH1D*)fKFactor->Get("GJets_1j_NLO/nominal_G");

  hZLO  = (TH1D*)fKFactor->Get("ZJets_LO/inv_pt");
  hWLO  = (TH1D*)fKFactor->Get("WJets_LO/inv_pt");
  hALO  = (TH1D*)fKFactor->Get("GJets_LO/inv_pt_G");
 
  hZEWK = (TH1D*)fKFactor->Get("EWKcorr/Z");
  hWEWK = (TH1D*)fKFactor->Get("EWKcorr/W");
  hAEWK = (TH1D*)fKFactor->Get("EWKcorr/photon");

  hZEWK->Divide(hZNLO);   hWEWK->Divide(hWNLO);   hAEWK->Divide(hANLO);
  hZNLO->Divide(hZLO);    hWNLO->Divide(hWLO);    hANLO->Divide(hALO);

  btagCalib = new BTagCalibration("csvv2",(dirPath+"/CSVv2_ichep.csv").Data());
  hfReader = new BTagCalibrationReader(btagCalib,BTagEntry::OP_LOOSE,"comb","central");
  lfReader = new BTagCalibrationReader(btagCalib,BTagEntry::OP_LOOSE,"incl","central");
  hfUpReader = new BTagCalibrationReader(btagCalib,BTagEntry::OP_LOOSE,"comb","up");
  lfUpReader = new BTagCalibrationReader(btagCalib,BTagEntry::OP_LOOSE,"incl","up");
  hfDownReader = new BTagCalibrationReader(btagCalib,BTagEntry::OP_LOOSE,"comb","down");
  lfDownReader = new BTagCalibrationReader(btagCalib,BTagEntry::OP_LOOSE,"incl","down");

  sj_btagCalib = new BTagCalibration("csvv2",(dirPath+"/subjet_CSVv2_ichep.csv").Data());
  sj_hfReader = new BTagCalibrationReader(sj_btagCalib,BTagEntry::OP_LOOSE,"lt","central");
  sj_lfReader = new BTagCalibrationReader(sj_btagCalib,BTagEntry::OP_LOOSE,"incl","central");
  sj_hfUpReader = new BTagCalibrationReader(sj_btagCalib,BTagEntry::OP_LOOSE,"lt","up");
  sj_lfUpReader = new BTagCalibrationReader(sj_btagCalib,BTagEntry::OP_LOOSE,"incl","up");
  sj_hfDownReader = new BTagCalibrationReader(sj_btagCalib,BTagEntry::OP_LOOSE,"lt","down");
  sj_lfDownReader = new BTagCalibrationReader(sj_btagCalib,BTagEntry::OP_LOOSE,"incl","down");

//  ak8jec = new JetCorrectorParameters((dirPath+"/Spring16_25nsV6_MC_Uncertainty_AK8PFPuppi.txt").Data());
//  ak8unc = new JetCorrectionUncertainty(*ak8jec);
}

void NeroSkimmer::AddBadEvent(int r, int l, ULong64_t e) {
  EventObj badEvent;
  badEvent.run = r; badEvent.lumi = l; badEvent.evt = e;
  badEvents.insert(badEvent);
}

void NeroSkimmer::AddLumi(int run, int lumi) {
  auto run_ = goodLumiList.find(run);
  if (run_==goodLumiList.end()) {
    // does not exist
    std::unordered_set<int> newlist;
    newlist.insert(lumi);
    goodLumiList[run]=newlist;
  } else {
    // run exists
    run_->second.insert(lumi);
  }
}

bool NeroSkimmer::PassEventFilters() {
  EventObj currentEvent;
  currentEvent.run = runNumber; currentEvent.lumi = lumiNumber; currentEvent.evt = eventNumber;
  bool r=(badEvents.find(currentEvent)==badEvents.end());
  if (!r && DEBUG) PInfo("MitPanda::NeroSkimmer::PassEventFilters",TString::Format("failing %i %i %llu due to filter",runNumber,lumiNumber,eventNumber));
  return r;
}

bool NeroSkimmer::PassGoodLumis() {
  auto list_ = goodLumiList.find(runNumber);
  if (list_!=goodLumiList.end()) {
    bool r=(list_->second.find(lumiNumber) != list_->second.end());
    if (!r && DEBUG) PInfo("MitPanda::NeroSkimmer::PassGoodLumis",TString::Format("failing %i %i %llu due to lumi list",runNumber,lumiNumber,eventNumber));
    return r;
  }
  if (DEBUG) PInfo("MitPanda::NeroSkimmer::PassGoodLumis",TString::Format("failing %i %i %llu due to lumi list",runNumber,lumiNumber,eventNumber));
  return false;
}

bool NeroSkimmer::PassPreselection() {
  if (preselBits==0)
    return true;
  if (preselBits & kDY) {
    if (nLooseLep==2 && (nLooseElectron==0||nLooseMuon==0) && 60<diLepMass && diLepMass<120) {
     return true;
    } 
  }
  bool isGood=false;
  if (preselBits & kMonotopCA15) {
    if (nSelectedCA15fj==1 && CA15fj1_pt>250) { 
      if ( (puppimet>200 || UZmag>200 || UWmag>200 || UAmag>200) ||
            (met>200 || pfUZmag>200 || pfUWmag>200 || pfUAmag>200) ) {
            isGood = true;
      }
    }
  }
  if (preselBits & kMonotopRes) {
    isGood = (nSelectedJet>2 && (puppimet>150 || met>150));
  }
  if (preselBits & kMonojet) {
    if (nSelectedJet>0 && jet1Pt>100 && jet1IsTight==1 && TMath::Abs(jet1Eta)<2.4) {
      if ( (met>180 || pfUZmag>180 || pfUWmag>180 || pfUAmag>180) ||
            (puppimet>180 || UZmag>180 || UWmag>180 || UAmag>180) ) {
        isGood = true;
      }
    }
  }
  if (preselBits & kVBF) {
    if (nSelectedJet>1 && jet1Pt>100 && jet1IsTight==1) {
      if ( (met>180 || pfUZmag>180 || pfUWmag>180 || pfUAmag>180) ||
            (puppimet>180 || UZmag>180 || UWmag>180 || UAmag>180) ) {
        isGood = true;
      }
    }
  }

  // triggers are special. anded with everything else
  if ((preselBits & kTriggers) && isData) {
    isGood &= (trigger!=0);
  }
  
  return isGood;
}

bool NeroSkimmer::IsMatched(std::vector<unsigned int> *leptonIndices, std::vector<unsigned int> *photonIndices, double deltaR2, double eta, double phi) {
  if (leptonIndices) {  
    for (auto iL : *leptonIndices) {
      if (leptons->pt(iL)>0) {
        double test_deltaR2 = DeltaR2(leptons->eta(iL),leptons->phi(iL),eta,phi);
        if (test_deltaR2 < deltaR2)
          return true;
      }
    }
  }
  if (photonIndices) {
    for (auto iA : *photonIndices) {
      if (photons->pt(iA)>0) {
        double test_deltaR2 = DeltaR2(photons->eta(iA),photons->phi(iA),eta,phi);
        if (test_deltaR2 < deltaR2)
          return true;
      }
    }
  }
  return false;
}

void NeroSkimmer::EvalBtagSF(std::vector<btagcand> cands, std::vector<double> sfs, float &sf0, float &sf1) {
  sf0 = 1; sf1 = 1;
  float prob_mc0=1, prob_data0=1;
  float prob_mc1=0, prob_data1=0;
  unsigned int nC = cands.size();

  for (unsigned int iC=0; iC!=nC; ++iC) {
    double sf_i = sfs[iC];
    double eff_i = cands[iC].eff;
    prob_mc0 *= (1-eff_i);
    prob_data0 *= (1-sf_i*eff_i);
    float tmp_mc1=1, tmp_data1=1;
    for (unsigned int jC=0; jC!=nC; ++jC) {
      if (iC==jC) continue;
      double sf_j = sfs[jC];
      double eff_j = cands[jC].eff;
      tmp_mc1 *= (1-eff_j);
      tmp_data1 *= (1-eff_j*sf_j);
    }
    prob_mc1 += eff_i * tmp_mc1;
    prob_data1 += eff_i * sf_i * tmp_data1;
  }
  
  if (nC>0) {
    sf0 = prob_data0/prob_mc0;
    sf1 = prob_data1/prob_mc1;
  }
}

void NeroSkimmer::EvalBtagSF(std::vector<btagcand> cands, std::vector<double> sfs, float &sf0, float &sf1, float &sf2) { 
  EvalBtagSF(cands,sfs,sf0,sf1); // get 0,1

  sf2=1;
  float prob_mc2=0, prob_data2=0;
  unsigned int nC = cands.size();

  for (unsigned int iC=0; iC!=nC; ++iC) {
    double sf_i = sfs[iC], eff_i = cands[iC].eff;
    for (unsigned int jC=iC+1; jC!=nC; ++jC) {
      double sf_j = sfs[jC], eff_j = cands[jC].eff;
      float tmp_mc2=1, tmp_data2=1;
      for (unsigned int kC=0; kC!=nC; ++kC) {
        if (kC==iC || kC==jC) continue;
        double sf_k = sfs[kC], eff_k = cands[kC].eff;
        tmp_mc2 *= (1-eff_k);
        tmp_data2 *= (1-eff_k*sf_k);
      }
      prob_mc2 += eff_i * eff_j * tmp_mc2;
      prob_data2 += eff_i * sf_i * eff_j * sf_j * tmp_data2;
    }
  }

  if (nC>0) {
    sf2 = prob_data2/prob_mc2;
  }

}

double NeroSkimmer::EvalCSVWeight(unsigned int iPt, unsigned int iEta, float csv, int flav, unsigned int iSys) {
  if (csv<0 || csv>1)
    return 1;
  if (flav==5) {
    return getVal(hBNew[iPt][iSys],csv);
  } else if (flav==4) {
    return getVal(hCNew[iPt][iSys],csv);
  } else if (flav==0) {
    return getVal(hLFNew[iPt][iEta][iSys],csv);
  }
  PError("MitPanda::NeroSkimmer::EvalCSVWeight",TString::Format("Encountered unrecognized flavor %i",flav).Data());
  return 1;
}

bool NeroSkimmer::Run(double scaledXsec, Long64_t entry/*=-1*/) {
  if (scaledXsec!=1) PError("MitPanda::NeroSkimmer::Run","WARNING: you are using a scaled xsec != 1. This is discouraged");
    // better to normalize offline
  unsigned int nZero, nEntries;
  if (entry>=0) {
    nZero = entry;
    nEntries = entry+1;
  } else {
    nZero = 0;
    nEntries = tIn->GetEntries();
  }
  if (!fOut || !tOut || !tIn) {
    PError("MitPanda::NeroSkimmer::Run","NOT SETUP CORRECTLY");
    exit(1);
  }
  if (nZero==0 && maxEvents>0)
    nEntries = maxEvents;

//  nZero = nEntries-1000;

  float treeMCWeight = 0; if (!isData) tIn->SetBranchAddress("mcWeight",&treeMCWeight);

  float puMin = 0, puMax=999;
  float sf_eleEtaMax=9999, sf_muEtaMax=9999;
  float sf_elePtMin=0, sf_elePtMax=9999;
  float sf_muPtMin=0, sf_muPtMax=9999;
  float genBosonPtMin=150, genBosonPtMax=1000;
  if (!isData) {
    puMin = hPUWeight->GetBinCenter(1);
    puMax = hPUWeight->GetBinCenter(hPUWeight->GetNbinsX());

    const TAxis *sf_eleEta = hEleTight->GetXaxis();
    sf_eleEtaMax = sf_eleEta->GetBinCenter(sf_eleEta->GetNbins());

    const TAxis *sf_elePt = hEleTight->GetYaxis();
    sf_elePtMin = sf_elePt->GetBinCenter(1); sf_elePtMax = sf_elePt->GetBinCenter(sf_elePt->GetNbins());

    const TAxis *sf_muEta = hMuTight->GetXaxis();
    sf_muEtaMax = sf_muEta->GetBinCenter(sf_muEta->GetNbins());

    const TAxis *sf_muPt = hMuTight->GetYaxis();
    sf_muPtMin = sf_muPt->GetBinCenter(1); sf_muPtMax = sf_muPt->GetBinCenter(sf_muPt->GetNbins());

    genBosonPtMin = hZNLO->GetBinCenter(1); genBosonPtMax = hZNLO->GetBinCenter(hZNLO->GetNbinsX());
  }

  std::vector<double> vbtagpt {50,70,100,140,200,300,670};
  std::vector<double> beff  {0.60592 , 0.634613, 0.645663, 0.646712, 0.649283, 0.621973, 0.554093};
  std::vector<double> ceff  {0.122067, 0.119659, 0.123723, 0.132141, 0.143654, 0.143127, 0.133581};
  std::vector<double> lfeff {0.014992, 0.012208, 0.011654, 0.011675, 0.015165, 0.016569, 0.020099};
  Binner btagpt(vbtagpt);

  std::vector<double> vnewbtagpt {30,40,60,100,160};
  std::vector<double> vnewbtageta {0.8,1.6,2.41};
  Binner newbtagpt(vnewbtagpt);
  Binner newbtageta(vnewbtageta);

  std::vector<unsigned int> metTriggers {0,1,2,3};
  std::vector<unsigned int> eleTriggers {4,5};
  std::vector<unsigned int> phoTriggers {13,14};
  std::vector<unsigned int> muTriggers  {9,10,11};
  if (!fromBambu) {
    metTriggers = {0,1,2,3,4,5,6};
    eleTriggers = {14,15,16,17,18};
    phoTriggers = {20,21,24}; // removing DoubleEG and gamma+MET triggers
    //eleTriggers = {14,15,16,17,18,19};
    //phoTriggers = {19,20,21,22,23,24};
    muTriggers = {7,8,9,10,12,12,13};
  }

  TStopwatch *sw = 0;
  if (DEBUG) sw = new TStopwatch();
  unsigned int iE=0;
  ProgressReporter pr("MitPanda::NeroSkimmer::Run",&iE,&nEntries,10);

  for (iE=nZero; iE!=nEntries; ++iE) {
    if (DEBUG) sw->Start(true);
    pr.Report();
    ResetBranches();
    tIn->GetEntry(iE);
    // event info
    // float tmpWeight = (event->mcWeight>0) ? 1 : -1;
    float tmpWeight = (treeMCWeight>=0) ? 1 : -1;
    mcWeight = tmpWeight*scaledXsec;
    runNumber = event->runNum;
    lumiNumber = event->lumiNum;
    eventNumber = event->eventNum;
    metFilter = (event->selBits&1);
    unsigned int evtSel = event->selBits;
    newMetFilter = ( (evtSel&1) && (evtSel&(1<<8)) && (evtSel&(1<<9)) ) ? 1 : 0; // factorize 2016 filters
    truepu = genParticles->puTrueInt;
    npv = vertices->npv;
    if (!isData)
      puWeight = getVal(hPUWeight,bound(npv,puMin,puMax));

    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("0: %u %i %i %llu %f",iE,runNumber,lumiNumber,eventNumber,sw->RealTime()*1000)); sw->Start(); }

    if (isData && applyJson && !(PassGoodLumis() && PassEventFilters()))
      continue;

    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("0.5: %f",sw->RealTime()*1000)); sw->Start(); }

    // met
    met = mets->pt(0);
    metphi = mets->phi(0);
    TLorentzVector vMET,vPuppiMET; 
    vMET.SetPtEtaPhiM(met,0,metphi,0);
    puppimet = mets->metPuppi->Pt();
    puppimetphi = mets->metPuppi->Phi();
    vPuppiMET.SetPtEtaPhiM(puppimet,0,puppimetphi,0);
    calomet = mets->caloMet_Pt;
    calometphi = mets->caloMet_Phi;
    pfcalobalance = TMath::Abs(met-calomet)/met;
    if (usePuppiMET) {
      sumET = mets->sumEtRawPuppi;
    } else {
      sumET = mets->sumEtRaw;
    }
//    trkmet = mets->trackMet->Pt();
    TVector2 vMETNoMu; vMETNoMu.SetMagPhi(met,metphi);

    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("1: %f",sw->RealTime()*1000)); sw->Start(); }

    // leptons
    std::vector<unsigned int> looseLeptons;
    std::vector<unsigned int> cleaningLeptons; // for further cleaning
    std::vector<unsigned int> cleaningElectrons; // for further cleaning
    unsigned int nAllLeps = leptons->size();
    for (unsigned int iL=0; iL!=nAllLeps; ++iL) {
      float pt = leptons->pt(iL);
      if (pt<1)
        continue;
      float eta = leptons->eta(iL);
      int pdgId = leptons->pdgId->at(iL);
      float iso = leptons->iso->at(iL);
      if (TMath::Abs(pdgId)==11 ) {
        // electron
        float aeta = TMath::Abs(eta);
        if (pt<10 || TMath::Abs(eta)>2.5 || (aeta>1.4442 && aeta<1.566))
          continue;
        if (!(leptons->passSelection(iL,BareLeptons::LepVeto) && 
              LeptonIsolation(pt,eta,iso,pdgId,kIsoVeto)))
          continue;
        ++nLooseLep; ++nLooseElectron;
        if (nLooseLep==1) {
          looseLep1PdgId=pdgId;    
          looseLep1Pt = pt; looseLep1Eta = eta; looseLep1Phi = leptons->phi(iL);
        }
        else if (nLooseLep==2) {
          looseLep2PdgId=pdgId;
          looseLep2Pt = pt; looseLep2Eta = eta; looseLep2Phi = leptons->phi(iL);
          if (looseLep1IsTight==1) {
            cleaningLeptons.push_back(iL); // if leading lepton is tight, save the subleading loose one
            cleaningElectrons.push_back(iL);
          }
        }
        looseLeptons.push_back(iL);  
        
        if (leptons->passSelection(iL,BareLeptons::LepTight) &&
            LeptonIsolation(pt,eta,iso,pdgId,kIsoTight) &&  
            pt>40 && TMath::Abs(eta)<2.5) {
          if (nLooseLep==1)
            looseLep1IsTight=1;
          else if (nLooseLep==2)
            looseLep2IsTight=1;
          nTightElectron++;
          if (nTightElectron==1) {
            cleaningLeptons.push_back(iL); // save first tight electron
            cleaningElectrons.push_back(iL);
          }
        }

      } else if (TMath::Abs(pdgId)==13 ) {
        // muon
        if (pt<10 || TMath::Abs(eta)>2.4)
          continue;
        if (!(leptons->passSelection(iL,BareLeptons::LepLoose) && 
              LeptonIsolation(pt,eta,iso,pdgId,kIsoLoose)))
          continue;
        TVector2 vMu;
        if (pt>0 && leptons->phi(iL)==leptons->phi(iL))
          vMu.SetMagPhi(pt,leptons->phi(iL));
        else
          vMu.SetMagPhi(0,0);
        vMETNoMu = vMETNoMu + vMu;
        ++nLooseLep; ++nLooseMuon;
        if (nLooseLep==1){
          looseLep1PdgId=pdgId;    
          looseLep1Pt = pt; looseLep1Eta = eta; looseLep1Phi = leptons->phi(iL);
        }
        else if (nLooseLep==2) {
          looseLep2PdgId=pdgId;
          looseLep2Pt = pt; looseLep2Eta = eta; looseLep2Phi = leptons->phi(iL);
          if (looseLep1IsTight==1)
            cleaningLeptons.push_back(iL); // if leading lepton is tight, save the subleading loose one
        }
        looseLeptons.push_back(iL);  

        if (leptons->passSelection(iL,BareLeptons::LepTight) && 
            LeptonIsolation(pt,eta,iso,pdgId,kIsoTight) && 
            pt>20 && TMath::Abs(eta)<2.4) {
          if (nLooseLep==1)
            looseLep1IsTight=1;
          else if (nLooseLep==2)
            looseLep2IsTight=1;
          nTightMuon++;
          if (nTightMuon==1)
            cleaningLeptons.push_back(iL); // save first tight muon
        }
      }
    }
    metnomu = vMETNoMu.Mod();


    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("2: %f",sw->RealTime()*1000)); sw->Start(); }

    // photons
    unsigned int nAllPhotons = photons->size();
    std::vector<unsigned int> loosePhotons,cleaningPhotons; // tight photons are for cleaning
    for (unsigned int iA=0; iA!=nAllPhotons; ++iA) {
      if ( (photons->selBits->at(iA) & BarePhotons::PhoLoose)==0 )
        continue;
      float pt = photons->pt(iA); 
      if (pt<1)
        continue;
      float eta = photons->eta(iA), phi = photons->phi(iA);
      if (pt<15 || TMath::Abs(eta)>2.5) {
        continue;
      }
      if (IsMatched(&cleaningElectrons,0,0.16,eta,phi)) {
        continue;
      }
      nLoosePhoton++;
      if (nLoosePhoton==1) {
          loosePho1Pt = pt; loosePho1Eta = eta; loosePho1Phi = phi;
      }
      loosePhotons.push_back(iA);
      if ( ((photons->selBits->at(iA) & BarePhotons::PhoMedium)!=0) &&
           pt>175 && TMath::Abs(eta)<1.4442 ) {
        if (nLoosePhoton==1){
          loosePho1IsTight=1;
        }
        nTightPhoton++;
        cleaningPhotons.push_back(iA);
      } 
    }

    // add photon purity weight if data (only relevant for SinglePhoton, but do it for everything for now)
    if (isData && nLoosePhoton>0) {
      if (loosePho1Pt>=175 && loosePho1Pt<200)
        photonPurityWeight = 0.04802;
      else if (loosePho1Pt>=200 && loosePho1Pt<250)
        photonPurityWeight = 0.04241;
      else if (loosePho1Pt>=250 && loosePho1Pt<300)
        photonPurityWeight = 0.03641;
      else if (loosePho1Pt>=300 && loosePho1Pt<350)
        photonPurityWeight = 0.0333;
      else if (loosePho1Pt>=350)
        photonPurityWeight = 0.02544;
    }

    sf_eleTrig = 1;
    sf_metTrig = 1;
    sf_phoTrig = 1;
    if (!isData) {
      sf_metTrig = getVal(hMetTrig,bound(metnomu,0,1000));

      if (nLooseElectron>0 && TMath::Abs(looseLep1PdgId)==11 && looseLep1IsTight==1 && nLooseMuon==0) {
        float eff1=0, eff2=0;
        if (looseLep1Pt<100) {
          eff1 = getVal(hEleTrigLow,bound(looseLep1Eta,-1.*sf_eleEtaMax,sf_eleEtaMax),bound(looseLep1Pt,0,sf_elePtMax));
        } else {
          if (TMath::Abs(looseLep1Eta)<1.4442) {
            eff1 = getVal(hEleTrigB,bound(looseLep1Pt,0,1000));
          }
          if (1.5660<TMath::Abs(looseLep1Eta) && TMath::Abs(looseLep1Eta)<2.5) {
            eff1 = getVal(hEleTrigE,bound(looseLep1Pt,0,1000));
          }
        }
        if (nLooseElectron>1 && TMath::Abs(looseLep2PdgId)==11) {
          if (looseLep2Pt<100) {
            eff2 = getVal(hEleTrigLow,bound(looseLep2Eta,-1.*sf_eleEtaMax,sf_eleEtaMax),bound(looseLep2Pt,0,sf_elePtMax));
          } else {
            if (TMath::Abs(looseLep2Eta)<1.4442) {
              eff2 = getVal(hEleTrigB,bound(looseLep2Pt,0,1000));
            }
            if (1.5660<TMath::Abs(looseLep2Eta) && TMath::Abs(looseLep2Eta)<2.5) {
              eff2 = getVal(hEleTrigE,bound(looseLep2Pt,0,1000));
            }
          }
        }
        sf_eleTrig = 1 - (1-eff1)*(1-eff2);
      }

      if (nLoosePhoton>0 && loosePho1IsTight)
        sf_phoTrig = getVal(hPhoTrig,bound(loosePho1Pt,160,1000));
    }

    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("3: %f",sw->RealTime()*1000)); sw->Start(); }

    // things to do with leptons and photons:
    // reco the boson and compute recoils
    TLorentzVector vObj1, vObj2; // stuff recoils are wrt to
    TLorentzVector vUW, vUZ, vUA;
    TLorentzVector vpfUW, vpfUZ, vpfUA;
    if (nLooseLep>0) { 
      // W recoil
      int lep1Idx = looseLeptons[0]; 
      float lepMass = (TMath::Abs(looseLep1PdgId)==11) ? ELECTRON_MASS : MUON_MASS;
      vObj1.SetPtEtaPhiM(leptons->pt(lep1Idx),leptons->eta(lep1Idx),leptons->phi(lep1Idx),lepMass);
      
      vUW = vPuppiMET+vObj1;
      UWmag = vUW.Pt(); UWphi = vUW.Phi();

      vpfUW = vMET+vObj1;
      pfUWmag = vpfUW.Pt(); pfUWphi = vpfUW.Phi();

      if (nLooseLep==1) { recoBosonPt = UWmag; recoBosonPhi = UWphi; }
      if (nLooseLep>1 && looseLep1PdgId+looseLep2PdgId==0) {
        // Z recoil: opposite sign
        int lep2Idx = looseLeptons[1]; 
        vObj2.SetPtEtaPhiM(leptons->pt(lep2Idx),leptons->eta(lep2Idx),leptons->phi(lep2Idx),lepMass); 
        recoBosonPt = (vObj1+vObj2).Pt(); recoBosonPhi = (vObj1+vObj2).Phi();
        diLepMass = (vObj1+vObj2).M();
      
        vUZ = vUW+vObj2;
        UZmag = vUZ.Pt(); UZphi = vUZ.Phi();
      
        vpfUZ = vpfUW+vObj2;
        pfUZmag = vpfUZ.Pt(); pfUZphi = vpfUZ.Phi();
      
        TLorentzVector vU;
        vU.SetPtEtaPhiM(UZmag,0,UZphi+TMath::Pi()-recoBosonPhi,0);
        Uperp = vU.Py(); Upara =  vU.Px();
      
        TLorentzVector vpfU;
        vpfU.SetPtEtaPhiM(pfUZmag,0,pfUZphi+TMath::Pi()-recoBosonPhi,0);
        pfUperp = vpfU.Py(); pfUpara =  vpfU.Px();
      }
    }
    if (nLoosePhoton>0) {
      int pho1Idx = loosePhotons[0];
      vObj1.SetPtEtaPhiM(photons->pt(pho1Idx),photons->eta(pho1Idx),photons->phi(pho1Idx),0);

      vUA = vPuppiMET+vObj1;
      UAmag = vUA.Pt(); UAphi = vUA.Phi();

      vpfUA = vMET+vObj1;
      pfUAmag = vpfUA.Pt(); pfUAphi = vpfUA.Phi();

      if (nLooseLep==0) { recoBosonPt = photons->pt(pho1Idx); recoBosonPhi = photons->phi(pho1Idx); }
    }

    unsigned int nAllGenParticles=0;
    if (!isData) {
      nAllGenParticles = genParticles->size();
      nGenJet = genParticles->jetP4->GetEntries();
      if (nGenJet>0) {
        genJet1Pt = ((TLorentzVector*)genParticles->jetP4->At(0))->Pt();
        genJet1Eta = ((TLorentzVector*)genParticles->jetP4->At(0))->Eta();
      }
    }

    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("4: %f",sw->RealTime()*1000)); sw->Start(); }

    unsigned int nAllCA15 = ca15Jets->size();
    for (unsigned int iJ=0; iJ!=nAllCA15; ++iJ) {
      float pt = ca15Jets->pt(iJ); 
      float rawpt = ca15Jets->rawPt->at(iJ);
      float ptCut = 180;
      if (pt<1)
        continue;
      float eta = ca15Jets->eta(iJ);
      if (pt<ptCut || rawpt<ptCut || TMath::Abs(eta)>2.4)
        continue;

      float phi = ca15Jets->phi(iJ);
      if (IsMatched(&cleaningLeptons,&cleaningPhotons,2.25,eta,phi))
        continue;
      nSelectedCA15fj++;

      if (nSelectedCA15fj==1) {
        CA15fj1_pt    = pt; 
        CA15fj1_eta   = eta;
        CA15fj1_phi   = ca15Jets->phi(iJ);
        CA15fj1_tau1  = ca15Jets->tau1->at(iJ);
        CA15fj1_tau2  = ca15Jets->tau2->at(iJ);
        CA15fj1_tau3  = ca15Jets->tau3->at(iJ);
        CA15fj1_mSD   = ca15Jets->softdropMass->at(iJ);
        CA15fj1_rawPt = ca15Jets->rawPt->at(iJ);

        // do jec unc
//        ak8unc->setJetPt(pt); ak8unc->setJetEta(eta);
//        float unc = ak8unc->getUncertainty(true);
//        CA15fj1_ptUp = pt*(1+unc); CA15fj1_ptDown = pt*(1-unc);

        if ((ca15Jets->selBits->at(iJ) & BareJets::JetLoose)!=0) 
          CA15fj1_isLoose=1;
        if ((ca15Jets->selBits->at(iJ) & BareJets::mjId2015)!=0) 
          CA15fj1_isTight=1;

        int nSubjets = ca15Jets->nSubjets->at(iJ);
        int firstSubjet = ca15Jets->firstSubjet->at(iJ);
        CA15fj1_maxsjbtag = -10;
        CA15fj1_minsjbtag = 10;
        std::vector<int> sjIndices(nSubjets),sjFlavors(nSubjets,0);
        std::vector<float> sjCSVs(nSubjets,-1.);
        std::vector<float> sjMinDRs(nSubjets,999.);
        std::vector<btagcand> sjbtagcands;
        for (int iSJ=firstSubjet; iSJ!=nSubjets+firstSubjet; ++iSJ) {
          sjIndices[iSJ-firstSubjet]=iSJ;
          sjCSVs[iSJ-firstSubjet] = ca15Jets->subjet_btag->at(iSJ);
          CA15fj1_maxsjbtag = TMath::Max(CA15fj1_maxsjbtag,ca15Jets->subjet_btag->at(iSJ));
          CA15fj1_minsjbtag = TMath::Min(CA15fj1_minsjbtag,ca15Jets->subjet_btag->at(iSJ));
        }
        TClonesArray *subjets = ca15Jets->subjet;
        int iLeadingSJ=-1;
        for (int iSJ=0; iSJ!=nSubjets; ++iSJ) {
          TLorentzVector *sj = (TLorentzVector*) subjets->At(sjIndices[iSJ]);
          if (sj->Pt() > sj1Pt) {
            sj1Pt = sj->Pt();
            iLeadingSJ = iSJ;
          }
        }
        if (!isData) {
          CA15fj1_isHF=false; CA15fj1_isMatched=false;
          int topIndex=-1;
          for (unsigned int iP=0; iP!=nAllGenParticles; ++iP) { // first loop to match jet to c,b,t
                                                                // and also match partons to subjets
            int absPdgId = TMath::Abs(genParticles->pdgId->at(iP));
            if ((absPdgId>6||absPdgId<4)&&(absPdgId!=21)) 
              continue;
            if (absPdgId==4 || absPdgId==5)
              isHF=true;
            float genEta = genParticles->eta(iP);
            float genPhi = genParticles->phi(iP);
            if (DeltaR2(CA15fj1_eta,CA15fj1_phi,genEta,genPhi)<2.25) {
              if (absPdgId==4 || absPdgId==5) 
                CA15fj1_isHF=true;
              else if (absPdgId==6)
                topIndex=iP;
            }
            if (absPdgId==6)
              continue; // no need to match tops to subjets
            for (int iSJ=0; iSJ!=nSubjets; ++iSJ) {
              if (sjFlavors[iSJ]!=0)
                continue;
              TLorentzVector *sj = (TLorentzVector*) subjets->At(sjIndices[iSJ]);
              float dr2 = DeltaR2(sj->Eta(),sj->Phi(),genEta,genPhi);
              if (dr2<0.16) {
                if (absPdgId==5||absPdgId==4)
                  sjFlavors[iSJ] = absPdgId;
                else
                  sjFlavors[iSJ] = 0; // lump all LF together
              } // matched
            } // subjet loop
          } // parton loop

          if (iLeadingSJ>=0) {
            sj1Flav = sjFlavors[iLeadingSJ];
          }

          if (topIndex>=0) { // match fatjet to merged top
            float topEta = genParticles->eta(topIndex), topPhi = genParticles->phi(topIndex);
            float topSize=0;
            unsigned int foundBits=0;
            for (unsigned int iP=0; iP!=nAllGenParticles; ++iP) { // second loop to match t to bqq'
              int absPdgId = TMath::Abs(genParticles->pdgId->at(iP));
              if (absPdgId>5)
                continue;
              if (absPdgId==5) {
                int bParent = genParticles->parent->at(iP);
                if (bParent==topIndex) {
                  foundBits |= (1<<0);
                  float genEta = genParticles->eta(iP);
                  float genPhi = genParticles->phi(iP);
                  topSize = (float)TMath::Max(topSize,(float)DeltaR2(genEta,genPhi,topEta,topPhi));
                } // b parent is matched top
              } else {
                int qParent = genParticles->parent->at(iP);
                if (qParent<0)
                  continue;
                if (TMath::Abs(genParticles->pdgId->at(qParent))==24) {
                  int wParent = genParticles->parent->at(qParent);
                  if (wParent==topIndex) {
                    foundBits |= ((foundBits&2)==0) ? (1<<1) : (1<<2);
                    float genEta = genParticles->eta(iP);
                    float genPhi = genParticles->phi(iP);
                    topSize = (float)TMath::Max(topSize,(float)DeltaR2(genEta,genPhi,topEta,topPhi));
                  } // w parent is top
                } // q parent is w
              } // q is LF
              if (foundBits==7 || topSize>1.44) // no need to proceed
                break;
            } // loop over gen particles
            if (foundBits==7 && topSize<1.44)
              CA15fj1_isMatched=true;
          } // found a gen top
         
          // evaluate CSV weight for subjets
          sf_newSjBtag=1;
          sf_newSjBtagHFUp=1;
          sf_newSjBtagHFDown=1;
          sf_newSjBtagLFUp=1;
          sf_newSjBtagLFDown=1;
          for (int iSJ=0; iSJ!=nSubjets; ++iSJ) {
            TLorentzVector *sj = (TLorentzVector*) subjets->At(sjIndices[iSJ]);
            float sjPt = sj->Pt();
            if (sjPt<20)
              continue;
            float sjEta = TMath::Abs(sj->Eta());
            if (sjEta>2.41)
              continue;
            unsigned int iPt = newbtagpt.bin(sjPt);
            unsigned int iEta = newbtageta.bin(sjEta);
            int flavor = sjFlavors[iSJ];
            float csv = sjCSVs[iSJ];
            if (flavor>0 && iPt>=NHFPTBINS)
              iPt = NHFPTBINS-1;
            else if (flavor==0 && iPt>=NLFPTBINS)
              iPt = NLFPTBINS-1;
            float cent = EvalCSVWeight(iPt,iEta,csv,flavor,0);
            float hfup = cent, hfdown = cent, lfup = cent, lfdown = cent;
            if (flavor==5) {
              hfup   = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,1)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,3)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,5)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,7)-cent,2) );
              hfdown = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,2)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,4)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,6)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,8)-cent,2) );
              hfup = bound(cent+hfup,0,999); hfdown = bound(cent-hfdown,0,999);
            } else if (flavor==4) {
              hfup   = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,1)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,3)-cent,2) );
              hfdown = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,2)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,4)-cent,2) );
              hfup = bound(cent+hfup,0,999); hfdown = bound(cent-hfdown,0,999);
            } else {
              lfup   = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,1)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,3)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,5)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,7)-cent,2) );
              lfdown = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,2)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,4)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,6)-cent,2)
                                   +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,8)-cent,2) );
              lfup = bound(cent+lfup,0,999); lfdown = bound(cent-lfdown,0,999);
            }
            sf_newSjBtag *= cent;
            sf_newSjBtagHFUp *= hfup;
            sf_newSjBtagHFDown *= hfdown;
            sf_newSjBtagLFUp *= lfup;
            sf_newSjBtagLFDown *= lfdown;
          }

          // now let's evaluate subjet btag SFs
          std::vector<btagcand> sj_btagcands;
          std::vector<double> sj_sf_cent, sj_sf_bUp, sj_sf_bDown, sj_sf_mUp, sj_sf_mDown;
          for (int iSJ=0; iSJ!=nSubjets; ++iSJ) {
            TLorentzVector *sj = (TLorentzVector*) subjets->At(sjIndices[iSJ]);
            int flavor = sjFlavors[iSJ];
            double sjPtMax = (flavor<4) ? 1000 : 450.;
            float sjPt = sj->Pt();
            float sjEta = sj->Eta();
            float btagUncFactor=1;
            if (sjPt>sjPtMax) {
              btagUncFactor=2;
              sjPt = bound(sj->Pt(),0,sjPtMax-1); 
            }  
            double eff,sf,sfUp,sfDown;
            unsigned int bin = btagpt.bin(sjPt);
            if (flavor==5) {
              eff = beff[bin];
              sf = sj_hfReader->eval(BTagEntry::FLAV_B,sjEta,sjPt,0);
              sfUp = sj_hfUpReader->eval(BTagEntry::FLAV_B,sjEta,sjPt,0);
              sfDown = sj_hfDownReader->eval(BTagEntry::FLAV_B,sjEta,sjPt,0);
            } else if (flavor==4) {
              eff = ceff[bin];
              sf = sj_hfReader->eval(BTagEntry::FLAV_C,sjEta,sjPt,0);
              sfUp = sj_hfUpReader->eval(BTagEntry::FLAV_C,sjEta,sjPt,0);
              sfDown = sj_hfDownReader->eval(BTagEntry::FLAV_C,sjEta,sjPt,0);
            } else {
              eff = lfeff[bin];
              sf = sj_lfReader->eval(BTagEntry::FLAV_UDSG,sjEta,sjPt,0);
              sfUp = sj_lfUpReader->eval(BTagEntry::FLAV_UDSG,sjEta,sjPt,0);
              sfDown = sj_lfDownReader->eval(BTagEntry::FLAV_UDSG,sjEta,sjPt,0);
            }
            sfUp = btagUncFactor*(sfUp-sf)+sf;
            sfDown = btagUncFactor*(sfDown-sf)+sf;
            sj_btagcands.push_back(btagcand(sjIndices[iSJ],flavor,eff,sf,sfUp,sfDown));
            sj_sf_cent.push_back(sf);
            if (flavor>0) {
              sj_sf_bUp.push_back(sfUp); sj_sf_bDown.push_back(sfDown);
              sj_sf_mUp.push_back(sf); sj_sf_mDown.push_back(sf);
            } else {
              sj_sf_bUp.push_back(sf); sj_sf_bDown.push_back(sf);
              sj_sf_mUp.push_back(sfUp); sj_sf_mDown.push_back(sfDown);
            }
          } //subjet loop
          EvalBtagSF(sj_btagcands,sj_sf_cent,sf_sjbtag0,sf_sjbtag1,sf_sjbtag2);
          EvalBtagSF(sj_btagcands,sj_sf_bUp,sf_sjbtag0BUp,sf_sjbtag1BUp,sf_sjbtag2BUp);
          EvalBtagSF(sj_btagcands,sj_sf_bDown,sf_sjbtag0BDown,sf_sjbtag1BDown,sf_sjbtag2BDown);
          EvalBtagSF(sj_btagcands,sj_sf_mUp,sf_sjbtag0MUp,sf_sjbtag1MUp,sf_sjbtag2MUp);
          EvalBtagSF(sj_btagcands,sj_sf_mDown,sf_sjbtag0MDown,sf_sjbtag1MDown,sf_sjbtag2MDown);
        } // !isData
      } // first CA15
    } // CA15 loop

    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("5: %f",sw->RealTime()*1000)); sw->Start(); }

    // ak4 jets
    std::vector<unsigned int> cleanedJets, btagCandJets;
    TLorentzVector vTmpJet;
    unsigned int nAllAK4 = ak4Jets->size();
    for (unsigned int iJ=0; iJ!=nAllAK4; ++iJ) {
      float pt = ak4Jets->pt(iJ);
      if (pt<1)
        continue;
      float eta = ak4Jets->eta(iJ);
      float phi = ak4Jets->phi(iJ);
      std::vector<unsigned int> empty;
      if (!IsMatched(&cleaningLeptons,&cleaningPhotons,0.16,eta,phi))
        cleanedJets.push_back(iJ);
    }

    unsigned int indices[3] = {0,0,0};
    int iLeadingJ=-1; 
    for (auto iJ : cleanedJets) { 
      float pt = ak4Jets->pt(iJ);
      float eta = ak4Jets->eta(iJ);
      if (pt<30 || TMath::Abs(eta)>4.5 || 
          (ak4Jets->selBits->at(iJ) & BareJets::JetLoose)==0) {
        continue;
      }

      float phi = ak4Jets->phi(iJ);
      float mass = ak4Jets->mass(iJ);

      //dPhis to met and recoils
      vTmpJet.SetPtEtaPhiM(pt,eta,phi,mass);

      dphimet = std::min(TMath::Abs(vTmpJet.DeltaPhi(vMET)),(double)dphimet);
      dphipuppimet = std::min(TMath::Abs(vTmpJet.DeltaPhi(vPuppiMET)),(double)dphipuppimet);
      if (nLooseLep>0) {
        dphiUW = std::min(TMath::Abs(vTmpJet.DeltaPhi(vUW)),(double)dphiUW);
        dphipfUW = std::min(TMath::Abs(vTmpJet.DeltaPhi(vpfUW)),(double)dphipfUW);
        if (nLooseLep>1) {
          dphiUZ = std::min(TMath::Abs(vTmpJet.DeltaPhi(vUZ)),(double)dphiUZ);
          dphipfUZ = std::min(TMath::Abs(vTmpJet.DeltaPhi(vpfUZ)),(double)dphipfUZ);
        }
      }
      if (nLoosePhoton>0) {
        dphiUA = std::min(TMath::Abs(vTmpJet.DeltaPhi(vUA)),(double)dphiUA);
        dphipfUA = std::min(TMath::Abs(vTmpJet.DeltaPhi(vpfUA)),(double)dphipfUA);
      }
     
      ++nSelectedJet;
      
      float btag = ak4Jets->bDiscr->at(iJ);
      if (TMath::Abs(eta)>2.5)
        btag = -1;

      if (btag>0.460) 
        ++jetNBtags;

      if (nSelectedCA15fj>0 && TMath::Abs(eta)<2.41
           && DeltaR2(CA15fj1_eta,CA15fj1_phi,eta,phi)>2.25) {
        btagCandJets.push_back(iJ);
        if (pt>jetIso1Pt) {
          jetIso1Pt = pt;
          iLeadingJ = iJ;
        }
        if (btag>0.460)
          ++jetNIsoBtags;
      }
     
      if (nSelectedJet==1) {
        jet1Btag = btag;
        jet1Pt = pt;
        jet1Eta = eta;
        if ((ak4Jets->selBits->at(iJ) & BareJets::mjId2015)!=0) {
          jet1IsTight=1;
        }
        indices[0] = iJ;
      } else if (nSelectedJet==2) {
        jet2Btag = btag;
        jet2Pt = pt;
        jet2Eta = eta;
        indices[1] = iJ;
      } else if (nSelectedJet==3) {
        indices[2] = iJ;
      }       
    }
    
    if (nSelectedJet>2) {
      jet123Mass = (ak4Jets->momentum(indices[0]) + 
                    ak4Jets->momentum(indices[1]) + 
                    ak4Jets->momentum(indices[2])).M();
    }
    if (nSelectedJet>1) {
      jet12Mass = (ak4Jets->momentum(indices[0]) +
                   ak4Jets->momentum(indices[1])).M();
      jet12DEta = TMath::Abs(ak4Jets->eta(indices[0]) -
                             ak4Jets->eta(indices[1]));
    }

    // done with MET, recoil, jets, leptons
    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("6: %f",sw->RealTime()*1000)); sw->Start(); }


    // taus
    unsigned int nAllTaus = taus->size();
    for (unsigned int iT=0; iT!=nAllTaus; ++iT) {
      if ( (taus->selBits->at(iT) & 4)!=4 || (taus->selBits->at(iT) & 2)!=2 )
        continue;
      if ( taus->isoDeltaBetaCorr->at(iT)>5. ) 
        continue;
      float pt = taus->pt(iT); float eta = taus->eta(iT);
      if (pt<18 || TMath::Abs(eta)>2.3)
        continue;
      float phi = taus->phi(iT);
      if (IsMatched(&cleaningLeptons,0,0.16,eta,phi))
        continue;
      ++nSelectedTau;
    }

    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("7: %f",sw->RealTime()*1000)); sw->Start(); }
    
    // triggers
    if (isData) {
      for (auto iT : metTriggers) { if (triggers->triggerFired->at(iT)==1) trigger |= kMET; }
      for (auto iT : eleTriggers) { if (triggers->triggerFired->at(iT)==1) trigger |= kSingleEle; }
      for (auto iT : phoTriggers) { if (triggers->triggerFired->at(iT)==1) trigger |= kSinglePho; }
      for (auto iT : muTriggers)  { if (triggers->triggerFired->at(iT)==1) trigger |= kSingleMu; }
      
      // special crap for trigger studies
      if (!fromBambu&&false) {
        if (triggers->triggerFired->at(0)==1 || triggers->triggerFired->at(1)==1 || triggers->triggerFired->at(2)==1)
          trigger |= kMET170;
        if (triggers->triggerFired->at(3)==1 || triggers->triggerFired->at(4)==1 || triggers->triggerFired->at(5) || triggers->triggerFired->at(6))
          trigger |= kMETMHT;
      }
    }

    if (!PassPreselection())
      continue;  //objects are all built, no need to continue if failing preselection
    
    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("8: %f",sw->RealTime()*1000)); sw->Start(); }

    // reweight tt, described here: https://twiki.cern.ch/twiki/bin/viewauth/CMS/TopPtReweighting#Eventweight
    if (!isData && processType==kTT) {
      sf_tt = 1;
      float pt_t=0, pt_tbar=0;
      for (unsigned int iP=0; iP!=nAllGenParticles; ++iP) {
        int pdgid = genParticles->pdgId->at(iP);  
        if (pdgid==6)
          pt_t = TMath::Min(400.,genParticles->pt(iP));
        else if (pdgid==-6)
          pt_tbar = TMath::Min(400.,genParticles->pt(iP));
        if (pt_t>0 && pt_tbar>0)
          break;
      }
      sf_tt = TMath::Sqrt( TMath::Exp(0.156-0.00137*pt_t) * TMath::Exp(0.156-0.00137*pt_tbar) );
    }

    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("9: %f",sw->RealTime()*1000)); sw->Start(); }

    if (!isData) {
         
      // compute btag SFs
      sf_newBtag=1;
      sf_newBtagHFUp=1;
      sf_newBtagHFDown=1;
      sf_newBtagLFUp=1;
      sf_newBtagLFDown=1;
      std::vector<btagcand> btagcands;
      std::vector<double> sf_cent, sf_bUp, sf_bDown, sf_mUp, sf_mDown;
      for (auto iJ : btagCandJets) {
        float pt = ak4Jets->pt(iJ);
        float eta = ak4Jets->eta(iJ);
        float phi = ak4Jets->phi(iJ);
        float boundedpt = bound(pt,0,669.99);
        float btagUncFactor = (pt>670) ? 2. : 1.;
        int flavor = 0;

        // get parton
        for (unsigned int iP=0; iP!=nAllGenParticles; ++iP) {
          int absPdgId = TMath::Abs(genParticles->pdgId->at(iP));
          if (absPdgId>5 && absPdgId!=21)
            continue;
          float gen_eta = genParticles->eta(iP);
          float gen_phi = genParticles->phi(iP);
          float dr = DeltaR2(eta,phi,gen_eta,gen_phi);
          if (dr<0.16) {
            if (absPdgId==4||absPdgId==5) {
              flavor = absPdgId;
              break;
            } else
              flavor=0;
          }
        }
        if (iLeadingJ>=0 && (int)iJ==iLeadingJ) 
          jetIso1Flav=flavor;

        // first calculate CSV weight
        if (pt>20 && TMath::Abs(eta)<2.41) {
          unsigned int iPt = newbtagpt.bin(pt);
          unsigned int iEta = newbtageta.bin(TMath::Abs(eta));
          float csv = ak4Jets->bDiscr->at(iJ); 
          if (flavor>0 && iPt>=NHFPTBINS)
            iPt = NHFPTBINS-1;
          else if (flavor==0 && iPt>=NLFPTBINS)
            iPt = NLFPTBINS-1;
          float cent = EvalCSVWeight(iPt,iEta,csv,flavor,0);
          float hfup = cent, hfdown = cent, lfup = cent, lfdown = cent;
          if (flavor==5) {
            hfup   = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,1)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,3)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,5)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,7)-cent,2) );
            hfdown = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,2)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,4)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,6)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,8)-cent,2) );
            hfup = bound(cent+hfup,0,999); hfdown = bound(cent-hfdown,0,999);
          } else if (flavor==4) {
            hfup   = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,1)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,3)-cent,2) );
            hfdown = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,2)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,4)-cent,2) );
            hfup = bound(cent+hfup,0,999); hfdown = bound(cent-hfdown,0,999);
          } else {
            lfup   = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,1)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,3)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,5)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,7)-cent,2) );
            lfdown = TMath::Sqrt( TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,2)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,4)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,6)-cent,2)
                                 +TMath::Power(EvalCSVWeight(iPt,iEta,csv,flavor,8)-cent,2) );
            lfup = bound(cent+lfup,0,999); lfdown = bound(cent-lfdown,0,999);
          }
          sf_newBtag *= cent;
          sf_newBtagHFUp *= hfup;
          sf_newBtagHFDown *= hfdown;
          sf_newBtagLFUp *= lfup;
          sf_newBtagLFDown *= lfdown;
        }

        // now do CSV SF as described here https://twiki.cern.ch/twiki/bin/view/CMS/BTagSFMethods#1b_Event_reweighting_using_scale
        double eff,sf,sfUp,sfDown;
        unsigned int bin = btagpt.bin(boundedpt);
        if (flavor==5) {
          eff = beff[bin];
          sf = hfReader->eval(BTagEntry::FLAV_B,eta,boundedpt,0);
          sfUp = hfUpReader->eval(BTagEntry::FLAV_B,eta,boundedpt,0);
          sfDown = hfDownReader->eval(BTagEntry::FLAV_B,eta,boundedpt,0);
        } else if (flavor==4) {
          eff = ceff[bin];
          sf = hfReader->eval(BTagEntry::FLAV_C,eta,boundedpt,0);
          sfUp = hfUpReader->eval(BTagEntry::FLAV_C,eta,boundedpt,0);
          sfDown = hfDownReader->eval(BTagEntry::FLAV_C,eta,boundedpt,0);
        } else {
          eff = lfeff[bin];
          sf = lfReader->eval(BTagEntry::FLAV_UDSG,eta,boundedpt,0);
          sfUp = lfUpReader->eval(BTagEntry::FLAV_UDSG,eta,boundedpt,0);
          sfDown = lfDownReader->eval(BTagEntry::FLAV_UDSG,eta,boundedpt,0);
        }
        sfUp = btagUncFactor*(sfUp-sf)+sf;
        sfDown = btagUncFactor*(sfDown-sf)+sf;
        btagcands.push_back(btagcand(iJ,flavor,eff,sf,sfUp,sfDown));
        sf_cent.push_back(sf);
        if (flavor>0) {
          sf_bUp.push_back(sfUp); sf_bDown.push_back(sfDown);
          sf_mUp.push_back(sf); sf_mDown.push_back(sf);
        } else {
          sf_bUp.push_back(sf); sf_bDown.push_back(sf);
          sf_mUp.push_back(sfUp); sf_mDown.push_back(sfDown);
        }
      }

      EvalBtagSF(btagcands,sf_cent,sf_btag0,sf_btag1);
      EvalBtagSF(btagcands,sf_bUp,sf_btag0BUp,sf_btag1BUp);
      EvalBtagSF(btagcands,sf_bDown,sf_btag0BDown,sf_btag1BDown);
      EvalBtagSF(btagcands,sf_mUp,sf_btag0MUp,sf_btag1MUp);
      EvalBtagSF(btagcands,sf_mDown,sf_btag0MDown,sf_btag1MDown);

    } 

    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("10: %f",sw->RealTime()*1000)); sw->Start(); }
  
    // get out the gen boson and scale factor
    if (!isData) {
      bool found=(processType!=kA&&processType!=kZ&&processType!=kW); 
        // if we don't need to look for a boson, let's pretend we already found it
      int targetPdgId=24;
      if (processType==kZ) targetPdgId=23;
      else if (processType==kA) targetPdgId=22;

      for (unsigned int iP=0; iP!=nAllGenParticles; ++iP) {
        if (found) 
          break;
        int absPdgId = TMath::Abs(genParticles->pdgId->at(iP));
        if (absPdgId!=targetPdgId) 
          continue;
        float tmp_genBosonPt = genParticles->pt(iP);
        float tmp_genBosonPhi = genParticles->phi(iP);
        if (processType==kZ) {
          trueGenBosonPt = tmp_genBosonPt;
          genBosonPt = bound(tmp_genBosonPt,genBosonPtMin,genBosonPtMax);
          genBosonPhi = tmp_genBosonPhi;
          sf_qcdZ = getVal(hZNLO,genBosonPt);
          sf_ewkZ = getVal(hZEWK,genBosonPt);
          found=true;
        } else if (processType==kA) {
          // for photons, we take the highest pT
          if (tmp_genBosonPt>genBosonPt) {
            trueGenBosonPt = tmp_genBosonPt;
            genBosonPt = bound(tmp_genBosonPt,genBosonPtMin,genBosonPtMax);
            genBosonPhi = tmp_genBosonPhi;
            sf_qcdA = getVal(hANLO,genBosonPt);
            sf_ewkA = getVal(hAEWK,genBosonPt);
          }
        } else if (processType==kW) {
          trueGenBosonPt = tmp_genBosonPt;
          genBosonPt = bound(tmp_genBosonPt,genBosonPtMin,genBosonPtMax);
          genBosonPhi = tmp_genBosonPhi;
          sf_qcdW = getVal(hWNLO,genBosonPt);
          sf_ewkW = getVal(hWEWK,genBosonPt);
          found=true;
        }
      }

      if (processType==kMonotopSignal) {
        for (unsigned int iP=0; iP!=nAllGenParticles; ++iP) {
          int absPdgId = TMath::Abs(genParticles->pdgId->at(iP));
          if (absPdgId==9000002||absPdgId==9000003||absPdgId==32) { // resonant fermion or fcnc mediators
            trueGenBosonPt = genParticles->pt(iP);
            genBosonPt = bound(trueGenBosonPt,genBosonPtMin,genBosonPtMax);
            genBosonPhi = genParticles->phi(iP);
            genBosonEta = genParticles->eta(iP);
            genBosonMass = genParticles->mass(iP);
          } else if (absPdgId==6) {
            genTopPt = genParticles->pt(iP);
          }
          if (genBosonPt>0 && genTopPt>0)
            break;
        }
      }
    }
    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("11: %f",sw->RealTime()*1000)); sw->Start(); }

    // lepton SFs
    if (!isData) {
      sf_lep=1;
      if (nLooseLep>0) {
        int idx = looseLeptons[0];
        if (TMath::Abs(looseLep1PdgId)==11) {
          float pt = bound(leptons->pt(idx),sf_elePtMin,sf_elePtMax);
          float eta = bound(TMath::Abs(leptons->eta(idx)),0,sf_eleEtaMax);
          if (looseLep1IsTight==1) 
            sf_lep = getVal(hEleTight,eta,pt);
          else
            sf_lep = getVal(hEleVeto,eta,pt);
          sf_lepTrack = getVal(hEleTrack,bound(leptons->eta(idx),-2.5,2.5),bound(npv,0,50));
        } else if (TMath::Abs(looseLep1PdgId)==13) {
          float pt = bound(leptons->pt(idx),sf_muPtMin,sf_muPtMax);
          float eta = bound(TMath::Abs(leptons->eta(idx)),0,sf_muEtaMax);
          if (looseLep1IsTight==1) 
            sf_lep = getVal(hMuTight,eta,pt);
          else
            sf_lep = getVal(hMuLoose,eta,pt);
          sf_lepTrack = getVal(hMuTrack,bound(npv,0,50));
        }
      }
      if (nLooseLep>1) {
        int idx = looseLeptons[1];
        if (TMath::Abs(looseLep2PdgId)==11) {
          float pt = bound(leptons->pt(idx),sf_elePtMin,sf_elePtMax);
          float eta = bound(TMath::Abs(leptons->eta(idx)),0,sf_eleEtaMax);
          if (looseLep2IsTight==1) 
            sf_lep *= getVal(hEleTight,eta,pt);
          else
            sf_lep *= getVal(hEleVeto,eta,pt);
          sf_lepTrack *= getVal(hEleTrack,bound(leptons->eta(idx),-2.5,2.5),bound(npv,0,50));
        } else if (TMath::Abs(looseLep2PdgId)==13) {
          float pt = bound(leptons->pt(idx),sf_muPtMin,sf_muPtMax);
          float eta = bound(TMath::Abs(leptons->eta(idx)),0,sf_muEtaMax);
          if (looseLep2IsTight==1) 
            sf_lep *= getVal(hMuTight,eta,pt);
          else
            sf_lep *= getVal(hMuLoose,eta,pt);
          sf_lepTrack *= getVal(hMuTrack,bound(npv,0,50));
        }
      }
    } else {
      sf_lep=1;
      sf_lepTrack=1;
    }
    if (DEBUG) { PInfo("MitPanda::NeroSkimmer::Run",TString::Format("12: %f",sw->RealTime()*1000)); sw->Start(); }

    tOut->Fill(); 
  } // entry loop
  if (DEBUG) { delete sw; sw=0; }
  return true;
} // Run()

