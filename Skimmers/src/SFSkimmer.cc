#include "../interface/SFSkimmer.h"
#include "TMath.h"
#include <iostream>

// SFSkimmer
SFSkimmer::SFSkimmer() {
  tOut = new sfTree();
}

SFSkimmer::~SFSkimmer() {
  delete tOut;
  delete fOut;
}

void SFSkimmer::Init(TTree *outTree) {
  tOut->WriteTree(outTree);
}

void SFSkimmer::SetOutFile(TString outFile) {
  fOut = new TFile(outFile,"RECREATE");
  outTreePtr = new TTree("Events","Events"); 
  Init(outTreePtr);
}

void SFSkimmer::AddTree(TTree *t, bool data, int mu, bool match, bool tt) {
  inTrees.push_back(t);
  isDatas.push_back(data);
  isMus.push_back(mu);
  isMatcheds.push_back(match);
  isTTs.push_back(tt);
}

void SFSkimmer::AddFile(TString fPath, bool data, int mu, bool match, bool tt) {
  TFile *ftmp = TFile::Open(fPath.Data());
  ownedFiles.push_back(ftmp);
  TTree *events = (TTree*)ftmp->Get("events");
  AddTree(events,data,mu,match,tt);
}

void SFSkimmer::ResetBranches() {
  tOut->Reset();
}

void SFSkimmer::Terminate() {
  if (fOut) {
    fOut->WriteTObject(outTreePtr,outTreePtr->GetName(),"Overwrite");
  }
  fOut->Close();
  for (TFile *f : ownedFiles)
    f->Close();
}

void SFSkimmer::Run() {
  unsigned int nTrees = inTrees.size();
  unsigned int nTotal=0, nAccepted=0;
  for (unsigned int iT=0; iT!=nTrees; ++iT) {
    TTree *tIn = inTrees.at(iT);
    bool isData = isDatas.at(iT);
    int isMu = isMus.at(iT);
    bool match = isMatcheds.at(iT);
    bool tt = isTTs.at(iT);
    unsigned int nEntries = tIn->GetEntries();
    nTotal += nEntries;

    tOut->tag->SetPtEtaPhiM(100,0,0,10);
    tOut->probe->SetPtEtaPhiM(400,0,0,175);

    float normalizedWeight, sf_lep, sf_tt, sf_btag1, sf_sjbtag1, sf_btag1BUp, sf_sjbtag1BUp, sf_eleTrig;
    int nmu, nele, npho, ntau, btags, lep1istight;
    int runNumber, lumiNumber, trigger, nCA15, isMatched;
    ULong64_t eventNumber;
    float UWmag, mSD, tau3, tau2, sjbtag, puppimet, pt, eta;
    tIn->SetBranchStatus("*",0);
    if (!isData) {
      activateBranch(tIn,"normalizedWeight",&normalizedWeight);
      activateBranch(tIn,"sf_lep",&sf_lep);
      activateBranch(tIn,"sf_tt",&sf_tt);
      activateBranch(tIn,"sf_btag1",&sf_btag1);
      activateBranch(tIn,"sf_sjbtag1",&sf_sjbtag1);
      activateBranch(tIn,"sf_btag1BUp",&sf_btag1BUp);
      activateBranch(tIn,"sf_sjbtag1BUp",&sf_sjbtag1BUp);
      activateBranch(tIn,"CA15fj1_isMatched",&isMatched);
      activateBranch(tIn,"CA15fj1_eta",&eta);
      activateBranch(tIn,"CA15fj1_pt",&pt);
      activateBranch(tIn,"sf_eleTrig",&sf_eleTrig);
    }
    activateBranch(tIn,"nLooseElectron",&nele);
    activateBranch(tIn,"nLooseMuon",&nmu);
    activateBranch(tIn,"nLoosePhoton",&npho);
    activateBranch(tIn,"nTau",&ntau);
    activateBranch(tIn,"jetNIsoBtags",&btags);
    activateBranch(tIn,"looseLep1IsTight",&lep1istight);
    activateBranch(tIn,"UWmag",&UWmag);
    activateBranch(tIn,"CA15fj1_mSD",&mSD);
    activateBranch(tIn,"CA15fj1_tau3",&tau3);
    activateBranch(tIn,"CA15fj1_tau2",&tau2);
    activateBranch(tIn,"CA15fj1_maxsjbtag",&sjbtag);
    activateBranch(tIn,"puppimet",&puppimet);
    activateBranch(tIn,"runNumber",&runNumber);
    activateBranch(tIn,"eventNumber",&eventNumber);
    activateBranch(tIn,"lumiNumber",&lumiNumber);
    activateBranch(tIn,"trigger",&trigger);
    activateBranch(tIn,"nCA15fj",&nCA15);

    unsigned int iE=0;
    ProgressReporter pr("MitPanda::SFSkimmer::Run",&iE,&nEntries,10); 

    for (iE=0; iE!=nEntries; ++iE) {
      ResetBranches();
      tIn->GetEntry(iE);
      pr.Report();

      // apply selection
      if (lep1istight==1 && (npho+ntau)==0 && UWmag>250 && nCA15==1) {
        if (type==kTopTag && (sjbtag<0.46 || btags!=1))
          continue;
        if (type==kSJBTag && (tau3/tau2>0.61 || btags!=1))
          continue;
        if (isData) {
          bool passMuon = (nmu==1 && nele==0 && (trigger&1)!=0);
          bool passElectron = (nmu==0 && nele==1 && puppimet>40 && (trigger&2)!=0);
          if (isMu==1 && !passMuon)
            continue;
          if (isMu==0 && !passElectron)
            continue;
          if (isMu==2 && !(passMuon||passElectron))
            continue;
          tOut->scale1fb = 1; tOut->scale1fb_tt0 = 1; tOut->scale1fb_tt2 = 1; 
          tOut->scale1fb_btag = 1; tOut->scale1fb_sjbtag = 1;
        } else {
          bool passMuon = (nmu==1 && nele==0);
          bool passElectron = (nmu==0 && nele==1 && puppimet>40);
          if (!(passMuon||passElectron))
            continue;
          if (tt) {
            if (match && !isMatched)
              continue;
            if (!match && isMatched)
              continue;
          }
          float trigEff = (nmu==1) ? 1 : sf_eleTrig;
          tOut->scale1fb        = 1000*normalizedWeight*sf_tt*      sf_btag1*   sf_sjbtag1*   sf_lep*trigEff;
          tOut->scale1fb_tt0    = 1000*normalizedWeight*            sf_btag1*   sf_sjbtag1*   sf_lep*trigEff;
          tOut->scale1fb_tt2    = 1000*normalizedWeight*sf_tt*sf_tt*sf_btag1*   sf_sjbtag1*   sf_lep*trigEff;
          tOut->scale1fb_btag   = 1000*normalizedWeight*sf_tt*      sf_btag1BUp*sf_sjbtag1*   sf_lep*trigEff;
          tOut->scale1fb_sjbtag = 1000*normalizedWeight*sf_tt*      sf_btag1*   sf_sjbtag1BUp*sf_lep*trigEff;
        }

        nAccepted++;
        tOut->probe->SetPtEtaPhiM(pt,eta,0,mSD); // should maybe store phi and mass in tree, but is it relevant?
        tOut->runNum = (unsigned int) runNumber;
        tOut->lumiSec = (unsigned int) lumiNumber;
        tOut->evtNum = (unsigned int) eventNumber;
        tOut->mass = mSD;
        tOut->qprobe = 1; tOut->qtag = -1;
        tOut->npv = 15; tOut->npu = 15;
        if (type==kTopTag) {
          tOut->pass = (tau3/tau2<0.61) ? 1 : 0;
        } else if (type==kSJBTag) {
          tOut->pass = (sjbtag>0.46) ? 1 : 0;
        }
        tOut->treePtr->Fill();
      }
    }

  }

  PInfo("MitPanda::SFSkimmer::Run",TString::Format("Accepted %u/%u\n",nAccepted,nTotal));
}
