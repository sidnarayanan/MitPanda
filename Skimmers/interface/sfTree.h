
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TLorentzVector.h"
#include "TClonesArray.h"
#include "genericTree.h"
class sfTree : public genericTree {
  public:
    sfTree();
    ~sfTree();
    void ReadTree(TTree *t);
    void WriteTree(TTree *t);
    void Reset();
    
	unsigned int runNum=0;
	unsigned int lumiSec=0;
	unsigned int evtNum=0;
	unsigned int npv=0;
	float npu=0;
	unsigned int pass=0;
	float scale1fb=0;
	float scale1fb_tt0=0;
	float scale1fb_tt2=0;
	float scale1fb_sjbtag=0;
	float scale1fb_btag=0;
	float mass=0;
	int qtag=0;
	int qprobe=0;
	TLorentzVector *tag=0;
	TLorentzVector *probe=0;
};

sfTree::sfTree() {
    	runNum=0;
	lumiSec=0;
	evtNum=0;
	npv=0;
	npu=0;
	pass=0;
	scale1fb=0;
	scale1fb_tt0=0;
	scale1fb_tt2=0;
	scale1fb_sjbtag=0;
	scale1fb_btag=0;
	mass=0;
	qtag=0;
	qprobe=0;
	tag = new TLorentzVector();
	probe = new TLorentzVector();
}

sfTree::~sfTree() {
    	delete tag; tag = 0;
	delete probe; probe = 0;
}

void sfTree::Reset() {
    	runNum = 0;
	lumiSec = 0;
	evtNum = 0;
	npv = 0;
	npu = 0;
	pass = 0;
	scale1fb = 0;
	scale1fb_tt0 = 0;
	scale1fb_tt2 = 0;
	scale1fb_sjbtag = 0;
	scale1fb_btag = 0;
	mass = 0;
	qtag = 0;
	qprobe = 0;
}

void sfTree::ReadTree(TTree *t) {
      treePtr = t;
      treePtr->SetBranchStatus("*",0);
    	treePtr->SetBranchStatus("runNum",1);
	treePtr->SetBranchAddress("runNum",&runNum);
	treePtr->SetBranchStatus("lumiSec",1);
	treePtr->SetBranchAddress("lumiSec",&lumiSec);
	treePtr->SetBranchStatus("evtNum",1);
	treePtr->SetBranchAddress("evtNum",&evtNum);
	treePtr->SetBranchStatus("npv",1);
	treePtr->SetBranchAddress("npv",&npv);
	treePtr->SetBranchStatus("npu",1);
	treePtr->SetBranchAddress("npu",&npu);
	treePtr->SetBranchStatus("pass",1);
	treePtr->SetBranchAddress("pass",&pass);
	treePtr->SetBranchStatus("scale1fb",1);
	treePtr->SetBranchAddress("scale1fb",&scale1fb);
	treePtr->SetBranchStatus("scale1fb_tt0",1);
	treePtr->SetBranchAddress("scale1fb_tt0",&scale1fb_tt0);
	treePtr->SetBranchStatus("scale1fb_tt2",1);
	treePtr->SetBranchAddress("scale1fb_tt2",&scale1fb_tt2);
	treePtr->SetBranchStatus("scale1fb_sjbtag",1);
	treePtr->SetBranchAddress("scale1fb_sjbtag",&scale1fb_sjbtag);
	treePtr->SetBranchStatus("scale1fb_btag",1);
	treePtr->SetBranchAddress("scale1fb_btag",&scale1fb_btag);
	treePtr->SetBranchStatus("mass",1);
	treePtr->SetBranchAddress("mass",&mass);
	treePtr->SetBranchStatus("qtag",1);
	treePtr->SetBranchAddress("qtag",&qtag);
	treePtr->SetBranchStatus("qprobe",1);
	treePtr->SetBranchAddress("qprobe",&qprobe);
	treePtr->SetBranchStatus("tag",1);
	treePtr->SetBranchAddress("tag",&tag);
	treePtr->SetBranchStatus("probe",1);
	treePtr->SetBranchAddress("probe",&probe);
}

void sfTree::WriteTree(TTree *t) {
      treePtr = t;
    	treePtr->Branch("runNum",&runNum,"runNum/i");
	treePtr->Branch("lumiSec",&lumiSec,"lumiSec/i");
	treePtr->Branch("evtNum",&evtNum,"evtNum/i");
	treePtr->Branch("npv",&npv,"npv/i");
	treePtr->Branch("npu",&npu,"npu/F");
	treePtr->Branch("pass",&pass,"pass/i");
	treePtr->Branch("scale1fb",&scale1fb,"scale1fb/F");
	treePtr->Branch("scale1fb_tt0",&scale1fb_tt0,"scale1fb_tt0/F");
	treePtr->Branch("scale1fb_tt2",&scale1fb_tt2,"scale1fb_tt2/F");
	treePtr->Branch("scale1fb_sjbtag",&scale1fb_sjbtag,"scale1fb_sjbtag/F");
	treePtr->Branch("scale1fb_btag",&scale1fb_btag,"scale1fb_btag/F");
	treePtr->Branch("mass",&mass,"mass/F");
	treePtr->Branch("qtag",&qtag,"qtag/I");
	treePtr->Branch("qprobe",&qprobe,"qprobe/I");
	treePtr->Branch("tag","TLorentzVector",&tag,128000,0);
	treePtr->Branch("probe","TLorentzVector",&probe,128000,0);
}
