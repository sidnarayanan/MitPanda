
#ifndef SFSkimmer_h
#define SFSkimmer_h

#include "sfTree.h"

#include <TROOT.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>
#include "../../Tools/interface/Common.h"

/*
 * skimmer that produces trees
 * for top-tag or b-tag SF measurement
 */

class SFSkimmer {
public :
  enum SFType {
    kTopTag,
    kSJBTag
  };

  SFSkimmer();
  ~SFSkimmer();
  void Init(TTree *outTree);
  void SetOutFile(TString outFile);
  void AddTree(TTree *t,bool data, int mu, bool match, bool tt);
  void AddFile(TString fPath,bool data, int mu, bool match, bool tt);
  void ResetBranches();
  void Run();
  void Terminate();

  SFType type = kTopTag;

private:
  std::vector<TFile*> ownedFiles; 
  std::vector<TTree*> inTrees;
  std::vector<bool>   isDatas;
  std::vector<int>   isMus; // 0=electron, 1=mu, 2=both
  std::vector<bool>   isMatcheds;
  std::vector<bool>   isTTs;
  sfTree *tOut=0;
  TFile *fOut=0;
  TTree *outTreePtr=0;

};

#endif

