//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Nov 30 17:13:45 2015 by ROOT version 6.02/05
// from TTree NeroSkimmer/NeroSkimmer
// found on file: nero_0007.root
//////////////////////////////////////////////////////////

#ifndef NeroSkimmer_h
#define NeroSkimmer_h

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include "TClonesArray.h"
#include "TLorentzVector.h"
#include "NeroProducer/Core/interface/BareAll.hpp"
#include "NeroProducer/Core/interface/BarePhotons.hpp"
#include "NeroProducer/Core/interface/BareMet.hpp"
#include "NeroProducer/Core/interface/BareLeptons.hpp"
#include "NeroProducer/Core/interface/BareJets.hpp"
#include "NeroProducer/Core/interface/BareFatJets.hpp"
#include "NeroProducer/Core/interface/BareTaus.hpp"
#include "NeroProducer/Core/interface/BareTrigger.hpp"
#include "NeroProducer/Core/interface/BareMonteCarlo.hpp"
#include "NeroProducer/Core/interface/BareVertex.hpp"
#include "NeroProducer/Core/interface/BareEvent.hpp"

#include "../../Tools/interface/Common.h"

#include "vector"
#include <string>
#include <unordered_map>
#include <unordered_set>
#include "CondFormats/BTauObjects/interface/BTagEntry.h"
#include "CondFormats/BTauObjects/interface/BTagCalibration.h"
#include "CondFormats/BTauObjects/interface/BTagCalibrationReader.h"
#include "BTagCalibrationStandalone.h"
//#include "CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"
//#include "CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h"
//#include "CondFormats/JetMETObjects/interface/JetCorrectionUncertainty.h"

#define NHFPTBINS 5
#define NLFPTBINS 4
#define NLFETABINS 3
#define NBSYS 9
#define NCSYS 5

class btagcand {
  public:
    btagcand(unsigned int i, int f,double e,double cent,double up,double down) {
      idx = i;
      flav = f;
      eff = e;
      sf = cent;
      sfup = up;
      sfdown = down;
    }
    ~btagcand() { }
    int flav, idx;
    double eff, sf, sfup, sfdown;
};


class NeroSkimmer {
public :

  enum PreselectionBit {
   kMonotopCA15 =(1<<0),
   kMonotopRes  =(1<<1),
   kMonojet     =(1<<2),
   kTriggers    =(1<<3),
   kOFTTbar     =(1<<4),
   kDY          =(1<<5),
   kVBF         =(1<<6),
  };

  enum LepIsoType {
    kIsoVeto,
    kIsoLoose,
    kIsoMedium,
    kIsoTight
  };

  enum ProcessType { 
    kNone,
    kZ,
    kW,
    kA,
    kTT,
    kMonotopSignal
  };

  enum TriggerBits {
    kMET       =(1<<0),
    kSingleEle =(1<<1),
    kSinglePho =(1<<2),
    kSingleMu  =(1<<3),
    kMET170    =(1<<4),
    kMETMHT    =(1<<5)
  };

  enum Regions {
    kMonotopSR     =(1<<0),
    kMonotopMuTTCR =(1<<1),
    kMonotopEleTTCR=(1<<2),
    kMonotopMuZCR  =(1<<3),
    kMonotopEleZCR =(1<<4),
    kMonotopMuWCR  =(1<<5),
    kMonotopEleWCR =(1<<6),
    kMonotopACR    =(1<<7)
  };

  NeroSkimmer();
  ~NeroSkimmer();
  void Init(TTree *tree, TTree *alltree);
  void ResetBranches();
  Bool_t Run(double scaledXsec, Long64_t entry=-1);
  void Terminate();
  void SetOutputFile(TString fOutName);
  bool PassPreselection();
  bool PassEventFilters();
  bool PassGoodLumis();
  bool LeptonIsolation(double,double,double,int,LepIsoType);
  bool IsMatched(std::vector<unsigned int>*,std::vector<unsigned int>*,double,double,double);

  void SetPreselectionBit(PreselectionBit b,bool on=true) {
    if (on) 
      preselBits |= b;
    else 
      preselBits &= ~b;
  }

  void AddBadEvent(int r, int l, ULong64_t e); 
  void AddLumi(int run, int lumi);
  void EvalBtagSF(std::vector<btagcand> v, std::vector<double> sfs, float &sf0, float &sf1);
  void EvalBtagSF(std::vector<btagcand> v, std::vector<double> sfs, float &sf0, float &sf1, float &sf2);
  double EvalCSVWeight(unsigned int iPt, unsigned int iEta, float csv, int flav, unsigned int iSys=0);
  void set_leptonslabel(const char *s) { label_leptons = s; }
  void set_photonslabel(const char *s) { label_photons = s; }
  void set_ak4Jetslabel(const char *s) { label_ak4Jets = s; }
  void set_ak8Jetslabel(const char *s) { label_ak8Jets = s; }
  void set_ca15Jetslabel(const char *s) { label_ca15Jets = s; }
  void set_tauslabel(const char *s) { label_taus = s; }
  void SetDataDir(const char *s);
  bool usePuppiMET=false; // for recoils
  bool isData=false; // to do gen matching, etc
  bool applyJson=true; // only applied if isData&&applyJson
  ProcessType processType=kNone; // used to save gen bosons and apply k-factors and apply ttbar reweighting
  bool fromBambu=true; // sometimes we want to run on miniaod and stuff is different
  int maxEvents=-1;
private:

//  JetCorrectorParameters *ak8jec=0;
//  JetCorrectionUncertainty *ak8unc=0;

  BTagCalibration *btagCalib=0;
  BTagCalibrationReader *hfReader=0, *hfUpReader=0, *hfDownReader=0;
  BTagCalibrationReader *lfReader=0, *lfUpReader=0, *lfDownReader=0;

  BTagCalibration *sj_btagCalib;
  BTagCalibrationReader *sj_hfReader=0, *sj_hfUpReader=0, *sj_hfDownReader=0;
  BTagCalibrationReader *sj_lfReader=0, *sj_lfUpReader=0, *sj_lfDownReader=0;

  std::unordered_map<int,std::unordered_set<int>> goodLumiList;
  std::unordered_set<EventObj> badEvents;

  TFile *fEleSF=0, *fEleSFTight=0;
  TH2D *hEleVeto, *hEleTight;
  TFile *fMuSF=0, *fMuSFTight=0;
  TH2D *hMuLoose, *hMuTight;
  TFile *fEleSFTrack=0, *fMuSFTrack=0;
  TH1D *hMuTrack; TH2D *hEleTrack;

  TFile *fPU=0;
  TH1D *hPUWeight;

  TFile *fKFactor=0;
  TH1D *hZNLO, *hANLO, *hWNLO;
  TH1D *hZLO,  *hALO,  *hWLO;
  TH1D *hZEWK, *hAEWK, *hWEWK;
  TFile *fEleTrigB, *fEleTrigE, *fPhoTrig, *fEleTrigLow, *fMetTrig;
  TH1D *hEleTrigB, *hEleTrigE, *hPhoTrig, *hMetTrig;
  //TH1D *hEleTrigBUp=0, *hEleTrigBDown=0, *hEleTrigEUp=0, *hEleTrigEDown=0;
  TH2D *hEleTrigLow;
  
  TFile *fHFNew=0,*fLFNew=0;
  TH1D *hBNew[NHFPTBINS][NBSYS];
  TH1D *hCNew[NHFPTBINS][NCSYS];
  TH1D *hLFNew[NLFPTBINS][NLFETABINS][NBSYS];

  unsigned long preselBits=0;
  
  TFile *fOut;
  TTree *tOut;
  TTree *tIn;

  BareEvent      *event; 
  BareTrigger    *triggers;
  BareLeptons    *leptons;
  BarePhotons    *photons;
  BareJets       *ak4Jets;
  BareFatJets    *ak8Jets;
  BareFatJets    *ca15Jets;
  BareTaus       *taus;
  BareMet        *mets;
  BareMonteCarlo *genParticles;
  BareVertex     *vertices;

  std::vector<BareCollection*> collections;

  std::string label_leptons = "lep";
  std::string label_photons = "photon";
  std::string label_ak4Jets = "";
  std::string label_ak8Jets = "AK8CHS";
  std::string label_ca15Jets = "CA15CHS";
  std::string label_taus = "tau";

  float mcWeight=1; 
  float sf_ewkZ=1;
  float sf_ewkW=1;
  float sf_ewkA=1;
  float sf_qcdZ=1;
  float sf_qcdW=1;
  float sf_qcdA=1;
  float sf_lep=1;
  float sf_lepTrack=1;

  float sf_btag0=1;
  float sf_btag0BUp=1;
  float sf_btag0BDown=1;
  float sf_btag0MUp=1;
  float sf_btag0MDown=1;
  float sf_btag1=1;
  float sf_btag1BUp=1;
  float sf_btag1BDown=1;
  float sf_btag1MUp=1;
  float sf_btag1MDown=1;
  float sf_sjbtag0=1;
  float sf_sjbtag0BUp=1;
  float sf_sjbtag0BDown=1;
  float sf_sjbtag0MUp=1;
  float sf_sjbtag0MDown=1;
  float sf_sjbtag1=1;
  float sf_sjbtag1BUp=1;
  float sf_sjbtag1BDown=1;
  float sf_sjbtag1MUp=1;
  float sf_sjbtag1MDown=1;
  float sf_sjbtag2=1;
  float sf_sjbtag2BUp=1;
  float sf_sjbtag2BDown=1;
  float sf_sjbtag2MUp=1;
  float sf_sjbtag2MDown=1;

  float sf_newBtag=1;
  float sf_newBtagHFUp=1;
  float sf_newBtagHFDown=1;
  float sf_newBtagLFUp=1;
  float sf_newBtagLFDown=1;
  float sf_newSjBtag=1;
  float sf_newSjBtagHFUp=1;
  float sf_newSjBtagHFDown=1;
  float sf_newSjBtagLFUp=1;
  float sf_newSjBtagLFDown=1;

  float sf_tt=1;
  float sf_eleTrig=1;
//  float sf_eleTrigUp=1;
//  float sf_eleTrigDown=1;
  float sf_phoTrig=1;
  float sf_metTrig=1;
  float puWeight=1;
  float photonPurityWeight=1;
  float finalWeight=1;

  int region=0;

  int runNumber=0; 
  ULong64_t eventNumber=0; 
  int lumiNumber=0; 
  int npv=0;
  int truepu=0;
  int metFilter=1; // warning: only filled in miniaod. applied 'offline' in bambu using AddBadEvent()
  int newMetFilter=1;

  float met=0; 
  float metphi=0; 
  float metnomu=0;
  float puppimet=0; 
  float puppimetphi=0; 
  float calomet=0;
  float calometphi=0;
  float pfcalobalance=0;
  float sumET=0;
  float trkmet=0;

  float UWmag=0;
  float UWphi=0;
  float UZmag=0;
  float UZphi=0;
  float UAmag=0;
  float UAphi=0;
  float Uperp=0;
  float Upara=0;

  float pfUWmag=0;
  float pfUWphi=0;
  float pfUZmag=0;
  float pfUZphi=0;
  float pfUAmag=0;
  float pfUAphi=0;
  float pfUperp=0;
  float pfUpara=0;

  float dphimet=0; 
  float dphipuppimet=0; 
  float dphiUW=0; 
  float dphiUZ=0; 
  float dphiUA=0;
  float dphipfUW=0; 
  float dphipfUZ=0; 
  float dphipfUA=0;

  float trueGenBosonPt=0;   // precise definition will depend on processType
  float genBosonPt=0;   // genBosonPt is truncated at the bounds of NLO corrections
  float genBosonEta=0; 
  float genBosonMass=0;  
  float genBosonPhi=0;
  float genTopPt=0;
  float recoBosonPt=0;  // precise definition will depend on number of leptons/photons found
  float recoBosonPhi=0;

  int nSelectedJet=0; 
  float jet1Phi=0; 
  float jet1Pt=0; 
  float jet1Eta=0; 
  float jet1Btag=0; 
  float jet2Phi=0; 
  float jet2Pt=0; 
  float jet2Eta=0; 
  float jet2Btag=0; 
  int jet1IsTight=-1;
  float genJet1Pt=0; 
  float genJet1Eta=0; 
  int nGenJet=0;
  float jetIso1Pt=0;
  int jetIso1Flav=0;
  float sj1Pt=0;
  int sj1Flav=0;
#if 0
  float jet3Phi=0; 
  float jet3Pt=0; 
  float jet3Eta=0; 
  float jet3Btag=0; 
  float jet4Phi=0; 
  float jet4Pt=0; 
  float jet4Eta=0; 
  float jet4Btag=0; 
#endif
  float jet123Mass=0; 
  float jet12Mass=0;
  float jet12DEta=-1;
  int jetNBtags=0; 
  int jetNIsoBtags=0;

  int nSelectedCA15fj=0; 
  float CA15fj1_tau1=0; 
  float CA15fj1_tau2=0; 
  float CA15fj1_tau3=0; 
  float CA15fj1_mSD=0; 
  float CA15fj1_pt=0; 
  float CA15fj1_ptUp=0; 
  float CA15fj1_ptDown=0; 
  float CA15fj1_phi=0; 
  float CA15fj1_eta=0; 
  float CA15fj1_maxsjbtag=0; 
  float CA15fj1_minsjbtag=0;
//  float CA15fj1_NNResponse =0; 
  int CA15fj1_isMatched=0; 
  int CA15fj1_mjID=0;
  float CA15fj1_rawPt=0;
  int CA15fj1_selBits=0;
  int CA15fj1_isHF=0;
  int CA15fj1_isLoose=0;
  int CA15fj1_isTight=0;
  int isHF=0; // event has b or c

#if 0
  int nSelectedAK8fj=0; 
  float AK8fj1_tau1=0; 
  float AK8fj1_tau2=0; 
  float AK8fj1_tau3=0; 
  float AK8fj1_mSD=0; 
  float AK8fj1_pt=0; 
  float AK8fj1_phi=0; 
  float AK8fj1_eta=0; 
  float AK8fj1_maxsjbtag=0; 
  float AK8fj1_NNResponse =0; 
  int AK8fj1_isMatched=0; 
#endif

  int nLoosePhoton=0; 
  int nTightPhoton=0; 
  int nLooseLep=0; 
  int nLooseElectron=0; 
  int nLooseMuon=0; 
  int nTightLep=0; 
  int nTightElectron=0; 
  int nTightMuon=0; 
  int nSelectedTau=0; 
  float diLepMass=0;  // defined wrt to leading 2 loose leptons
  int looseLep1PdgId=0;
  int looseLep2PdgId=0;
  int looseLep1IsTight=0;
  int looseLep2IsTight=0;
  int loosePho1IsTight=0;
  float looseLep1Pt=0;
  float looseLep1Eta=0;
  float looseLep1Phi=0;
  float looseLep2Pt=0;
  float looseLep2Eta=0;
  float looseLep2Phi=0;
  float loosePho1Pt=0;
  float loosePho1Eta=0;
  float loosePho1Phi=0;


  int trigger=0;

};

#endif

