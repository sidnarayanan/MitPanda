
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TLorentzVector.h"
#include "TClonesArray.h"
#include "genericTree.h"
class LimitTree : public genericTree {
  public:
    LimitTree();
    ~LimitTree();
    void ReadTree(TTree *t);
    void WriteTree(TTree *t);
    void Reset();
    
	float met=0;
	float metphi=0;
	float mt=0;
	float genVpt=0;
	float genVphi=0;
	float weight=0;
};

LimitTree::LimitTree() {
    	met=0;
	metphi=0;
	mt=0;
	genVpt=0;
	genVphi=0;
	weight=0;
}

LimitTree::~LimitTree() {
    }

void LimitTree::Reset() {
    	met = 0;
	metphi = 0;
	mt = 0;
	genVpt = 0;
	genVphi = 0;
	weight = 0;
}

void LimitTree::ReadTree(TTree *t) {
      treePtr = t;
      treePtr->SetBranchStatus("*",0);
    	treePtr->SetBranchStatus("met",1);
	treePtr->SetBranchAddress("met",&met);
	treePtr->SetBranchStatus("metphi",1);
	treePtr->SetBranchAddress("metphi",&metphi);
	treePtr->SetBranchStatus("mt",1);
	treePtr->SetBranchAddress("mt",&mt);
	treePtr->SetBranchStatus("genVpt",1);
	treePtr->SetBranchAddress("genVpt",&genVpt);
	treePtr->SetBranchStatus("genVphi",1);
	treePtr->SetBranchAddress("genVphi",&genVphi);
	treePtr->SetBranchStatus("weight",1);
	treePtr->SetBranchAddress("weight",&weight);
}

void LimitTree::WriteTree(TTree *t) {
      treePtr = t;
    	treePtr->Branch("met",&met,"met/F");
	treePtr->Branch("metphi",&metphi,"metphi/F");
	treePtr->Branch("mt",&mt,"mt/F");
	treePtr->Branch("genVpt",&genVpt,"genVpt/F");
	treePtr->Branch("genVphi",&genVphi,"genVphi/F");
	treePtr->Branch("weight",&weight,"weight/F");
}
