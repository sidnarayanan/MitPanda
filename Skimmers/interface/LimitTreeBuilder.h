
#ifndef LimitTreeBuilder_h
#define LimitTreeBuilder_h

#include <TROOT.h>
#include <TTree.h>
#include <TTreeFormula.h>
#include <TFile.h>
#include <TSelector.h>
#include <TString.h>
#include "limitTree.h"
#include <map>

int treeCounter=0; // used to give distinct names to trees

// used to map input variables
class VariableMap {
public:
  VariableMap(TString met, TString metphi, TString mt, TString genVpt, TString genVphi) {
      map["met"] = met;
      map["metphi"] = metphi;
      map["mt"] = mt;
      map["genVpt"] = genVpt;
      map["genVphi"] = genVphi;
  }
  ~VariableMap() {}
  const char *operator()(TString s) { 
    auto found = map.find(s); 
    return (found==map.end()) ? "" : found->second.Data();
    
  }
private:
  std::map<TString,TString> map;
};

// defines a process for a given region and choice of systematic shifts
class Process {
public:
  Process(TString n, TTree *in, VariableMap *vPtr, TString sel, TString w);
  ~Process();
  void Run();
  TTree *GetTree() { return limitTree->treePtr; }
  TString name;
  TString syst="";
private:
  LimitTree *limitTree=0;
  TTree *inputTree=0;
  TString selection;
  TString weight;
  TString metvar,metphivar,mtvar,genvptvar,genvphivar;
};

// defines a region, which is essentially just a list of Processes
class Region {
public:
  Region(TString n) { name = n; }
  ~Region() {}
  void AddProcess(Process *p) { ps.push_back(p); }
  std::vector<Process*> GetProcesses() { return ps; }
  void Run() { for (auto p : ps) { printf("%s ",name.Data()); p->Run(); } }
  TString name;
private:
  std::vector<Process*> ps;
};

class LimitTreeBuilder {
public :

  LimitTreeBuilder() {}
  ~LimitTreeBuilder() {}
  void SetOutFile(TString f) { fOut = new TFile(f,"RECREATE"); }
  void AddRegion(Region *r) { regions.push_back(r); }
  void cd() { fOut->cd(); }
  void Run() { for (auto r : regions) { fOut->cd(); r->Run(); } }
  void Output();
private:
  std::vector<Region*> regions;
  TFile *fOut=0;
};

#endif

