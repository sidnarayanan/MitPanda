
#ifndef TopTagSkimmer_h
#define TopTagSkimmer_h

#include "flatTagTree.h"
#include "topTagTree.h"

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>

class TopTagSkimmer {
public :
  TopTagSkimmer();
  ~TopTagSkimmer();
  void Init(TTree *inTree, TTree *outTree);
  void Init(TString inFile, TString outFile);
  void ResetBranches();
  void Run();
  void Terminate();

  bool isSignal=false; // to apply matching
  float xsec=1.;
private:

  flatTagTree *tOut=0;
  topTagTree *tIn=0;
  TFile *fIn=0, *fOut=0;
  TTree *outTreePtr=0;

};

#endif

